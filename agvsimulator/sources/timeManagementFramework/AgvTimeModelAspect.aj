/*
 * Created on Oct 7, 2004
 *
 */
package timeManagementFramework;
import agentwise.agv.controller.ActiveEntity;
import agentwise.agv.model.agv.CommunicationHandler;
import agentwise.agv.model.agv.CommunicationModule;
import agentwise.agv.model.environment.AgentToEnvironmentInterface;
import agentwise.agv.model.environment.DropOrderInfluence;
import agentwise.agv.model.environment.Influence;
import agentwise.agv.model.environment.MoveStepToStationInfluence;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.PickOrderInfluence;
import agentwise.agv.model.environment.WaitAgvInfluence;
import agentwise.agv.model.environment.communication.CommunicationInstance;


/**
 * In this aspect the points in the program execution
 * where some time constraints have to be enforced
 * are defined. For each kind of activity that needs
 * a duration enforced to it this aspect registers
 * that activity with the Time Management Framework
 * when it occurs and identifies where that activity happens
 * by weaving the registration code into the system
 * as triggers for certain events. 
 * 
 * @author tomdw
 *
 */
public aspect AgvTimeModelAspect {
    
    /**
	 * Up here, you can identify all activities which execution needs to be
	 * controlled according to a time model.
	 * For each activity, define a pointcut, and register the activity.
	 */

    // PERCEPTION
    /**
     * Indentifies where a perception is done
     */
    pointcut perception(Identifiable a) : call (* AgentToEnvironmentInterface.sense(..)) && target(a);
    
    // ACTIONS
    /**
     * Identifies where a pick order action  is generated
     * 
     */
    pointcut pickorder(Identifiable a, PickOrderInfluence i): call(* AgentToEnvironmentInterface.influence(..)) && args(i) && target(a);
	
    /**
     * Identifies where a drop order action is generated
     */
    pointcut droporder(Identifiable a, DropOrderInfluence i): call(* AgentToEnvironmentInterface.influence(..)) && args(i) && target(a);
	
    /**
     * Identifies where a step to follow a segment is generated
     * @param a
     * @param i
     */
    pointcut followSegmentStep(Identifiable a, MoveStepToStationInfluence i): call(* AgentToEnvironmentInterface.influence(..)) && args(i) && target(a);
	
    /**
     * Identifies where a waitsecond is generated
     * @param a
     * @param i
     */
    pointcut waitsecond(Identifiable a, WaitAgvInfluence i): call(* AgentToEnvironmentInterface.influence(..)) && args(i) && target(a);
	
    
    // COMMUNICATION
    /**
     * Identifies where a broadcast with an Ad-Hoc Network is done
     */
	pointcut broadcast(Identifiable a, CommunicationModule cm) :  call (* AgentToEnvironmentInterface.broadcast(..)) && target(a) && this(cm);
	
	/**
	 * Identifies where a unicast is done
	 */	
	pointcut unicast(Identifiable a, CommunicationModule cm) : call(* AgentToEnvironmentInterface.unicast(..)) && target(a) && this(cm);
	
	/**
     * Identifies where a broadcast with an Ad-Hoc Network is done
     * by a CommunicationHandler active entity
     */
	pointcut broadcastbyhandler(Identifiable a, CommunicationInstance ci) :  call (* AgentToEnvironmentInterface.broadcast(..)) && target(a) && this(ci);
	
	/**
	 * Identifies where a unicast is done by a CommunicationHandler active entity
	 */	
	pointcut unicastbyhandler(Identifiable a, CommunicationInstance ci) : call(* AgentToEnvironmentInterface.unicast(..)) && target(a) && this(ci);
	
	/** 
	 * Identifies where a wait is done by the CommunicationHandler entity
	 * 
	 */
	pointcut waitbyhandler(CommunicationHandler h): call(* CommunicationHandler.waitOneTimeStep(..)) && target(h);
	
	/**
	 * Identifies where an agv does a receive to get a message from its inbox
	 */
	pointcut receive(Identifiable a) : call(* AgentToEnvironmentInterface.receive(..)) && target(a);
	
	// OTHER ACTIVE ENTITIES
	/**
	 * Identifies when the OrderLocationManager initiates the behaviour to generate/remove orders from the OrderLocations
	 */
	pointcut stepOrderLocationManager(Identifiable a, OrderLocationManager m): call(* ActiveEntity.step(..)) && target(a) && target(m);
		
	/* All advises register the activities with the time manager */
	before(Identifiable a): perception(a) {
		registerActivity(a, "sense");
	}
	
	before(Identifiable a, CommunicationModule cm): broadcast(a, cm) {
	    registerActivity(a, "broadcast");
	}
	
	before(Identifiable a, CommunicationModule cm): unicast(a, cm) {
	    registerActivity(a, "unicast");
	}
	
	before(CommunicationHandler h): waitbyhandler(h) {
	    DummyIdentifiable i = new DummyIdentifiable(h.getEnvironment().getName()+"c");
	    registerActivity(i, "wait");
	}
	
	before(Identifiable a, CommunicationInstance ci): broadcastbyhandler(a, ci) {
	    DummyIdentifiable i = new DummyIdentifiable(a.getName()+"c");
	    registerActivity(i, "broadcast");
	}
	
	before(Identifiable a, CommunicationInstance ci): unicastbyhandler(a, ci) {
	    DummyIdentifiable i = new DummyIdentifiable(a.getName()+"c");
	    registerActivity(i, "unicast");
	}
	
	before(Identifiable a): receive(a) {
	    registerActivity(a, "receive");
	}
	
	before(Identifiable a, Influence i): droporder(a,i) {
	    registerActivity(a, "droporder");
	}
	
	before(Identifiable a, Influence i): pickorder(a,i) {
	    registerActivity(a, "pickorder");
	}
	
	before(Identifiable a, Influence i): followSegmentStep(a,i) {
	    registerActivity(a, "followsegmentstep");
	}
	
	before(Identifiable a, Influence i): waitsecond(a,i) {
	    registerActivity(a, "waitsecond");
	}
	
	after(Identifiable a, OrderLocationManager m): stepOrderLocationManager(a,m) {
	    registerActivity(a, "stepOrderLocationManager");
	}
	
	

	/**
	 * Registers that an activity with a given should be executed
	 * by a given identifiable entity (active entity)
	 * 
	 * @param a The given identifiable
	 * @param activity The activity name
	 */
	private void registerActivity(Identifiable a, String activity){
	    // TRY AROUND IS MODIFICATION OF TOMDW
	    try {
	        TimeManager.getInstance().getAgentTimeModel(a.getName()).registerActivity(activity);
	    } catch (NullPointerException e) {
	        // if something wrong then this was at end of simulation, no problem
	        //System.out.println("NullPointer for "+a.getName());
	    }
	}

}
