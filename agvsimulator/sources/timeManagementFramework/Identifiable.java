/*
 * Created on Jun 28, 2004
*/
package timeManagementFramework;

/**
 * @author Alexander Helleboogh
 * 
 * This interface is used to indicate the boundaries of an entity which execution
 * has to be controlled.
 * All objects returning the same name are considered as being part of the same entity
 */
public interface Identifiable {
    
    /**
     * @return The name of the entity this object belongs to.
     */
    public String getName();

}
