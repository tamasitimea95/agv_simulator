/*
 * Created on 03-Feb-2005
 *
 */
package timeManagementFramework;

/**
 * @author tomdw
 *
 */
public class DummyIdentifiable implements Identifiable {

    /**
     * Constructs an instance of DummyIdentifiable
     *
     * 
     */
    public DummyIdentifiable(String name) {
        super();
        this.name = name;
    }

    /**
     * @see timeManagementFramework.Identifiable#getName()
     */
    public String getName() {
        return name;
    }

    private String name;
}
