package timeManagementFramework;
import java.util.Iterator;
import java.util.Vector;

/*
 * Created on Mar 14, 2004
 *
 */

/**
 * @author Alexander Helleboogh
 * @taskforce AgentWise
 * @institution DistriNet K.U.Leuven
 * @date Mar 14, 2004
 * 
 * The time manager which controls execution progress of all activity
 * Singleton pattern is used (since at all times, there can at most be one
 * single instance of the TimeManager
 */
public class TimeManager implements Runnable{
	
	private static TimeManager instance;
	private boolean clockUpdated;
	private boolean stop;	
	private Vector agentTimeModels;
	private Thread controleThread;
	
	/**
	 * Returns a unique instance of the TimeManager
	 * @return a unique instance of the TimeManager
	 */
	public synchronized static TimeManager getInstance(){
		if(instance == null) instance = new TimeManager();
		return instance;
	}
	
	/**
	 * The hidden constructor of this class
	 *
	 */
	private TimeManager(){
		agentTimeModels	= new Vector();
		clockUpdated = false;
		stop = false;
		controleThread = new Thread(this);
	}
	
	/**
	 * Starts the execution of the TimeManager after all AgentTimeModels have been
	 * registered.
	 */
	public void start(){
		controleThread.start();
	}
	
	
	/**
	 * Stop the execution of the TimeManager
	 */
	public synchronized void stop(){
		stop = true;
		controleThread = new Thread(this);
	}
	
	/**
	 * Registers a new AgentTimeModel to be controlled by the TimeManager.
	 * If the TimeManager already contains the AgentTimeModel, nothing happens.
	 * @param m The AgentTimeModel to add
	 * 
	 */
	public synchronized void addAgentTimeModel(AgentTimeModel m){
		if(! agentTimeModels.contains(m)){
			agentTimeModels.add(m);
			m.setTimeManager(this);
//			m.getAgent().doStep();	
		}
	}
	
	/**
	 * Returns the AgentTimeModel for the given agent
	 * @param agentName the name of the agent the time models has to be returned for
	 * @return the time model of agent ag or NULL when there is no time model found for agent ag
	 */
	public AgentTimeModel getAgentTimeModel(String agentName){
		for (Iterator iter = agentTimeModels.iterator(); iter.hasNext();) {
		AgentTimeModel element = (AgentTimeModel) iter.next();
		if (element.getAgentId().equals(agentName)) return element;

	}
	return null;
	}
	
	
	
	
	
	/**
	 * Used by an AgentTimeModel to register a clock update
	 */
	
	protected synchronized void registerClockUpdate(){
		setClockUpdated(true);
		notifyAll();
	}
	
	private synchronized void setClockUpdated(boolean b){
		clockUpdated=b;
	}
	
	protected synchronized boolean isClockUpdated(){
		return clockUpdated;
	}

	
	public void run(){
		while (! stop){
		
				synchronized(this){
				while(isClockUpdated() == false){
					try{
						
						wait();
					}
					catch(InterruptedException e){
					}
			
				}
				setClockUpdated(false);
				}
				
			
				
				
				float time = Float.MAX_VALUE;
				for (Iterator iter = agentTimeModels.iterator(); iter.hasNext();) {
					AgentTimeModel element = (AgentTimeModel) iter.next();
					if (element.getCurrentTimeStamp() < time) time = element.getCurrentTimeStamp();
					}
				
			//	System.out.println("current TS = "+time);
			//	Collections.sort(agentTimeModels);
			//	float time=((AgentTimeModel)agentTimeModels.get(0)).getCurrentTimeStamp();
			//	System.out.println(time);
				for (Iterator iter = agentTimeModels.iterator(); iter.hasNext();) {
					AgentTimeModel element = (AgentTimeModel) iter.next();
					element.process(time);
				}
				}
		
				instance = null;
		
	}
	
	
}
