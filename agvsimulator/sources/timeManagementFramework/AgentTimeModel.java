package timeManagementFramework;

import java.util.HashMap;


/**
 * @author Alexander Helleboogh
 * @taskforce AgentWise
 * @institution DistriNet K.U.Leuven
 * @date Mar 14, 2004
 */
public class AgentTimeModel{

	public static final int INTERNALACTIVITY = 0;
	public static final int EXTERNALACTIVITY = 1;
	private float currentTimeStamp;
	private String agentId;
	HashMap durations;
	private boolean canProceed;
	private boolean isWaiting;
	private TimeManager timeManager;
	
	class DurationData{
		private float duration;
		private int activitytype;
		
		public DurationData (float duration, int activityType){
			if (activityType != AgentTimeModel.EXTERNALACTIVITY && activityType != AgentTimeModel.INTERNALACTIVITY) throw new IllegalArgumentException("Illegal activitytype");
			this.duration= duration;
			activitytype = activityType;
		}
		
		public int getActivityType(){
			return activitytype;
		}
		public float getDuration(){
			return duration;
		}
	}
	
	/**
	 * Sets the TimeManager which controls the execution of this AgentTimeModel
	 * To be used by the TimeManger only
	 * @param m The TimeManager
	 */
	protected synchronized void setTimeManager(TimeManager m){
		timeManager = m;
	}
	
	/**
	 * @return The TimeManger which controls the execution of this AgentTimeModel
	 */
	public synchronized TimeManager getTimeManager(){
		return timeManager;
	}
	
	/**
	 * @return The current time of this AgentTimeModel
	 */
	public synchronized float getCurrentTimeStamp() {
		return currentTimeStamp;
	}

	/**
	 * Changes the current time stamp of this AgentTimeModel
	 * @param currentTimeStamp the new current time stamp
	 */
	private synchronized void setCurrentTimeStamp(float currentTimeStamp) {
		this.currentTimeStamp = currentTimeStamp;
	}
	
	/**
	 * Increases the current time stamp of this AgentTimeModel by the given duration
	 * @param duration
	 */
	protected synchronized void increaseCurrentTimeStamp(float duration){
		setCurrentTimeStamp(getCurrentTimeStamp()+duration);
	}

	/**
	 * A new AgentTimeModel that is linked with the agent with the given name
	 */
	public AgentTimeModel(String agentId) {

		setAgentId(agentId);
		setCurrentTimeStamp(0);
		durations = new HashMap();
		canProceed = false;
	}
	
	public synchronized boolean equals(Object other){
		try{
			return ((AgentTimeModel)other).getAgentId().equals(this.getAgentId());
		}
		catch(ClassCastException e){
			return false;
		}
	}
	
	
	protected synchronized boolean canProceed(){
		return canProceed;
	}
	
	/**
	 * 
	 * @return The name of the agent that this AgentTimeModel controls
	 */
	public synchronized String getAgentId() {
		return agentId;
	}

	/**
	 * @param agent
	 */
	private synchronized void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	/**
	 * Registers a new activity to be controlled by this AgentTimeModel
	 * @param activityName The name of the activity
	 * @param duration The duration of the activity (expressed in logical time units)
	 * @param activityType The type of activity. Causality errors are allowed for internal activities
	 * However, the execution of external activities is controlled such that no causality errors can occur
	 */
	public synchronized void registerActivityDuration(String activityName, float duration, int activityType){
			if(durations.containsKey(activityName)) throw new IllegalArgumentException("An activity with this name is already registered");
			durations.put(activityName,new DurationData(duration,activityType));
	}
	
	protected synchronized float getDuration(String activityName){
		return((DurationData)durations.get(activityName)).getDuration();
	}
	
	protected synchronized int getActivityType(String activityName){
		return((DurationData)durations.get(activityName)).getActivityType();
	}

	/**
	 * Called by the aspect upon execution of an activity
	 * @param activityName the name of the activity which is executed
	 */
	public synchronized void registerActivity(String activityName){
		if(activityName == null || (! durations.containsKey(activityName))) throw new IllegalArgumentException("Illegal or unregistered activityname");
		float duration = getDuration(activityName);
		increaseCurrentTimeStamp(duration);
		int activityType = getActivityType(activityName);
		getTimeManager().registerClockUpdate();
		//System.out.println(getAgentId()+ " registreert "+ activityName + " dus tijd is " + getCurrentTimeStamp());
		if(activityType == AgentTimeModel.EXTERNALACTIVITY){
			while(! canProceed()){
				try{
					//System.out.println("Agent "+getAgentId()+ " wacht op "+getCurrentTimeStamp());
					isWaiting = true;
					wait();
					isWaiting = false;
				}
				catch(InterruptedException e){}
			}
			canProceed = false;
			//System.out.print(getAgentId()+" proceeding at "+ getCurrentTimeStamp()+": ");
			notifyAll();
		}
		notifyAll();
	}
	

	public synchronized void process(float time){
		if(getCurrentTimeStamp()==time && isWaiting){
			canProceed = true;
		//	System.out.println(getAgentId()+" genotified "+ time);
			notifyAll();
		}
		
	}


}