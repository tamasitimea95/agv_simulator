/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.elteMod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import agentwise.agv.model.agv.Knowledge;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.communication.KnowledgeMessage;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.PositionMessage;
import agentwise.agv.model.environment.communication.StringMessage;

import agentwise.agv.model.agv.map.*;
import agentwise.agv.controller.OrderManager;
import agentwise.agv.elteMod.SimpleGlobal;

public class TwoEdgeLookAheadBehavior extends SimpleGlobal {

	/**
	 * Constructs an instance of TwoEdgeLookAheadBehavior
	 */
	public TwoEdgeLookAheadBehavior() {
		super();
	}

	private boolean isFirstStep;
	private boolean isNeedTarget;
	

	/**
	 * @see agentwise.agv.model.agv.behaviour.BehaviourModule#initialize()
	 */
	public void initialize() {
		NMB_AGVs = SimpleGlobal.NMB_AGVs;
		
		isFirstStep = true;
		isNeedTarget = false; // it should be override at beginning of the first step
		String logmsg = "";
		
		// path is empty at this point, it is filled at the first step
		
		path = new ArrayList<Link>();
		logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
		logmsg += "NMB_AGVs=" + NMB_AGVs + "\n";
		logmsg += getAgv().getEnvironment().getName() + ": initialize - path after extendRndPath\n";
		logmsg += WriteOut(path) + "\n";
		logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
		Logger.Send(logmsg);
	}

	public void step() {
		//TODO isMyOrderFree
		String logmsg = "";
		String logMsgPathLength = "";
		for (int i = 0; i <= 10; i++)
			getActionExecutionModule().waitSecond();
		if (Logger.getDebugLevel() >= 2) {
			logmsg += "                       " + 
				getAgvName() + ": path at begining of step: " + WriteOut(path) + "\n";
			Logger.Send(logmsg);
			logmsg = "";
		}
		//handle first step, fill path
		if(isFirstStep)
		{			
			targetOrderID = -1;
			pathLengthCounter = 0;
			var currentNode = findCurrentNode();
			path.add(new WaitLink(currentNode));
			isNeedTarget = manageFirstStep(true);
			isFirstStep = false;
			timeSinceLastPickupOrDelivery = 0;
		}
		if ((mode == RANDOM_WALK && targetOrderID != -1 && !Logger.IsMyTargetOrderFree(this)))
		{
			if(Logger.getDebugLevel() >= 1)
			{
				logmsg += getAgvName() + ": Order has been taken by another Agv \n";
				logmsg += "Number of unnecessary steps: " + pathLengthCounter + "\n";
			}
			isNeedTarget = true;
			targetOrderID = -1;
			pathLengthCounter = 0;
		}
		if (Logger.getDebugLevel() >= 4) {
			logmsg += "                       " + 
					getAgvName() + ": path after if(isFirstStep): " + WriteOut(path) + "\n";
		}
		// the try-catch structure is included for error tracing
		try {	
			// ERRORTRACING, CP1: at the beginning of a step, the path should not be empty
			if (path.size() == 0) {
				Logger.Send(getAgvName() + ": CP1");
				throw new Exception(getAgv().getEnvironment().getName() + ": CP1");
			}

			// if the AGV is on the StartNode of its Path OR already on a link, it should go ahead
			if (getPerceptionModule().senseIsOnSegment() || 
					(!(path.get(0) instanceof WaitLink) && 
					getPerceptionModule().senseCurrentMapComponentId().equals(path.get(0).getStartNode().getID()))) {
				if (Logger.getDebugLevel() >= 4) {
					logmsg += "                       " + 
							getAgvName() + ": mapID: " + getPerceptionModule().senseCurrentMapComponentId() + 
							" path.get(0).getStartId: " + path.get(0).getStartNode().getID() +
							" path.get(0).getEndId: " + path.get(0).getEndNode().getID() +"\n";
				}
				String destinationID = path.get(0).getEndNode().getID();
				getActionExecutionModule().moveStepToStation(destinationID);
				
			}
			
			// if we reached a station/location which is not the StartNode of our Path, we need to reorientate and synchronize
			else {
				// ERRORTRACING, CP2: agv should move according to its plan
				if (//!(path.get(0) instanceof WaitLink) && 
						getPerceptionModule().senseCurrentMapComponentId() != path.get(0).getEndNode().getID()) {
					Logger.Send(getAgvName() + ": CP2");
					throw new Exception(getAgv().getEnvironment().getName() + " CP2 it is on " + findCurrentNode().getID() + 
							"node instead of " + path.get(0).getEndNode().getID());
				}
				
				Collection<Order> orderCollection = OrderManager.getInstance().getCurrentOrders();
				List<Order> orders = orderCollection.stream().collect(Collectors.toList());
				if (Logger.getDebugLevel() >= 2) {
					logmsg = "";
					logmsg += "orders:\n";
					for(int i = 0; i < orders.size(); ++i)
					{
						logmsg += orders.get(i).getId() + ": " + 
								orders.get(i).getStartStationId() + "->" + 
								orders.get(i).getDestinationStationId() + ", inProgress: " + 
								orders.get(i).getProgress()+"\n";
					}
					Logger.Send(logmsg);
				}
				Link doneLink = path.remove(0);
				logmsg = "";
				if (!(doneLink instanceof WaitLink)) {
					if (Logger.getDebugLevel() >= 1)
						logmsg += getAgv().getEnvironment().getName() + ": reached Node " + doneLink.getEndNode().getID() + "\n";
					if(isNeedTarget)
					{
						isNeedTarget = manageFirstStep(false);
						//isFirstStep = false;
					}
					if (Logger.getDebugLevel() >= 2)
						logmsg += "                       " + 
									getAgvName() + ": path after if(isNeedTarget): " + WriteOut(path) + "\n";						
					if (mode == RANDOM_WALK) {
						if (getPerceptionModule().senseCanPickOrder()) {
							pickOrderNow();
							timeSinceLastPickupOrDelivery = 0;
						}
						//TODO: do we need this here?
						else if (path.isEmpty()) {
							if (targetOrderID != -1) {
								Order targetOrder = Logger.getOrderById(this, targetOrderID);
								path = dijkstra(findCurrentNode(), targetOrder.getStartStationId());
								pathLengthCounter = 0;
								pathLengthExpected = path.size();
							}
							else {
								path = calculateRndPath();
								isNeedTarget = true;
							}
						}
					}
					else if (mode == DELIVER_ORDER) {
						if (path.isEmpty()) {
							// ERRORTRACING, CP3: if we have an empty path in DELIVER_ORDER mode, we should be able to drop our order
							if (!getPerceptionModule().senseCanDropOrder())
							{
								Logger.Send(getAgvName() + ": CP3");
								throw new Exception(getAgv().getEnvironment().getName() + " CP3");
							}
							
							if (Logger.DropAt(this, getPerceptionModule().senseCurrentMapComponentId())) {
								
								//write out the expected path length in the beginning of the calculation, and the actual path length travelled 
								if (Logger.getDebugLevel() >= 1)
									{
										logMsgPathLength = "";
										logMsgPathLength += getAgvName() + ": Delivered order \n";
										logMsgPathLength += "Expected path length: " + pathLengthExpected + " actual path length: " + pathLengthCounter + "\n";
										Logger.Send(logMsgPathLength);
										logMsgPathLength = "";
									}
								mode = RANDOM_WALK;
								timeSinceLastPickupOrDelivery = 0;
								
								//calculate path to  random free order
								if (Logger.SelectRandomFreeOrder(this)) {
									Order targetOrder = Logger.getOrderById(this, targetOrderID);
									path = dijkstra(findCurrentNode(), targetOrder.getStartStationId());
									
									//calculate path length and set the counter, expected path length
									if (Logger.getDebugLevel() >= 1)
									{
									        logmsg += "Planned to a new free order, expected path length: " + path.size() + "\n";
									}
									pathLengthCounter = 0;
									pathLengthExpected = path.size();
									
								}
								else 
								{
									isNeedTarget = true;
									//move away from the location
									List<Link> links = new ArrayList(findCurrentNode().getRealOutLinks());
									Link randomOutLink = randomFromCollection(links);
									path.add(randomOutLink);
								}
								if (Logger.getDebugLevel() >= 1) {
									logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
									logmsg += getAgv().getEnvironment().getName() + ": step - path after dropping order\n";
									logmsg += WriteOut(path) + "\n";
									logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
								}
							}
						}
					}
				}
				else 
				{	
					if (Logger.getDebugLevel() >= 1)
						logmsg += getAgv().getEnvironment().getName() + ": waited one round\n";
					if (path.isEmpty()) {
						if (getPerceptionModule().senseCanPickOrder()) {
							pickOrderNow();
						}
						else if (isNeedTarget) {
							isNeedTarget = manageFirstStep(false);
						}
						else {
							path.add(new WaitLink(findCurrentNode()));
						}
					}
				}
				if (Logger.getDebugLevel() >= 1)
					Logger.Send(logmsg);
				logmsg = "";
				
				// ERRORTRACING, CP4: at this point, every vehicle should have a valid path (containing at least 1 link)
				if (path.isEmpty()) {
					Logger.Send(getAgvName() + ": CP4");
					Logger.StopSimulation();
					throw new Exception(getAgv().getEnvironment().getName() + " CP4");
				}
				
				
				//Syncronization
				sync(2);
				
				//update the counters
				
				if (mode == RANDOM_WALK && targetOrderID == -1 && !(doneLink instanceof WaitLink) )
				{
					randomPathCounter++;
				}
				if((mode == RANDOM_WALK && targetOrderID != -1) ||(mode == DELIVER_ORDER))
				{
					pathLengthCounter++;
				}
				
				timeSinceLastPickupOrDelivery++;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (!logmsg.isEmpty())
				Logger.Send(logmsg);
		}

	}
	
	public void cleanUp() {
		
	}

	public BehaviourModule cloneBehaviour() {
		return new TwoEdgeLookAheadBehavior();
	}
	
	//___________________________________________HELP_METHODS______________________________________________________________________________	

	private void pickOrderNow() {
		String logmsg = "";
		if(Logger.PickOrderAt(this, getPerceptionModule().senseCurrentMapComponentId())) { 
			String logMsgPathLength = "";
			//write out the expected path length in the beginning of the calculation, and the actual path length travelled
			String logmsgerror = "targetOrderId = " + targetOrderID + "senseorderholding " + getPerceptionModule().senseOrderHolding().getId();
			Logger.Send(logmsgerror);
			if (Logger.getDebugLevel() >= 1 && targetOrderID == getPerceptionModule().senseOrderHolding().getId())
			{
                logMsgPathLength += getAgvName() + ": Reached planned order to pick \n";
                logMsgPathLength += "Expected path length: " + pathLengthExpected + " actual path length: " + pathLengthCounter + "\n";
                Logger.Send(logMsgPathLength);
                logMsgPathLength = "";
			}
			else
			{
                logMsgPathLength += getAgvName() + ": Reached unplanned order to pick \n";
                logMsgPathLength += "Number of random steps: "+ randomPathCounter + "\n";
                Logger.Send(logMsgPathLength);
                logMsgPathLength = "";
                randomPathCounter = 0;
                setTargetOrderId(getPerceptionModule().senseOrderHolding().getId());
			}

			List<Link> emptyList = new ArrayList<Link>();
			try {
				path = dijkstra(findCurrentNode(), getPerceptionModule().senseOrderHolding().getTo());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//path = calculatePath(getAgv(), getPerceptionModule().senseCurrentMapComponentId(), getPerceptionModule().senseOrderHolding().getTo(), emptyList);
			//calculate path length and set the counter, expected path length
			if (Logger.getDebugLevel() >= 1) 
			{
				logmsg += "Picked order, expected path length: " + path.size() + "\n";
			}
			pathLengthCounter = 0;
			pathLengthExpected = path.size();
			if (Logger.getDebugLevel() >= 1) {
				logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
				logmsg += getAgv().getEnvironment().getName() + ": step - path after picking order\n";
				logmsg += WriteOut(path) + "\n";
				logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
				if(path.size() == 0)
					logmsg += getAgv().getEnvironment().getName() + ": calcpath wrong \n";
			}
			mode = DELIVER_ORDER;
			isNeedTarget = false;
		}
		else {
			if (Logger.getDebugLevel() >= 1)
				// Should not get here, because it should throw exception in Loggercd 
				logmsg += "no order was found at" + findCurrentNode() + ", but it sensed!!!\n";
		}
		if (Logger.getDebugLevel() >= 1)
			Logger.Send(logmsg);
	}
	/**
	 * creates a message to decide the ordering of the AGVs during synchronization
	 * @return returns a SimpleKnowledgeMessage containing the mode (available through "mode") and
	 *         rank (available through "rank")
	 */
	private SimpleKnowledgeMessage createRankMessage(Integer mode, Double rank) {
		Knowledge knowledge = new Knowledge();
		knowledge.put("mode", mode);
		knowledge.put("rank", rank);
		
		return new SimpleKnowledgeMessage(knowledge);
	}
	
	/**
	 * creates a message to share the path of an agv with other vehicles for local conflict management
	 * @return returns a SimpleKnowledgeMessage containing the next link (available through "link") along the path
	 */
	private SimpleKnowledgeMessage createPathMessage(List<Link> path) {
		Knowledge knowledge = new Knowledge();
		knowledge.put("link", path.get(0));
		
		return new SimpleKnowledgeMessage(knowledge);
	}
	


}
