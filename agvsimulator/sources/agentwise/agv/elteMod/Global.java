package agentwise.agv.elteMod;

import java.util.ArrayList;
import java.util.List;

import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.agv.map.Link;
import agentwise.agv.model.environment.Order;

public abstract class Global extends BehaviourModule {
	public volatile static int NMB_AGVs = 0;	
	
	// TODO: constructor may be?
	protected List<Link> path = new ArrayList<Link>();
	protected List <List<Link>> alreadyGeneratedPaths = new ArrayList<List<Link>>();
	protected List <List<Link>> allPaths = new ArrayList<List<Link>>();
	protected List<Order> orders;
	protected String targetID = "--";
	protected long targetOrderID = -1;
	protected final int verbose = 3;
	protected int mode = RANDOM_WALK;

	protected static int RANDOM_WALK = 0;
	protected static int DELIVER_ORDER = 1;
	
	public void setTargetOrderId(long id) {
		targetOrderID = id;
	}
	public long getTargetOrderId() {
		return targetOrderID;
	}
	public String getAgvName() {
		return getAgv().getEnvironment().getName();
	}
	public List<List<Link>> getAllPaths() {
		return allPaths;
	}
	public long PickUpTheOrder() {
		getActionExecutionModule().pickOrder();
		// The id of OrderInfo should be the same as the Order
		return this.getPerceptionModule().senseOrderHolding().getId();
	}
	public long DropTheOrder() {
		long dropedOrderId = this.getPerceptionModule().senseOrderHolding().getId();
		getActionExecutionModule().dropOrder();
		return dropedOrderId;
	}
	public void WaitSecond() {
		getActionExecutionModule().waitSecond();
	}
}
