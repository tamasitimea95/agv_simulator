package agentwise.agv.elteMod;

import java.util.List;

import agentwise.agv.model.agv.map.Link;

public interface DisjointOperator {
	public boolean decide(List<Link> path1, List<Link> path2);
}
