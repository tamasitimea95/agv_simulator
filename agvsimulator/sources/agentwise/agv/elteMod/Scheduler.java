package agentwise.agv.elteMod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import agentwise.agv.model.agv.map.Link;

import static java.util.stream.Collectors.*;

 

public class Scheduler {
	public enum Response 
	{ 
		FATAL, LIVELOCK, WAIT, SUCCESS;
	}
	private boolean fatalStatus;
	private boolean livelockStatus;
	private int round;
	private int lastOrderRound;
	private Set<String> OkIds = new HashSet<String>();
	
	// TODO find good elements
	private int maxTimeSincePickupOrDelivery = 10;
	private int maxSnapshots = 5;
	private int maxSnapshotByCar = 10;
	
	public boolean getFatalStatus(Global agv)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		boolean result = false;
		try {
			result = fatalStatus;
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getFatalStatus: by("+agv.getAgvName()+")\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	public void setFatalStatus(Global agv, boolean status)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		try {
			fatalStatus = status;
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.setFatalStatus: by("+agv.getAgvName()+")\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	public boolean getLivelockStatus(Global agv)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		boolean result = false;
		try {
			result = livelockStatus;
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getLivelockStatus: by("+agv.getAgvName()+")\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
		
	}
	
	public void setLivelockStatus(Global agv, boolean status)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		boolean result = false;
		try {
			livelockStatus = status;
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.setLivelockStatus: by("+agv.getAgvName()+")\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	public int getRound()
	{
		
		return round;
	}
	
	public int getLastOrderRound()
	{
		return lastOrderRound;
	}
	
	public void setLastOrderRound(int round)
	{
		lastOrderRound = round;
	}
	
	public boolean isTheEndOfTheOrders()
	{
		return round > lastOrderRound;
	}
	
	// TODO: replace the agvname to ID
	public void SendIn(Global agv, Double rank) {
		sendIn(agv, rank, ranksAgvIds, ranksPairs);
		if (this.getRankCount(agv) == Global.NMB_AGVs) {
			if (Logger.getDebugLevel() >= 1) {
				String logmsg = "Scheduler: Ranks are collected:\n";
				logmsg += this.logRanks(agv);
				Logger.Send(logmsg);
			}
		}
	}
	
	public void SendIn(Global agv, List<Link> path) {
		sendIn(agv, path, pathsAgvIds, pathsPairs);
		if (this.getPathCount(agv) == Global.NMB_AGVs) {
			if (Logger.getDebugLevel() >= 1) {
				String logmsg = "Scheduler: Paths are collected:\n";
				logmsg += this.logPaths(agv);
				Logger.Send(logmsg);
			}
		}
	}
	
	public void SendIn(Global agv, Integer time) {
		sendIn(agv, time, timesAgvIds, timesPairs);
		if (this.getTimeCount(agv) == Global.NMB_AGVs) {
			if (Logger.getDebugLevel() >= 1) {
				String logmsg = "Scheduler: Times are collected:\n";
				logmsg += this.logTimes(agv);
				Logger.Send(logmsg);
			}
			
			//at this point we have everything about snapshots and times, check if a livelock can be detected
			boolean livelock = CheckLivelock();
			if(livelock)
			{
				Logger.Send("!!!!!!!!!!!!!!!!!\n");
				Logger.Send("LIVELOCK DETECTED \n");
				Logger.Send("!!!!!!!!!!!!!!!!!\n");
				
				livelockStatus = true;
			}
		}
	}
	
	// TODO: replace the agvname to ID
	private <T> void sendIn(Global agv, T value, Set<String> set, List<Pair<T>> pairs) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		if (Logger.getDebugLevel() >= 2)
			Logger.Send("@@@ After trylock IN sendIn\n");
		try {
			if (set.contains(agv.getAgvName())) {
				Logger.Send(agv.getAgvName() + " is already in ranks, the value is not updated.\n");
			}
			else {
				pairs.add(new Pair(agv.getAgvName(), value));
				set.add(agv.getAgvName());
				//Logger.Send("----- The " + agv.getAgvName() + "is added to the set.\n");
			}
		} 
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.sendIn: by("+agv.getAgvName()+")\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	private String logRanks(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		String result = "";
		try {
			Collections.sort(ranksPairs,
					(o1, o2) -> o1.getValue().compareTo(o2.getValue()) * (-1));
			
			for (int i = 0; i < ranksPairs.size(); ++i) {
				result += ranksPairs.get(i).getText() + ": " + ranksPairs.get(i).getValue() + "\n";
			}
		} 
		catch(Exception e) {
			Logger.Send("Exception in logRanks, Scheduler.SendIn(Global, Double):\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	@SuppressWarnings("finally")
	private String logPaths(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		String result = "";
		String pathString;
		SortPathsPairs(); 
		try {
			for (int i = 0; i < pathsPairs.size(); ++i) {
				pathString = getString(pathsPairs.get(i).getValue());
				result += pathsPairs.get(i).getText() + ": " + pathString + "\n";
				
				// add the car snapshot into the snapshot list
				AddNewSnapshotByCar(pathString,i);
			}
		} 
		catch(Exception e) {
			Logger.Send("Exception in logPaths, Scheduler.SendIn(Global, Double):\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			
			// we will save a snapshot of the paths in the current moment, which is contained in the result string
			AddNewSnapshot(result);
			
			return result;
		}
	}
	
	@SuppressWarnings("finally")
	private String logTimes(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		String result = "";
		SortTimesPairs(); 
		try {
			timesCounts = new ArrayList<Integer>();
			for (int i = 0; i < timesPairs.size(); ++i) {
				result += timesPairs.get(i).getText() + ": " + timesPairs.get(i).getValue() + "\n";
				
				// also add the time to timesCount list
				timesCounts.add(timesPairs.get(i).getValue());

			}
		} 
		catch(Exception e) {
			Logger.Send("Exception in logTimes, Scheduler.SendIn(Global, Double):\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			
			
			return result;
		}
	}
	
	private void SortPathsPairs()
	{
		Collections.sort(pathsPairs,
						(o1, o2) -> (o1.getText().compareTo(o2.getText())));
	}
	
	private void SortTimesPairs()
	{
		Collections.sort(timesPairs,
						(o1, o2) -> (o1.getText().compareTo(o2.getText())));
	}
	
	/*
	Add the new snapshot in the list, if it is a new snapshot, otherwise update the count of the snapshot in the list
	*/
	private void AddNewSnapshot(String snapshot) {
		int idx = snapshots.indexOf(snapshot);
		if(idx == -1)
		{
			snapshots.add(snapshot);
			snapshotCounts.add(1);
		}
		else
		{
			snapshotCounts.set(idx, snapshotCounts.get(idx)+1);
		}
	}
	
	private void AddNewSnapshotByCar(String snapshot, int carIdx) {
		
		List<String> emptyStringList = new ArrayList<String>();
		List<Integer> emptyIntList = new ArrayList<Integer>();

		while(snapshotsByCar.size() <= carIdx) {
			snapshotsByCar.add(emptyStringList);
			snapshotCountsByCar.add(emptyIntList);
			
		}
		
		int idx = snapshotsByCar.get(carIdx).indexOf(snapshot);
		
		if(idx == -1)
		{
			snapshotsByCar.get(carIdx).add(snapshot);
			snapshotCountsByCar.get(carIdx).add(1);
		}
		else
		{
			snapshotCountsByCar.get(carIdx).set(idx, snapshotCountsByCar.get(carIdx).get(idx)+1);
		}
	}
	
	public boolean CheckLivelock() {
		
		String logmsg = "times: [";
		for(int i = 0; i < timesCounts.size(); ++i)
			logmsg += timesCounts.get(i) + ", ";
		logmsg += "] \n";
		Logger.Send(logmsg);
		
		logmsg = "snapshots: [";
		for(int i = 0; i < snapshotCounts.size(); ++i)
			logmsg += snapshotCounts.get(i) + ", ";
		logmsg += "] \n";
		Logger.Send(logmsg);
		
		logmsg = "snapshots by car: \n [";
		for(int i = 0; i < snapshotCountsByCar.size(); ++i)
		{
			for(int j = 0; j < snapshotCountsByCar.get(i).size(); ++j)
			{
				logmsg += " " + snapshotCountsByCar.get(i).get(j) + ", ";
			}
			logmsg += "\n";
		}
		logmsg += "] \n";
		Logger.Send(logmsg);
		
		logmsg = "";
		
		int currentMaxTimeSinceLastPickupOrDelivery = Collections.max(timesCounts);
		
		int currentMaxSnapshot = 0;
		if(snapshotCounts.size() > 0)
		{
			currentMaxSnapshot = Collections.max(snapshotCounts);
		}
		
		int currentMaxSnapshotByCar = 0;
		List <Integer> currentMaxSnapshots = new ArrayList<Integer>();
		if(snapshotCountsByCar.size() > 0)
		{
			for(int i = 0; i < snapshotCountsByCar.size(); ++i)
			{
				int max = Collections.max(snapshotCountsByCar.get(i));
				currentMaxSnapshots.add(max);
			}
			currentMaxSnapshotByCar = Collections.max(currentMaxSnapshots);
		}
		
		return currentMaxTimeSinceLastPickupOrDelivery >= maxTimeSincePickupOrDelivery 
			&& currentMaxSnapshot >= maxSnapshots
			&& currentMaxSnapshotByCar >= maxSnapshotByCar;
	}

	
	// TODO: do not sort again in every getPosition
	@SuppressWarnings("finally")
	public int getPosition(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		int position = -2;
		try {
			Collections.sort(ranksPairs,
					(o1, o2) -> o1.getValue().compareTo(o2.getValue()) * (-1));
			String logmsg = "";
			if (Logger.getDebugLevel() >= 2)
				logmsg += "@@@ IN Scheduler.getPosition: the agv ids: ";
			for (int i = 0; i < ranksPairs.size(); ++i) {
				if (Logger.getDebugLevel() >= 2)
					logmsg += ranksPairs.get(i).getText() + ", ";
				if (agv.getAgvName().equals(ranksPairs.get(i).getText()))
					position = i;
			}
			if (Logger.getDebugLevel() >= 2)
				Logger.Send(logmsg);
		} 
		catch(Exception e) {
			Logger.Send("Exception in getPosition, Scheduler.SendIn(Global, Double):\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return position;
		}
	}
	
	@SuppressWarnings("finally")
	public int getPathCount(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		int result = -1;
		try {
			result = pathsAgvIds.size();
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getPathCount:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	@SuppressWarnings("finally")
	public int getRankCount(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		int result = -1;
		try {
			result = ranksAgvIds.size();
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getPathCount:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	@SuppressWarnings("finally")
	public int getTimeCount(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		int result = -1;
		try {
			result = timesAgvIds.size();
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getTimeCount:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	// because of compatibility it return with List instead of Set
	@SuppressWarnings("finally")
	public List<Link> getForbiddenLinks(Global agv, int depth) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		List<Link> result = null;
		depth--;
		try {
			Set<Link> results = new HashSet<Link>();
			String logmsg = "";
			if (Logger.getDebugLevel() >= 2)
				logmsg += "@@@ IN Scheduler.getForbiddenLinks used agv ids: ";
			for (Pair<List<Link>> pathPair: pathsPairs) {
				if (Logger.getDebugLevel() >= 2)
					logmsg += pathPair.getText() + ", ";
				
				if(depth < pathPair.getValue().size())
				{
					results.add(pathPair.getValue().get(depth));
				}
			}
			if (Logger.getDebugLevel() >= 2) {
				logmsg += "\n";
				Logger.Send(logmsg);
			}
			result = new ArrayList<>(results);
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.getForbiddenLinks:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
			return result;
		}
	}
	
	public String getString(List<Link> path)
	{
		String result = "";
		for (int i = 0; i < path.size(); ++i)
		{
			result += path.get(i).getStartNode().getID() + "->"; 
		}
		result += path.get(path.size()-1).getEndNode().getID();
		return result;
	}
	
	public void RequestReset(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		try {
			// if the reset has not happened yet
			if (ranksAgvIds.size() > 0) {
				resetRequesters.add(agv.getAgvName());
				if (resetRequesters.equals(ranksAgvIds)) {
					reset();
				}
			}
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.RequestReset:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	synchronized public static Scheduler getInstance() {
		if (instance == null)
			instance = new Scheduler();
		return instance;
	}
	private static Scheduler instance;
	
	private Scheduler() {
		rl_messages = new ReentrantLock();
		round = 0;
		timesCounts = new ArrayList<Integer>();
		resetSnapshots();
		reset();
	}
	
	private void reset() {
		OkIds.clear();
		ranksAgvIds = new HashSet<String>();
		ranksPairs = new ArrayList<Pair<Double>>();
		pathsAgvIds = new HashSet<String>();
		pathsPairs = new ArrayList<Pair<List<Link>>>();
		timesAgvIds = new HashSet<String>();
		timesPairs = new ArrayList<Pair<Integer>>();
		resetRequesters = new HashSet<String>();
		round++;
		if(Logger.getDebugLevel() >= 1)
		{
			Logger.Send("Scheduler: ROUND " + round + "\n");
		}
	}
	
	public void resetSnapshots( ) {
		snapshots = new ArrayList<String>();
		snapshotCounts = new ArrayList<Integer>();
		
		snapshotsByCar = new ArrayList<List<String>>();
		snapshotCountsByCar = new ArrayList<List<Integer>>();
	}
	
	private ReentrantLock rl_messages;
	private Set<String> ranksAgvIds;
	private List<Pair<Double>> ranksPairs;
	private Set<String> pathsAgvIds;
	private List<Pair<List<Link>>> pathsPairs;
	private Set<String> timesAgvIds;
	private List<Pair<Integer>> timesPairs;
	private Set<String> resetRequesters;
	
	private List<String> snapshots;
	private List<Integer> snapshotCounts;
	
	private List<List<String>> snapshotsByCar;
	private List<List<Integer>> snapshotCountsByCar;
	
	private List<Integer> timesCounts;
	
	private class Pair<T> {
		public Pair(String text, T value) {
			this.text = text;
			this.value = value;
		}
		public Pair(Pair other) {
			// this does not work well, if the values have different types
			text = other.text;
			value = (T) other.value;
		}
		public String getText() {
			return text;
		}
		public T getValue() {
			return value;
		}
		@Override
		public boolean equals(Object o) {
			if (o == this)
				return true;
			if (!(o instanceof Pair))
				return false;
			
			return this.text.equals(((Pair)o).text);
		}
		private String text;
		private T value;
	}
	
	public void UpdatePaths(Map<String, List<Link>> newPaths, Global agvJustForWaitCommand)
	{
		while (!rl_messages.tryLock())
			agvJustForWaitCommand.WaitSecond();
		try {
			pathsPairs.clear();
			for (String AGVId: newPaths.keySet()) 
			{
				pathsAgvIds.add(AGVId);
				pathsPairs.add(new Pair<List<Link>>(AGVId, newPaths.get(AGVId)));
			}
			this.fatalStatus = false;
			this.livelockStatus = false;
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.UpdatePaths:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	//this agv has a path that is good at the moment
	public void SendOk(Global agv)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		try {
			OkIds.add(agv.getAgvName());
			Logger.Send("added id " + agv.getAgvName() + "\n");
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.RequestReset:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	//gets the status of the scheduler (SUCCESS -  all paths are OK, FATAL - there is a conflict, WAIT - we don't know it yet)
	@SuppressWarnings("finally")
	public Response getResponse(Global agv)
	{	
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		Response result = Response.WAIT;
		try {
			String resp_logmsg = "IN RESPONSE (" + agv.getAgvName() +"): size=" + OkIds.size() + " / " + Global.NMB_AGVs + "\n";
			if(fatalStatus == true)
			{
				resp_logmsg += "  Resp: FATAL\n";
				Logger.Send(resp_logmsg);
				result = Response.FATAL;
			}
			if(livelockStatus == true)
			{
				resp_logmsg += "  Resp: LIVELOCK\n";
				Logger.Send(resp_logmsg);
				result = Response.LIVELOCK;
			}
			if(OkIds.size() == Global.NMB_AGVs)
			{
				resp_logmsg += "  Resp: SUCCESS\n";
				Logger.Send(resp_logmsg);
				result = Response.SUCCESS;
			}
		}
		catch(Exception e) {
			Logger.Send("Exception in Scheduler.RequestReset:\n" +
					e.getMessage() + "\n");
		}
		finally {
			Logger.Send("In RESP, finally ("+agv.getAgvName()+")="+result+"\n");
			rl_messages.unlock();
			return result;
		}		
	}
}
