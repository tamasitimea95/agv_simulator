package agentwise.agv.elteMod;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.agv.map.*;

public class Locations {
	public List<Node> NodeList;
	
	public Locations(InformationMap infomap) {
		//NodeList = new ArrayList<Node>();
		List<Node> nodes = new ArrayList<Node>(infomap.getNodes());
		//find the locations (which have exactly 1 degree)	
		NodeList = nodes.stream()
				.filter(node -> node.getOutLinks().size() == 1)
				.collect(Collectors.toList());
	};
	
	public Node GetClosestLocation(Agv agv)
	{
		List<Link> emptyList = new ArrayList<Link>();
		List<Link> minPath = SimpleGlobal.calculatePath(agv, agv.getPerceptionModule().senseCurrentMapComponentId(), NodeList.get(0).getID(), emptyList);
		int minPathSize = minPath.size();
		Node closestLocation = NodeList.get(0);
		for(int i = 1; i < NodeList.size(); ++i)
		{
			List<Link> path = SimpleGlobal.calculatePath(agv, agv.getPerceptionModule().senseCurrentMapComponentId(), NodeList.get(i).getID(), emptyList);
			if(path.size() < minPathSize)
			{
				minPath = path;
				minPathSize = path.size();
				closestLocation = NodeList.get(i);
			}
		}
		return closestLocation;
	}

}


