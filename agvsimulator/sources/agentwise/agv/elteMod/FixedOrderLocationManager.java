/*
 * Created on Oct 25, 2004
 *
 */
package agentwise.agv.elteMod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Queue;
import java.util.Set;
import java.util.LinkedList;

import agentwise.agv.controller.Controller;
import agentwise.agv.controller.OrderManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Station;

import java.io.*; 

public class FixedOrderLocationManager extends OrderLocationManager {
	
	//private List<List<Order>> FixedOrders;
	private Map<Integer, List<Order>> FixedOrders;
	private Set<Order> notDoneOrders;
	private Map<String, Queue<Order>> locationQueues;
	private Controller controller;
	private int ordersSize;
	private int lastOrderRound;
	/**
	 * Constructor
	 * @throws IOException 
	 */
	public FixedOrderLocationManager(String fileName, Controller controller) throws IOException {
		super();
		
		//FixedOrders = new ArrayList<List<Order>>();
		FixedOrders = new HashMap<Integer, List<Order>>();
		this.controller = controller;
		notDoneOrders = new HashSet<Order>();
		
		//read file wth the given orders, and save it in the FixedOrders structure
		File file = new File(fileName.substring(0, fileName.length() - 3) + "txt");
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		String line;
		String logmsg = "Input Orders: \n";
		String[] ordersString = new String[] {""};
		while ((line = br.readLine()) != null)
		{
			if(Logger.getDebugLevel() >= 1)
			{
				logmsg += line + "\n";
			}
			List<Order> orders = new ArrayList<Order>();
			ordersString  = line.split(" ");
			int ind = Integer.valueOf(ordersString[0]);
			for(int i = 1; i < ordersString.length; i = i+2)
			{
				String in = "location_" + ordersString[i]; 
				String out = "location_" + ordersString[i+1];
				
				Order o = new Order(in, out);
				orders.add(o);
				notDoneOrders.add(o);
			}
			FixedOrders.put(ind, orders);
		}
		ordersSize = FixedOrders.size();
		lastOrderRound =  Integer.parseInt(ordersString[0]);
		logmsg += "last order round = " + lastOrderRound + "\n";
		logmsg += "########################################################################################################################### \n";
		Logger.Send(logmsg);
	}
	
	public int getOrdersSize()
	{
		return ordersSize;
	}
	
	public int getLastOrderRound()
	{
		return lastOrderRound;
	}
	
	/* (non-Javadoc)
	 * @see agentwise.agv.controller.ActiveEntity#step()
	 */
	public void step() {
		if (Scheduler.getInstance().getRound() > 500) {
			Logger.Send("LIVE LOCK");
			Controller.getInstance().stopSimulation();
		}
		if (isFinished()) {
		Logger.Send("SUCCESFUL");	
			Controller.getInstance().stopSimulation();
		}
		generateOrders();
		takeAwayOrders();
	}

	protected void generateOrders(){
		if (locationQueues == null) {
			locationQueues = new HashMap<String, Queue<Order>>();
			Collection<OrderLocation> locations = getPickOrderLocations();
			for (OrderLocation loc: locations) {
				Logger.Send(loc.getID());
				locationQueues.put(loc.getID(), new LinkedList<Order>());
			}
		}
		//get the orders from the FixedOrders list, and add it to the OrderManager
		Collection<OrderLocation> orderLocationCollection = getPickOrderLocations();
		int i = Scheduler.getInstance().getRound();
		if(FixedOrders.containsKey(i))
		{
			List<Order> orders = FixedOrders.get(i);
		
			if(orders.size() != 0) //if the list is empty, we have nothing to do
			{
				for(int j = 0; j < orders.size(); ++j)
				{
					Order o = orders.get(j);				
					List<OrderLocation> orderLocations = orderLocationCollection.stream()
							.filter(ord -> ord.getID().equals(o.getStartStationId()))
							.collect(Collectors.toCollection(ArrayList::new));
					OrderLocation ol = orderLocations.get(0);
					locationQueues.get(ol.getID()).add(o);
					/*if(!ol.hasOrder())
					{
						ol.addOrder(o);
						OrderManager.getInstance().addOrder(o);
					}*/
				}
				FixedOrders.remove(i);
			}
		}

		for(OrderLocation loc: orderLocationCollection) {
			if (!loc.hasOrder() && !locationQueues.get(loc.getID()).isEmpty()) {
				Order newOrder = locationQueues.get(loc.getID()).remove(); 
				loc.addOrder(newOrder);
				OrderManager.getInstance().addOrder(newOrder);
			}
		}
	}
	
	protected void takeAwayOrders(){
		Iterator iter = getDropOrderLocations().iterator();
		while(iter.hasNext()){
			OrderLocation ol = (OrderLocation)iter.next();
			if(ol.hasOrder()){
				Order o = ol.removeOrder();
				OrderManager.getInstance().removeOrder(o);
			}
		}	
	}
	
	protected boolean isFinished() {
		boolean isfinished = true;
		for (Order o: notDoneOrders) {
			if (!o.isDone()) {
				isfinished = false;
				break;
			}
		}
		return isfinished;
	}
}
