package agentwise.agv.elteMod;

public class NoSolutionException extends Exception {
	public NoSolutionException(String errorMessage) {
		super(errorMessage);
	}
}
