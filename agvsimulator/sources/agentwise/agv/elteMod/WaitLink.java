package agentwise.agv.elteMod;

import agentwise.agv.model.agv.map.Link;
import agentwise.agv.model.agv.map.Node;

public class WaitLink extends Link {

	public WaitLink(Node atNode) {
		super("wait", atNode, atNode);
	}

}
