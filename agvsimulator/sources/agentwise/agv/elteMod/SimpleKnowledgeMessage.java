package agentwise.agv.elteMod;

import java.util.List;

import agentwise.agv.model.agv.Knowledge;
import agentwise.agv.model.agv.map.Link;
import agentwise.agv.model.environment.communication.KnowledgeMessage;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.PositionMessage;
import agentwise.agv.util.Coordinate;

public class SimpleKnowledgeMessage extends KnowledgeMessage {

	public SimpleKnowledgeMessage(Knowledge knowledge) {
		super(knowledge);
	}

	@Override
	public Message copy() {
		Knowledge newKnowledge = new Knowledge();
		newKnowledge.putAll(knowledge);
		
		SimpleKnowledgeMessage result = new SimpleKnowledgeMessage(newKnowledge);
        result.setSender(getSender());
        return result;
	}

	@Override
	public void update() {
	}

}
