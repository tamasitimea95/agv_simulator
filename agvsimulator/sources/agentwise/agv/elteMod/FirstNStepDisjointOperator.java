package agentwise.agv.elteMod;

import java.util.List;

import agentwise.agv.model.agv.map.Link;

public class FirstNStepDisjointOperator implements DisjointOperator {
	private int length;
	public FirstNStepDisjointOperator(int n)
	{
		length = n;
	}
	public boolean decide(List<Link> path1, List<Link> path2)
	{
		int n = Math.min(length, Math.min(path1.size(), path2.size()));
		for(int i = 0; i < n; ++i)
		{
			if((!(path1.get(i) instanceof WaitLink && path1.get(i) instanceof WaitLink) && path1.get(i).getID().equals(path2.get(i).getID())) || 
					path1.get(i).getEndNode().getID().equals(path2.get(i).getEndNode().getID()) ||
					path1.get(i).getStartNode().getID().equals(path2.get(i).getStartNode().getID())
			  )
				return false;
		}
		return true;
	}
}
