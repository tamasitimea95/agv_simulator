/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.elteMod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import agentwise.agv.model.agv.Knowledge;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.KnowledgeMessage;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.PositionMessage;
import agentwise.agv.model.environment.communication.StringMessage;
import agentwise.agv.controller.*;
import agentwise.agv.model.environment.*;

import agentwise.agv.model.agv.map.*;

public class SimpleBehavior extends SimpleGlobal {

	/**
	 * Constructs an instance of RandomPathBehaviour
	 */
	public SimpleBehavior() {
		super();
	}

	private boolean isFirstStep;
	private boolean isNeedTarget;

	/**
	 * @see agentwise.agv.model.agv.behaviour.BehaviourModule#initialize()
	 */
	public void initialize() {
		Node currentNodeInMap = findCurrentNode();
		isFirstStep = true;
		
		// path is empty at this point, it is filled at the first step
		
		path = new ArrayList<Link>();
		String logmsg = "---------------------\n";
		logmsg += getAgv().getEnvironment().getName() + ": initialize - path after init\n";
		logmsg += WriteOut(path) + "\n";
		logmsg += "---------------------\n";
		Logger.Send(logmsg);
	}

	public void step() {
		for (int i = 0; i <= 10; i++)
			getActionExecutionModule().waitSecond();
		String logmsg = "";
		//handle first step, fill path
		if(isFirstStep)
		{
			logmsg += getAgv().getEnvironment().getName() + ": first step\n";
			Collection<Order> orderCollection = OrderManager.getInstance().getCurrentOrders();
			orders = orderCollection.stream().collect(Collectors.toList());
			path = calculatePathToOrder(orders);
			
			logmsg += "---------------------\n";
			logmsg += getAgv().getEnvironment().getName() + ": step - path in the first step\n";
			logmsg += WriteOut(path) + "\n";
			logmsg += "---------------------\n";
			Logger.Send(logmsg);
			logmsg = "";
			
			isFirstStep = false;
		}
		
		// the try-catch structure is included for error tracing
		try {
			// if the agv is on a link, it should go ahead
			if (getPerceptionModule().senseIsOnSegment()) {
				String destinationID = path.get(0).getEndNode().getID();
				getActionExecutionModule().moveStepToStation(destinationID); 
			}  
			// it takes more than one step to leave a location (but not a station)
			else if (!(path.get(0) instanceof WaitLink) && 
					getPerceptionModule().senseCurrentMapComponentId().equals(path.get(0).getStartNode().getID())) {
				String destinationID = path.get(0).getEndNode().getID();
				logmsg += getAgv().getEnvironment().getName() + ": leaving the location..." 
					+ getPerceptionModule().senseCurrentMapComponentId() + " to " + destinationID + "\n";
				Logger.Send(logmsg);
				logmsg = "";
				getActionExecutionModule().moveStepToStation(destinationID);
			}
			else {
				// ERRORTRACING, CP1: agv should move according to its plan
				if (!(path.get(0) instanceof WaitLink) && 
						(getPerceptionModule().senseCurrentMapComponentId() != path.get(0).getEndNode().getID()))
					throw new Exception(getAgv().getEnvironment().getName() + " CP1");

				//ERRORTRACING, CP2: when we arrive on a segment, path should not be empty
				if (path.size() == 0)
					throw new Exception("CP2");
				
				Collection<Order> orderCollection = OrderManager.getInstance().getCurrentOrders();
				orders = orderCollection.stream().collect(Collectors.toList());
				logmsg += "orders:\n";
				for(int i = 0; i < orders.size(); ++i)
				{
					logmsg += orders.get(i).getId() + ": " + 
							orders.get(i).getStartStationId() + "->" + 
							orders.get(i).getDestinationStationId() + ", inProgress: " + 
							orders.get(i).getProgress() + "\n";
				}
				
				Link doneLink = path.remove(0);
				
				logmsg += getAgv().getEnvironment().getName() + ": remove " + doneLink.getStartNode().getID() + 
						" -> " + doneLink.getEndNode().getID() + "\n";
				logmsg += "---------------------\n";
				logmsg += getAgv().getEnvironment().getName() + ": step - path after removing the first link\n";
				logmsg += WriteOut(path) + "\n";
				logmsg += "---------------------\n";
				Logger.Send(logmsg);
				logmsg = "";
				
				// if the current link on our path is a WaitLink, then we can skip this part
				if (!(doneLink instanceof WaitLink)) {
					if (mode == RANDOM_WALK) {
						if (getPerceptionModule().senseCanPickOrder()) 
						{
							if(Logger.PickOrderAt(this, getPerceptionModule().senseCurrentMapComponentId())) { 
								List<Link> emptyList = new ArrayList<Link>();
								path = calculatePath(getAgv(), getPerceptionModule().senseCurrentMapComponentId(), getPerceptionModule().senseOrderHolding().getTo(), emptyList);
								logmsg += "---------------------\n";
								logmsg += getAgv().getEnvironment().getName() + ": step - path after picking order\n";
								logmsg += WriteOut(path) + "\n";
								logmsg += "---------------------\n";
								if(path.size() == 0)
									logmsg += getAgv().getEnvironment().getName() + ": calcpath wrong \n";
								mode = DELIVER_ORDER;
								isNeedTarget = false;
							}
							else {
								logmsg += "no order was found!!!\n";
							}
						}
						else if (path.size() == 1) 
						{
							path = extendRndPath(path.get(0));
							logmsg += "---------------------\n";
							logmsg += getAgv().getEnvironment().getName() + ": step - path after extendRndPath\n";
							logmsg += WriteOut(path) + "\n";
							logmsg += "---------------------\n";
							if(path.size() == 0)
								logmsg += getAgv().getEnvironment().getName() + ": extendpath wrong \n";	
						}
					}
					else if (mode == DELIVER_ORDER) 
					{
						if (path.isEmpty()) {
							// ERRORTRACING, CP3: if we have an empty path in DELIVER_ORDER mode, we should be able to drop our order
							if (!getPerceptionModule().senseCanDropOrder())
								throw new Exception("CP3");
							
							getActionExecutionModule().dropOrder();
							
							mode = RANDOM_WALK;
							
							//calculate path to a random free order
							path = calculatePathToOrder(orders);
							
							logmsg += "---------------------\n";
							logmsg += getAgv().getEnvironment().getName() + ": step - path after dropping order\n";
							logmsg += WriteOut(path) + "\n";
							logmsg += "---------------------\n";
						}
					}
				}	
				logmsg += WriteOut(path) + "\n";	
				// ERRORTRACING, CP4: at this point, every vehicle should have a valid path
				if (path.size() == 0)
					throw new Exception("CP4");
				// ERRORTRACING, CP5: path should have length >= 2, EXCEPT when the avg is one step away
				// from delivering an order
				if ((path.size() == 1) && (mode != DELIVER_ORDER))
				{
					path = extendRndPath(path.get(0));
					logmsg += "---------------------\n";
					logmsg += getAgv().getEnvironment().getName() + ": step - path after correcting exception CP5\n";
					logmsg += WriteOut(path) + "\n";
					logmsg += "---------------------\n";
					throw new Exception("CP5");
				}
				Logger.Send(logmsg);
				// ___________________________SYNCHRONISATION__________________________________
				//System.out.println(getAgv().getEnvironment().getName() + ": synchro started");
				double rank = Math.random();
				SimpleKnowledgeMessage msg = createPathMessage(path, mode, rank);
				getCommunicationModule().broadcast(msg);

				Integer OKs = 0;
				while (OKs < (NMB_AGVs - 1)) {
					Message receivedMsg = getCommunicationModule().receive();
					while (receivedMsg != null) {
						if (receivedMsg instanceof SimpleKnowledgeMessage) {
							SimpleKnowledgeMessage pathMsg = (SimpleKnowledgeMessage) receivedMsg;
							resolvePathConflict(pathMsg, rank);
							OKs += 1;
						}

						receivedMsg = getCommunicationModule().receive();
					}

					// if you still need to receive PathMessages from other avgs, wait before trying again
					if (OKs < (NMB_AGVs - 1)) {
						for (int i = 0; i <= 20; i++)
							getActionExecutionModule().waitSecond();
					}
					
				}
				//System.out.println(getAgv().getEnvironment().getName() + ": synchro finished");
				// ________________________SYNCHRO FINISHED______________________________

				//start the vehicle along its path
				if (!(path.get(0) instanceof WaitLink)) {
					System.out.println(getAgv().getEnvironment().getName() + ": moving along path");
					System.out.println("---------------------");
					System.out.println(getAgv().getEnvironment().getName() + ": step - path after moving along path");
					WriteOut(path);
					System.out.println("---------------------");
					String destinationID = path.get(0).getEndNode().getID();
					getActionExecutionModule().moveStepToStation(destinationID);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void resolvePathConflict(SimpleKnowledgeMessage pathMsg, double rank) {
		double otherMode = (int) pathMsg.getKnowledge().get("mode");
		double otherRank = (double) pathMsg.getKnowledge().get("priority");
		Link otherFirstLink = (Link)pathMsg.getKnowledge().get("link1");
		boolean isOnLocation = (path.get(0).getStartNode().outLinks.size() == 1);
		boolean isOtherOnLocation = (otherFirstLink.getStartNode().outLinks.size() == 1);
		if(isOtherOnLocation) 
		{
			System.out.println(getAgv().getEnvironment().getName() + ": OtherFirstLink: "+ otherFirstLink.getStartNode().getID() + "->" + otherFirstLink.getEndNode().getID());
			System.out.println(getAgv().getEnvironment().getName() + ": Other vehicle is on location " + otherFirstLink.getStartNode().getID());
		}
		if(isOnLocation)
		{
			return;
		}		
		if((mode == RANDOM_WALK && otherMode == DELIVER_ORDER 
				|| (mode == otherMode && rank < otherRank)) //the other rank is bigger
				|| isOtherOnLocation) //the other vehicle is on the location
		{
			List<Link> restrictedLinks = new ArrayList<Link>();
			String destID = hasConflict(pathMsg, restrictedLinks);
			if(!destID.equals(""))
			{	
				System.out.println(getAgv().getEnvironment().getName() + ": has conflict");
				List<Link> subpath = calculatePath(getAgv(), getPerceptionModule().senseCurrentMapComponentId(), destID, restrictedLinks);
				if(subpath.size() == 0)
				{
					System.out.println(getAgv().getEnvironment().getName() + ": cant find alternative path");
					boolean firstLinkConflict = path.get(0).getEndNode().getID().equals(otherFirstLink.getStartNode().getID());
					System.out.println("  " + getAgv().getEnvironment().getName() + ": IsOtherOnLocation = " + isOtherOnLocation);
					System.out.println("  " + getAgv().getEnvironment().getName() + ": firstLinkConflict = " + firstLinkConflict);
					if(isOtherOnLocation && firstLinkConflict) //we have to step back somewhere
					{
						System.out.println(getAgv().getEnvironment().getName() + ": step back");
						//TODO code duplicate
						// find the Node you are currently staying at within the InformationMap
						String currentStationID = getPerceptionModule().senseCurrentMapComponentId();
						Collection<Node> nodes = getAgv().getInformationMap().getNodes();
						Node currentNodeInMap = nodes.stream()
								.filter(node -> node.getID().equals(currentStationID))
								.findFirst()
								.get();
						
						// find a link that is not the bad one
						Collection<Link> oldOutLinks = currentNodeInMap.getOutLinks();
						List<Link> outLinks1 = new ArrayList<Link>(oldOutLinks);
						outLinks1.removeIf(l -> l.getID().equals(otherFirstLink.getID()));	
						System.out.println(getAgv().getEnvironment().getName() + ": removed " + otherFirstLink.getID());
						Link extralink1 = outLinks1.stream()
						        .skip((int) (outLinks1.size() * Math.random()))
						        .findFirst()
						        .get();
						
						//find the exact same link back
						Collection<Link> oldOutLinks2 = extralink1.getEndNode().getOutLinks();
						List<Link> outLinks2 = new ArrayList<Link>(oldOutLinks2);
						outLinks2.removeIf(l -> !l.getID().equals(extralink1.getID()));						
						Link extralink2 = outLinks2.stream()
						        .skip((int) (outLinks2.size() * Math.random()))
						        .findFirst()
						        .get(); 
						System.out.println(getAgv().getEnvironment().getName() + ": added " + extralink1.getID());
						System.out.println(getAgv().getEnvironment().getName() + ": added " + extralink1.getID());		
						path.add(0, extralink2);
						path.add(0, extralink1);
						System.out.println("---------------------");
						System.out.println(getAgv().getEnvironment().getName() + ": step - resolvePathConflict - path when stepping back");
						WriteOut(path);
						System.out.println("---------------------");
						
					}
					else //we wait
					{
						System.out.println(getAgv().getEnvironment().getName() + ": wait");
						WaitLink waitlink = new WaitLink(path.get(0).getStartNode());
						path.add(0, waitlink);
						System.out.println("---------------------");
						System.out.println(getAgv().getEnvironment().getName() + ": step - resolvePathConflict - path when waiting");
						WriteOut(path);
						System.out.println("---------------------");
					}
				}
				else
				{
					System.out.println("---------------------");
					System.out.println(getAgv().getEnvironment().getName() + ": step - resolvePathConflict - path before removing first segments");
					WriteOut(path);
					System.out.println("---------------------");
					
					if(restrictedLinks.size() == 1)
					{
						path.remove(0);
					}
					if(restrictedLinks.size() == 2)
					{
						path.remove(1);
						path.remove(0);
					}
					System.out.println("---------------------");
					System.out.println(getAgv().getEnvironment().getName() + "step - resolvePathConflict - path after removing first segments");
					WriteOut(path);
					System.out.println("---------------------");
					path.addAll(0, subpath);
					System.out.println("---------------------");
					System.out.println(getAgv().getEnvironment().getName() + "step - resolvePathConflict - path after addign subpath");
					WriteOut(path);
					System.out.println("---------------------");
				}
			}
		}
	}

	/* returns an empty String, if there is no conflict, and the ID of the endpoint, if it finds a conflict */
	private String hasConflict(SimpleKnowledgeMessage pathMsg, List<Link> restrictedLinks) {
		Link otherLink1 = (Link)pathMsg.getKnowledge().get("link1");
		Link otherLink2 = (Link)pathMsg.getKnowledge().get("link2");
		/*if(otherLink2 == null)	
		{
			System.out.println(getAgv().getEnvironment().getName() + "otherlink2 is empty, no conflict");
			return "";
		}
		*/
		//case 1, 2, 3
		if(otherLink1.getEndNode().getID().equals(path.get(0).getEndNode().getID()))
		{
			restrictedLinks.add(path.get(0));
			restrictedLinks.add(path.get(1));
			return path.get(1).getEndNode().getID();
		}
		//case 4
		else if(otherLink1.getStartNode().getID().equals(path.get(0).getEndNode().getID()) && 
				otherLink1.getEndNode().getID().equals(path.get(0).getStartNode().getID()))
		{
			restrictedLinks.add(path.get(0));
			return path.get(0).getEndNode().getID();
		}
		return "";
	}

	/**
	 * creates a path message including the next 1 or 2 links of the avg for local conflict management.
	 * @param path the path of the avg
	 * @param mode the mode of the avg
	 * @return returns a knowledge message containing the mode, the nmbOfSteps (1 or 2) and the associated links, and
	 * 		   the randomly assigned avgPrio
	 */
	private SimpleKnowledgeMessage createPathMessage(List<Link> path, Integer mode, Double rank) {
		Knowledge knowledge = new Knowledge();
		knowledge.put("mode", mode);
		knowledge.put("link1", path.get(0));
		knowledge.put("priority", rank);
		knowledge.put("nmbSteps", 1);
		
		if (path.size()>1) {
			knowledge.put("link2", path.get(1));
			knowledge.put("nmbSteps", 2);
		}	
		
		return new SimpleKnowledgeMessage(knowledge);
	}

	public void cleanUp() {
		// end broadcast communication
		//getCommunicationModule().cancelperiodiccommunication(broadcastid);
	}

	/**
	 * @see agentwise.agv.model.agv.behaviour.BehaviourModule#cloneBehaviour()
	 */
	public BehaviourModule cloneBehaviour() {
		return new SimpleBehavior();
	}
}
