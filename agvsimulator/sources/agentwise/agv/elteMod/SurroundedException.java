package agentwise.agv.elteMod;

public class SurroundedException extends Exception {
	
	public SurroundedException(String message) {
		super(message);
	}
	
}