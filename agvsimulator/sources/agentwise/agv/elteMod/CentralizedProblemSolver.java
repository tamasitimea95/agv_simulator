package agentwise.agv.elteMod;

import java.util.List;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import ilog.concert.*;
import ilog.cplex.*;

import agentwise.agv.model.agv.map.Link;

public class CentralizedProblemSolver {
	public enum Response 
	{ 
		UPDATE_NEEDED, WAIT, SUCCESS ;
	} 
	private Map<String, List<List<Link>>> allPathsMap = new HashMap<String, List<List<Link>>>();
	private List<Pair> conversionBetweenTheIndices = new ArrayList<Pair>();
	private Set<String> alreadySentPathInCurrentRound = new HashSet<String>();
	private Map<String, List<Link>> solution = new HashMap<String, List<Link>>();
	private int numberOfAffectedAGVs = 0;
	private ReentrantLock rl_messages;
	private DisjointOperator disjointOperator;
	
	synchronized public static CentralizedProblemSolver getInstance(int step) {
		if (instance == null)
			instance = new CentralizedProblemSolver(new FirstNStepDisjointOperator(step));
		return instance;
	}
	private static CentralizedProblemSolver instance;
	
	private CentralizedProblemSolver(DisjointOperator disjointOperator) {
		rl_messages = new ReentrantLock();
		this.disjointOperator = disjointOperator;
	}
	
	private class Pair {
		public Pair(String text, Integer value) {
			this.text = text;
			this.value = value;
		}
		public Pair(Pair other) {
			// this does not work well, if the values have different types
			text = other.text;
			value = (Integer) other.value;
		}
		public String getText() {
			return text;
		}
		public Integer getValue() {
			return value;
		}
		@Override
		public boolean equals(Object o) {
			if (o == this)
				return true;
			if (!(o instanceof Pair))
				return false;
			
			return this.text.equals(((Pair)o).text);
		}
		private String text;
		private Integer value;
	}
	
	public void StartSolvingProcess(int numberOfAffectedAGVs, Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		try { 
			alreadySentPathInCurrentRound.clear();
			this.numberOfAffectedAGVs = numberOfAffectedAGVs;
			allPathsMap.clear();
			solution.clear();
		} catch(Exception e) {
			Logger.Send("Exception in CentralizedProblemSolver.StartSolvingProcess:\n" +
					e.getMessage() + "\n");
		}
		finally {
			this.rl_messages.unlock();
		}
	}
	
	public void UpdatePaths(Global agv)
	{
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		try {
			String id = agv.getAgvName();
			List<List<Link>> paths = agv.getAllPaths();
			
			//if the id already exist, it will replace with new paths, if it doesn't, it will insert it
			if(allPathsMap.containsKey(id))
			{
				allPathsMap.replace(id, paths);
			}
			else
			{
				allPathsMap.put(id, paths);
			}
			alreadySentPathInCurrentRound.add(id);
			Logger.Send("=========> Solver gets " + paths.size() + " new path from: " + agv.getAgvName()+"\n");
			solveProblemIfEnoughPathReceived(agv);
		}
		catch(Exception e) {
			Logger.Send("Exception in CentralizedProblemSolver.UpdatePaths: by agvid="+agv.getAgvName()+"\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_messages.unlock();
		}
	}
	
	public Response getResponse(Global agv, List<Link> resultPathForagv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		Response result = Response.WAIT;
		try { 
			if (!solution.isEmpty())
			{
				result = Response.SUCCESS;
			}
			
			if (!alreadySentPathInCurrentRound.contains(agv.getAgvName()))
			{
				result = Response.UPDATE_NEEDED;
			}
			
		} catch(Exception e) {
			Logger.Send("Exception ("+agv.getAgvName()+") in CentralizedProblemSolver.getResponse:\n" +
					e.getMessage() + "\n");
		}
		finally {
			this.rl_messages.unlock();
			return result;
		}
	}
	
	public List<Link> getNewPath(Global agv) {
		while (!rl_messages.tryLock())
			agv.WaitSecond();
		List<Link> result = new ArrayList<Link>();
		try { 
			for (int i=0; i<this.solution.get(agv.getAgvName()).size();++i)
				result.add(this.solution.get(agv.getAgvName()).get(i));
			String lgmsg= "IN getNewPath ("+agv.getAgvName()+") result: \n"+
		               ((SimpleGlobal)agv).WriteOut(result) + "\n by the way the solution:\n";
			for (String AGVId: this.solution.keySet())
			{
				lgmsg+=AGVId+": "+((SimpleGlobal)agv).WriteOut(this.solution.get(AGVId));
				lgmsg+="\n";
			}
			Logger.Send(lgmsg);
		} catch(Exception e) {
			Logger.Send("Exception in CentralizedProblemSolver.getResponse:\n" +
					e.getMessage() + "\n");
		}
		finally {
			this.rl_messages.unlock();
			return result;
		}
	}
	
	private void solveProblemIfEnoughPathReceived(Global agvJustForWaitCommand) {	
		if (this.alreadySentPathInCurrentRound.size() == this.numberOfAffectedAGVs) {
			String logpathsmsg = "The paths from cars before using the offline solver:\n";
			for (String AGVId: this.allPathsMap.keySet())
			{
				logpathsmsg+=AGVId+": [\n";
				for (int pathi=0; pathi<allPathsMap.get(AGVId).size();++pathi) {
					logpathsmsg+=((SimpleGlobal)agvJustForWaitCommand).WriteOut(allPathsMap.get(AGVId).get(pathi))+"\n";
				}
				logpathsmsg+="]\n";
			}
			Logger.Send(logpathsmsg);
			String logmsg = "";
			try {
				//solution.clear();
				//solution = this.GetDisjointPathsBacktrack(agvJustForWaitCommand); // TODO remove agv parameter
				solution = this.GetDisjointPathsCplex();
				sendSolutionToScheduler(agvJustForWaitCommand);
				logmsg += "Offline solver found solution\n";
				for (String AGVId: this.solution.keySet())
				{
					logmsg+=AGVId+": "+((SimpleGlobal)agvJustForWaitCommand).WriteOut(this.solution.get(AGVId));
					logmsg+="\n";
				}
				this.conversionBetweenTheIndices.clear();
			}
			catch (NoSolutionException e) {
				this.alreadySentPathInCurrentRound.clear();
				this.conversionBetweenTheIndices.clear();
				logmsg += "Offline solver did NOT find solution\n";
			}
			Logger.Send(logmsg);
		}
	}
	
	private void sendSolutionToScheduler(Global agvJustForWaitCommand) {
		Scheduler.getInstance().UpdatePaths(solution, agvJustForWaitCommand);
	}
	
	
	//returns the selected one path for each agv if possible by using the cplex solver, otherwise it will return an empty map
	private Map<String, List<Link>> GetDisjointPathsCplex() throws NoSolutionException
	{
		Map<String, List<Link>> result = new HashMap<String, List<Link>>();	
		int numberOfCars = allPathsMap.size();

		getConversionBetweenTheIndices();
		
		double[][] A = getIncidenceMatrix();
		double[] c = getWeightFunction();
		
		double[] x = CplexIndependentSet.run(A, c, numberOfCars);
		
		if(Logger.getDebugLevel() >= 2)
		{
			String logmsg = "x = ";
			for(int i = 0; i < x.length; ++i)
			{
				logmsg += " " + x[i];
			}
			logmsg += "\n";
			Logger.Send(logmsg);
		}
		
		List<String> carIdOrder = getCarOrders();
		List<Integer> whichPathUsedByCars = getPathsFromIndependentSetResult(x, carIdOrder);
		
		if(whichPathUsedByCars.contains(-1))
			throw new NoSolutionException("No solution was found for the offline problem!");
		
		List<List<Link>> resultPaths = extractAlreadySelectedPaths(numberOfCars, carIdOrder, whichPathUsedByCars);
		result = generateMapForResult(resultPaths, carIdOrder);
		return result;
	}

	private int getNumberOfAllPaths(List<Integer> pathSizesByCar) {
		int n = 0;
		
		for (Map.Entry<String,List<List<Link>>> car : allPathsMap.entrySet()) {
			int size = car.getValue().size();
			pathSizesByCar.add(size);
			n += size;    
		}
		return n;
	}
	
	private void getConversionBetweenTheIndices() {
		List<Integer> pathSizes = new ArrayList<Integer>();
		int n = getNumberOfAllPaths(pathSizes);
		
		for (Map.Entry<String,List<List<Link>>> car : allPathsMap.entrySet()) {
			String currentCarId = car.getKey();
			List<List<Link>> currentCarPaths = car.getValue();
			for(int i = 0; i < currentCarPaths.size(); ++i)
			{
				conversionBetweenTheIndices.add(new Pair(currentCarId,i));
			}
		}
	}
	
	private int getIndexOf(List<Pair> conversionBetweenTheIndices, String carId, int idx) {
		for(int i = 0; i < conversionBetweenTheIndices.size(); ++i)
		{
			if(conversionBetweenTheIndices.get(i).getText() == carId && conversionBetweenTheIndices.get(i).getValue() == idx)
			{
				return i;
			}
		}
		return -1;
	}
	
	private double[] getWeightFunction() {;
		List<Integer> pathSizes = new ArrayList<Integer>();
		int n = getNumberOfAllPaths(pathSizes);
		
		double[] c = new double[n];
		
		String carId = "";
		int idx = 0;
		
		for(int i = 0; i < n; ++i)
		{
			carId = conversionBetweenTheIndices.get(i).getText();
			idx = conversionBetweenTheIndices.get(i).getValue();
			List<Link> currentPath = allPathsMap.get(carId).get(idx);
			c[i] = currentPath.size()*2;
			
			if(currentPath.get(0) instanceof WaitLink)
			{
				c[i] += 3;
			}
		}
		return c;
	}
	
	private double[][] getIncidenceMatrix() {
		
		// first we get the number of all paths together
		List<Integer> pathSizes = new ArrayList<Integer>();
		int n = getNumberOfAllPaths(pathSizes);
		
		List<List<Integer>> edges = getEdges();
		
		int m = edges.size();
		
		double[][] A = new double[m][n];
		
		for(int j = 0; j < m; ++j)
		{
			List<Integer> currentEdges = edges.get(j);
			for(int i = 0; i < n; ++i)
			{
				A[j][i] = currentEdges.get(i);
			}
		}
		
		return A;
	}

	private List<List<Integer>> getEdges() {
		
		List<List<Integer>> edges = new ArrayList<List<Integer>>();
		
		List<Integer> pathSizesByCar = new ArrayList<Integer>();
		int n = getNumberOfAllPaths(pathSizesByCar);
		
		if(Logger.getDebugLevel() >= 2)
			Logger.Send("conversionBetweenTheIndices: \n");
		
		for(int i = 0; i < n; ++i)
		{
			String carId = conversionBetweenTheIndices.get(i).getText();
			int idx = conversionBetweenTheIndices.get(i).getValue();
			if(Logger.getDebugLevel() >= 2)
				Logger.Send("i = " + i + " carId = " + carId + " idx = " + idx + "\n");
		}
		
		if(Logger.getDebugLevel() >= 2)
			Logger.Send("Adding the edges to the incidence matrix: \n");
		
		for (Map.Entry<String,List<List<Link>>> car1 : allPathsMap.entrySet()) {
			String carId1 = car1.getKey();
			List<List<Link>> carPaths1 = car1.getValue();
			
			for(int idx1 = 0; idx1 < carPaths1.size(); ++idx1)
			{
				List<Link> carPath1 = carPaths1.get(idx1);
				for(Map.Entry<String,List<List<Link>>> car2 : allPathsMap.entrySet())
				{
					String carId2 = car2.getKey();
					List<List<Link>> carPaths2 = car2.getValue();
					for(int idx2 = 0; idx2 < carPaths2.size(); ++idx2)
					{
						List<Link> carPath2 = carPaths2.get(idx2);

						//check if the two paths are conflicting, if yes, add an edge corresponding to this conflict
						if(!(carId1 == carId2 && idx1 == idx2) && !disjointOperator.decide(carPath1, carPath2))
						{
							int i1 = getIndexOf(conversionBetweenTheIndices, carId1, idx1);
							int i2 = getIndexOf(conversionBetweenTheIndices, carId2, idx2);
							
							if(Logger.getDebugLevel() >= 3)
								Logger.Send("Conflict between the paths at places " + i1 + "(" + carId1 + ", " + idx1 + ") and " + i2 + "(" + carId2 + ", " + idx2 + ") \n");
							
							
							List<Integer> newEdge = new ArrayList<Integer>(Collections.nCopies(n, 0));
							newEdge.set(i1, 1);
							newEdge.set(i2, 1);
							
							if(!edges.contains(newEdge))
							{
								edges.add(newEdge);
								if(Logger.getDebugLevel() >= 2)
								{
									String logmsg = "added edge : ";
									for(int i = 0; i < newEdge.size(); ++i)
									{
										logmsg += newEdge.get(i) + " ";
									}
									logmsg += "\n";
									Logger.Send(logmsg);
								}
							}
						}					
					}	
				}
			}
		}
		return edges;
	}
	
	//gets idx-s for each car from the result of the independent set problem
	private List<Integer> getPathsFromIndependentSetResult(double[] x, List<String> carIds) {
		
		List<Integer> whichPathUsedByCars = initInvalidPathIndices(carIds.size());
		
		int idx = 0;
		String carId = "";
		
		for(int i = 0; i < x.length; ++i)
		{
			if(x[i] == 1)
			{
				carId = conversionBetweenTheIndices.get(i).getText();
				idx = conversionBetweenTheIndices.get(i).getValue();
				
				for(int j = 0; j < carIds.size(); ++j)
				{
					if(carId == carIds.get(j))
					{
						whichPathUsedByCars.set(j, idx);
					}
				}
			}
		}
		
		return whichPathUsedByCars;
	}

	//returns the selected one path for each agv if possible, otherwise it will return an empty map
	private Map<String, List<Link>> GetDisjointPathsBacktrack(Global agv) throws NoSolutionException
	{
		Map<String, List<Link>> result = new HashMap<String, List<Link>>();
		
		int numberOfCars = allPathsMap.size();
		List<String> carIdOrder = getCarOrders();
		List<Integer> whichPathUsedByCars = initInvalidPathIndices(carIdOrder.size());
		String lgmsg = "";
		lgmsg+="order: ";
		for (int ii=0; ii < carIdOrder.size(); ++ii) {
			lgmsg+=carIdOrder.get(ii)+", ";
		}
		lgmsg+="\n";
		Logger.Send(lgmsg); lgmsg="";
		int currentCarIndex = 0;
		boolean arePathsConflicting = false;
			
		do {
			do {
				Logger.Send("whichPathUsedByCars = nextPathForCurrentCar(whichPathUsedByCars, currentCarIndex);\n");
				whichPathUsedByCars = nextPathForCurrentCar(whichPathUsedByCars, currentCarIndex);
				Logger.Send("List<List<Link>> alreadySelectedPaths = extractAlreadySelectedPaths(currentCarIndex, carIdOrder, whichPathUsedByCars);\n");
				List<List<Link>> alreadySelectedPaths = extractAlreadySelectedPaths(currentCarIndex, carIdOrder, whichPathUsedByCars);
				Logger.Send("List<Link> actualPath = getActualPath(currentCarIndex, carIdOrder, whichPathUsedByCars);\n");
				List<Link> actualPath = getActualPath(currentCarIndex, carIdOrder, whichPathUsedByCars);
				Logger.Send("arePathsConflicting = !isPathDisjoint(actualPath, alreadySelectedPaths);\n");
				arePathsConflicting = !isPathDisjoint(actualPath, alreadySelectedPaths);
				//////
				lgmsg+="which path: ";
				for (int ii=0; ii < whichPathUsedByCars.size(); ++ii) {
					lgmsg+=whichPathUsedByCars.get(ii)+", ";
				}
				lgmsg+="\n";
				lgmsg+=arePathsConflicting?"conflict":"NO conflict";
				lgmsg+="\n................";
				lgmsg+="actualPath: "+((SimpleGlobal)agv).WriteOut(actualPath)+"\n";
				for (int aspi=0;aspi<alreadySelectedPaths.size();++aspi) {
					lgmsg+="already: "+((SimpleGlobal)agv).WriteOut(alreadySelectedPaths.get(aspi))+"\n";
				}
				Logger.Send(lgmsg); lgmsg="";
			} while(arePathsConflicting && 
					whichPathUsedByCars.get(currentCarIndex) < allPathsMap.get(carIdOrder.get(currentCarIndex)).size() - 1);		
			if(arePathsConflicting) {
				do {
				whichPathUsedByCars.set(currentCarIndex, -1);
				--currentCarIndex;
				} while (currentCarIndex > -1 &&
						whichPathUsedByCars.get(currentCarIndex) == allPathsMap.get(carIdOrder.get(currentCarIndex)).size() - 1);
			}
			else
				++currentCarIndex;
			Logger.Send("currentCarIndex="+currentCarIndex+" numberOfCars="+numberOfCars+"\n");
		} while (currentCarIndex < numberOfCars && currentCarIndex > -1);
		
		if(currentCarIndex <= -1)
			throw new NoSolutionException("No solution was found for the offline problem!");
		
		List<List<Link>> resultPaths = extractAlreadySelectedPaths(currentCarIndex, carIdOrder, whichPathUsedByCars);
		result = generateMapForResult(resultPaths, carIdOrder);
		return result;
	}
	
	private List<String> getCarOrders()
	{
		List<String> result = new ArrayList<String>();
		for (Map.Entry<String,List<List<Link>>> car : allPathsMap.entrySet()) { 
            String carId = (String)car.getKey();
            result.add(carId);
		}
		return result;
	}
	
	private List<Integer> initInvalidPathIndices(int listSize) 
	{
		Integer[] integers = new Integer[listSize];
		Arrays.fill(integers, -1);
		List<Integer> integerList = Arrays.asList(integers);
		return integerList;
	}
	
	private List<Integer> nextPathForCurrentCar(List<Integer> whichPathUsedByCars, int currentIndexInCarOrder)
	{
		int newPathIndex = whichPathUsedByCars.get(currentIndexInCarOrder) + 1;
		whichPathUsedByCars.set(currentIndexInCarOrder, newPathIndex);
		return whichPathUsedByCars;
	}
	
	//it checks if a path is disjoint from other paths
	private boolean isPathDisjoint(List<Link> newPath, List<List<Link>> paths)
	{
		int howManyLinksAreWaitLink = 0;
		for(List<Link> path : paths)
		{
			if(!disjointOperator.decide(newPath, path))
				return false;
			if(path.get(0) instanceof WaitLink)
				++howManyLinksAreWaitLink;
		}
		if(newPath.get(0) instanceof WaitLink && howManyLinksAreWaitLink == paths.size())
			return false;
		return true;
	}
	
	private List<List<Link>> extractAlreadySelectedPaths(int currentCarIndex, List<String> carIds, List<Integer> carPathIndices)
	{
		List<List<Link>> result = new ArrayList<List<Link>>();
		for(int i = 0; i < currentCarIndex; ++i)
		{
			result.add(allPathsMap.get(carIds.get(i)).get(carPathIndices.get(i)));
		}
		return result;
	}
	private List<Link> getActualPath(int currentCarIndex, List<String> carIds, List<Integer> carPathIndices)
	{
		List<Link> result = allPathsMap.get(carIds.get(currentCarIndex)).get(carPathIndices.get(currentCarIndex));
		return result;
	}
	private Map<String, List<Link>> generateMapForResult (List<List<Link>> paths, List<String> ids)
	{
		Map<String, List<Link>> result = new HashMap<String, List<Link>>();
		
		for(int i = 0; i < ids.size(); ++i)
		{
			result.put(ids.get(i), paths.get(i));
		}
		
		return result;
	}
}
