package agentwise.agv.elteMod;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.agv.map.Link;
import agentwise.agv.model.agv.map.Node;
import agentwise.agv.model.environment.Order;
import agentwise.agv.controller.OrderManager;
import agentwise.agv.elteMod.Scheduler.Response;
import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.agv.map.*;

public abstract class SimpleGlobal extends Global {
	
	protected int pathLengthCounter;
	protected int pathLengthExpected;
	protected int randomPathCounter;
	protected int timeSinceLastPickupOrDelivery;
	
	/**
	 * calculates the shortest path from the current position of the avg to the destination using BFS
	 * @param destinationID the ID of the destination
	 * @param RestrictedLinks is the list of the links that can not be used
	 * @return the shortest path described as a list of consecutive links
	 */
	public static List<Link> calculatePath(Agv agv,String currentStationID, String destinationID, List<Link> RestrictedLinks) 
	{
		// initialize variables
		Queue<Node> nodeQ = new LinkedBlockingQueue<Node>();
		Queue<List<Link>> pathQ = new LinkedBlockingQueue<List<Link>>();
		
		// find the Node you are currently staying at within the InformationMap
		Collection<Node> nodes = agv.getInformationMap().getNodes();
		Node currentNodeInMap = nodes.stream()
				.filter(node -> node.getID().equals(currentStationID))
				.findFirst()
				.get();
		
		nodeQ.add(currentNodeInMap);
		pathQ.add(new ArrayList<Link>());

		List<String> visitedNodeIDs = new ArrayList<String>();

		while (!nodeQ.isEmpty()) {
		
			Node currentNode = nodeQ.poll();
			List<Link> currentPath = pathQ.poll();
			Collection<Link> outLinks = currentNode.getOutLinks();
			

			for (Link link: outLinks) {
				if(!RestrictedLinks.contains(link)) 
				{
					if (!visitedNodeIDs.contains(link.getEndNode().getID())) {

						if (link.getEndNode().getID().equals(destinationID)) {
							currentPath.add(link);
							
							//System.out.println(String.format("path calculated: %s -> %s", currentStationID, destinationID));
							for (Link l: currentPath) {
								//System.out.print(String.format("%s -> %s; ", l.getStartNode().getID(), l.getEndNode().getID())); 							
							}
							
							return currentPath;
						}

						visitedNodeIDs.add(link.getEndNode().getID());
						//visitedNodeIDs.add(currentNode.getID());
						
						nodeQ.add(link.getEndNode());
						List<Link> newPath = new ArrayList<Link>(currentPath);
						newPath.add(link);
						pathQ.add(newPath);
					}
				}
			}
		}

		// in this case we traversed the whole graph without finding an augmenting path
		// this should NOT happen!
		return new ArrayList<Link>();
	}
	
	public List<Link> CalculatePath(String destinationID, List<Link> RestrictedLinks) {
		List<Link> resultPath = new ArrayList<Link>();
		try {
			var currentNode = findCurrentNode();
			if (currentNode.getID().equals(destinationID))
			{
				resultPath.add(new WaitLink(currentNode));
			}
			else
			{
				resultPath = dijkstra(findCurrentNode(), destinationID);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Logger.Send(getAgvName() + ": Something went wrong with Dijkstra");
			e.printStackTrace();
		}
				/*calculatePath(
				this.getAgv(),
				this.getAgv().getPerceptionModule().senseCurrentMapComponentId(),
				destinationID,
				RestrictedLinks
				);*/
		if (resultPath.isEmpty()) {
			// it should not happpen
			targetID = "--";
		}
		else 
			targetID = destinationID;
		
		return resultPath;
	}
	protected boolean manageFirstStep(boolean beginning) {
		String logmsg = "";
		String logMsgPathLength = "";
		boolean isFirstStep = true;
		if (!getPerceptionModule().senseIsOnSegment()) {
			boolean success = false;
			try {
				success = Logger.SelectRandomFreeOrder(this);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Logger.Send("Dijkstra had some issues.");
				e.printStackTrace();
			}
			if (success) {
				Order targetOrder = Logger.getOrderById(this, targetOrderID);
				try {
					path = CalculatePath(targetOrder.getStartStationId(), new ArrayList<Link>());
							//dijkstra(findCurrentNode(), targetOrder.getStartStationId());
					
					//add a first waitlink in order to avoid collisions in the beginning of the algorithm
					if(beginning)
					{
						path.add(0, new WaitLink(findCurrentNode()));
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//calculate path length and set the counter, expected path length
				int displayPathSize = path.size();
				if (path.size() == 1 && path.get(0) instanceof WaitLink) displayPathSize = 0;
				if (Logger.getDebugLevel() >= 1) 
				{					
					logmsg += getAgvName() + ": Found an order in the first step, expected path length: " + displayPathSize + "\n";
					logMsgPathLength += "Number of random steps: "+ randomPathCounter + "\n";
	                Logger.Send(logMsgPathLength);
	                logMsgPathLength = "";
	                randomPathCounter = 0;
				}
				pathLengthCounter = 0;
				pathLengthExpected = displayPathSize;
				isFirstStep = false;
			}
			else if (path.isEmpty()) {
				//if we are on a location, we have to step out, except in the first step
				if(findCurrentNode().getID().startsWith("location") && Scheduler.getInstance().getRound() > 1)
				{
					path = calculateRndPath();
				}
				else
				{
					path.add(new WaitLink(findCurrentNode()));
				}
			}
			if (Logger.getDebugLevel() >= 1) {
				logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
				logmsg += getAgv().getEnvironment().getName() + ": step - path in the manageFirstStep (target searching)\n";
				if (isFirstStep)
					logmsg += getAgv().getEnvironment().getName() + 
							": The path is random, it have not found free order.\n";
				else
					logmsg += getAgv().getEnvironment().getName() +
							": targetID of path = " + targetID + "\n";
				logmsg += WriteOut(path) + "\n";
				logmsg += "---------------------------------------------------------------------------------------------------------------------------\n";
				Logger.Send(logmsg);
			}
		}
		return isFirstStep;
	}
	
	public String WriteOut(List<Link> path) {
		String logOutput = "";
		if (verbose > 0) {
			logOutput += this.getAgv().getEnvironment().getName() + ": path: ";
			if (verbose == 1) {
				if(path.isEmpty())
					logOutput += "empty";
				else if(path.size() == 1)
					logOutput += path.get(0).getStartNode().getID() + "->" + path.get(0).getID();
				else
					logOutput += path.get(0).getStartNode().getID() + "->" + path.get(0).getID() + "->" + path.get(1).getID();
			}
			else {
				if (path.isEmpty())
					logOutput += "empty";
				else {
					logOutput += path.get(0).getStartNode().getID();
					for (Link link: path) {
						logOutput += "->" + link.getID() + "->" + link.getEndNode().getID();
					}
				}
			}			
		}
		return logOutput;
	}
	
	/**
	 * Calculates a list containing one link, which is randomly chosen from the links originating at the agv's current position.
	 */
	protected List<Link> calculateRndPath() {
		Node currentNodeInMap = findCurrentNode();
		
		// choose an outgoing Link from this Node at random
		Collection<Link> outLinks = currentNodeInMap.getOutLinks();
		
		outLinks = outLinks.stream()
				.filter(l -> !(l instanceof WaitLink))
				.collect(Collectors.toCollection(ArrayList::new));
		Link randomOutLink = randomFromCollection(outLinks);
		
		List<Link> path =  new ArrayList<Link>();
		path.add(randomOutLink);
		return path;
	}
	
	protected List<Link> extendRndPath(Link nextLink) {
		Collection<Link> oldOutLinks = nextLink.getEndNode().getOutLinks();
		List<Link> outLinks = oldOutLinks.stream()
				.filter(l -> !(l instanceof WaitLink))
				.collect(Collectors.toCollection(ArrayList::new));
		if (outLinks.size() > 1)
			outLinks.removeIf(l -> l.getEndNode().getID().equals(nextLink.getStartNode().getID()));
		Link rndOutLink = randomFromCollection(outLinks);
		
		List<Link> path = new ArrayList<Link>();
		path.add(nextLink);
		path.add(rndOutLink);
		return path;
		
	}
	
	protected List<Order> getPossibleOrdersFromIt(Collection<Order> orders){
		List<Order> possibleOrders = orders.stream()
				  .filter(order -> order.getProgress() == false)
				  .collect(Collectors.toList());
		return possibleOrders;
	}
	protected List<Link> calculatePathToOrder(Collection<Order> possibleOrders) {
		List<Link> path = new ArrayList<Link>();
				
		if (!possibleOrders.isEmpty()) 
		{	
			//pick a free order random
			List<Link> emptyList = new ArrayList<Link>();
			Order rndOrder = possibleOrders.stream()
					.skip((int) (possibleOrders.size() * Math.random()))
					.findFirst()
					.get();
			path = CalculatePath(rndOrder.getStartStationId(), emptyList);
			rndOrder.setProgress(true);
			targetOrderID = rndOrder.getId();
			if (Logger.getDebugLevel() >= 1) 
				Logger.Send(this.getAgv().getEnvironment().getName() + 
					": planning route to the closest location:" + 
					path.get(path.size()-1).getEndNode().getID());
		} 
		else 
		{ 
			if (Logger.getDebugLevel() >= 1)
				Logger.Send(this.getAgv().getEnvironment().getName() + ": no order was found!!!\n");
			//TODO do we need this here?
			path = calculateRndPath();
			path = extendRndPath(path.get(0));
		}
		return path;
	}
	
	protected void sync(int i) throws Exception {
		String logmsg = "";
		if (Logger.getDebugLevel() >= 2) {
			logmsg += String.format("%s: synchro started, original next step: %s -> %s\n", getAgv().getEnvironment().getName(),
					path.get(0).getStartNode().getID(), path.get(0).getEndNode().getID() + " IN SYNC\n");
			Logger.Send(logmsg);
			logmsg = "";
		}
		//Step 0: calculate your rank and broadcast your rankMsg
		// prioritize AGVs delivering orders
		double rank = Math.random();
		Node currentNode = findCurrentNode();	

		rank = rank + CalculateRank(targetOrderID, currentNode, "B");

		Scheduler.getInstance().SendIn(this, rank);
		if (Logger.getDebugLevel() >= 2)
			Logger.Send(getAgvName() + " sent the rank. IN SYNC\n");
		while (Scheduler.getInstance().getRankCount(this) < NMB_AGVs) {
			if (Logger.getDebugLevel() >= 3)
				Logger.Send(getAgvName() + ": in while IN SYNC RankCount = " + Scheduler.getInstance().getRankCount(this) +"\n");
			WaitSecond();
		}
		
		int position = Scheduler.getInstance().getPosition(this);
		if (Logger.getDebugLevel() >= 3)
			Logger.Send(getAgvName() + " got the position: " + position + " IN SYNC\n");
		
		while (Scheduler.getInstance().getPathCount(this) < position) {
			if (Logger.getDebugLevel() >= 3)
				Logger.Send(getAgvName() + ": in while IN SYNC getPathCount = " + Scheduler.getInstance().getPathCount(this) +"\n");
			WaitSecond();
		}
		
		Boolean currentPosFree = true;
		Boolean nextPosFree = true;
		Boolean nextLinkFree = true;
		List<List<Link>> blockedLinksInDepth = new ArrayList<List<Link>>();
		for(int j = 1; j <= i; ++j) 
		{
			List<Link> links = Scheduler.getInstance().getForbiddenLinks(this, j);
			blockedLinksInDepth.add(links);
		}
		
		logmsg = "";
		List<List<Link>> restrictedLinks = blockedLinksInDepth;
		if (Logger.getDebugLevel() >= 1)
		{
			logmsg += getAgvName() + ": Restricted Links:\n";
			for (int k = 0; k < restrictedLinks.size(); ++k)
			{
				logmsg += "level="+k+": ";
				for (int j = 0; j < restrictedLinks.get(k).size(); ++j)
				{
					Link logLink = restrictedLinks.get(k).get(j);
					logmsg += logLink.getStartNode().getID() + "--" + logLink.getID() + "->" + logLink.getEndNode().getID() 
							+ ", ";
				}
				logmsg += "\n";
			}
			
		}
		if (Logger.getDebugLevel() >= 1) {
			Logger.Send(logmsg);
			logmsg = "";
		}
		
		//check if a conflict can occur in the future
		for(int j = 0; j < i && j < path.size(); ++j)
		{
			for(Link blockedLink: blockedLinksInDepth.get(j))
			{
				if(j == 0 && blockedLink.getEndNode().getID().equals(path.get(0).getStartNode().getID()))
				{
					currentPosFree = false;	
				}
				
				if (!(blockedLink instanceof WaitLink) && blockedLink.getID().equals(path.get(j).getID()) ||
					 (blockedLink instanceof WaitLink && blockedLink.getStartNode().getID().equals(path.get(j).getStartNode().getID())))
				{
					nextLinkFree = false;
				}
				if (blockedLink.getEndNode().getID().equals(path.get(j).getEndNode().getID()))
				{
					nextPosFree = false;
				}										
			}
		}
		
		if (!nextPosFree || !nextLinkFree) {
			if (Logger.getDebugLevel() >= 1)
				logmsg += getAgvName() + ": detected a possible conflict, needs to recalculate path, " + 
						"currentPosFree = " + currentPosFree + ", nextPosFree = " + nextPosFree +
						", nextLinkFree = " + nextLinkFree + " \n";
			try {
				List<Link> altPath = new ArrayList<Link>();
				//if it is in completely random mode, then just pick a random link which is not blocked for the next step
				if (mode == RANDOM_WALK && targetOrderID == -1) {
					altPath = pickRndPathWithTaboos(blockedLinksInDepth.get(0));//TODO fix AssertionError, when there is no free outlink				
				}
				// if we are delivering order, or our agv has a target order, we have to recalculate the path with the restricted links
				if (mode == DELIVER_ORDER || (mode == RANDOM_WALK && targetOrderID != -1)) {
					String targetID;
					if (mode == DELIVER_ORDER)
						targetID = getPerceptionModule().senseOrderHolding().getTo();
					else
						targetID = Logger.getOrderById(this, targetOrderID).getStartStationId();
					//List<Link> altPath = calculatePathWithTaboos(targetID, blockedLinks);
					altPath = calculatePathWithRestrictions(targetID, blockedLinksInDepth);
				}
				
				if(altPath.size() == 0)
				{
					Logger.Send("FATAL detected by " + getAgvName() + " with position= "
						    +position+": all paths and current position blocked \n" + logmsg);
					Scheduler.getInstance().setFatalStatus(this, true);
					CentralizedProblemSolver.getInstance(i).StartSolvingProcess(position+1, this);//position 0..n-1
					CommunicateOffline(i);
				}
				else {
					path = altPath;
				}
			} catch (SurroundedException e) {
				if (Logger.getDebugLevel() >= 1)
					logmsg += getAgv().getEnvironment().getName() + ": Surrounded Exception IN SYNC\n";
				if (currentPosFree)
					path.add(0, new WaitLink(path.get(0).getStartNode()));
				else {
					if (Logger.getDebugLevel() >= 0)
						Logger.Send("FATAL (Surrounded Exception) detected by " + getAgvName() + " with position= "
					    +position+": all paths and current position blocked \n" + logmsg);
					Scheduler.getInstance().setFatalStatus(null, true);
					CentralizedProblemSolver.getInstance(i).StartSolvingProcess(position+1, this);//position 0..n-1
					CommunicateOffline(i);
					//throw new Exception("FATAL by " + getAgvName() + ": all paths AND current position blocked IN SYNC");				
				}
			}
		}
		
		Scheduler.getInstance().SendIn(this, path);
		if (Logger.getDebugLevel() >= 1 && !logmsg.isEmpty()) {
			Logger.Send(logmsg);
			logmsg = "";
		}
		
		//check if a livelock has occured, and use the centralized solver if needed
		if(Scheduler.getInstance().getLivelockStatus(this))
		{
			CentralizedProblemSolver.getInstance(i).StartSolvingProcess(NMB_AGVs, this);
			CommunicateOffline(i);
			Scheduler.getInstance().setLivelockStatus(this, true);
			Scheduler.getInstance().resetSnapshots();
		}
		
		Scheduler.getInstance().SendIn(this, timeSinceLastPickupOrDelivery);
		if (Logger.getDebugLevel() >= 1 && !logmsg.isEmpty()) {
			Logger.Send(logmsg);
			logmsg = "";
		}
		
		Scheduler.getInstance().SendOk(this);
		
		//then waits for the other AGV-s
		String logmsg1;
		Response r;
		do {
			do {
				
				r = Scheduler.getInstance().getResponse(this);
				logmsg1 = "wait " + getAgvName() + " resp=" + r + "\n";
				Logger.Send(logmsg1);
				logmsg1 = "";
				this.WaitSecond();
			} while (r == Response.WAIT);
				
			if(r == Response.FATAL || r == Response.LIVELOCK)
			{
				CommunicateOffline(i);
			}
			Logger.Send(this.getAgvName()+": not success yet\nresponse: "+r.toString()+"\n");
		} while (r != Response.SUCCESS);
		
		if (Logger.getDebugLevel() >= 2)
			Logger.Send(getAgvName() + " sent the path. IN SYNC\n" + WriteOut(path) + "\n");
		Scheduler.getInstance().RequestReset(this);
		if (Logger.getDebugLevel() >= 2)
			Logger.Send(getAgvName() + " sent the reset request. IN SYNC\n");
		// While the reset is not happening
		while (Scheduler.getInstance().getRankCount(this) > 0) {
			WaitSecond();
		}
	}
	
	/**
	 * finds the Node at which the AGV is currently staying (within the InformationMap)
	 */
	protected Node findCurrentNode() {
		String currentStationID = getPerceptionModule().senseCurrentMapComponentId();
		Collection<Node> nodes = getAgv().getInformationMap().getNodes();
		return nodes.stream()
				.filter(node -> node.getID().equals(currentStationID))
				.findFirst()
				.get();
	}
	
	protected <T> T randomFromCollection(Collection<T> coll) {
	    int num = (int) (Math.random() * coll.size());
	    for(T t: coll) if (--num < 0) return t;
	    throw new AssertionError();
	}
	
	protected List<Link> pickRndPathWithTaboos(List<Link> taboos) throws Exception {
		Node currentNodeInMap = findCurrentNode();
		
		// choose an outgoing Link from this Node at random, which is not taboo
		Collection<Link> outLinks = filterTaboos(currentNodeInMap.getOutLinks(), taboos);
		outLinks = outLinks.stream()
				.filter(l -> !(l instanceof WaitLink))
				.collect(Collectors.toCollection(ArrayList::new));
		
		if(outLinks.size() == 0) {
			return new ArrayList<Link>(); 
		}
		
		Link randomOutLink = randomFromCollection(outLinks);
		List<Link> path = new ArrayList<Link>();
		path.add(randomOutLink);
		if (Logger.getDebugLevel() >= 4) 
			Logger.Send("the result of the pickRndPathWithTaboos for " + getAgvName() + ": " + WriteOut(path) + "\n");
		return path;
	}
	
	protected Collection<Link> filterTaboos(Collection<Link> oldOutLinks, List<Link> taboos) throws SurroundedException {
		List<Link> outLinks = new ArrayList<Link>(oldOutLinks);
		String logmsg = "";
		if (Logger.getDebugLevel() >= 2) {
			logmsg += "Filter these links: ";
			for (Link l: outLinks)
				logmsg += l.getID() + ", ";
			logmsg += "\n";
			logmsg += "with these taboos: ";
			for (Link t: taboos)
				logmsg += t.getID() + ", ";
			logmsg += "\n";
		}
		
		// filter all links, which end at a node, where a taboo link ends
		outLinks.removeIf(link -> {
			String id = link.getEndNode().getID();
			Boolean remove = false;
			for (Link tabooLink: taboos) {
				if (tabooLink.getEndNode().getID().equals(id))
					remove = true;
			}
			return remove;
		});
		
		// filter all links, which are contained in backward direction in taboos
		outLinks.removeIf(link -> {
			Boolean remove = false;
			for (Link tabooLink: taboos) {
				if (tabooLink.getID().equals(link.getID()))
					remove = true;
			}
			return remove;
		});
		
		if (outLinks.isEmpty()) 
			throw new SurroundedException(getAgv().getEnvironment().getName() + ": taboos restrict every possible way out");
		if (Logger.getDebugLevel() >= 2) {
			logmsg += "these links remain: ";
			for (Link l: outLinks)
				logmsg += l.getID() + ", ";
			logmsg += "\n";
			Logger.Send(logmsg);
		}
		
		return outLinks;
	}
	
	protected List<Link> calculatePathWithTaboos(String destinationID, List<Link> taboos) throws Exception {
		// initialize variables
		Queue<Node> nodeQ = new LinkedBlockingQueue<Node>();
		Queue<List<Link>> pathQ = new LinkedBlockingQueue<List<Link>>();
		List<String> visitedNodeIDs = new ArrayList<String>();
		Boolean firstNode = true;
		String logmsg = "";
		
		Node currentNodeInMap = findCurrentNode();
		logmsg += String.format("calcPwithTaboos from %s to %s\n", currentNodeInMap.getID(), destinationID);
		
		nodeQ.add(currentNodeInMap);
		pathQ.add(new ArrayList<Link>());

		while (!nodeQ.isEmpty()) {
		
			Node currentNode = nodeQ.poll();
			List<Link> currentPath = pathQ.poll();
			Collection<Link> outLinks = currentNode.getOutLinks();
	
			// we need to respect the taboos only in the first step, and then never again
			if (firstNode) {
				outLinks = filterTaboos(outLinks, taboos);
				firstNode = false;
			}
			if (Logger.getDebugLevel() >= 4) {
				logmsg += "current node: " + currentNode.getID() + "\n";
				logmsg += "filtered out links: ";
				for (Link l: outLinks)
					logmsg += l.getID() + ", ";
				logmsg += "\n";
			}

			for (Link link: outLinks) {
				if (!visitedNodeIDs.contains(link.getEndNode().getID())) {
					if (link.getEndNode().getID().equals(destinationID)) {
						currentPath.add(link);
						if (Logger.getDebugLevel() >= 4) {
							logmsg += "the result of the calculatePathWithTaboos for " + getAgvName() + ": " + WriteOut(currentPath) + "\n";
							Logger.Send(logmsg);
						}
						return currentPath;
					}
					visitedNodeIDs.add(link.getEndNode().getID());					
					nodeQ.add(link.getEndNode());
					List<Link> newPath = new ArrayList<Link>(currentPath);
					newPath.add(link);
					pathQ.add(newPath);
				}
			}
		}
        if (Logger.getDebugLevel() >= 4) 
        	Logger.Send(logmsg);
		// in this case we traversed the whole graph without finding an augmenting path
		throw new Exception(getAgv().getEnvironment().getName() + ": calculatePathWithTaboos failed to find a path");
	}
	
	//calculate shortest path from the agv to the destination with restricted links in each part of the path
	protected List<Link> calculatePathWithRestrictions(String destinationID, List<List<Link>> restrictedLinks) throws Exception
	{
		List<Link> result = new ArrayList<Link>();
		
		Node currentNodeInMap = findCurrentNode();
		int maxLevel = restrictedLinks.size();
		result = fullAlternatePathSearching(currentNodeInMap, restrictedLinks, maxLevel, destinationID);
	
		if (Logger.getDebugLevel() >= 1)
			Logger.Send(getAgvName() + ": IN calculatePath: alternate path: " + this.WriteOut(result) + "\n");
		
		return result;
	}
	
	// TODO: path saving, give back one node (node selecting implement) and path
	private List<Link> fullAlternatePathSearching(Node startNode, List<List<Link>> restrictedLinks, int maxLevel, String destinationID) throws Exception
	{
		// TODO: use String in restrictedLinks instead of converting 
		List<List<String>> restrictedLinkIds = new ArrayList<List<String>>();
		List<List<String>> restrictedLinkEndNodeIds = new ArrayList<List<String>>();
		for (int i = 0; i < restrictedLinks.size(); ++i)
		{
			restrictedLinkIds.add(new ArrayList<String>());
			restrictedLinkEndNodeIds.add(new ArrayList<String>());
			for (int j = 0; j < restrictedLinks.get(i).size(); ++j)
			{
				restrictedLinkIds.get(i).add(restrictedLinks.get(i).get(j).getID());
				restrictedLinkEndNodeIds.get(i).add(restrictedLinks.get(i).get(j).getEndNode().getID());
			}
		}
		List<Node> maxLevelNodes = new ArrayList<Node>();
		Queue<AbstractMap.SimpleEntry<Node, Integer>> queue = new LinkedList<AbstractMap.SimpleEntry<Node, Integer>>();
		Map<SimpleEntry<String, Integer>, Link> getFrom = new HashMap<SimpleEntry<String, Integer>, Link>();
		int level = 0;
		queue.add(new AbstractMap.SimpleEntry<Node, Integer>(startNode, level));
		while (!queue.isEmpty() && level < maxLevel)
		{
			AbstractMap.SimpleEntry<Node, Integer> currentElem = queue.remove();
			Node currentNode = currentElem.getKey();
			level = currentElem.getValue();
			if (level < maxLevel)
			{
				//TODO: waitLink management (now a little bit undefined behaviour)
				boolean isFoundWaitLink = false;
				for (Object l: currentNode.getOutLinks())
				{
					Link link = (Link)l;
					if (
						(!(link instanceof WaitLink) && 
							!(restrictedLinkIds.get(level).contains((link).getID()) || 
							  restrictedLinkEndNodeIds.get(level).contains(((link).getEndNode().getID())))) ||
						(!isFoundWaitLink && link instanceof WaitLink) && 
							!(restrictedLinkEndNodeIds.get(level).contains(((link).getEndNode().getID())))
					)
					
					{
						SimpleEntry<Node, Integer> newPair = new SimpleEntry<Node, Integer>(
								((Link)link).getEndNode(),
								level + 1);
						queue.add(newPair);
						getFrom.put(new SimpleEntry<String, Integer>(newPair.getKey().getID(), 
								                                     newPair.getValue()), (Link)link);
						if (link instanceof WaitLink) {
							isFoundWaitLink = true;
						}
					}
				}
				if (!isFoundWaitLink) {
					Link waitLink = new WaitLink(currentNode);
					if (!(restrictedLinkEndNodeIds.get(level).contains(((waitLink).getEndNode().getID()))))				
					{
						SimpleEntry<Node, Integer> newPair = new SimpleEntry<Node, Integer>(
								((Link)waitLink).getEndNode(),
								level + 1);
						queue.add(newPair);
						getFrom.put(new SimpleEntry<String, Integer>(newPair.getKey().getID(), 
								                                     newPair.getValue()), (Link)waitLink);
					}
				}
			}
			// It collects the all maxLevel nodes
			else 
			{
				maxLevelNodes.add(currentNode);
				while (!queue.isEmpty() && level == maxLevel)
				{
					currentElem = queue.remove();
					currentNode = currentElem.getKey();
					level = currentElem.getValue();
					if (level == maxLevel)
					{
						maxLevelNodes.add(currentNode);
					}
				}
			}
		}
		// TODO: node selector
		if(maxLevelNodes.isEmpty())
		{
			List<Link> emptyList = new ArrayList<Link>();
			return emptyList;
		}
		else
		{	
			Node finalNode = randomFromCollection(maxLevelNodes);
			// We should not get fatal here
			List<Link> minStaticPath = dijkstra(finalNode, destinationID);
			int minPathLength = minStaticPath.size();
			for (Node node : maxLevelNodes)
			{
				if (node.getID().equals(destinationID))
				{
					finalNode = node;
					minStaticPath = new ArrayList<Link>();
					break;
				}
				else 
				{
					List<Link> staticPath = dijkstra(node, destinationID);
					int staticPathLength = staticPath.size();
					if (staticPathLength < minPathLength)
					{
						finalNode = node;
						minPathLength = staticPathLength;
						minStaticPath = staticPath;
					}
				}
			}
			if (Logger.getDebugLevel() >= 2)
			{
				String logmsg = getAgvName() + ":\n";
				Iterator it = getFrom.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry)it.next();
					logmsg += "(" + ((SimpleEntry<String, Integer>)pair.getKey()).getKey() +", " + 
							((SimpleEntry<String, Integer>)pair.getKey()).getValue() + ") = " + 
							((Link)pair.getValue()).getStartNode().getID() + "->" + 
							((Link)pair.getValue()).getEndNode().getID() + 
							" hashcode=" + ((SimpleEntry<String, Integer>)pair.getKey()).hashCode() + "\n";
				}
				Logger.Send(logmsg);
			}
			List<Link> result = generatePathFromSearch(getFrom, new SimpleEntry<String, Integer>(finalNode.getID(), maxLevel));
			result.addAll(minStaticPath);
			return result;
		}
	}
	
	private List<Link> generatePathFromSearch(Map<SimpleEntry<String, Integer>, Link> getFrom, SimpleEntry<String, Integer> targetEntry)
	{
		List<Link> result = new ArrayList<Link>();
		int level = targetEntry.getValue();
		SimpleEntry<String, Integer> currentEntry = targetEntry;
		while (level > 0)
		{
			Link currentLink = getFrom.get(currentEntry);
			result.add(currentLink);
			level = currentEntry.getValue() - 1;
			currentEntry = new SimpleEntry<String, Integer>(currentLink.getStartNode().getID(), level);
		}
		Collections.reverse(result);
		return result;
	}
	
	protected List<Link> dijkstra(Node startNode, String targetNodeId) throws Exception
	{
		// A custom comparator that compares two Strings by their length.
        Comparator<SimpleEntry<Node, Integer>> distanceComparator = (e1, e2) -> {
        	return e1.getValue() - e2.getValue();
        };
        PriorityQueue<SimpleEntry<Node, Integer>> prioQueue = new PriorityQueue<SimpleEntry<Node, Integer>>(distanceComparator);
        Set<String> doneSet = new HashSet<String>();
        prioQueue.add(new SimpleEntry<Node, Integer>(startNode, 0));
        boolean reachedTheTarget = false;
        Map<SimpleEntry<String, Integer>, Link> getFrom = new HashMap<SimpleEntry<String, Integer>, Link>();
        SimpleEntry<Node, Integer> finalEntry = null;
        while (!reachedTheTarget && !prioQueue.isEmpty())
        {
        	SimpleEntry<Node, Integer> currentEntry = prioQueue.remove();
        	Node currentNode = currentEntry.getKey();
        	int currentDistance = currentEntry.getValue();
        	// because of the multiple inserting into the priority queue
        	if (currentNode.getID().equals(targetNodeId))
        	{
        		reachedTheTarget = true;
        		finalEntry = currentEntry;
        	}
        	else if (!doneSet.contains(currentNode.getID()))
        	{
        		for (Object link: currentNode.getOutLinks())
				{
        			Node endNode = ((Link)link).getEndNode();
	        		if (!doneSet.contains(endNode.getID()))
    				{
	        			prioQueue.add(new SimpleEntry<Node, Integer>(
	        					endNode,
	        					currentDistance + 1));
	        			getFrom.put(
	        					new SimpleEntry<String, Integer>(endNode.getID(), currentDistance + 1),
	        					(Link)link);
    				}
				}
        		doneSet.add(currentNode.getID());
        	}
        }
        if (finalEntry == null)
        	throw new Exception(getAgvName() + ": Fatal, there is no route from " + startNode.getID() + 
        			" to " + targetNodeId + "!");
        
        return generatePathFromSearch(getFrom, new SimpleEntry<String, Integer>(finalEntry.getKey().getID(), finalEntry.getValue()));
	}
	
	protected void initAllPaths(int step, String destinationId)
	{
		this.allPaths.clear();
		this.alreadyGeneratedPaths.clear();
		
		Node currentNode = findCurrentNode();
		Queue<List<Link>> pathQueue = new LinkedList<List<Link>>();
		
		for(Object l : currentNode.getOutLinks())
		{
			List<Link> listToAdd = new ArrayList<Link>();
			if(!((Link)l instanceof WaitLink))
			{
				listToAdd.add((Link)l);
				pathQueue.add(listToAdd);
				if(Logger.getDebugLevel() >= 3)
				{
					Logger.Send("Added:" + this.WriteOut(listToAdd) + " to the queue, size = " + listToAdd.size() + "\n");
				}
			}
		}
		List<Link> listToAdd = new ArrayList<Link>();
		listToAdd.add(new WaitLink(currentNode));
		pathQueue.add(listToAdd);
		if(Logger.getDebugLevel() >= 3)
		{
			Logger.Send("Added:" + this.WriteOut(listToAdd) + " to the queue, size = " + listToAdd.size() + "\n");
		}

		while(!pathQueue.isEmpty())
		{
			List<Link> currentList = pathQueue.poll();
			if(Logger.getDebugLevel() >= 3)
			{
				Logger.Send("Popped:" + this.WriteOut(currentList) + " from the queue, size = " + currentList.size() + "\n");
			}
			Node endNode = currentList.get(currentList.size()-1).getEndNode();
			if(currentList.size() == step)
			{
				try {
					currentList.addAll(dijkstra(endNode, destinationId));
				} catch (Exception e) {
					Logger.Send("Failed to find path in dijkstra!");
					e.printStackTrace();
				}
				alreadyGeneratedPaths.add(currentList);
			}
			else
			{
				for(Object l : endNode.getOutLinks())
				{
					List<Link> listToAddToQueue = new ArrayList<Link>(currentList);
					if(!((Link)l instanceof WaitLink))
					{
						listToAddToQueue.add((Link)l);
						pathQueue.add(listToAddToQueue);
						if(Logger.getDebugLevel() >= 3)
							{
								Logger.Send("Added:" + this.WriteOut(listToAddToQueue) + " to the queue, size = " + listToAddToQueue.size() + "\n");
							}
					}
				}
				List<Link> listToAddToQueue = new ArrayList<Link>(currentList);
				listToAddToQueue.add(new WaitLink(endNode));
				pathQueue.add(listToAddToQueue);
				if(Logger.getDebugLevel() >= 3)
				{
					Logger.Send("Added:" + this.WriteOut(listToAddToQueue) + " to the queue, size = " + listToAddToQueue.size() + "\n");
				}
			}
		}
		Collections.sort(alreadyGeneratedPaths, (l1,l2) -> Integer.compare(l1.size(),l2.size())*(-1));
		
		if(Logger.getDebugLevel() >= 2)
		{
			String logmsg = "ALL PATHS GENERATED , there are " + alreadyGeneratedPaths.size() + " paths \n";
			for(int i = 0; i < alreadyGeneratedPaths.size(); ++i)
			{
				logmsg += this.WriteOut(alreadyGeneratedPaths.get(i)) + "\n";
			}
			Logger.Send(logmsg);
		}
		
		for(int i = 0; i < alreadyGeneratedPaths.size(); ++i)
		{
			allPaths.add(alreadyGeneratedPaths.get(i));
		}
	}
	
	protected void updateAllPaths()
	{
		try
		{
			allPaths.add(alreadyGeneratedPaths.get(alreadyGeneratedPaths.size()-1));
			alreadyGeneratedPaths.remove(alreadyGeneratedPaths.size()-1);
		}
		catch(Exception e)
		{
			String logmsg = "Failed to calculate new path!";
			Logger.Send(logmsg);
		}
	}
	
	protected void CommunicateOffline(int step)
	{
		CentralizedProblemSolver.Response response;
		String destinationId = this.path.get(this.path.size() - 1).getEndNode().getID();
		this.initAllPaths(step, destinationId);
		do {
			CentralizedProblemSolver.getInstance(step).UpdatePaths(this);
			do {
				this.WaitSecond();
				response = CentralizedProblemSolver.getInstance(step).getResponse(this, null);
			} while(response.equals(CentralizedProblemSolver.Response.WAIT));
		} while(!response.equals(CentralizedProblemSolver.Response.SUCCESS));
		this.path = CentralizedProblemSolver.getInstance(step).getNewPath(this);
		Logger.Send("----> " + this.getAgvName()+"was SUCCESS with path: " + this.WriteOut(this.path));
	}
	
	protected int CalculateRank(long targetOrderID, Node currentNode, String prioType) 
	{
		if (Logger.getDebugLevel() >= 2)
		{
			Logger.Send("CalculateRank for " + this.getAgvName() + " targetOrderId " + targetOrderID + "prio type " + prioType);
		}
		int result = 0;
		
		if(prioType.equals("C"))
		{
			result += this.path.size();
		}
		
		if(prioType.equals("B") && !Scheduler.getInstance().isTheEndOfTheOrders())
		{
			if (targetOrderID != -1)
			{
				result += 1;
				
			}
			
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 1)
				result += 6;
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 2)
				result += 4;
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 3)
				result += 2;
		}
		else // prio type = "A" or is the end of the orders in case of "B"
		{
			if (targetOrderID != -1)
			{
				result += 3;
				
			}
			
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 1)
				result += 6;
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 2)
				result += 2;
			if (currentNode.getInLinks().stream().filter(l -> !(l instanceof WaitLink)).count() == 3)
				result += 1;
		}
		if (Logger.getDebugLevel() >= 2)
		{
			Logger.Send(this.getAgvName() + "add prio " + result);
		}
		return result;
	}
}
