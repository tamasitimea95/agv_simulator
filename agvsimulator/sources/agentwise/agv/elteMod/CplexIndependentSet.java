package agentwise.agv.elteMod;	
	
import ilog.concert.*;
import ilog.cplex.*;


public class CplexIndependentSet {
   public static double[] run(double[][] A, double[] c, int k) {
      try (IloCplex cplex = new IloCplex()) {
         IloNumVar[][] var = new IloNumVar[1][];
         IloRange[][]  rng = new IloRange[1][];
         
         int n = c.length;
 
         populateByRow(cplex, var, rng, A, c, k);
         
         Logger.Send("populateByRow done \n");
 
         if ( cplex.solve() ) {
            double[] x     = cplex.getValues(var[0]);
            double[] slack = cplex.getSlacks(rng[0]);

            System.out.println("Solution status = " + cplex.getStatus());
            System.out.println("Solution value  = " + cplex.getObjValue());

            for (int j = 0; j < x.length; ++j) {
               System.out.println("Variable " + j + ": Value = " + x[j]);
            }

            for (int i = 0; i < slack.length; ++i) {
               System.out.println("Constraint " + i + ": Slack = " + slack[i]);
            }
            
            return x;
         }
         else
         {
        	 Logger.Send("Failed to find routes!!! \n");
        	 double[] invalid = new double[n];
        	 for(int i = 0; i < n; ++i)
        	 {
        		 invalid[i] = -1;
        	 }
        	 
        	 return invalid;
         }
      }
      catch (IloException e) {
         System.err.println("Concert exception caught '" + e + "' caught");
         System.exit(-1);
      }
      return null;
   }


   static void populateByRow (IloMPModeler  model,
                              IloNumVar[][] var,
                              IloRange[][]  rng,
                              double[][] A, 
                              double[] c,
                              int k ) throws IloException {
	   
      // First define the variables, three continuous and one integer
	  int n = c.length;
	  int m = A.length;
	  
	  if(Logger.getDebugLevel() >= 3)
	  {
		  Logger.Send(" n = " + n + ", m = " + m + ", k = " + k + "\n");
		  
		  String logmsg = "A =  \n";
		  for(int i = 0; i < n; ++i)
		  {
			  for(int j = 0; j < m; ++j)
			  {
				  logmsg += A[j][i] + " ";
			  }
			  logmsg += "\n";
		  }
		  
		  Logger.Send(logmsg);
		  
		  logmsg = "c = \n";
		  for(int i = 0; i < n; ++i)
		  {
			  logmsg += c[i]+ " ";
		  }
		  logmsg += "\n";
		  
		  Logger.Send(logmsg);
	  }
	   
      double[]        xlb = new double[n];
      double[]        xub = new double[n];
      IloNumVarType[] xt  = new IloNumVarType[n];
      
      for(int i = 0; i < n; ++i) {
    	  xlb[i] = 0.0;
    	  xub[i] = 1.0;
    	  xt[i] = IloNumVarType.Bool;
      }
      
      IloNumVar[] x = model.numVarArray(n, xlb, xub, xt);
      var[0] = x;

      // Objective Function:  minimize cx 
      model.addMinimize(model.scalProd(x, c));

      // m constraints: Ax <= 1 and sum x_i = k, where k denotes the number of cars  
      rng[0] = new IloRange[m+1];
      
      for(int j = 0; j < m; ++j)
      {
    	  IloNumExpr Ax = model.constant(0);
    	  
    	  for(int i = 0; i < n; ++i)
    	  {
    		  Ax = model.sum(Ax, model.prod(A[j][i], x[i]));
    	  }
    	  rng[0][j] = model.addLe(Ax, 1);
      }
      int[] one = new int[n];
      for(int i = 0; i < n; ++i)
      {
    	  one[i] = 1;
      }
	  
      rng[0][m] = model.addEq(model.scalProd(x,one), k);
      Logger.Send("Added last eq");
   }
}
