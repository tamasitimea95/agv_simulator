package agentwise.agv.elteMod;

import java.util.LinkedList;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantLock;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.HashSet;

import agentwise.agv.controller.Controller;
import agentwise.agv.controller.OrderManager;
import agentwise.agv.model.environment.Order;

// TODO: send in a file or collect the msgs in a array
public class Logger {
	private static ReentrantLock rl_log;
	private static ReentrantLock rl_orders;
	private static List<Order> orders;
	private static Set<Long> storaged_orders_ids;
	private static int debugLevel;
	private static PrintWriter logFile;
	private static Controller controller;
	
	public static void Initialize(int debugLevel, String logFileName, Controller cont) {
		rl_log = new ReentrantLock();
		rl_orders = new ReentrantLock();
		orders = new LinkedList<Order>();
		storaged_orders_ids = new HashSet<Long>();
		Logger.debugLevel = debugLevel;
		controller = cont;
		try {
			logFile = new PrintWriter(logFileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("We can not open the log file.");
			e.printStackTrace();
		}
	}
	
	synchronized public static int getDebugLevel() {
		return debugLevel;
	}
	
	// TODO: probably we need a function for locking at start of filling the logmsg
	public static void Send(String msg) {
		rl_log.lock();
		try {
			if (msg.isEmpty())
				throw new Exception("Error in Logger.Send: the msg is empty.");
			logFile.println("###########################################################################################################################");
			logFile.print(msg);
			
			System.out.print(msg);
			
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			rl_log.unlock();
		}
	}
	
	public static void CloseLogFile() {
		rl_log.lock();
		try {
			logFile.flush();
			logFile.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			rl_log.unlock();
		}
	}
	
	public static void StopSimulation() {
		//controller.stopSimulation();
		Controller.getInstance().stopSimulation();
	}
	
	// The Random class theoretically threadsafe since Java 7
	public static boolean SelectRandomFreeOrder(Global globalAgv) {
		if (Logger.getDebugLevel() >= 3)
			Send("+=+=+=[LOGGER MSG]: Enter in Logger.SelectRandomFreeOrder agv: " + globalAgv.getAgvName() + "\n");
		while (!rl_orders.tryLock())
			globalAgv.WaitSecond();
		if (Logger.getDebugLevel() >= 3)
			Send(">>>>>>[LOGGER MSG]: rl_orders is LOCKED in Logger.SelectRandomFreeOrder by agv: " + globalAgv.getAgvName() + "\n");
		boolean result = false;
		try {
			Random rand = new Random();
			OrdersUpdate();
			List<Order> freeOrders = new LinkedList<Order>();
			for (var order: orders) {
				if (order.getStatus() == Order.Status.FREE) {
					freeOrders.add(order);
				}
			}
			if (!freeOrders.isEmpty()) {
				result = true;
				Order targetOrder = freeOrders.get(rand.nextInt(freeOrders.size()));
				targetOrder.setStatus(Order.Status.INPROGRESS);
				globalAgv.setTargetOrderId(targetOrder.getId());
			}
			else {
				globalAgv.setTargetOrderId(-1);
				if (Logger.getDebugLevel() >= 1)
					Send(globalAgv.getAgvName() + ": Not found free order\n");
			}
			
		}
		catch(Exception e) {
			Send("Exception in Logger.SelectRandomFreeOrder:\n" +
					e.getMessage() + "\n");
		}
		finally {
			rl_orders.unlock();
			//Send("<<<<<<[LOGGER MSG]: rl_orders is UNlocked in Logger.SelectRandomFreeOrder by agv: " + globalAgv.getAgvName() + "\n");
			return result;
		}
	}
	
	public static boolean PickOrderAt(Global globalAgv, String locationID) {
		if (Logger.getDebugLevel() >= 3)
			Send("+=+=+=[LOGGER MSG]: Enter in Logger.PickOrderAt agv: " + globalAgv.getAgvName() + "\n");
		while (!rl_orders.tryLock())
			globalAgv.WaitSecond();
		if (Logger.getDebugLevel() >= 3)
			Send(">>>>>>[LOGGER MSG]: rl_orders is LOCKED in Logger.PickOrderAt by agv: " + globalAgv.getAgvName() + "\n");
		boolean result = false;
		try {
			long orderId = globalAgv.PickUpTheOrder();
			OrdersUpdate();
			Order order = orders.stream().filter(ord -> ord.getId() == orderId).findAny().orElse(null);
			if (order == null) {
				throw new Exception("The following picked up order is not found in Logger: " + orderId); 
			}
			order.setStatus(Order.Status.PICKEDUP);
			result = true;
			String logmsg = "";
			if (orderId != globalAgv.getTargetOrderId()) {
				Order originalTargetOrder = orders.stream().filter(ord -> ord.getId() == globalAgv.getTargetOrderId())
						.findAny()
						.orElse(null);
				if (globalAgv.getTargetOrderId() != -1) {
					if (originalTargetOrder == null) {
						throw new Exception("The following original target order id is not found in Logger: " + globalAgv.getTargetOrderId());
					}
					originalTargetOrder.setStatus(Order.Status.FREE);
					//globalAgv.setTargetOrderId(orderId);
					if (Logger.getDebugLevel() >= 3)
						logmsg += globalAgv.getAgvName() + ": original target order changed to free status.\n";
				}
			}
			if (Logger.getDebugLevel() >= 3) {
				logmsg += globalAgv.getAgvName() + ": picked up an order at " + locationID + ".\n";
				Send(logmsg);
			}
		}
		catch(Exception e) {
			Send("Exception in Logger.PickOrderAt:\n" + e.getMessage() + "\n");
		}
		finally {
			rl_orders.unlock();
			if (Logger.getDebugLevel() >= 3)
				Send("<<<<<<[LOGGER MSG]: rl_orders is UNlocked in Logger.PickOrderAt by agv: " + globalAgv.getAgvName() + "\n");
			return result;
		}
	}
	
	public static boolean IsMyTargetOrderFree(Global globalAgv) {
		if (Logger.getDebugLevel() >= 3)
			Send("+=+=+=[LOGGER MSG]: Enter in Logger.IsMyTargetOrderFree agv: " + globalAgv.getAgvName() + "\n");
		while (!rl_orders.tryLock())
			globalAgv.WaitSecond();
		if (Logger.getDebugLevel() >= 3)
			Send(">>>>>>[LOGGER MSG]: rl_orders is LOCKED in Logger.IsMyTargetOrderFree by agv: " + globalAgv.getAgvName() + "\n");
		boolean result = false;
		try {
			OrdersUpdate();
			long targetOrderId = globalAgv.getTargetOrderId();
			Order originalTargetOrder = orders.stream().filter(ord -> ord.getId() == targetOrderId)
					.findAny()
					.orElse(null);
			if (originalTargetOrder == null) {
				throw new Exception("The following original target order id is not found in Logger: " + targetOrderId);
			}
			result = originalTargetOrder.getStatus().equals(Order.Status.INPROGRESS);
		}
		catch(Exception e) {
			Send("Exception in Logger.IsMyTargetOrderFree:\n" + e.getMessage() + "\n");
		}
		finally {
			rl_orders.unlock();
			if (Logger.getDebugLevel() >= 3)
				Send("<<<<<<[LOGGER MSG]: rl_orders is UNlocked in Logger.IsMyTargetOrderFree by agv: " + globalAgv.getAgvName() + "\n");
			return result;
		}
	}
	
	public static boolean DropAt(Global globalAgv, String locationID) {
		if (Logger.getDebugLevel() >= 3)
			Send("+=+=+=[LOGGER MSG]: Enter in Logger.DropAt agv: " + globalAgv.getAgvName() + "\n");
		while (!rl_orders.tryLock())
			globalAgv.WaitSecond();
		if (Logger.getDebugLevel() >= 3)
			Send(">>>>>>[LOGGER MSG]: rl_orders is LOCKED in Logger.DropAt by agv: " + globalAgv.getAgvName() + "\n");
		boolean result = false;
		try {
			OrdersUpdate();
			long dropedOrderId = globalAgv.DropTheOrder();
			Order dropedOrder = orders.stream().filter(ord -> ord.getId() == dropedOrderId)
					.findAny()
					.orElse(null);
			if (dropedOrder == null) {
				throw new Exception("The following droped order id is not found in Logger: " + dropedOrderId);
			}
			dropedOrder.setStatus(Order.Status.DONE);
			globalAgv.setTargetOrderId(-1);
			result = true;
			if (Logger.getDebugLevel() >= 3)
				Send(globalAgv.getAgvName() + ": droped an order at " + locationID + ".\n");
		}
		catch(Exception e) {
			Send("Exception in Logger.DropAt:\n" + e.getMessage() + "\n");
		}
		finally {
			rl_orders.unlock();
			if (Logger.getDebugLevel() >= 3)
				Send("<<<<<<[LOGGER MSG]: rl_orders is UNlocked in Logger.DropAt by agv: " + globalAgv.getAgvName() + "\n");
			return result;
		}
	}
	
	public static Order getOrderById(Global globalAgv, long id) {
		if (Logger.getDebugLevel() >= 3)
			Send("+=+=+=[LOGGER MSG]: Enter in Logger.getOrderById agv: "
				+ globalAgv.getAgvName() +"\n");
		while (!rl_orders.tryLock())
			globalAgv.WaitSecond();
		if (Logger.getDebugLevel() >= 3)
			Send(">>>>>>[LOGGER MSG]: rl_orders is LOCKED in Logger.getOrderById by agv: " + globalAgv.getAgvName() + "\n");
		Order order = null;
		try {
			order = orders.stream().filter(ord -> ord.getId() == id)
					.findAny()
					.orElse(null);
			if (order == null) {
				throw new Exception("The following order id is not found in Logger: " + id);
			}
		}
		catch(Exception e) {
			Send("Exception in Logger.getOrderById:\n" + e.getMessage() + "\n");
		}
		finally {
			rl_orders.unlock();
			if (Logger.getDebugLevel() >= 3)
				Send("<<<<<<[LOGGER MSG]: rl_orders is UNlocked in Logger.getOrderById by agv: " + globalAgv.getAgvName() + "\n");
			return order;
		}
	}
	
	// TODO: remove the dead orders (which does not exist anymore)
	private static void OrdersUpdate() {
		Collection<Order> orders = OrderManager.getInstance().getCurrentOrders();
		for(var order: orders) {
			if (!storaged_orders_ids.contains(order.getId())) {
				Logger.orders.add(order);
				storaged_orders_ids.add(order.getId());
			}
		}
	}
}
