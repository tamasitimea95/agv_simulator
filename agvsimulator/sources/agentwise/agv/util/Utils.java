/*
 * Created on Oct 6, 2004
 *
 */
package agentwise.agv.util;

import java.io.File;

/**
 * A class containing some general methods
 * 
 * @author nelis
 */
public class Utils {

    /**
     * Get the extension of a file.
     * @param f The file
     * @return The extension of the file
     */  
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
}