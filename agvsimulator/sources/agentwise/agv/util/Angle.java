/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.util;

/**
 * 
 * This class represents an angle. The value
 * of the angle can be expressed in degrees and
 * radians and some utility methods are provided
 * to calculate with angles.
 * 
 * @author tomdw
 *
 */
public class Angle {
    /**
     * 
     * Constructs an instance of Angle with
     * a given value. The value is given in 
     * degrees if isRadians is false, it is
     * given in radians if isRadians is true.
     * The given value is automatically normalised
     * to the range 0-2*Math.PI or 0-360
     *
     * @param value The value of the angle
     * @param isRadians True if value in radians, false if in degrees
     */
    public Angle(double value, boolean isRadians) throws IllegalArgumentException {
        if (isRadians) setRadians(value);
        else setDegrees(value);
    }
    
    /**
     * Constructs an instance of Angle
     * based on the value contained in the
     * given Angle instance
     * 
     * @param other The other Angle instance
     */
    public Angle(Angle other) {
        this(other.getDegrees(), false);
    }

    /**
     * Sets the current value of the angle.
     * This value is given in degrees.
     * 
     * @param d the value of the angle in degrees
     */
    private void setValue(double d) {
        this.degrees = d;
    }
    
    /**
     * The value of the angle in degrees
     */
    private double degrees = 0.0;
    
    /**
     * Returns the value of the angle
     * in degrees.
     * 
     * @return a double representing the value of the angle in degrees
     */
    public double getDegrees() {
        return degrees;
    }
    
    /**
     * Returns the value of the angle
     * in radians
     * 
     * @return a double representing the value of the angle in radians
     */
    public double getRadians() {
        return Math.toRadians(degrees);
    }
    
    /**
     * Sets the value of this angle instance to
     * the given number of degrees.
     * 
     * @param value The new value for this angle in degrees
     */
    public void setDegrees(double value) {
        setValue(normaliseDegrees(value));
    }
    
    /**
     * Sets the value of this angle instance to
     * the given number of radians
     * 
     * @param value The new alue for this angle in radians
     */
    public void setRadians(double value) {
        setValue(normaliseDegrees(Math.toDegrees(value)));
    }

    /**
     * Normalises the given degrees to be within the range of 0
     * and 360 degrees.
     * 
     * @param d The given degrees to convert
     * @return a double that represents the same degrees in the range of 0 and 360 degrees
     */
    private double normaliseDegrees(double d) {
        double result = 0;
        result = Math.abs(d) % 360.0;
        if (d <= 0.0) result = 360.0 - result;
        return result;
    }
    
    /**
     * Adjusts the value of this Angle to turn a given number of degrees
     * towards the given Angle instance. This is done
     * by moving this angle towards the
     * goal angle via the shortest path (clockwise or
     * counter clockwise).
     * 
     * @param angleGoal The goal angleGoal
     * @param nbDegrees The given number of degrees
     */
    public void turnAngleTowards(Angle angleGoal, double nbDegrees) {
        Angle newAngle = new Angle(degrees - angleGoal.getDegrees(), false);
        if (newAngle.getDegrees() > 180.0) {
            setDegrees(getDegrees() + nbDegrees);
        } else {
            setDegrees(getDegrees() - nbDegrees);
        }
    }

    /**
     * Checks if the given angle is within range of this angle. The
     * range considered here is rounding the angle degree values to integer
     * values and checking their equality.
     * 
     * @param other The other Angle to check with
     * @return true if the given angle is in the integer rounding range of this angel, false otherwise
     */
    public boolean inRange(Angle other) {
        return Math.round(getDegrees()) == Math.round(other.getDegrees());
    }

    /**
     * Returns the difference in degrees between this
     * Angle and the given Angle
     * 
     * @param other The given angle
     * @return a double representing the difference
     */
    public double difference(Angle other) {
        Angle newAngle = new Angle(degrees - other.getDegrees(), false);
        if (newAngle.getDegrees() > 180.0) {
            return 360 - newAngle.getDegrees(); 
        } else {
            return newAngle.getDegrees();
        }        
    }

}
