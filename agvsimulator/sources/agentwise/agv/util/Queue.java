/*
 * Created on Oct 13, 2004
 *
 */
package agentwise.agv.util;


import java.util.LinkedList;

/**
 * A fifo queue
 * 
 * @author nelis
 */
public class Queue 
{
	/**
	 * List containing the queue members
	 */
     private LinkedList queueMembers;
     
	/**
	 * 
	 */
	public Queue() {
		super();
	}
     /**
      * Ad an element to the queue
      * 
      * @param element The element
      * 
      */
     public void enqueue(Object element)
     {
          queueMembers.add (element);
          //return element;
     }

     /**
      * Get an element from the queue (implemented in fifo order)
      * 
      * @return The element is already longest in the queue
      */     
     public Object dequeue() 
     {
          return queueMembers.removeFirst();
     }
     
     /**
      * Shows if the queue contains some elements
      * @return true if the queue is not empty, false otherwise
      */
     public boolean containsElemts(){
     	return !isEmpty();
     }
     
     /**
      * Shows if the queue is empty
      * @return true if the queue is empty, false otherwise
      */
     public boolean isEmpty(){
     	return queueMembers.isEmpty();
     }
}
