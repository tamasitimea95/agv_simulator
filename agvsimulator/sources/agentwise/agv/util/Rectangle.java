/*
 * Created on Oct 21, 2004
 *
 */
package agentwise.agv.util;

/**
 * 
 * This class represents a rectangle that
 * is defined by four coordinates and four
 * lines between those coordinates.
 * 
 * @author tomdw
 *
 */
public class Rectangle {
    
    /* The first coordinate */
    private Coordinate c1 = null;
    
    /**
     * Returns the first coordinate of the rectangle
     * @return a Coordinate instance
     */
    public Coordinate getCoordinate1() {
        return new Coordinate(c1);
    }
    
    /* The second coordinate */
    private Coordinate c2 = null;
    
    /**
     * Returns the second coordinate of the rectangle
     * @return a Coordinate instance
     */
    public Coordinate getCoordinate2() {
        return new Coordinate(c2);
    }
    
    /* The third coordinate */
    private Coordinate c3 = null;
    
    /**
     * Returns the third coordinate of the rectangle
     * @return a Coordinate instance
     */
    public Coordinate getCoordinate3() {
        return new Coordinate(c3);
    }
    
    /* The fourth coordinate */
    private Coordinate c4 = null;
    
    /**
     * Returns the fourth cooridnate of the rectangle
     * @return a Coordinate instance
     */
    public Coordinate getCoordinate4() {
        return new Coordinate(c4);
    }
    
    /**
     * Returns the coordinate that is in the middle
     * of the rectangle, at an equal distance from each
     * other coordinate.
     * 
     * @return a Coordinate instance
     */
    public Coordinate getMiddle() {
        Angle a1 = getCoordinate1().calculateAngleTowards(getCoordinate2());
        double dist1 = getCoordinate1().distanceTo(getCoordinate2());
        double dist2 = getCoordinate2().distanceTo(getCoordinate3());
        Angle a2 = getCoordinate2().calculateAngleTowards(getCoordinate3());
        return getCoordinate1().nextInAngle(a1,dist1/2).nextInAngle(a2,dist2/2);
        
    }
    
    /**
     * Returns the direction from coordinate 1 towards coordinate 2.
     * This is the direction from which an equal rectangle can be constructed
     * using the constructor that requires such a direction.
     * 
     * @return an Angle representing the direction of the rectangle
     */
    public Angle getDirection() {
        return c2.calculateAngleTowards(c1);
    }
    
    /**
     * 
     * Constructs an instance of Rectangle
     * to be defined by four given coordinates.
     * The result is such that getDirection() returns
     * the angle from c1 to c2, getHeight() returns
     * the distance between c2 and c3 or c1 and c4,
     * getWidth returns the distance between c1 and c2 or
     * c3 and c4.
     *
     * @param c1 The first coordinate
     * @param c2 The second coordinate
     * @param c3 The third coordinate
     * @param c4 The fourth coordinate
     * @throws IllegalArgumentException when the coordinates cannot form a legal rectangle (angles of 90 degrees between c1 and c2, c2 and c3, c3 and c4, c4 and c1
     * @throws NullPointerException when one coordinate is null
     */
    public Rectangle(Coordinate c1, Coordinate c2, Coordinate c3, Coordinate c4) throws IllegalArgumentException, NullPointerException{
        if (c1==null || c2==null || c3==null || c4==null) throw new NullPointerException("The given coordinates cannot be null");
        if (noRectangle(c1,c2,c3,c4)) throw new IllegalArgumentException("The given coordinates doe not represent a rectangle");
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
    }
    
    /**
     * 
     * Constructs an instance of Rectangle to be
     * calculated from the middle coordinate, the
     * direction in which to set the line from coordinate 1
     * towards coordinate 2 and a given width (distance(c1,c2))
     *  and height (distance(c2,c3)). The method getHeight will return
     * height, getWidth will return width and getDirection will
     * return direction and getMiddle will return a coordinate
     * instance equal to middle
     *
     * @param middle The middle Coordinate
     * @param direction The direction to use
     * @param width the width of the rectangle
     * @param height the height of the rectangle
     * @throws NullPointerException when the middle==null or direction==null
     */
    public Rectangle(Coordinate middle, Angle direction,double width, double height) throws NullPointerException {
        Angle newAngle = new Angle(direction);
        Angle newAngle2 = new Angle(direction);
        newAngle.setDegrees(direction.getDegrees()+90);
        c1 = middle.nextInAngle(newAngle2,width/2).nextInAngle(newAngle, height/2);
        newAngle2.setDegrees(direction.getDegrees()-180);
        c2 = middle.nextInAngle(newAngle2,width/2).nextInAngle(newAngle, height/2);
        newAngle.setDegrees(direction.getDegrees()-90);
        c3 = middle.nextInAngle(newAngle2,width/2).nextInAngle(newAngle,height/2);
        newAngle2.setDegrees(direction.getDegrees());
        c4 = middle.nextInAngle(newAngle2,width/2).nextInAngle(newAngle, height/2);
        
    }
    
    /**
     * Checks if a given rectangle intersects with
     * this rectangle.
     * 
     * @param other The given rectangle
     * @return true if they intersect, false otherwise
     */
    public boolean intersectsWith(Rectangle other) {
        if (other==null) return false;
        boolean result = false;
        if (other.intersectsWith(getLine12())) result = true;
        if (other.intersectsWith(getLine23())) result = true;
        if (other.intersectsWith(getLine34())) result = true;
        if (other.intersectsWith(getLine41())) result = true;
        return result;
    }
    
    /**
     * Checks if the given line intersects with
     * this rectangle
     * 
     * @param line The given line
     * @return true if the line intersects with the rectangle, false otherwise
     */
    public boolean intersectsWith(Line line) {
        if (line==null) return false;
        boolean result = false;
        if (line.intersectsLine(getLine12())) result = true;
        if (line.intersectsLine(getLine23())) result = true;
        if (line.intersectsLine(getLine34())) result = true;
        if (line.intersectsLine(getLine41())) result = true;
        return result;
    }
    
    /**
     * Returns a line from coordinate 1 towards coordinate 2
     * @return a Line instance representing that line
     */
    public Line getLine12() {
        return new Line(getCoordinate1(),getCoordinate2());
    }
    
    /**
     * Returns a line from coordinate 2 towards coordinate 3
     * @return a Line instance representing that line
     */
    public Line getLine23() {
        return new Line(getCoordinate2(),getCoordinate3());
    }
    
    /**
     * Returns a line from coordinate 3 towards coordinate 4
     * @return a Line instance representing that line
     */
    public Line getLine34() {
        return new Line(getCoordinate3(),getCoordinate4());
    }
    
    /**
     * Returns a line from coordinate 4 towards coordinate 1
     * @return a Line instance representing that line
     */
    public Line getLine41() {
        return new Line(getCoordinate4(),getCoordinate1());
    }

    /**
     * Checks if a rectangle constructed from
     * the given coordinates is really a rectangle. This means
     * that the angles in all coordinates must be 90 degrees.
     * 
     * @param c1 The first coordinate
     * @param c2 The second coordinate
     * @param c3 The third coordinate
     * @param c4 The fourth coordinate
     * @return true if the four coordinate form a valid rectangle, false otherwise
     */
    private boolean noRectangle(Coordinate c1, Coordinate c2, Coordinate c3, Coordinate c4) {
        double a1 = c1.calculateAngleTowards(c2).getDegrees();
        double a2 = c2.calculateAngleTowards(c3).getDegrees();
        double a3 = c3.calculateAngleTowards(c4).getDegrees();
        double a4 = c4.calculateAngleTowards(c1).getDegrees();
        return (closeTo(a1,90.0) && closeTo(a2,90.0) && closeTo(a3,90.0) && closeTo(a4,90.0));
    }

    /**
     * checks if a given angle value is close to 
     * another given value. Close to means in a range
     * of 1 from each other.
     * 
     * @param angle The angle to check
     * @param reference the reference angle
     * @return true if (angle-reference)<1
     */
    private boolean closeTo(double angle, double reference) {
        double range = 1.0;
        return ((angle < reference + range) && (angle > reference - range));
    }
    
    /**
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "c1="+c1.toString()+"c2="+c2.toString()+"c3="+c3.toString()+"c4="+c4.toString();
    }

    /**
     * @return a double representing the height of this rectangle
     */
    public double getHeight() {
        return c2.distanceTo(c3);
    }
    
    /**
     * 
     * @return a double representing the width of this rectangle
     */
    public double getWidth() {
        return c1.distanceTo(c2);
    }

    /**
     * Constructs and returns an new Rectangle instance that results
     * from turning this Rectangle in a given direction
     * 
     * @param newDirection The Angle representing the given direction
     * @return a new Rectangle instance
     */
    public Rectangle turn(Angle newDirection) {
        return new Rectangle(getMiddle(),newDirection,getWidth(),getHeight());
    }
    
    /**
     * Constructs and returns an new Rectangle instance that results
     * from moving this Rectangle to a given middle coordinate
     * 
     * @param middle a Coordinate representing the given middle coordinate
     * @return a new Rectangle instance
     */
    public Rectangle move(Coordinate middle) {
        return new Rectangle(middle, getDirection(),getWidth(),getHeight());
    }

}
