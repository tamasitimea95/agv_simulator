/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.util;

/**
 * A 2D coordinate represented by a X and Y value.
 * 
 * @author tomdw
 * 
 */
public class Coordinate {
   
    /**
     * Constructs an instance of Coordinate with
     * a given x value and y value.
     *
     * @param x The x value
     * @param y The y value
     */
    public Coordinate(double x, double y) {
        setX(x);
        setY(y);
    }
    
    /* The x value of the coordinate */
    private double x;
    
    /* The y value of the coordinate */
    private double y;
    
    /**
     * Returns the X value of this
     * coordinate instance
     * 
     * @return a double representing the x value
     */
    public double getX() {
        return x;
    }
    
    /**
     * Returns the Y value of this
     * coordinate instance
     * 
     * @return a double representing the y value
     */
    public double getY() {
        return y;
    }
    
    /**
     * Sets the x value of this coordinate.
     * 
     * @param x The value to set
     */
    private void setX(double x) {
        this.x = x;
    }
    
    /**
     * Sets the y value of this coordinate.
     * 
     * @param y The value to set
     */
    private void setY(double y) {
        this.y = y;
    }

    /**
     * Returns true if the given Coordinate equals this
     * coordinate. This is true when the x and y values are
     * the same.
     * 
     * @param c
     * @return true if the given coordinate equals this coordinate
     */
    public boolean equals(Coordinate c){
    	return (this.getX() == c.getX() && this.getY() == c.getY());
    }
    
    /**
     * Constructs a new duplicate Coordinate instance
     * based on this instance of Coordinate
     * 
     * @throws NullPointerException when the given coordinate is null
     */
    public Coordinate(Coordinate other) throws NullPointerException {
        this(other.getX(), other.getY());
    }

   
    /**
     * Checks if this Coordinate is within integer distance
     * (rounded to integer) of a given coordinate
     * 
     * @param other
     *            The coordinate of the position to check
     * @return true if the coordinate is within the integer distance from the given
     *         coordinate, false otherwise
     */
    public boolean isInRangeOf(Coordinate other) {
        return Math.round(other.getX()) ==  Math.round(getX()) && Math.round(other.getY()) == Math.round(getY());
    }

    /**
     * Returns the euclidean distance from this coordinate to the
     * given coordinate.
     * @param coordinate The given Coordinate
     * @return a double representing the euclidean distance between this coordinate and the given coordinate
     */
    public double distanceTo(Coordinate coordinate) {
        return Math.sqrt(Math.pow(getX()-coordinate.getX(),2)+Math.pow(getY()-coordinate.getY(),2));
    }
    
    /**
     * Calculates the angle of the direction in which the given Coordinate is situated
     * with respect to this Coordinate.
     * 
     * @param c The given Coordinate
     * @return an Angle representing the the goal angle
     */
    public Angle calculateAngleTowards(Coordinate c) {
        if (c.getX()==getX() && c.getY()==getY()) return new Angle(0,false);
        double angleGoal = Math.toDegrees(Math
                .asin((Math.abs(c.getY() - getY()))
                        / (Math.sqrt(Math.pow(c.getX() - getX(), 2)
                                + Math.pow(c.getY() - getY(), 2)))));
        if ((c.getX() - getX()) < 0 && (c.getY() - getY()) <= 0)
            angleGoal = 180 - angleGoal;
        if ((c.getX() - getX()) <= 0 && (c.getY() - getY()) > 0)
            angleGoal += 180;
        if ((c.getX() - getX()) > 0 && (c.getY() - getY()) >= 0)
            angleGoal = 360.0 - angleGoal;
        return new Angle(angleGoal, false);
    }
    
    /**
     * Returns a coordinate that is the next coordinate when
     * moving in a given angle and over a given stepsize
     * 
     * @param a The angle to move in
     * @param stepsize the distance to move with
     * @return a Coordinate instance represeting the resulting coordinate of that move
     */
    public Coordinate nextInAngle(Angle a,double stepsize) { 
	    double x = getX() + stepsize*Math.cos(a.getRadians());
	    double y = getY() - stepsize*Math.sin(a.getRadians()); // reverse y coordinate (going down)
		return new Coordinate(x,y);
    }

    public String toString() {
        return "("+getX()+", "+getY()+")";
    }
}
