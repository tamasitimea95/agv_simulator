/*
 * Created on Oct 12, 2004
 *
 */
package agentwise.agv.controller;

import java.util.Collection;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.InputOrderLocationManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.util.Coordinate;

/**
 * This Adapter implements all methods of
 * the ChangeObserver interface with a default
 * implementation of doing nothing.
 * 
 * This class can be subclassed in order for the subclass
 * to be able to implement only the needed methods.
 * 
 * @author tomdw
 *
 */
public class ChangeObserverAdapter implements ChangeObserver {

  
    /**
     * 
     * @see agentwise.agv.controller.ChangeObserver#updateAgvAdded(agentwise.agv.model.environment.AgvEnvironmentState)
     */
    public void updateAgvAdded(AgvEnvironmentState agvstate) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateAgvChanged(agentwise.agv.model.environment.AgvEnvironmentState)
     */
    public void updateAgvChanged(AgvEnvironmentState agvstate) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateNewSimulationSet(agentwise.agv.controller.Simulation)
     */
    public void updateNewSimulationSet(Simulation sim) {

    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateStartConstructionNewSimulation()
     */
    public void updateStartConstructionNewSimulation() {

    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateMapAdded(java.util.Collection)
     */
    public void updateMapAdded(Collection segments) {
      
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateSegmentChanged(agentwise.agv.model.environment.map.Segment)
     */
    public void updateSegmentChanged(Segment segment) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateStationChanged(agentwise.agv.model.environment.map.Station)
     */
    public void updateStationChanged(Station station) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateBroadCast(agentwise.agv.util.Coordinate, agentwise.agv.model.environment.communication.Message, double)
     */
    public void updateBroadCast(Coordinate senderC, Message msg, double range) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateOrderLocationChanged(agentwise.agv.model.environment.map.OrderLocation)
     */
    public void updateOrderLocationChanged(OrderLocation location) {

    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateUniCast(agentwise.agv.util.Coordinate, agentwise.agv.model.environment.communication.Message, agentwise.agv.model.environment.communication.NetworkID)
     */
    public void updateUniCast(Coordinate senderC, Message msg, NetworkID receiver) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateReceive(agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message)
     */
    public void updateReceive(NetworkID receiver, Message msg) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateNewOrder(agentwise.agv.controller.OrderManager, agentwise.agv.model.environment.Order)
     */
    public void updateNewOrder(OrderManager om, Order o) {
                
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateRemoveOrder(agentwise.agv.controller.OrderManager, agentwise.agv.model.environment.Order)
     */
    public void updateRemoveOrder(OrderManager om, Order o) {
        
    }

    /**
     * @see agentwise.agv.controller.ChangeObserver#updateStartUsingInputOrderLocationManager(agentwise.agv.model.environment.InputOrderLocationManager)
     */
    public void updateStartUsingInputOrderLocationManager(InputOrderLocationManager om) {
                
    }



}
