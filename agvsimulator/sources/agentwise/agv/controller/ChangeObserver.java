/*
 * Created on Oct 12, 2004
 *
 */
package agentwise.agv.controller;

import java.util.Collection;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.InputOrderLocationManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.util.Coordinate;

/**
 * This interface is implemented by
 * an observer that wants to observe
 * each change that can occur.
 * 
 * @author tomdw
 *
 */
public interface ChangeObserver {
    
    /**
     * This method is called when a new simulation is
     * set as the current simulation. The construction
     * of that simulation has already been done
     * 
     * @param sim The simulation that was set
     */
    public void updateNewSimulationSet(Simulation sim);
    
    /**
     * This method is called when the construction
     * of a new simulation is started. This can be usefull
     * to clear all leftovers from the previous simulation
     * at this point.
     *
     */
    public void updateStartConstructionNewSimulation();
    
    /**
     * This method is called when a new Agv was added to
     * the simulation.
     * 
     * @param agvstate The AgvEnvironmentState of the added Agv
     */
    public void updateAgvAdded(AgvEnvironmentState agvstate);
    
    /**
     * This method is called when an existing Agv changed
     * 
     * @param agvstate The AgvEnvironmentState of the changed Agv
     */
    public void updateAgvChanged(AgvEnvironmentState agvstate);
    
    /**
     * This method is called when a map of segments and
     * stations was added to the simulation
     * 
     * @param segments The map of segments, a collection of segments
     */
    public void updateMapAdded(Collection segments);
    
    /**
     * This method is called when a segment has changed state
     * 
     * @param segment The segment that changed
     */
    public void updateSegmentChanged(Segment segment);
    
    /**
     * This method is called when a station has changed state
     * 
     * @param station The station that changed
     */
    public void updateStationChanged(Station station);
    
    /**
     * This method is called when an OrderLocation has changed state
     * 
     * @param location The OrderLocation that changed
     */
    public void updateOrderLocationChanged(OrderLocation location);
    
    /**
     * This method is called when a broadcast was performed
     * in an ad-hoc infrastructure
     * 
     * @param senderC The coordinate of the sender of the broadcast
     * @param msg The message that was sent
     * @param range The range to wich the broadcast signal reached
     */
    public void updateBroadCast(Coordinate senderC, Message msg, double range);

    /**
     * This method is called when a unicast message was
     * sent
     * 
     * @param senderC The coordinate of the sender of the broadcast
     * @param msg The message that was sent
     * @param receiver The networkid of the receiver of the message
     */
    public void updateUniCast(Coordinate senderC, Message msg, NetworkID receiver);

    /**
     * This method is called when a CommunicationNode receives a message from its inbox
     * 
     * @param receiver The networkID of the receiving agv
     * @param msg The message that was sent
     */
    public void updateReceive(NetworkID receiver, Message msg);

    /**
     * This method is called when a new Order is added to the system.
     * 
     * @param om The OrderManager of the system
     * @param o The Order 
     */
    public void updateNewOrder(OrderManager om, Order o);

    /**
     * This method is called when an Order is removed from the
     * system.
     * 
     * @param om The OrderManager of the system
     * @param o The Order
     */
    public void updateRemoveOrder(OrderManager om, Order o);

    /**
     * This method is called when the system is starting to 
     * use an order location manager that expects as input
     * the new orders for the system.
     * 
     * @param om The InputOrderLocationManager that expects those Orders
     */
    public void updateStartUsingInputOrderLocationManager(InputOrderLocationManager om);
    

}
