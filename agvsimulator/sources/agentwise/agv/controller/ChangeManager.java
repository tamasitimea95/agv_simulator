/*
 * Created on Oct 12, 2004
 *
 */
package agentwise.agv.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.InputOrderLocationManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.util.Coordinate;

/**
 * This class manages changes that
 * occur in the system and the registrations
 * of interest for such changes. It is a singleton
 * so accessible from everywhere. It collaborates
 * intensively with the ChangeObserverAspect which
 * detects the changes and notifies this manager.
 * This manager will then notify the interested
 * and registered observers.
 * 
 * @author tomdw
 *
 */
public final class ChangeManager {

    /**
     *  The ChangeManager instance 
     **/
    private static ChangeManager instance = null;

    /**
     * Returns the single instance that is available
     * of this class. This is an implementation
     * of the Singleton pattern
     * 
     * @return the ChangeManager instance
     */
    public static ChangeManager getInstance() {
        if (instance == null) {
            instance = new ChangeManager();
        }
        return instance;
    }

    /**
     * 
     * Constructs an instance of ChangeManager.
     */
    private ChangeManager() { }

    /* THE COLLECTIONS OF OBSERVERS */
    private Collection observersMultiple = new HashSet();
    private Collection observersAgvAdded = new HashSet();
    private Collection observersNewSimulationSet = new HashSet();
    private Collection observersStartConstructionNewSimulation = new HashSet();
    private Collection observersMapAdded = new HashSet();
    private Collection observersAgvChanged = new HashSet();
    private Collection observersSegmentChanged = new HashSet();
    private Collection observersStationChanged = new HashSet();
    private Collection observersOrderLocationChanged = new HashSet();
    private Collection observersBroadCast = new HashSet();
    private Collection observersUniCast = new HashSet();
    private Collection observersReceive = new HashSet();
    private Collection observersNewOrder = new HashSet();
    private Collection observersRemoveOrder = new HashSet();
    private Collection observersStartUsingInputOrderLocationManager = new HashSet();
    
    /* REGISTRATION FOR EVENTS */
    /**
     * Register a given observer as interested in multiple
     * events (all events possible).
     * 
     * @param o The given observer
     */
    public void registerForMultipleEvents(ChangeObserver o) {
        if (o!=null && !observersMultiple.contains(o)) {
            observersMultiple.add(o);
        }
    }
    
    /**
     * Register a given observer as interested in the
     * event of adding an agv
     * 
     * @param o The given observer
     */
    public void registerForAgvAddedEvent(ChangeObserver o) {
        if (o!=null && !observersAgvAdded.contains(o))
        observersAgvAdded.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of setting a new simulation as the current simulation
     * 
     * @param o The given observer
     */
    public void registerForNewSimulationSetEvent(ChangeObserver o) {
        if (o!=null && !observersNewSimulationSet.contains(o))
        observersNewSimulationSet.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of starting the construction of a new
     * simulation
     * 
     * @param o The given observer
     */
    public void registerForStartConstructionNewSimulationEvent(ChangeObserver o) {
        if (o!=null && !observersStartConstructionNewSimulation.contains(o))
        observersStartConstructionNewSimulation.add(o);
    }
    

    /**
     * Register a given observer as interested in the
     * event of adding a map to the simulation
     * 
     * @param o The given observer
     */
    public void registerForMapAddedEvent(ChangeObserver o) {
        if (o!=null && !observersMapAdded.contains(o))
        observersMapAdded.add(o);
    }
    
    /**
     * Register a given observer as interested in the 
     * event of changes in an agv
     * 
     * @param o The given observer
     */
    public void registerForAgvChangedEvent(ChangeObserver o) {
        if (o!=null && !observersAgvChanged.contains(o))
        observersAgvChanged.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of changes in a segment
     * 
     * @param o The given observer
     */
    public void registerForSegmentChangedEvent(ChangeObserver o) {
        if (o!=null && !observersSegmentChanged.contains(o))
        observersSegmentChanged.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of changes in a station
     * 
     * @param o The given observer
     */
    public void registerForStationChangedEvent(ChangeObserver o) {
        if (o!=null && !observersStationChanged.contains(o))
        observersStationChanged.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of changes in an orderlocation
     * 
     * @param o The given observer
     */
    public void registerForOrderLocationChangedEvent(ChangeObserver o) {
        if (o!=null && !observersOrderLocationChanged.contains(o))
        observersOrderLocationChanged.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of sending a broadcast
     * 
     * @param o The given observer
     */
    public void registerForBroadCastEvent(ChangeObserver o) {
        if (o!=null && !observersBroadCast.contains(o))
        observersBroadCast.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of sending a unicast
     * 
     * @param o The given observer
     */
    public void registerForUniCastEvent(ChangeObserver o) {
        if (o!=null && !observersUniCast.contains(o))
        observersUniCast.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of receiving a message from the inbox
     * 
     * @param o The given observer
     */
    public void registerForReceiveEvent(ChangeObserver o) {
        if (o!=null && !observersReceive.contains(o))
        observersReceive.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of adding a new order in the system
     * 
     * @param o The given observer
     */
    public void registerForNewOrderEvent(ChangeObserver o) {
        if (o!=null && !observersNewOrder.contains(o))
            observersNewOrder.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of removing order(s) from the system
     * 
     * @param o The given observer
     */
    public void registerForRemoveOrderEvent(ChangeObserver o) {
        if (o!=null && !observersRemoveOrder.contains(o))
            observersRemoveOrder.add(o);
    }
    
    /**
     * Register a given observer as interested in the
     * event of starting to use an
     * input order location manager that wants
     * orders as input
     * 
     * @param o The given observer
     */
    public void registerForStartUseOfInputOrderLocationManagerEvent(ChangeObserver o) {
        if (o!=null && !observersStartUsingInputOrderLocationManager.contains(o))
            observersStartUsingInputOrderLocationManager.add(o);
    }
    
    /* SIGNALLING OF EVENTS */
    
    /**
     * Signals that a new Agv instance was added to
     * the simulation. 
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param agvstate The state of the new Agv
     */
    public void signalAgvAddedEvent(AgvEnvironmentState agvstate) {
        Iterator iter = observersAgvAdded.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateAgvAdded(agvstate);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateAgvAdded(agvstate);
        }
    }
    
    /**
     * Signals that a new simulation was set as the
     * current simulation.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param sim The new Simulation
     */
    public void signalNewSimulationSetEvent(Simulation sim) {
        Iterator iter = observersNewSimulationSet.iterator();
        while(iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateNewSimulationSet(sim);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateNewSimulationSet(sim);
        }
    }
    
    /**
     * Signals the start of the construction of a new
     * simulation.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     *
     */
    public void signalStartConstructionNewSimulationEvent() {
        Iterator iter = observersStartConstructionNewSimulation.iterator();
        while(iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateStartConstructionNewSimulation();
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateStartConstructionNewSimulation();
        }
    }
    
    

    /**
     * Signals that a new Map was added to
     * the simulation. 
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param segments The Collection containing the segments of the map
     */
    public void signalMapAddedEvent(Collection segments) {
        Iterator iter = observersMapAdded.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateMapAdded(segments);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateMapAdded(segments);
        }
    }
    
    
    
    /**
     * Signals that an agv has changed.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param agvstate The environment state of the changed Agv
     */
    public void signalAgvChangedEvent(AgvEnvironmentState agvstate) {
        Iterator iter = observersAgvChanged.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateAgvChanged(agvstate);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateAgvChanged(agvstate);
        }
    }
    
    

    /**
     * Signals that a segment has changed
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param segment The changed Segment
     */
    public void signalSegmentChangedEvent(Segment segment) {
        Iterator iter = observersSegmentChanged.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateSegmentChanged(segment);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateSegmentChanged(segment);
        }
    }
    
    
    
    /**
     * Signals that a station has changed
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param station The changed Station
     */
    public void signalStationChangedEvent(Station station) {
        Iterator iter = observersStationChanged.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateStationChanged(station);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateStationChanged(station);
        }
    }
    
    

    /**
     * Signals that a OrderLocation has changed
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param location The changed OrderLocation
     */
    public void signalOrderLocationChangedEvent(OrderLocation location) {
        Iterator iter = observersOrderLocationChanged.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateOrderLocationChanged(location);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateOrderLocationChanged(location);
        }
    }
    
    
    /**
     * Signal that a broadcast with an ad-hoc infrastructure
     * was sent.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param agvC The coordinate of the sender
     * @param msg The message sent
     * @param range The range of communication reach
     */
    public void signalBroadCastEvent(Coordinate agvC, Message msg, double range) {
        Iterator iter = observersBroadCast.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateBroadCast(agvC,msg, range);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateBroadCast(agvC,msg, range);
        }
    }
    
    
    /**
     * Signals that a unicast was sent.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param senderC The coordinate of the sender
     * @param msg The message that was sent
     * @param receiver The networkid of the receiver
     */
    public void signalUniCastEvent(Coordinate senderC, Message msg, NetworkID receiver) {
        Iterator iter = observersUniCast.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateUniCast(senderC,msg, receiver);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateUniCast(senderC,msg, receiver);
        }
    }
    
    
    /**
     * Signals that an agv has received a message from its inbox.
     * 
     * This will notify the observers registered for
     * this event but also those registered for
     * multiple events.
     * 
     * @param receiver The networkId of the receiver
     * @param msg The message received
     */
    public void signalReceiveEvent(NetworkID receiver, Message msg) {
        Iterator iter = observersReceive.iterator();
        while (iter.hasNext()) {
            ChangeObserver o = (ChangeObserver)iter.next();
            o.updateReceive(receiver,msg);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver o = (ChangeObserver)iterM.next();
            o.updateReceive(receiver, msg);
        }
    }
    
    /**
     * Signals that a new order was added tot the system.
     * 
     * This will notify the observers registered for this event but
     * also those registered for multiple events.
     * 
     * @param om The OrderManager of the system
     * @param o The Order that was added
     */
    public void signalNewOrderEvent(OrderManager om, Order o) {
        Iterator iter = observersNewOrder.iterator();
        while (iter.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iter.next();
            obs.updateNewOrder(om,o);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iterM.next();
            obs.updateNewOrder(om,o);
        }
    }
    
    /**
     * Signals that an order or multiple orders where
     * removed from the system. If multiple orders where removed
     * then the given order is null
     * 
     * This will notify the observers registered for this event but
     * also those registered for multiple events.
     * 
     * @param om The OrderManager of the system
     * @param o The Order that was added
     */
    public void signalRemoveOrderEvent(OrderManager om, Order o) {
        Iterator iter = observersRemoveOrder.iterator();
        while (iter.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iter.next();
            obs.updateRemoveOrder(om,o);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iterM.next();
            obs.updateRemoveOrder(om,o);
        }
    }
    
    /**
     * Signals that the system starts using
     * and order location manager that expects 
     * new Orders being given by the user.
     * 
     * This will notify the observers registered for this event but
     * also those registered for multiple events.
     * 
     * @param om The InputOrderLocationManager of the system
     */
    public void signalStartUseOfInputOrderLocationManager(InputOrderLocationManager om) {
        Iterator iter = observersStartUsingInputOrderLocationManager.iterator();
        while(iter.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iter.next();
            obs.updateStartUsingInputOrderLocationManager(om);
        }
        Iterator iterM = observersMultiple.iterator();
        while(iterM.hasNext()) {
            ChangeObserver obs = (ChangeObserver)iterM.next();
            obs.updateStartUsingInputOrderLocationManager(om);
        }
    }
    

}
