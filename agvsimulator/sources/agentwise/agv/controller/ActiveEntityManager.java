/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import timeManagementFramework.TimeManager;

/**
 * The ActiveEntityManager manages the execution of all ActiveEntity instances
 * in the simulation. By constructing an ActiveEntity instance it is
 * automatically added to this manager.
 * 
 * Then this manager is to be used to activate the activity in the simulation,
 * ie the active entities present. This manager then also allows to stop(),
 * pause(), or resume() the simulation.
 * 
 * This manager implements the Singleton pattern to be enforce that only one
 * manager is present in one program run and to be accessible.
 * 
 * @author tomdw
 *  
 */
public class ActiveEntityManager {

    /**
     * The single instance according to the Singleton pattern
     */
    private static ActiveEntityManager instance = null;

    /**
     * 
     * Constructs an instance of ActiveEntityManager. This constructor cannot be
     * used publicly. This class has to be used as a singleton pattern.
     *  
     */
    protected ActiveEntityManager() {
    }

    /**
     * Returns the instance of this ActiveEntityManager according to the
     * Singleton pattern. If there is no instance yet, it is constructed.
     * 
     * @return the ActiveEntityManager instance
     */
    public static ActiveEntityManager getInstance() {
        if (instance == null) {
            instance = new ActiveEntityManager();
        }
        return instance;
    }

    /**
     * Adds a given ActiveEntity instance to this manager in order for it to
     * manage its execution. If the given entity is null, then nothing happens.
     * 
     * If the entity is added during a running simulation this entity is
     * automatically activated also.
     * 
     * @param entity
     *            The ActiveEntity to manage.
     */
    public void addActiveEntity(ActiveEntity entity) {
        if (entity != null) {
            entities.add(entity);
            entity.activate(); // if added during run, activate
        }
    }

    /**
     * The collection of ActiveEntity instances
     */
    private Collection entities = new HashSet();

    /**
     * If the simulation is not running currently the Simulation is activated by
     * starting the Time Management infrastructure and making all ActiveEntities
     * active so that this ActiveEntityManager is running.
     *  
     */
    public synchronized void start() {
        if (!isRunning()) {
            setPaused(false);
            setRunning(true);
            TimeManager.getInstance().start();
            activateAll();
        }
    }

    /**
     * Activates all ActiveEntity instances that are to be managed by this
     * ActiveEntityManager
     *  
     */
    protected void activateAll() {
        Iterator iter = entities.iterator();
        while (iter.hasNext()) {
            ActiveEntity e = (ActiveEntity) iter.next();
            e.activate();
        }
    }

    /**
     * De-Activates all ActiveEntity instances that are to be managed by this
     * ActiveEntityManager.
     *  
     */
    private void deactivateAll() {
        Iterator iter = entities.iterator();
        while (iter.hasNext()) {
            ActiveEntity e = (ActiveEntity) iter.next();
            e.deactivate();
        }
    }

    /**
     * Stops a running simulation by stopping the ActiveEntity instances and the
     * management of those entities by this ActiveEntityManager. Also, the Time
     * Management is stopped.
     *  
     */
    public synchronized void stop() {
        if (isRunning()) {
            deactivateAll();
            TimeManager.getInstance().stop();
            setPaused(false);
            setRunning(false);
            System.exit(0);
        }
    }

    /**
     * Pauses a running simulation by pausing this manager. This also pauses all
     * ActiveEntity instances that are running.
     *  
     */
    public synchronized void pause() {
        if (isRunning() && !isPaused()) {
            setPaused(true);
        }
    }

    /**
     * Resumes a paused simulation by resuming this manager. This also resumes
     * all ActiveEntity instances that are paused.
     *  
     */
    public synchronized void resume() {
        if (isPaused()) {
            setPaused(false);
        }
    }

    /**
     * Checks if the simulatio is paused are not.
     * 
     * @return true if the simulation and thus this manager and its entities are
     *         paused
     */
    public boolean isPaused() {
        return paused && running;
    }

    /**
     * Sets if this manager is paused or not.
     * 
     * @param b
     *            the new situation for the paused state, true=paused, false=
     *            not paused
     */
    private void setPaused(boolean b) {
        paused = b;
    }

    /**
     * The boolean indicating if this manager is paused or not.
     */
    private boolean paused = false;

    /**
     * Checks if this manager or simulation is running or not.
     * 
     * @return true if this manager is running or not.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Sets the running state of this simulation or manager.
     * 
     * @param b
     *            the new state of running, true=running
     */
    private void setRunning(boolean b) {
        running = b;
    }

    /**
     * The boolean indicating if this manager is running or not
     */
    private boolean running = false;

}