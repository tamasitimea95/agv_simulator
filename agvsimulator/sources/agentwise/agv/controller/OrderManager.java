/*
 * Created on 15-Nov-2004
 *
 */
package agentwise.agv.controller;

import java.util.ArrayList;
import java.util.Collection;

import agentwise.agv.model.environment.Order;

/**
 * An OrderManager manages the Order instances
 * present in the system. When an Order is added
 * to the system then it should also be added to
 * this OrderManager. There is only one instance
 * of this class, due to the singleton pattern
 * used.
 * 
 * @author tomdw
 */
public class OrderManager {

    /**
     * Constructs an instance of OrderManager
     * without any orders registered with it.
     * 
     */
    private OrderManager() {
        super();
        
    }
    
    /**
     * The order instances registered with this
     * OrderManager
     */
    private Collection orders = new ArrayList();
    
    /**
     * Returns a collection with all
     * the Order instances currently added
     * to this OrderManager. These should
     * be equal to all the Order instances
     * currently present in the system.
     * 
     * @return a Collection of Order instances
     */
    public Collection getCurrentOrders() {
        return new ArrayList(orders);
    }
    
    /**
     * Registers a given Order instance with 
     * this OrderManager as being present in the system.
     * If the given order is null or is already registered
     * with this OrderManager then nothing happens.
     * 
     * @param o The Order to register
     */
    public void addOrder(Order o) {
        if (o!=null && !orders.contains(o)) {
            orders.add(o);
        }
    }

    /**
     * Returns the only instance of this OrderManager
     * present in this system run.
     * 
     * @return The instance of OrderManager
     */
    public static OrderManager getInstance() {
        if (instance==null) instance = new OrderManager();
        return instance;
    }
    
    /**
     * The only instance of OrderManager
     */
    private static OrderManager instance = null;

    /**
     * Removes a given Order from this OrderManager.
     * This should only be done when the order is no longer
     * in this system.
     * 
     * @param o The Order instance to remove
     */
    public void removeOrder(Order o) {
        orders.remove(o);
    }

    /**
     * Resets the OrderManager so that there are no orders left in it
     */
    public void reset() {
        orders.clear();
        Order.reset();
    }
}
