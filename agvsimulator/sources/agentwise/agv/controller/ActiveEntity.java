/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.controller;

/**
 * This abstract class has all the functionality
 * that is needed for an active entity in this
 * simulation. The entity extending this class
 * only has to implement the step() method that
 * must execute one step in the simulation of
 * that entity.
 *
 * Constructing an instance of this class automatically
 * registers it as a to manage active entity with
 * the ActiveEntityManager. This manager must be used
 * to start, stop, pause, or resume the activity
 * in this simulation.
 *
 * @author tomdw
 *
 */
public abstract class ActiveEntity implements Runnable {

    /**
     * This is the time that the active entity sleeps
     * each cycle so that other threads can become active
     *
     */
    public static final int SLEEPTIME = 2;

    /**
     *
     * Constructs an instance of ActiveEntity
     * and registers it as a to manage Active Entity
     * with ActiveEntityManager. The entity is not started
     * yet, this should be done through the ActiveEntityManager.
     *
     *
     */
    public ActiveEntity() {
        manager = ActiveEntityManager.getInstance();
        manager.addActiveEntity(this);
    }

    /**
     * A local reference to the active entity manager
     */
    private ActiveEntityManager manager = null;

    /**
     * This method is called by the thread to run.
     *
     * @see java.lang.Runnable#run()
     */
    public void run() {
        initialize();
        while (!stop) {
            try {
                Thread.sleep(SLEEPTIME);
            } catch (InterruptedException e) {
                /* thread has sleeped in order to enable
                 * other treads to get active
                 */
            }
            //-- BEHAVIOUR OF EACH STEP --//

            step();

            //-- END BEHAVIOUR OF EACH STEP --//

            // pause if necessary
            while (manager.isPaused()) {
                try {
                    Thread.sleep(SLEEPTIME);
                } catch (InterruptedException e) {
                    /* while pausing, the tread should
                     * regularly sleep in order to 
                     * enable other threads to get 
                     * active
                     */
                }
            }
        }
        cleanUp();
    }

    /**
     * Allows to deactivate this active entity. The
     * effect is that the thread controlling this entity
     * will stop running.
     *
     */
    protected void deactivate() {
        stop = true;
    }

    /**
     * Is set to true if the thread should stop
     * running.
     */
    private boolean stop = false;

    /**
     * Activates this entity by starting
     * a thread with it. This is only
     * done if the
     * ActiveEntityManager has started running
     * the simulation. Otherwise nothing happens.
     *
     */
    void activate() {
        if (manager.isRunning()) {
            Thread t = new Thread(this);
            t.start();
        }
    }

    /**
     * Executes one step of the simulation of this entity.
     * This method mus be implemented by each subclass
     * of ActiveEntity.
     *
     */
    public abstract void step();
    
    /**
     * Executes the clean up code that needs to be done after this
     * active entity has stopped executing. The default behaviour
     * is no operation.
     *
     */
    public void cleanUp() {
        
    }
    
    
    /**
     * Executes behavior that runs once at the very start of the ActiveEntity's
     * life. Default does nothing.
     */
    public void initialize() {
 
    }
}
