/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.controller;

import java.util.ArrayList;
import java.util.Collection;

import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.map.Map;

/**
 * This class represents a simulation. It contains the necessary
 * parts of that simulation and allows to inspect and access them.
 * 
 * @author tomdw, nelis
 *
 */
public class Simulation {
    

    /**
     * The OrderLocationManager that manages the behaviour of 
     * all OrderLocations in the simulation */
    private OrderLocationManager orderLocationManager;
    
    /**
     * The ActiveEntityManager that manages the activity of
     * the whole simulation, ie its ActiveEntities
     */
    private ActiveEntityManager aemanager = ActiveEntityManager.getInstance();
    
    /**
     * The Environment instance for the simulation
     * responsible for handling influences/actions
     */
    private Environment environment;
    
    /**
     * The collection of Agv instances that
     * are part of this simulation
     */
    private Collection agvs = new ArrayList();
    
    private Map map;
    
    /**
     * Constructs an empty instance of Simulation.
     * Building a Simulation is done through
     * the SimulationBuildDirector.
     *
     */
    public Simulation() {}
    
    /**
     * Sets the current environment of the simulation to
     * the given one.
     * 
     * @param env The given Environment instance
     * @throws IllegalArgumentException when the given environment is null
     */
    public void setEnvironment(Environment env) throws IllegalArgumentException {
        if (env==null) throw new IllegalArgumentException("A simulation needs an environment, it can not be null");
        environment = env;
    }
    
    
    
    /**
     * Returns the Environment instance of this
     * Simulation. The Environment is responsible for
     * handling influences/actions and providing
     * the communication infrastructure.
     * 
     * @return an instance of Environment
     */
    public Environment getEnvironment() {
        return environment;
    }
    
    /**
     * Adds a given Agv instance to the Simulation. 
     * 
     * @param agv The given Agv instance
     * @throws IllegalArgumentException when the given Agv is null
     */
    public void addAgv(Agv agv) throws IllegalArgumentException {
        if (agv==null) throw new IllegalArgumentException("The agv to add cannot be null");
    	agvs.add(agv);    	
    }
    
    
    /**
     * Removes a given Agv instance to the Simulation.
     * 
     * @param agv The given Agv instance
     * @throws IllegalArgumentException when the given Agv is null
     */
    public void removeAgv(Agv agv) {
        if (agv!=null) agvs.remove(agv);    	
    }
    
    /**
     * Returns a collection of Agvs that
     * are currently situated in this simulation.
     * It returns an empty collection if no
     * Agvs are present.
     * 
     * @return a instance of Collection containing Agv instances
     */
    public Collection getAgvs(){
    	return agvs;
    }

    
      
    /**
     * Starts the simulation and its active entities.
     * If the simulation is already running then nothing
     * happens.
     *
     */
    public void start() {
        aemanager.start();
    }
    
    /**
     * Indicates if this Simulation is running or not.
     * A simulation is also running if it is paused.
     * Use 'isPaused' to check this.
     * 
     * @return true if the simulation is running, false otherwise
     */
    public boolean isRunning() {
        return aemanager.isRunning();
    }
    
    /**
     * Stops the simulation and its active entities.
     * If no simulation is running nothing happens.
     *
     */
    public void stop() {
        aemanager.stop();
    }
    
    /**
     * Pauses the simulation and its active entities. If
     * there is no simulation running or the simulation is
     * already paused nothing happens.
     *
     */
    public void pause() {
        aemanager.pause();
    }
    
    /**
     * Resumes the simulation and its active entities. If the
     * simulation is not paused and running then nothing happens
     *
     */
    public void resume() {
        aemanager.resume();
    }

    /**
     * Indicates if the simulation is paused or not.
     * 
     * @return true if the simulation is paused, false otherwise
     */
    private boolean isPaused() {
        return aemanager.isPaused();
    }
    
    /**
	 * Returns the OrderLocationManager that this Simulation
	 * uses to manage the behaviour of the OrderLocations present
	 * in the simulation.
	 * 
	 * @return Returns the orderLocationManager.
	 */
	public OrderLocationManager getOrderLocationManager() {
		return orderLocationManager;
	}
	
	/**
	 * Sets the OrderLocationManager that this Simulation
	 * will use to manage the behaviour of the OrderLocations 
	 * present in the simulation
	 * 
	 * @param orderLocationManager The orderLocationManager to set.
	 * @throws IllegalArgumentException when the given orderLocationManager is null
	 */
	public void setOrderLocationManager(OrderLocationManager orderLocationManager) throws IllegalArgumentException {
		if (orderLocationManager==null) throw new IllegalArgumentException("The given OrderLocationManager for a Simulation cannot be null");
	    this.orderLocationManager = orderLocationManager;
	}
    /**
     * @return Returns the map.
     */
    public Collection getSegments() {
        return map.getSegments();
    }
    /**
     * @param map The map to set.
     */
    public void setMap(Map map) {
        this.map = map;
    }
}
