/*
 * Created on Oct 12, 2004
 *
 */
package agentwise.agv.controller;

import java.util.Collection;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.InputOrderLocationManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.communication.AdHocInfrastructure;
import agentwise.agv.model.environment.communication.CommunicationInfrastructure;
import agentwise.agv.model.environment.communication.CommunicationNode;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.map.Map;
import agentwise.agv.model.environment.map.MapComponent;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;

/**
 * This aspect weaves in at pointcuts where something 
 * interesting happens. For example, changes that could
 * interest a user interface etc.
 * 
 * @author tomdw
 * 
 */
public aspect ChangeObserverAspect {
    
    // NEW THINGS TO OBSERVE + SIMULATION RUN
    
    /**
     * Pointcut identifies where an Agv is added to the Simulation/Environment
     */
    pointcut addAgv(AgvEnvironmentState s): call (* Environment.registerAgv(..)) && args(s);
    
    /**
     * Advise executes after an agv add succeeded by signalling this
     * to the ChangeManager
     */
    void around(AgvEnvironmentState s): addAgv(s) {
        proceed(s); // throws
        ChangeManager.getInstance().signalAgvAddedEvent(s);
    }
    
    /**
     * Pointcut identifies where a new simulation is started to be constructed
     */
    pointcut startConstructionNewSimulation(): call(* Controller.constructSimulation(..));
    
    /**
     * Advise executes before the construction of a new simulation is
     * started.
     */
    before(): startConstructionNewSimulation() {
        ChangeManager.getInstance().signalStartConstructionNewSimulationEvent();
    }
    
    /**
     * Pointcut identifies where a new simulation is set as the current simulation
     */
    pointcut newSimulationSet(Simulation sim): call (* Controller.setNewSimulation(..)) && args(sim);

    /**
     * Advise executes after setting the current simulation to a new one succeeded
     */
    void around(Simulation sim): newSimulationSet(sim) {
        proceed(sim); // throws
        ChangeManager.getInstance().signalNewSimulationSetEvent(sim);
    }
    
    /**
     * Pointcut identifies where a Map is added to the Simulation/Environment
     */
    pointcut addSegments(Collection s): call (* Map.addSegments(..)) && args(s);
    
    /**
     * Advise executes after an map add succeeded by signalling this
     * to the ChangeManager
     */
    void around(Collection segments): addSegments(segments) {
        proceed(segments); // throws
        ChangeManager.getInstance().signalMapAddedEvent(segments);
    }
    
    // AGV CHANGES
    
    /**
     * Pointcut identifies where an agv changed. 
     * It is assumed that all changes are reflected in the
     * AgvEnvironmentState.
     *
     */
    pointcut agvChange(AgvEnvironmentState s): (
            call(* AgvEnvironmentState.setCoordinate(..)) 
            || call(* AgvEnvironmentState.setCurrentMapComponent(..))
            || call(* AgvEnvironmentState.setDirection(..))
            || call(* AgvEnvironmentState.setCurrentOrder(..))
            || call(* AgvEnvironmentState.removeCurrentOrder(..))
            || call(* AgvEnvironmentState.suspend(..))
    ) && target(s);

    /**
     * Advise executes after an Agv has changed. It is assumed
     * that after each behavioural step this signal is needed.
     */
    after(AgvEnvironmentState state): agvChange(state) {
        ChangeManager.getInstance().signalAgvChangedEvent(state);
    }
    
    // SEGMENT CHANGES
    
    /**
     * Pointcut identifies where a segment has changed. Also
     * changes in pheromones situated on that segment
     * are triggered. This is done by assuming that a change
     * in the submodule pheromoneinfrastructure is notified
     * to the segment through a changeInSubModule() method.
     */
    pointcut segmentChange(Segment s): (
            /*call (* Segment.addNextPoint(..))
            || call (* Segment.setEnd(..))
            || call (* Segment.setStart(..))
            ||*/ call (* MapComponent.dropPheromone(..))
            || call (* MapComponent.changeInSubModule(..))
            ) && target(s);
    
    /**
     * Advise executes after a segment change succeeded
     */
    void around(Segment s): segmentChange(s) {
        proceed(s); // throws
        ChangeManager.getInstance().signalSegmentChangedEvent(s);
    }
    
    // STATION CHANGES
    
    /**
     * Pointcut identifies where a station has changed. Also
     * changes in pheromones situated on that station
     * are triggered. This is done by assuming that a change
     * in the submodule pheromoneinfrastructure is notified
     * to the station through a changeInSubModule() method.
     */
    pointcut stationChange(Station s):(
            /*call (* Station.addInSegment(..))
            || call (* Station.addOutSegment(..))
            ||*/ call (* MapComponent.dropPhermone(..))
            || call (* Station.setCoordinate(..))
            ) && target(s);
    
    /**
     * Advise executes when a station change succeeded
     */
	void around(Station s): stationChange(s){
	    proceed(s); // throws
	    ChangeManager.getInstance().signalStationChangedEvent(s);
	}
	
	/**
	 * Pointcut identifies where a orderlocation gets an order set to it
	 */
	pointcut orderLocationOrderChange(OrderLocation l): call(* OrderLocation.setOrder(..)) && target(l);
	
	/**
	 * Advise executes after the successfull execution of setting an order to the orderlocation
	 * @param l
	 */
	after(OrderLocation l) returning: orderLocationOrderChange(l) {
	    ChangeManager.getInstance().signalOrderLocationChangedEvent(l);
	}
	
	
	// COMMUNICATION
	
	/**
	 * Pointcut identifies where a broadcast is done
	 */
	pointcut broadCast(AdHocInfrastructure infra, Message msg, CommunicationNode cNode): call (* CommunicationInfrastructure.broadcast(..)) && args(cNode,msg) && target(infra);

	/**
	 * Advise executes after the occurence of a broadcast
	 */
	after(AdHocInfrastructure infra, Message msg, CommunicationNode cNode): broadCast(infra,msg, cNode) {
	    ChangeManager.getInstance().signalBroadCastEvent(cNode.getCoordinate(), msg,infra.getCommunicationRange());
	}
	
	/**
	 * Pointcut identifies where a unicast is sent
	 * 
	 * @param infra The used CommunicationInfrastructure
	 * @param receiver The NetworkID of the receiver
	 * @param msg The Message sent
	 * @param senderNode The CommunicationNode of the sender
	 */
	pointcut unicast(CommunicationInfrastructure infra, NetworkID receiver, Message msg, CommunicationNode senderNode): call (* CommunicationInfrastructure.unicast(..)) && args(senderNode,receiver,msg) && target(infra);
	
	/**
	 * Advise executes when sending an unicast succeeded
	 * 
	 */
	after(CommunicationInfrastructure infra, NetworkID receiver, Message msg, CommunicationNode senderNode) returning: unicast(infra, receiver, msg, senderNode) {
	    ChangeManager.getInstance().signalUniCastEvent(senderNode.getCoordinate(), msg, receiver);
	}
	
	/**
	 * Pointcut identifies when an agv receives a message from its inbox
	 * 
	 * @param agvs The state of the Agv when receiving
	 */
	pointcut receive(AgvEnvironmentState agvs): call (Message AgvEnvironmentState.retrieveMessage(..)) && target(agvs);
	
	/**
	 * Advise executes before a receive is done
	 * and retreives the message that is to be received
	 */
	before(AgvEnvironmentState agvs): receive(agvs) {
	    msgReceived = agvs.getInbox().look();
	}
	
	/**
	 * The message to be received
	 */
	private Message msgReceived = null;
	
	/**
	 * Advise executes after a receive is done
	 * and uses the message that was retreived
	 * in the before advise as the message received
	 */
	after(AgvEnvironmentState agvs) returning: receive(agvs) {
	    if (msgReceived!=null) ChangeManager.getInstance().signalReceiveEvent(agvs.getAgvID(),msgReceived);
	}
	
	/**
	 * This pointcut identifies when a new order is added to the OrderManager
	 */
	pointcut newOrder(OrderManager om, Order o): call (* OrderManager.addOrder(..)) && target(om) && args(o);
	
	/**
	 * This advice executes after a new order was added to
	 * the OrderManager, interested changeobservers are
	 * notified
	 */
	after(OrderManager om, Order o) returning: newOrder(om,o) {
	    ChangeManager.getInstance().signalNewOrderEvent(om,o);
	}
	
	/**
	 * This pointcut identifies when an order is removed
	 * from the OrderManager
	 */
	pointcut removeOrder(OrderManager om, Order o): call (* OrderManager.removeOrder(..)) && target(om) && args(o);
	
	/**
	 * This pointcut identifies when the OrderManager
	 * is reset to an empty ordermanager
	 */
	pointcut resetOrderManager(OrderManager om): call (* OrderManager.reset(..)) && target(om);

	/**
	 * After an order is removed, this is signalled to
	 * registered changeobservers
	 */
	after(OrderManager om, Order o) returning: removeOrder(om,o) {
	    ChangeManager.getInstance().signalRemoveOrderEvent(om,o);
	}
	
	/**
	 * After the OrderManager was reset this
	 * should be signalled to the registered
	 * changeobservers
	 */
	after(OrderManager om) returning: resetOrderManager(om) {
	    ChangeManager.getInstance().signalRemoveOrderEvent(om,null);
	}
	
	/**
	 * This pointcut identifies the event where an InputOrderLocationManager
	 * is used as the orderlocationmanager.
	 */
	pointcut usesInputOrderLocationManager(InputOrderLocationManager om): call (* Simulation.setOrderLocationManager(..)) && args(om);
	
	/**
	 * After an InputOrderLocationManager was set, interested
	 * changeobservers (e.g. user interface to show the
	 * input interface) are notified
	 */
	after(InputOrderLocationManager om) returning: usesInputOrderLocationManager(om) {
	    ChangeManager.getInstance().signalStartUseOfInputOrderLocationManager(om);
	}
}
