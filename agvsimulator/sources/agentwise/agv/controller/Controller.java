/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import agentwise.agv.controller.builder.DefaultAgvBuilder;
import agentwise.agv.controller.builder.InformationMapBuilder;
import agentwise.agv.controller.builder.MapBuilder;
import agentwise.agv.controller.builder.ParseException;
import agentwise.agv.controller.builder.SimulationBuildDirector;
import agentwise.agv.elteMod.FixedOrderLocationManager;
import agentwise.agv.elteMod.OneEdgeLookAheadBehavior;
import agentwise.agv.elteMod.Scheduler;
import agentwise.agv.elteMod.TwoEdgeLookAheadBehavior;
import agentwise.agv.elteMod.Logger;
import agentwise.agv.elteMod.SimpleBehavior;
import agentwise.agv.model.agv.behaviour.RandomPathBehaviour;
import agentwise.agv.model.environment.InputOrderLocationManager;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.SimpleOrderLocationManager;
import agentwise.agv.model.environment.communication.AdHocInfrastructure;

/**
 * This is the facade to the controller part of the
 * system. It allows the gui to send commands to the system
 * and the controller part will then execute those commands and
 * adjust the simulation model accordingly.
 * 
 * The Controller implements the Singleton pattern so that
 * only one instance can exist and this was the status of the
 * system can be controlled in this class.
 * 
 * @author tomdw, nelis
 *
 */
public class Controller {
    
    /**
     * 
     * Constructs an instance of Controller.
     * 
     */
    private Controller() { }
    
    /* The only instance of the Controller class */
    private static Controller instance;
    
    /* The simulation instance that is present currently */
    private Simulation simulation = null;
    
    /**
     * Sets the current simulation to the given instance.
     * 
     * @param sim The new current simulation
     * @throws IllegalArgumentException when the given simulation is null
     */
    private void setNewSimulation(Simulation sim) throws IllegalArgumentException {
        if (sim==null) throw new IllegalArgumentException("the given simulation cannot be null");
        simulation = sim;
        OrderManager.getInstance().reset();
    }
    
    /**
     * Returns the current simulation
     * 
     * @return a Simulation instance
     */
    Simulation getSimulation() {
        return simulation;
    }
    
    public void constructSimulation(String fileName) {
    	try {
			constructSimulation(fileName, "E:");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
    /**
     * Constructs the simulation model, sets
     * it as the new current simulation, but
     * does not start the simulation.
     * 
     * In this operation, all global parameters should be set
     * (if you need to set a parameter you can not acces here,
     * contact your supervisor)
     * @throws IOException 
     */
    public void constructSimulation(String fileName, String logDir) throws ParseException, IOException {
        // construct the simulation
    	/*
    	 * Debug levels:
    	 * 1 - basic, you can reconstruct the behaviour of agvs
    	 * 2 - display the log of Scheduler
    	 * 3 - display the log of Logger
    	 * 4 - display the log of help functions
    	 */
        Logger.Initialize(1, logDir + "\\" + fileName.split("\\\\")[fileName.split("\\\\").length - 1]+".log.txt", this);
        
        //HOTSPOT, hier wordt alles geconfigureerd
        
        SimulationBuildDirector simBuildDirect = new SimulationBuildDirector();
        
        // Builders
        simBuildDirect.setAgvBuilder(new DefaultAgvBuilder());
        simBuildDirect.setMapBuilder(new MapBuilder());
        simBuildDirect.setImapBuilder(new InformationMapBuilder());
        
        // Own behaviors
        
        //simBuildDirect.setOrderLocationManager(new SimpleOrderLocationManager());
        FixedOrderLocationManager locationManager = new FixedOrderLocationManager(fileName, this);
        simBuildDirect.setOrderLocationManager(locationManager);
        Scheduler.getInstance().setLastOrderRound(locationManager.getLastOrderRound());
        
        //simBuildDirect.setDefaultBehaviorModule(new SimpleBehavior());
        //simBuildDirect.setDefaultBehaviorModule(new RandomPathBehaviour());
        //simBuildDirect.setDefaultBehaviorModule(new OneEdgeLookAheadBehavior());
        simBuildDirect.setDefaultBehaviorModule(new TwoEdgeLookAheadBehavior());
        simBuildDirect.setComInfrastructure(new AdHocInfrastructure(500.0));
        
        // General parameters
        simBuildDirect.setFileName(fileName);
        // TODO [nelis] identify and locate general parameters
        
        
        // Finaly, build the thing!
        simBuildDirect.buildSimulation();
            
        
        //new ConcreteSimulationBuilder()
        setNewSimulation(simBuildDirect.getSimulation());
    }

    /**
     * Returns the only instance of controller that can
     * be present in the system
     * 
     * @return the instance of Controller
     */
    public static Controller getInstance() {
        if (instance==null) instance = new Controller();
        return instance;
    }

    /**
     * Starts the current simulation. If that simulation is 
     * already running nothing happens. If no simulation is
     * present then also nothing happens.
     */
    public void startSimulation() {
        if (simulation!=null) simulation.start();
    }

    /**
     * Stops the current simulation. If there is no simulation
     * running then nothing happens
     */
    public void stopSimulation() {
    	Logger.Send("End of the log.");
        Logger.CloseLogFile();
        if (simulation!=null) simulation.stop();
    }
    
    /**
     * Pauses the current simulation. If there is no 
     * simulation nothing happens.
     *
     */
    public void pauseSimulation() {
        if (simulation!=null) simulation.pause();
    }
    
    /**
     * Resumes a paused simulation. If there is no
     * simulation or the simulation is not paused, nothing
     * happens.
     *
     */
    public void resumeSimulation() {
        if (simulation!=null) simulation.resume();
    }

    /**
     * Adds a new order to the OrderLocationManager of the current simulation
     * in order for that location manager to add it to the environment.
     * This method only has effect if the current OrderLocationManager
     * is a InputOrderLocationManager
     * 
     * @param fromStationId The station to put the order at
     * @param toStationId The station that is the destination for the order
     * @param priority The priority of the order
     */
    public void addOrder(String fromStationId, String toStationId, int priority) {
        if (simulation.getOrderLocationManager() instanceof InputOrderLocationManager) {
            InputOrderLocationManager m = (InputOrderLocationManager)simulation.getOrderLocationManager();
            m.addOrder(new Order(fromStationId, toStationId, priority));
        }
    }

    /**
     * Returns true if the current simulation is not stopped, but running
     * @return True if running, false otherwise
     */
    public boolean isRunning() {
        boolean result = false;
        if (getSimulation()!=null) {
            result = getSimulation().isRunning();
        }
        return result;
    }

}
