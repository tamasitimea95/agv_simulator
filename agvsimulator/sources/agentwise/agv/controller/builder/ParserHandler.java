package agentwise.agv.controller.builder;
/**
 * Interface for the handlers of our parser (Parser.java)
 * This interface defines for each tag in our XML-map a
 * start- and an end-method. Thus classes implementing this interface
 * specify what happens if the particilar tag is found.
 *     
 * @author nelis
 */
public interface ParserHandler
{
	/**
	 * Starttag AGV-map
	 * 
	 * @param name The name of this map
	 */
    public void startAgvMap(String name) throws Exception;
    
    /**
     * Endtag AGV-map
     */
    public void endAgvMap() throws Exception;

    /**
     * Starttag environment
     * 
     * @param width The with of the environment, specified in pixels
     * @param height The height of the environment, specified in pixels      
     */
    public void startEnvironment(double width, double height) throws Exception;
    /**
     * Endtag environment 
     */
    public void endEnvironment() throws Exception;
    
    /**
     * Starttag Stations
     */
    public void startStations() throws Exception;
    /**
     * Endtag Stations 
     */
    public void endStations() throws Exception;
    /**
     * Starttag Station
     * @param id The unique identifier of a station
     * @throws Exception
     */
    public void startStation(String id) throws Exception;
    /**
     * Endtag Station 
     */
    public void endStation() throws Exception;

    /**
     * Starttag AGVs
     * 
     */
    public void startAgvs() throws Exception;
    /**
     * Endtag Agvs 
     */
    public void endAgvs() throws Exception;

    /**
     * Starttag AGV
     * @param id The unique identifier of an AGV
     * @param startStation
     */
    public void startAgv(String id, String startStation) throws Exception;
    /**
     * Endtag Agv 
     */
    public void endAgv() throws Exception;
    
    /**
     * Starttag Locations 
     */
    public void startLocations() throws Exception;
    
    /**
     * Endtag Locations 
     */
    public void endLocations() throws Exception;
    
    /**
     * Starttag of a Location
     * @param id The unique identifier of a Location
     * @throws Exception
     */
    public void startLocation(String id, String type) throws Exception;
    /**
     * Endtag Location 
     */
    public void endLocation() throws Exception;

    /**
     * Starttag of a coordinate 
     * @param x The x-component of the coordinate
     * @param y The y-component of the coordinate
     * @throws Exception
     */
    public void startCoordinate(double x, double y) throws Exception;
    /**
     * Endtag Coordinate 
     */
    public void endCoordinate() throws Exception;
    /**
     * Start of describing the segments
     * @throws Exception
     */
    public void startSegments() throws Exception;
    /**
     * Endtag Segments 
     */
    public void endSegments() throws Exception;

    public void startSegment(String id, String from, String to) throws Exception;
    /**
     * Endtag Segment 
     */
    public void endSegment() throws Exception;  
}
