/*
 * Created on Jan 31, 2005
 * 
 */
package agentwise.agv.controller.builder;

import agentwise.agv.controller.Simulation;

import agentwise.agv.model.agv.ActionExecutionModule;
import agentwise.agv.model.agv.DefaultCommunicationModule;
import agentwise.agv.model.agv.PerceptionModule;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.communication.CommunicationInfrastructure;
import agentwise.agv.model.environment.map.Station;

import agentwise.agv.elteMod.SimpleGlobal;

/**
 * @author nelis
 *

 */
public class SimulationParserHandler implements ParserHandler {

    protected SimulationBuildDirector sbd;
    
    /**
     * 
     */
    public SimulationParserHandler(SimulationBuildDirector sbd) {
        super();
        setSbd(sbd);        
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startStations()
     */
    public void startStations() throws Exception{}

    /**
     * @see agentwise.agv.controller.builder.ParserHandler#startStation(java.lang.String)
     */
    public void startStation(String id) throws Exception{
        getMapBuilder().setCurrentId(id);
        getImapBuilder().addNode(id);
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startSegments()
     */
    public void startSegments() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startSegment(java.lang.String, java.lang.String, java.lang.String)
     */
    public void startSegment(String id, String from, String to) throws Exception{
        getMapBuilder().createSegment(id,from,to);
        getImapBuilder().addLink(id,from,to);
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startLocations()
     */
    public void startLocations() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startLocation(java.lang.String, java.lang.String)
     */
    public void startLocation(String id,String type) throws Exception{
        getMapBuilder().setCurrentId(id);
        getMapBuilder().setType(type);
        getImapBuilder().addNode(id);
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startEnvironment(double, double)
     */
    public void startEnvironment(double width, double height) throws Exception{
        getSbd().generateEnvironment(width,height);
        getMapBuilder().setOrderLocationManager(getOrderLocationManager());        
        getSbd().integrateOrderLocationManagerWithTimeManagement(getOrderLocationManager());        
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startCoordinate(double, double)
     */
    public void startCoordinate(double x, double y) throws Exception{
        getMapBuilder().setCurrentCoordinate(x,y);
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startAgvs()
     */
    public void startAgvs() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#startAgvMap(java.lang.String)
     */
    public void startAgvMap(String name) throws Exception{}

    /**
     * 
     * @see agentwise.agv.controller.builder.ParserHandler#startAgv(java.lang.String, java.lang.String)
     */
    public void startAgv(String id, String startStation) throws Exception{
    	
    	SimpleGlobal.NMB_AGVs++;
    	
        getAgvBuilder().setDirection(180.0);
    
        Station s = getSbd().getMapBuilder().getStation(startStation);
        getAgvBuilder().setStartStation(s);
        
        
        getAgvBuilder().setPerceptionModule(new PerceptionModule());
        getAgvBuilder().setActionExecutionModule(new ActionExecutionModule());
        getAgvBuilder().setBehaviourModule(generateNewBehaviourModule());
        getAgvBuilder().setCommunicationModule(new DefaultCommunicationModule());
        getAgvBuilder().setInformationMap(getImapBuilder().generateInformationMap());
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endAgv()
     */
    public void endAgv() throws Exception{        
        getAgvBuilder().build();
        getSimulation().addAgv(getAgvBuilder().getAgv());
        getAgvBuilder().resetBuilder();
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endAgvMap()
     */
    public void endAgvMap() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endAgvs()
     */
    public void endAgvs() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endCoordinate()
     */
    public void endCoordinate() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endEnvironment()
     */
    public void endEnvironment() throws Exception{        
        //simBuilder.setSegments(mapBuilder.getSegments());
        getSimulation().setMap(getMapBuilder().generateMap());                
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endLocation()
     */
    public void endLocation() throws Exception{
        getMapBuilder().generateLocation();
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endLocations()
     */
    public void endLocations() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endSegment()
     */
    public void endSegment() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endSegments()
     */
    public void endSegments() throws Exception{}

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endStation()
     */
    public void endStation() throws Exception{
        getMapBuilder().generateStation();
    }

    /**
     *
     * @see agentwise.agv.controller.builder.ParserHandler#endStations()
     */
    public void endStations() throws Exception{}

    /**
     * @return Returns the sbd.
     */
    protected SimulationBuildDirector getSbd() {
        return sbd;
    }
    /**
     * @param sbd The sbd to set.
     */
    protected void setSbd(SimulationBuildDirector sbd) {
        this.sbd = sbd;
    }

    /**
     * @return Returns the agvBuilder.
     */
    public AgvBuilder getAgvBuilder() {
        return getSbd().getAgvBuilder();
    }

    /**
     * @return Returns the comInfrastructure.
     */
    public CommunicationInfrastructure getComInfrastructure() {
        return getSbd().getComInfrastructure();
    }

    /**
     * @return Returns the defaultBehaviorModule.
     */
    public BehaviourModule generateNewBehaviourModule() {
        return getSbd().generateNewBehaviourModule();
    }

       /**
     * @return Returns the imapBuilder.
     */
    public InformationMapBuilder getImapBuilder() {
        return getSbd().getImapBuilder();
    }

    /**
     * @return Returns the mapBuilder.
     */
    public MapBuilder getMapBuilder() {
        return getSbd().getMapBuilder();
    }

    /**
     * @return Returns the orderLocationManager.
     */
    public OrderLocationManager getOrderLocationManager() {
        return getSbd().getOrderLocationManager();
    }

    /**
     * @return the simulation
     */
    public Simulation getSimulation(){
        return getSbd().getSimulation();
    }
}
