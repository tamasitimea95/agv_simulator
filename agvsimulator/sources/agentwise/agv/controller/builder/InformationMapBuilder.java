/*
 * Created on Dec 3, 2004
 * 
 */
package agentwise.agv.controller.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.agv.map.InformationMap;
import agentwise.agv.model.agv.map.Link;
import agentwise.agv.model.agv.map.Node;

/**
 * @author nelis
 *
 * This class is responsible for building the InformationMap of an AGV
 * (using a Builder like pattern)
 */
public class InformationMapBuilder{

 
    /**
     * Add a node
     * @param id The identifier for this station
     * @throws Exception
     */
    public void addNode(String id) throws Exception {
        // TODO [Davy S] Statische kost toevoegen
        nodes.add(new Node(id));
    }

 /**
  * Add a new link
  * @param id The identifier
  * @param from The station identifier from where the segment starts
  * @param to The station identifier where the segment ends
  * @throws Exception
  */
    public void addLink(String id, String from, String to)
            throws Exception {
        // TODO [Davy S] Statische kost toevoegen
        Link l = new Link(id,getNode(nodes,from),getNode(nodes,to));
        links.add(l);
    }

 /**
  * Generate a copy of the InformationMap
  * @return a copy of the information map
  */
    public InformationMap generateInformationMap(){
       ArrayList newNodes = new ArrayList();
       ArrayList newLinks = new ArrayList();
       
       Iterator i = nodes.iterator();
       while(i.hasNext()){
           newNodes.add(new Node(((Node)i.next()).getID()));           
       }
       
       i = links.iterator();
       while(i.hasNext()){
           Link l = (Link) i.next();
           newLinks.add(new Link(l.getID(),getNode(newNodes,l.getStartNode().getID()),
                   getNode(newNodes,l.getEndNode().getID())));         
       }
        
        
       InformationMap imap = new InformationMap(newNodes, newLinks);
       return imap;
    }
    
    
    
    /**
     * The collection of links in this map
     */
    protected Collection links = new ArrayList();
    /**
     * The collection of nodes in this map
     */
    protected Collection nodes = new ArrayList();
    
    /**
     * Get the node with the given id
     * @param id The id to search for
     * @throws IllegalArgumentException If the id is not found
     */
    public Node getNode(Collection nodes, String id) throws IllegalArgumentException{
    	Iterator iter = nodes.iterator();
    	while(iter.hasNext()){
    		Node s = (Node) iter.next();
    		if(id.equals(s.getID())){
    			return s;
    		}
    	}
    	throw new IllegalArgumentException("Node ID does not exist");
    	//return null;
    }

}
