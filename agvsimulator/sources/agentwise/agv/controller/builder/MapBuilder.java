/*
 * Created on Oct 11, 2004
 *
 */
package agentwise.agv.controller.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.map.Map;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.util.Coordinate;

/**
 * Builds the map of the simulation
 * 
 * @author nelis
 */
public class MapBuilder {

    /**
     * The collection of segments in this map
     */
    protected Collection segments = new ArrayList();

    /**
     * The collection of stations in this map
     */
	protected Collection stations = new ArrayList();
	/**
	 * The order location manager of this simulation 
	 */
	protected OrderLocationManager orderLocationManager;
	
	/**
	 * The currentID
	 */
	protected String currentID;

    /**
     * The type     
     */
    protected String type;

    /**
     * The current coordinate
     */
	protected Coordinate currentCoord;
	
	/**
	 * The stepsize on a segment
	 */
	private double stepsize = 40;
	
	/**
	 * Constructor
	 *
	 */
	public MapBuilder(){		
	}	
    
	/**
	 * Add a segment to the collection of segments
	 * @param s The segment to add
	 */
    protected void addToSegments(Segment s){
    	segments.add(s);
    }
    
    /**
     * Add a station to the collection of stations
     * @param s
     */
    protected void addToStations(Station s){
    	stations.add(s);
    }

    /**
     * Get all segments
     */
    public Collection getSegments() {
        return segments;
    }
 
    /**
     * Get the station with the given id
     * @param id The id to search for
     * @throws IllegalArgumentException If the id is not found
     */
    public Station getStation(String id) throws IllegalArgumentException{
    	Iterator iter = stations.iterator();
    	while(iter.hasNext()){
    		Station s = (Station) iter.next();
    		if(id.equals(s.getID())){
    			return s;
    		}
    	}
    	throw new IllegalArgumentException("Station ID does not exist");
    	//return null;
    }
    
    /**
     * Sets the current id
     * @param id The id to set
     */
    public void setCurrentId(String id){
    	currentID = id;
    }

    /**
     * Sets the current type 
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Reset the values of a station
     *
     */
    public void resetStationValues(){
    	currentID = null;
    	type = null;
    	currentCoord = null;
    }
    
    /**
     * Set the current coordinate 
     * @param x The x-component of the coordinate
     * @param y The y-component of the coordinate
     */
    public void setCurrentCoordinate(double x, double y) {
    	currentCoord = new Coordinate(x,y);
    }
    
    /**
     * Generate a new station according to the previously given
     * information
     *
     */
    public void generateStation(){
    	addToStations(constructStation());
    	resetStationValues();
    }
    
    /**
     * Constructs the instance of Station used
     * when generating a Station
     * 
     * @return a Station instance with right ID and coordinate
     */
    protected Station constructStation() {
        return new Station(currentID,currentCoord);
    }

    /**
     * Generate a new location according to the previously given
     * information
     *
     */
    public void generateLocation(){
    	OrderLocation loc = constructOrderLocation();
		
    	if(this.type.equals("in")){
    		loc.setInOutProperties(true,false);
    		orderLocationManager.addPickOrderLocation(loc);
    	}
    	else if(this.type.equals("out")) {
    		loc.setInOutProperties(false,true);
    		orderLocationManager.addDropOrderLocation(loc);
    	}else{
    		loc.setInOutProperties(true,true);
    		orderLocationManager.addBufferOrderLocations(loc);
    	}    	
    	addToStations(loc);
    	resetStationValues();
    }

    /**
     * Constructs the instance of OrderLocation used
     * when generating a OrderLocation
     * 
     * @return a OrderLocation instance with right ID and coordinate
     */
    protected OrderLocation constructOrderLocation() {
        OrderLocation loc = new OrderLocation(currentID,currentCoord);
        return loc;
    }

    /**
     * Create a segment with the given id from station 'from' to station 'to'
     * @param id The id of this new segment
     * @param from The id of the station where the segment starts
     * @param to The id of the station where the segment ends
     */
    public void createSegment(String id, String from, String to) {
    	Station sfrom = getStation(from);
    	Station sto = getStation(to);
    	
    	Segment s = createSegment(id,sfrom,sto);
    	addToSegments(s);
    }
    
    /**
     * Set the stepsize of a segment
     * @param size The size
     * @throws IllegalArgumentException The size must be bigger or equal to one
     */
    public void setStepSize(int size) throws IllegalArgumentException {
        if (size<1) throw new IllegalArgumentException("The coordinates in a segment should at least be seperated by a stepsize of 1");
        stepsize = size;
    }
    
    public Map generateMap(){
        Map map = new Map();
        map.addSegments(getSegments());
        return map;
    }
    
    /**
     * Create a segment and calculates all its relevant coordinates
     * @param id The id of the segment to create
     * @param from The id of the station where the segment starts
     * @param to The id of the station where the segment ends
     * @return the newly generated segment
     */
    
    protected Segment createSegment(String id,Station from,Station to){
    	Segment segment = new Segment(from);
    	segment.setID(id);
    	
    	Coordinate start = from.getCoordinate();
    	Coordinate end = to.getCoordinate();
    	
    	Coordinate coord = new Coordinate(start.getX(),start.getY());
    	
    	int xsign = 1,ysign=1;
    	
    	if(start.getX() > end.getX()) { xsign=-1; } 
    	if(start.getY() > end.getY()) { ysign=-1; }
    	
    	if(start.getX() == end.getX()){
    	    double t = stepsize;
    		while(!(coord.distanceTo(end)<=stepsize)){
    			coord = new Coordinate(coord.getX(),coord.getY()+ysign*t);
    			segment.addNextPoint(coord);
    		}
    	}else if(start.getY() == end.getY()){
    	    double t = stepsize;
    		while(!(coord.distanceTo(end)<=stepsize)){
    			coord = new Coordinate(coord.getX()+xsign*t,coord.getY());
    			segment.addNextPoint(coord);
    		}
    	}else{
    		double t = 0.0;
    		while(!(coord.distanceTo(end)<=stepsize)){
    		    t = t + stepsize; 
    		    //double x = start.getX() + t*Math.cos(start.calculateAngleTowards(end).getRadians());
    		    //double y = start.getY() + -t*Math.sin(start.calculateAngleTowards(end).getRadians());
    			//coord = new Coordinate(x,y);
    		    coord = start.nextInAngle(start.calculateAngleTowards(end),t);
    			segment.addNextPoint(coord);			
    		}
    	}
    	
    	segment.setEnd(to);
    	
    	from.addOutSegment(segment);
    	to.addInSegment(segment);
    	
    	return segment;
    }
    
    /*public void startDimensions(double width, double height) throws Exception{}
    public void endDimensions() throws Exception{}*/
    
    //*******************************************************************
    
   /* public double createSegment(Station startStation, Station endStation, double prevDir, boolean test) {
        Segment segment = buildSegmentFromTo(startStation, endStation, new Angle(prevDir, false), test);
        if (!test) segments.add(segment);
        return lastDir.getDegrees();
    }
    
   private Segment buildSegmentFromTo(Station s1, Station s2, Angle prevDir, boolean test) throws IllegalArgumentException {
        double distance = s1.getCoordinate().distanceTo(s2.getCoordinate());
        if (distance <= (360/Math.PI)) throw new IllegalArgumentException("The distance between two stations must be at least "+ (360/Math.PI) + " , not "+distance);
        
        
        Segment segment = null;
        if (!test) segment = new Segment(s1);
        try {
            Coordinate currentC = s1.getCoordinate();
            Angle angleDirection = new Angle(prevDir);
            while (!currentC.isInRangeOf(s2.getCoordinate(),1)) {
                Angle angleGoal = currentC.calculateAngleTowards(s2.getCoordinate());
	            //if (Math.abs(angleGoal - angleDirection) >= 1.0) {
	                angleDirection.turnAngleTowards(angleGoal);
	            //} else {
	                double currX = currentC.getX();
	                double currY = currentC.getY();
	                double xplus = Math.cos(angleDirection.getRadians());
	                double yplus = -Math.sin(angleDirection.getRadians());
	                currentC = new Coordinate(currX + xplus, currY + yplus);
	                if (!test) segment.addNextPoint(currentC);
	            //}
	        }
	        if (!test) segment.setEnd(s2);
	        lastDir = angleDirection;
        } catch (IllegalArgumentException e) {
            System.out.println("There was an illegal segment buid");
        }
        
        if (!test) {
            s1.addOutSegment(segment);
            s2.addInSegment(segment);
        }
        return segment;
    }*/
	
    /**
     * @return Returns the orderLocationManager.
     */
    public OrderLocationManager getOrderLocationManager() {
        return orderLocationManager;
    }
    /**
     * @param orderLocationManager The orderLocationManager to set.
     */
    public void setOrderLocationManager(
            OrderLocationManager orderLocationManager) {
        this.orderLocationManager = orderLocationManager;
    }
}
