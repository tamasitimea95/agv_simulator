package agentwise.agv.controller.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class parses an XML-document containing the simulation.
 * The parser uses the EchoParserHandler to output the XML-file
 * to the screen and the ParserHandler to performs some actions on 
 * parsing.
 * 
 * @author nelis
 */
public final class Parser extends DefaultHandler
{
    /**
     * The locator of the document being parsed.  
     */

    private Locator locator = null;


    /**
     * The handlers for this case
     */
    protected Collection handlers = new ArrayList();
    
    /**
     * Set the user's parser handler.
     * @param handler The user's parser handler.
     * 
     * @uml.property name="handler"
     */
    public void addHandler(ParserHandler handler) {
        handlers.add(handler);
    }

    /**
     * Parse a document and call the user's parser handler. 
     * @param input The inputsource of the XML-map.
     * @param validate Indicates if the document needs to be validated. Set this to true.
     * @throws ParseException when the validation/parsing failed
     */

    public void parse(InputSource input, boolean validate)
    throws ParseException {
       /* if(echo = true){
        	echoHandler = new EchoBuildDirector();
        }*/
        
        try
        {
            // use the default parser
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(validate);

            // we are the SAX event handler
            DefaultHandler handler = this;

            // parse the input
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(input, handler);
        }
        catch (SAXParseException e)
        {
            throw new ParseException("PARSE-ERROR : " + e.getMessage() + (e.getLineNumber() > 0 ? (" (line " + e.getLineNumber() + ")") : ""));
        }
        catch (Exception e)
        {
            throw new ParseException("ERROR : " + e.getMessage());
        }
    }


    /**
     * SAX2 event handler.
     * Receive notification of the start of an element.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        try
        {
            // AgvMap element
            if (qName.equals("sim:AgvMap"))
            {
                // call handler
            	String name = attributes.getValue("name");
                if (name == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"name\".");
                
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startAgvMap(name);
                }
                /*if (    echoHandler != null) echoHandler.startAgvMap(name);
                if (    handler != null) handler.startAgvMap(name);*/
            }
            // Environment element
            else if (qName.equals("Environment"))
            {
            	// parse width attribute
                String width = attributes.getValue("width");
                if (width == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"width\".");
                double widthd = Double.parseDouble(width);
                
                // parse height attribute
                String height = attributes.getValue("height");
                if (height == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"height\".");
                double heightd = Double.parseDouble(height);
            	
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startEnvironment(widthd,heightd);
                }
                // call handler
               /* if (echoHandler != null) echoHandler.startEnvironment(widthd,heightd);
                if (    handler != null) handler.startEnvironment(widthd,heightd);*/
            }
            // Stations element
            else if (qName.equals("Stations"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startStations();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startStations();
                if (    handler != null) handler.startStations();*/
            }
            // Station element
            else if (qName.equals("Station"))
            {
                // parse id attribute
                String id = attributes.getValue("id");
                if (id == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"id\".");
                 
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startStation(id);
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startStation(id);
                if (    handler != null) handler.startStation(id);*/
            }
            // Locations element
            else if (qName.equals("Locations"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startLocations();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startLocations();
                if (    handler != null) handler.startLocations();*/
            }
            // Location element
            else if (qName.equals("OrderLocation"))
            {
            	// parse id attribute
                String id = attributes.getValue("id");
                if (id == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"id\".");
                
                // parse type attribute
                String type = attributes.getValue("type");
                if (type == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"type\".");
                
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startLocation(id,type);
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startLocation(id,type);
                if (    handler != null) handler.startLocation(id,type);*/
            }
            // Segments element
            else if (qName.equals("Segments"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startSegments();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startSegments();
                if (    handler != null) handler.startSegments();*/
            }
            // Segment element
            else if (qName.equals("Segment"))
            {
            	// parse id attribute
                String id = attributes.getValue("id");
                if (id == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"id\".");
                
                // parse from attribute
                String from = attributes.getValue("from");
                if (from == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"from\".");
                
                // parse to attribute
                String to = attributes.getValue("to");
                if (to == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"to\".");
                
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startSegment(id,from,to);
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startSegment(id,from,to);
                if (    handler != null) handler.startSegment(id,from,to);*/
            }
            // Coordinate element
            else if (qName.equals("Coordinate"))
            {
            	// parse x attribute
                String x = attributes.getValue("x");
                if (x == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"x\".");
                double dx = Double.parseDouble(x);
                
                // parse x attribute
                String y = attributes.getValue("y");
                if (y == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"y\".");
                double dy = Double.parseDouble(y); 
                	
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startCoordinate(dx,dy);
                }	
                // call handler
                /*if (echoHandler != null) echoHandler.startCoordinate(dx,dy);
                if (    handler != null) handler.startCoordinate(dx,dy);*/
            }
            // Agvs element
            else if (qName.equals("Agvs"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startAgvs();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startAgvs();
                if (    handler != null) handler.startAgvs();*/
            }
            // Agv element
            else if (qName.equals("Agv"))
            {
            	// parse id attribute
                String id = attributes.getValue("id");
                if (id == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"id\".");
                
                // parse startStation attribute
                String startStation = attributes.getValue("startStation");
                if (startStation == null) throw new ParseException("Element \"" + qName + "\" requires attribute \"startStation\".");
                
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).startAgv(id,startStation);
                }
                // call handler
                /*if (echoHandler != null) echoHandler.startAgv(id,startStation);
                if (    handler != null) handler.startAgv(id,startStation);*/
            }
            // Unknown element
            else
            {
                throw new ParseException("Unknown element \"" + qName + "\".");
            }
        }
        // catch any exception and turn it into a SAXParseException
        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new SAXParseException(null, locator, exception);
        }
    }


    /**
     * SAX2 event handler.
     * Receive notification of the end of an element.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        try
        {
            // AgvMap element
            if (qName.equals("sim:AgvMap"))
            {
                // call handler
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endAgvMap();
                }
               /* if (echoHandler != null) echoHandler.
                if (    handler != null) handler.endAgvMap();*/
            }
//          Environment element
            else if (qName.equals("Environment"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endEnvironment();
                }
                // call handler
             /*   if (echoHandler != null) echoHandler.endEnvironment();
                if (    handler != null) handler.endEnvironment();*/
            }
//          Stations element
            else if (qName.equals("Stations"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endStations();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endStations();
                if (    handler != null) handler.endStations();*/
            }
            // Station element
            else if (qName.equals("Station"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endStation();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endStation();
                if (    handler != null) handler.endStation();*/
            }
            // Locations element
            else if (qName.equals("Locations"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endLocations();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endLocations();
                if (    handler != null) handler.endLocations();*/
            }
            // Location element
            else if (qName.equals("OrderLocation"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endLocation();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endLocation();
                if (    handler != null) handler.endLocation();*/
            }
            // Segments element
            else if (qName.equals("Segments"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endSegments();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endSegments();
                if (    handler != null) handler.endSegments();*/
            }
            // Segment element
            else if (qName.equals("Segment"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endSegment();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endSegment();
                if (    handler != null) handler.endSegment();*/
            }
            // Coordinate element
            else if (qName.equals("Coordinate"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endCoordinate();
                }
                // call handler
              /*  if (echoHandler != null) echoHandler.endCoordinate();
                if (    handler != null) handler.endCoordinate();*/
            }
            // Agvs element
            else if (qName.equals("Agvs"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endAgvs();
                }
                // call handler
              /*  if (echoHandler != null) echoHandler.endAgvs();
                if (    handler != null) handler.endAgvs();*/
            }
            // Agv element
            else if (qName.equals("Agv"))
            {
                Iterator iter = handlers.iterator();
                while(iter.hasNext()){
                    ((ParserHandler) iter.next()).endAgv();
                }
                // call handler
                /*if (echoHandler != null) echoHandler.endAgv();
                if (    handler != null) handler.endAgv();*/
            }
            // Unknown element
            else
            {
                throw new ParseException("Unknown element \"" + qName + "\".");
            }
        }
        // catch any exception and turn it into a SAXParseException
        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new SAXParseException(exception.getMessage(), locator, exception);
        }

    }

    /**
     * SAX2 event handler.
     * Receive an object for locating the origin of SAX document events.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void setDocumentLocator(Locator locator)
    {
        this.locator = locator;
    }


    /**
     * SAX2 event handler.
     * Receive notification of a parser warning.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void warning(SAXParseException e)
    {
        // report the warning and continue
        System.err.println("SDLParser.warning() : WARNING : " + e);
    }


    /**
     * SAX2 event handler.
     * Receive notification of a recoverable parser error.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void error(SAXParseException e) throws SAXParseException
    {
        // abort the parse anyway
        throw e;
    }

    /**
     * SAX2 event handler.
     * Report a fatal XML parsing error.
     * You should not call this method, it is called by the SAX2 parser.
     */

    public void fatalError(SAXParseException e) throws SAXParseException
    {
        // abort the parse
        throw e;
    }
}
