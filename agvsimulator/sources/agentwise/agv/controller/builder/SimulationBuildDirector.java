/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.controller.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.xml.sax.InputSource;

import timeManagementFramework.AgentTimeModel;
import timeManagementFramework.TimeManager;
import agentwise.agv.controller.Simulation;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.communication.AdHocInfrastructure;
import agentwise.agv.model.environment.communication.CommunicationInfrastructure;
import agentwise.agv.model.environment.cremodel.Collector;
import agentwise.agv.model.environment.cremodel.SimpleEffector;
import agentwise.agv.model.environment.cremodel.SimpleReactor;

/**
 * <p>This class is responsible to build a specific configuration
 * for the simulation environment and the entities
 * (agvs, stations, ...) in that environment.</p>
 *
 * <p>This is the 'director' in the Builder pattern.
 *  -> Both Mapbuilder and AgvBuilder are the 'builders'</p>
 * 
 * @author nelis
 * 
 */
public class SimulationBuildDirector{

    /**
     * The simulation that is being build
     */
    protected Simulation simulation; 
    
    /**
     * The mapbuilder
     */
    protected MapBuilder mapBuilder;
    /**
     * The AGV builder
     */
    protected AgvBuilder agvBuilder;

    /**
     * The builder for the internal information map of an AGV
     */
    protected InformationMapBuilder imapBuilder;
    
    
    /**
     * The default behavior module
     */
    protected BehaviourModule defaultBehaviorModule;
    
    /**
     * The manager for order logations
     */
    protected OrderLocationManager orderLocationManager;
    
    /**
     * The communication infrastructure
     */
    protected CommunicationInfrastructure comInfrastructure;
    
    /**
     * The name of the XML file
     */
    protected String fileName;
    
      
    /**
     *
     * Constructs an instance of SimulationBuildDirector
     * and prepares it to start building the
     * simulation environment.
     *
     */
    public SimulationBuildDirector() {
        setSimulationParserHandler(new SimulationParserHandler(this));
    }

    
    /**
     * @return Returns the agvBuilder.
     */
    public AgvBuilder getAgvBuilder() {
        return agvBuilder;
    }
    /**
     * @param agvBuilder The agvBuilder to set.
     */
    public void setAgvBuilder(AgvBuilder agvBuilder) {
        this.agvBuilder = agvBuilder;
    }
    /**
     * @return Returns the comInfrastructure.
     */
    public CommunicationInfrastructure getComInfrastructure() {
        return comInfrastructure;
    }
    /**
     * @param comInfrastructure The comInfrastructure to set.
     */
    public void setComInfrastructure(
            CommunicationInfrastructure comInfrastructure) {
        this.comInfrastructure = comInfrastructure;
    }
    /**
     * @return Returns the defaultBehaviorModule.
     */
    public BehaviourModule getDefaultBehaviorModule() {
        return defaultBehaviorModule;
    }
    
    /**
     * Generate a clone of the default behaviour module
     * @return Returns the defaultBehaviorModule.
     */
    public BehaviourModule generateNewBehaviourModule() {
        return defaultBehaviorModule.cloneBehaviour();
    }
    
    /**
     * @param defaultBehaviorModule The defaultBehaviorModule to set.
     */
    public void setDefaultBehaviorModule(BehaviourModule defaultBehaviorModule) {
        this.defaultBehaviorModule = defaultBehaviorModule;
    }
    /**
     * @return Returns the imapBuilder.
     */
    public InformationMapBuilder getImapBuilder() {
        return imapBuilder;
    }
    /**
     * @param imapBuilder The imapBuilder to set.
     */
    public void setImapBuilder(InformationMapBuilder imapBuilder) {
        this.imapBuilder = imapBuilder;
    }
    /**
     * @return Returns the mapBuilder.
     */
    public MapBuilder getMapBuilder() {
        return mapBuilder;
    }
    /**
     * @param mapBuilder The mapBuilder to set.
     */
    public void setMapBuilder(MapBuilder mapBuilder) {
        this.mapBuilder = mapBuilder;
    }

       
    /**
     * @return Returns the orderLocationManager.
     */
    public OrderLocationManager getOrderLocationManager() {
        return orderLocationManager;
    }
    
    /**
     * @param orderLocationManager The orderLocationManager to set.
     */
    public void setOrderLocationManager(
            OrderLocationManager orderLocationManager) {
        this.orderLocationManager = orderLocationManager;
    }
    
    
    /**
     * @return Returns the fileName.
     */
    public String getFileName() {
        return fileName;
    }
    /**
     * @param fileName The fileName to set.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    
    //------------------------------------------------------
    /**
     * Builds the specific simulation environment according
     * to the current configuration of this simulation
     * builder. It will not start the simulation though.
     */
    public void buildSimulation() throws FileNotFoundException, ParseException{
        setSimulation(new Simulation());
        
        readFile(fileName);
    }

    /**
     * @return the simulation
     */
    public Simulation getSimulation(){
        return simulation;
    }
    
	/**
	 * Read the xml file containing the description of the simulation
	 * @param filename The filename
	 * @throws FileNotFoundException If the file can not be found
	 * @throws ParseException If the file contains an invalid description
	 */
    protected void readFile( String filename) throws FileNotFoundException, ParseException
    {    // create file and file input stream
            File file = new File(filename);
            FileInputStream fileInputStream = new FileInputStream(file);
            InputSource inputSource = new InputSource(fileInputStream);

            // create the parser and parse the input file
            Parser parser = new Parser();
            //ConcreteMapBuilder mapbuilder = new ConcreteMapBuilder(sim);

            parser.addHandler(new EchoBuildDirector());
            //parser.addHandler(new InformationMapBuildDirector());
            parser.addHandler(this.getSimulationParserHandler());
            

            // if the output bothers you, set echo to false
            // also, if loading a large file, set echo to false
            // you should leave validate to true
            // if the docuement is not validated, the parser will not detect syntax errors
            parser.parse(inputSource,  false); //throws
    }
    
    public void setSimulationParserHandler(SimulationParserHandler parserhandler) {
        if (parserhandler!=null) {
            this.parserhandler = parserhandler;
        }
    }
    
    private SimulationParserHandler parserhandler;
    
    public SimulationParserHandler getSimulationParserHandler() {
        return parserhandler;
    }

    /**
     * Build the environment of the simulation
     * @param width The width
     * @param height The height
     */
    public void generateEnvironment(double width, double height) {
        Environment env = new Environment(width,height
    	        , new Collector()
    	        , new SimpleReactor()
    	        , new SimpleEffector()
    	        , new AdHocInfrastructure(500));
    	//    	 TODO [all] 500cm? is dat realistisch?
    	getSimulation().setEnvironment(env);
    	getAgvBuilder().setEnvironment(env);
    }
    
    /**
     * Integrate the orderLocationManager with the TimeManagment framework
     * @param olm The OrderLocationManager to integrate
     */
    void integrateOrderLocationManagerWithTimeManagement(OrderLocationManager olm) {
        AgentTimeModel tma = new AgentTimeModel(olm.getName());
        TimeManager.getInstance().addAgentTimeModel(tma);
        tma.registerActivityDuration("stepOrderLocationManager",1,AgentTimeModel.EXTERNALACTIVITY);
        
    }
    
    /**
     * @param psimulation The simulation to set.
     */
    public void setSimulation(Simulation psimulation) {
        this.simulation = psimulation;
    }
}
