package agentwise.agv.controller.builder;
/**
 * An exception possible trown when parsing
 *  
 * @author nelis
 */
public class ParseException extends Exception
{
    /**
     * 
     * Constructs an instance of ParseException
     *
     * @param message The message explaining the exception
     */
    public ParseException(String message)
    {
        super(message);
    }
}
