/*
 * Created on Jan 28, 2005
 * 
 */
package agentwise.agv.controller.builder;

import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.environment.AgentToEnvironmentInterface;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentAccessPoint;

/**
 * @author nelis
 *

 */
public class DefaultAgvBuilder extends AgvBuilder {

    
    /**
     * Build the AGV
     */
    public void build() throws IllegalStateException {
    	
        checkBuilderState();
        Agv result = null;
                
        AgvEnvironmentState agvEnvironmentState = new AgvEnvironmentState();
        agvEnvironmentState.setCurrentMapComponent(getStartStation());
        agvEnvironmentState.setCoordinate(getStartStation().getCoordinate());
        agvEnvironmentState.setDirection(getDirection());
        agvEnvironmentState.setCurrentOrder(getCurrentOrder());
        
        
        getEnvironment().registerAgv(agvEnvironmentState);
        
        AgentToEnvironmentInterface environmentap = null;
        environmentap = new EnvironmentAccessPoint(getEnvironment(), agvEnvironmentState);       
        setAgv(new Agv(environmentap));
        getAgv().setPerceptionModule(getPerceptionModule());
        getAgv().setCommunicationModule(getCommunicationModule());
        getAgv().setActionExecutionModule(getActionExecutionModule());
        getAgv().setBehaviourModule(getBehaviourModule());
        getAgv().setInformationMap(getImap());
        integrateWithTimeManagmentFramework(getAgv());
        //resetBuilder();
      
    }
}
