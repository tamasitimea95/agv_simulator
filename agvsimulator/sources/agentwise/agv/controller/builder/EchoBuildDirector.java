/*
 * Created on Oct 6, 2004
 *
 */
package agentwise.agv.controller.builder;

/**
 * A parserhandler that outputs the XML-map onto the standard output 
 * 
 * @author nelis
 */
public class EchoBuildDirector implements ParserHandler {

	/**
	 * The number of indents
	 */
	private int indent; 
	/**
	 * The string to indent
	 */
    static final private String indentString = "   ";
	
	/**
	 * Constructor
	 */
	public EchoBuildDirector() {
		super();
	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startAgvMap(java.lang.String)
	 */
	public void startAgvMap(String name) throws Exception {
		indent = 0;
        System.out.println("<AgvMap>");

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endAgvMap()
	 */
	public void endAgvMap() throws Exception {
		System.out.println("</AgvMap>");
	}

	/**
	 * Print the startenvironment tag
	 */
	public void startEnvironment(double width, double height) throws Exception{
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<Environment "+
        		"width="+ width +
				" height="+ height +
				">"
		);
	}
	/**
	 * Print the end environment tag
	 */
    public void endEnvironment() throws Exception{
    	for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Environment>");
        indent--;
    }
    
    	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startStations()
	 */
	public void startStations() throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("<Stations>");

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endStations()
	 */
	public void endStations() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Stations>");
        indent--;
	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startStation(java.lang.String)
	 */
	public void startStation(String id) throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<Station " +
        		"id="+ id + 
				">"
		);        
	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endStation()
	 */
	public void endStation() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Station>");
        indent--;

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startLocation(java.lang.String)
	 */
	public void startLocations() throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("<OrderLocations>");

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endLocation()
	 */
	public void endLocations() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</OrderLocations>");
        indent--;

	}

	 public void startLocation(String id, String type) throws Exception{
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<OrderLocation " +
        		"id="+ id + 
				" type=" + type +
				">"
		); 	
	 }
	 
	 public void endLocation() throws Exception{
	 	for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</OrderLocation>");
        indent--;
	 }
	
	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startCoordinate(double, double)
	 */
	public void startCoordinate(double x, double y) throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<Coordinate " +
        		"x="+ x +
				" y="+ y +
				">"
		); 

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endCoordinate()
	 */
	public void endCoordinate() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Coordinate>");
        indent--;

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startSegments()
	 */
	public void startSegments() throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("<Segments>");
	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endSegments()
	 */
	public void endSegments() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Segments>");
        indent--;

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startSegement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void startSegment(String id, String from, String to)
			throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<Segment " +
        		"id="+ id +
				" from="+ from +
				" to=" + to + 
				">"
		); 

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endSegment()
	 */
	public void endSegment() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Segment>");
        indent--;

	}
	
	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startAgvs()
	 */
	public void startAgvs() throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("<Agvs>");
	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endAgvs()
	 */
	public void endAgvs() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Agvs>");
        indent--;

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#startAgv(java.lang.String, java.lang.String)
	 */
	public void startAgv(String id, String startStation)
			throws Exception {
		indent++;
        for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println(
        		"<Agv " +
        		"id="+ id +
				" startStation="+ startStation +
				">"
		); 

	}

	/* (non-Javadoc)
	 * @see agentwise.agv.model.environment.xmlreader.ParserHandlerAgvMap#endAgv()
	 */
	public void endAgv() throws Exception {
		for (int i = 0; i < indent; i++) System.out.print(indentString);
        System.out.println("</Agv>");
        indent--;

	}
}
