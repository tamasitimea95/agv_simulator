/*
 * Created on Oct 5, 2004
 *
 */
package agentwise.agv.controller.builder;

import timeManagementFramework.AgentTimeModel;
import timeManagementFramework.TimeManager;
import agentwise.agv.model.agv.ActionExecutionModule;
import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.agv.CommunicationModule;
import agentwise.agv.model.agv.PerceptionModule;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.agv.map.InformationMap;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.Order;
import agentwise.agv.model.environment.map.Station;

/**
 * This class allows to build Agvs. First you configure
 * the builder with the necessary modules (perceptionmodule,
 * actionexecutionmodule, behaviourmodule, communicationmodule, ...)
 *  and information. Then you call the build() method and an Agv is returned to you.
 * 
 * @author nelis, tomdw 
 * 
 */
public abstract class AgvBuilder {

    /**
     * @return Returns the actionExecutionModule.
     */
    public ActionExecutionModule getActionExecutionModule() {
        return actionExecutionModule;
    }
    /**
     * @return Returns the behaviourModule.
     */
    public BehaviourModule getBehaviourModule() {
        return behaviourModule;
    }
    /**
     * @return Returns the communicationModule.
     */
    public CommunicationModule getCommunicationModule() {
        return communicationModule;
    }
    /**
     * @return Returns the startStation.
     */
    public Station getStartStation() {
        return startStation;
    }

    /**
     * The agv (build)
     */
    protected Agv agv;
    
    /**
     * 
     * @uml.property name="perceptionModule"
     */
    /* The perceptionmodule for the agv to build */
    private PerceptionModule perceptionModule = new PerceptionModule(); // default module

    /**
     * 
     * @uml.property name="communicationModule"
     */
    /* The communicationmodule for the agv to build */
    private CommunicationModule communicationModule;// = new DefaultCommunicationModule(); //default

    /**
     * 
     * @uml.property name="behaviourModule"
     */
    /* The behaviourmodule for the agv to build */
    private BehaviourModule behaviourModule; // default

    /**
     * 
     * @uml.property name="actionExecutionModule"
     */
    /* the actionexecutionmodule for the agv to build */
    private ActionExecutionModule actionExecutionModule;// = new ActionExecutionModule(); // default module

    /**
     * 
     */
    /* The information map for the AGV to be build */
    protected InformationMap imap;
    
    /**
     * 
     * @uml.property name="direction"
     */
    /* the direction in degrees for the agv to build */
    private double direction = 0.0;

	/* indicates if the direction in degrees for the agv was already set */
    private boolean directionIsSet = false;

    /**
     * 
     * @uml.property name="station"
     */
    /* the station on which the agv starts its behaviour */
    private Station startStation = null;

    /**
     * 
     * @uml.property name="currentOrder" 
     */
    /* the order the agv is currently holding */
    protected Order currentOrder = null;


    /**
     * 
     * @uml.property name="environment"
     */
    /* the environment in which the agv is situated, the central part */
    protected Environment environment = null;

    /**
     * Sets the PerceptionModule to be used by the agv to build.
     * 
     * @param pmodule The PerceptionModule to use
     * @throws IllegalArgumentException when pModule==null
     * 
     * @uml.property name="perceptionModule"
     */
    public void setPerceptionModule(PerceptionModule pmodule)
        throws IllegalArgumentException {
        if (pmodule == null)
            throw new IllegalArgumentException(
                "The given perception module cannot be null");
        perceptionModule = pmodule;
    }

    /**
     * Sets the CommunicationModule to be used by the agv to build.
     * 
     * @param cmodule The CommunicationModule to use
     * @throws IllegalArgumentException when cmodule==null
     * 
     * @uml.property name="communicationModule"
     */
    public void setCommunicationModule(CommunicationModule cmodule)
        throws IllegalArgumentException {
        if (cmodule == null)
            throw new IllegalArgumentException(
                "The given communication module cannot be null");
        communicationModule = cmodule;
    }

    /**
     * Sets the BehaviourModule to be used by the agv to build. This module
     * determines the behaviour of the agv and uses the other modules
     * to achieve that behaviour.
     * 
     * @param bmodule the BehaviourModule to use
     * @throws IllegalArgumentException when bmodule==null
     * 
     * @uml.property name="behaviourModule"
     */
    public void setBehaviourModule(BehaviourModule bmodule)
        throws IllegalArgumentException {
        if (bmodule == null)
            throw new IllegalArgumentException(
                "The given behaviourmodule cannot be null");
        behaviourModule = bmodule;
    }

    /**
     * Sets the ActionExecutionModule to be used by the agv to execute actions on the
     * environment.
     * 
     * @param aemodule The ActionExecutionModule to use
     * @throws IllegalArgumentException when the aemodule==null
     * 
     * @uml.property name="actionExecutionModule"
     */
    public void setActionExecutionModule(ActionExecutionModule aemodule)
        throws IllegalArgumentException {
        if (aemodule == null)
            throw new IllegalArgumentException(
                "The given actionexecutionmodule cannot be null");
        actionExecutionModule = aemodule;
    }

    /**
     * Sets the direction to start the agv in to the given
     * number of degrees.
     * 
     * @param angle the number of degrees starting from the horizontal east direction (0)
     * 
     * @uml.property name="direction"
     */
    public void setDirection(double angle) {
        direction = angle;
        directionIsSet = true;
    }

    /**
     * Sets the station on which the agv should start.
     * The coordinate of the agv will be the coordinate of the station.
     * 
     * @param component
     * 
     * @uml.property name="station"
     */
    public void setStartStation(Station component) {
        startStation = component;
    }

    /**
     * Sets the order the agv is currently holding.
     * 
     * @param order The Order instance for the agv to hold
     * 
     * @uml.property name="currentOrder"
     */
    public void setCurrentOrder(Order order) {
        currentOrder = order;
    }

    /**
     * Sets the environment (the central part) to handle
     * the actions and to provide the communicationinfrastructure
     * that the agv should use.
     * 
     * @param env The environment to use
     * 
     * @uml.property name="environment"
     */
    public void setEnvironment(Environment env) {
        environment = env;
    }

    
    
    
    /**
     * Builds the Agv according to the current configuration of this builder.
     * 
     * @throws IllegalStateException when the current configuration of this builder does not allow to build an Agv
     */
    
    public abstract void build() throws IllegalStateException;
   
    
    
    /**
     * Checks if the current state of this builder is
     * complete in order for it to build an Agv.
     * 
     * @throws IllegalStateException when there was something missing
     */
    protected void checkBuilderState() throws IllegalStateException {
        if (perceptionModule==null) throw new IllegalStateException("You must first set the PerceptionModule");
        if (behaviourModule==null) throw new IllegalStateException("You must first set the BehaviourModule");
        if (communicationModule==null) throw new IllegalStateException("You must first set the CommunicationModule");        
        if (actionExecutionModule==null) throw new IllegalStateException("You must first set the ActionExecutionModule");
        
        if (environment==null) throw new IllegalStateException("You must first set the Environment for the Agv");
        if (!directionIsSet) throw new IllegalStateException("You must first set the direction for the agv");
        if (startStation==null) throw new IllegalStateException("You must first set the starting Station for the agv");
        if (imap==null) throw new IllegalStateException("You must first set the information map for the agv");
    }

    /**
     * Resets the state of this builder to
     * be ready to be configured for a new
     * agv to build.
     *
     */
    public void resetBuilder() {
        directionIsSet = false;
        startStation = null;
        currentOrder = null;
        imap = null;  
        perceptionModule = null;
        behaviourModule = null;
        communicationModule = null;
        actionExecutionModule = null;
        agv = null;
    }

    /**
     * Integrate this AGV with the TimeManagmentFramework (a framework to enforce
     * logical time constrains)
     * @param agv the AGV to integrate
     */
    protected void integrateWithTimeManagmentFramework(Agv agv) {
        //integrate with Time Management Framework
        AgentTimeModel tma = new AgentTimeModel(agv.getEnvironment().getName());
        TimeManager.getInstance().addAgentTimeModel(tma);
        tma.registerActivityDuration("sense",0.1f,AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("followsegmentstep",10,AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("broadcast",0,AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("unicast",0, AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("receive",0,AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("droporder", 50, AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("pickorder", 50, AgentTimeModel.EXTERNALACTIVITY);
        tma.registerActivityDuration("waitsecond", 1, AgentTimeModel.EXTERNALACTIVITY);
        
        // For handling communication in parrallel with normal agv control flow
        AgentTimeModel tma2 = new AgentTimeModel(agv.getEnvironment().getName()+"c");
        TimeManager.getInstance().addAgentTimeModel(tma2);
        tma2.registerActivityDuration("broadcast",0,AgentTimeModel.EXTERNALACTIVITY);
        tma2.registerActivityDuration("unicast",0, AgentTimeModel.EXTERNALACTIVITY);
        tma2.registerActivityDuration("wait",1,AgentTimeModel.EXTERNALACTIVITY);
        
    }

    /**
     * @return Returns the imap.
     */
    public InformationMap getImap() {
        return imap;
    }
    /**
     * @param imap The imap to set.
     */
    public void setInformationMap(InformationMap imap) {
        this.imap = imap;
    }
    /**
     * @return Returns the agv.
     */
    public Agv getAgv() {
        return agv;
    }
    /**
     * @param agv The agv to set.
     */
    protected void setAgv(Agv agv) {
        this.agv = agv;
    }
    /**
     * @return Returns the direction.
     */
    public double getDirection() {
        return direction;
    }
    /**
     * @return Returns the directionIsSet.
     */
    public boolean isDirectionIsSet() {
        return directionIsSet;
    }
    /**
     * @return Returns the environment.
     */
    public Environment getEnvironment() {
        return environment;
    }
    /**
     * @return Returns the perceptionModule.
     */
    public PerceptionModule getPerceptionModule() {
        return perceptionModule;
    }
    /**
     * @return Returns the currentOrder.
     */
    protected Order getCurrentOrder() {
        return currentOrder;
    }
}
