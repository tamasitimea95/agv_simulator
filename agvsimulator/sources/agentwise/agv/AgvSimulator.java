/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv;

import java.io.IOException;

import javax.swing.SwingUtilities;

import agentwise.agv.controller.Controller;
import agentwise.agv.controller.builder.ParseException;
import agentwise.agv.view.MainWindow;

/**
 * This is the class that has to be started
 * to run the automated guided vehicle
 * simulator.
 *
 * @author tomdw
 *
 */
public final class AgvSimulator {

    /**
     * 
     * Constructs an instance of AgvSimulator
     * but is set private because this class
     * only contains static members thus
     * instantiating it is not necessary.
     *
     */
    private AgvSimulator() { }

    /**
     * The main starting method that creates a controller,
     * and loads a gui
     *
     * @param args The arguments given via the command line
     */
    public static void main(final String[] args) {
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        loadUI();
                    }
                });
    	/*try {
    		
			Controller.getInstance().constructSimulation("E:\\MSc\\Projektek\\AGV_projekt\\agv_simulator\\agvsimulator\\XML\\blokk_1_foku_allomasokkal\\blokk_33_4l_3c.xml", "E:\\MSc\\Projektek\\AGV_projekt\\logs");
    		System.out.println(args[0]);
    		System.out.println(args[1]);
    		Controller.getInstance().constructSimulation(args[0], args[1]);
			Controller.getInstance().startSimulation();
			System.out.println("MAIN VEGE");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/    
    }

    /**
     * Loads the user interface and registers it with the controller
     *
     */
    private static void loadUI() {
        MainWindow window = MainWindow.getInstance();
    }
}
