/*
 * Created on 15-Nov-2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import agentwise.agv.controller.OrderManager;
import agentwise.agv.model.environment.map.OrderLocation;

/**
 * An InputOrderLocationManager accepts Orders as input and
 * will then autonomously add them at the right OrderLocation.
 * Removing Orders from out-orderlocations is done stochastically
 * according to the algorithm in SimpleOrderLocationManager
 * 
 * @author tomdw
 *
 */
public class InputOrderLocationManager extends SimpleOrderLocationManager {

    /**
     * Constructs an instance of InputOrderLocationManager
     *
     * 
     */
    public InputOrderLocationManager() {
        super();
    }
    
    /**
     * Adds a given order to this orderlocationmanager for
     * it to process by putting it on the starting station.
     * 
     * @param order The Order to put in the environment 
     */
    public void addOrder(Order order) {
        if (order!= null){
            if (ordersToProcess.keySet().contains(order.getStartStationId())) {
                Collection c = (Collection)ordersToProcess.get(order.getStartStationId());
                if (!c.contains(order)) {
                    synchronized(c){
                        c.add(order);
                        OrderManager.getInstance().addOrder(order);
                    }
                }
            } else {
                Collection c = new ArrayList();
                c.add(order);
                synchronized(ordersToProcess) {
                    ordersToProcess.put(order.getStartStationId(),c);
                    OrderManager.getInstance().addOrder(order);
                }
            }
        }
    }
    
    /**
     * Contains for each starting station id a list
     * of orders that have to be added to it in the
     * arriving order.
     */
    private HashMap ordersToProcess = new HashMap();
    
    /**
	 * Put the orders added to this OrderLocationManager
	 * at the right OrderLocations, only if that orderlocation
	 * is free.
	 *
	 */
	protected void generateOrders(){
		Iterator iter = getPickOrderLocations().iterator();
		while(iter.hasNext()){
			OrderLocation ol = (OrderLocation)iter.next();
			if (!ol.hasOrder()) {
				if (ordersToProcess.keySet().contains(ol.getID())) {
				    List c = (List)ordersToProcess.get(ol.getID());
				    Order o = null;
				    synchronized(c) {
				        o = (Order)c.remove(0);
				    }
				    if (c.isEmpty()) {
				        synchronized(ordersToProcess) {
				            ordersToProcess.remove(ol.getID());
				        }
				    }
				    ol.addOrder(o);
				    OrderManager.getInstance().addOrder(o);
				} // orders present for this orderlocation?
			}// has an order?
		} //while
	}

}
