/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.Collection;

/**
 * An influence is an attempt to bring a certain effect about
 * in the environment
 * 
 * @author nelis
 */
public abstract class Influence {
	
	/**
	 * Constructor
	 *
	 */
	public Influence(){
		super();
	}
	 
	
	/**
	 * Calculate the effects of this influence in this 
	 * environment
	 * 
	 * @param envState The state of the environment 
	 * @return The effects of this influence
	 */
	public abstract Collection calculateEffects(EnvironmentState envState);	
	
}
