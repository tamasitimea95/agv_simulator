/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.List;

import agentwise.agv.model.agv.OrderInfo;
import agentwise.agv.model.environment.communication.CommunicationNode;
import agentwise.agv.model.environment.communication.DefaultNetworkID;
import agentwise.agv.model.environment.communication.Inbox;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.map.MapComponent;
import agentwise.agv.util.Angle;
import agentwise.agv.util.Coordinate;
import agentwise.agv.util.Rectangle;

/**
 * This class contains the state of one AGV in the environment
 * 
 * @author nelis
 */
public class AgvEnvironmentState implements CommunicationNode{

	/**
	 * Constructor
	 */
	public AgvEnvironmentState() {
		super();
	}

	/**
	 * The current coordinate 
	 */
	private Coordinate coordinate = null;
	/**
	 * The current orientation direction of the AGV
	 */
	private Angle direction = null;
	/**
	 * The inbox
	 */
	private Inbox inbox = new Inbox();
	/**
	 * The network and AGV id of the AGV
	 */
	private NetworkID networkID = DefaultNetworkID.nextUniqueID();
	/**
	 * The mapcomponent on which the AGV resides
	 */
	private MapComponent currentMapComponent = null;
	/**
	 * The order that the AGV is holding
	 */
	private Order currentOrder;
	/**
	 * Represents if the AGV is suspended or not
	 */
	private boolean isSuspended = false;
	
	/**
	 * Checks if this Agv is suspended. If
	 * it is then the Agv will not continue its
	 * activity.
	 * 
	 * @return true if the agv is suspended, false otherwise
	 */
	public boolean isSuspended() {
	    return isSuspended;
	}
	
	/**
	 * Terminates this agv
	 *
	 */
	public void suspend() {
	    isSuspended = true;
	}
	
	/**
	 * Returns the surrounding box for this Agv.
	 * This can be used for collision detection.
	 * 
	 * @return a Rectangle instance
	 */
	public Rectangle getBox() {
	    double width = 100;
	    double height = 100;
	    Angle reverse = new Angle(direction);
	    reverse.setDegrees(direction.getDegrees()+180);
	    Coordinate middle = coordinate.nextInAngle(reverse,width*0.23);
	    Rectangle result = new Rectangle(middle,direction,width,height);
	    return result;
	}
	/**
	 * @return The coordinate of this AGV 
	 * @see agentwise.agv.model.environment.communication.CommunicationNode#getCoordinate()
	 */
	public Coordinate getCoordinate() {
	    if (coordinate==null) return coordinate;
	    return new Coordinate(coordinate);
	}
	
	/**
	 * @return The current mapcomponent on which this AGV resides
	 */
	public MapComponent getCurrentMapComponent() {
	    return currentMapComponent;
	}

    /**
     * Returns the current direction of this AGV as an Angle. Zero degrees means
     * an AGV facing horizontally east, 90 degrees means an AGV facing
     * vertically north, and so on. The degrees are always given between 0 and
     * 360 degrees.
     * 
     * @return a Angle containing the angle value in degrees
     * 
     */
	public Angle getDirection() {
	    return new Angle(direction);
	}
	
	/**
	 * Sets the coordinate of the Agv that uses this access point
	 * 
	 * @param c The new coordinate of the Agv
	 * @throws IllegalArgumentException when the given coordinate is null
	 */
	public void setCoordinate(Coordinate c) throws IllegalArgumentException {
	    if (c==null) throw new IllegalArgumentException("The given coordinate cannot be null");
	    this.coordinate = new Coordinate(c);
	}
	
	/**
	 * Sets the current mapcomponent for this AGV
	 * @param current
	 * @throws IllegalArgumentException
	 */
	public void setCurrentMapComponent(MapComponent current) throws IllegalArgumentException {
	    if (current==null) throw new IllegalArgumentException("The current component of the map on which the agv is located cannot be null");
	    currentMapComponent = current;
	}
	
	/**
	 * Sets the current direction of this AGV in degrees. Zero degrees means an
	 * AGV facing horizontally east, 90 degrees means an AGV facing vertically
	 * north, and so on. The given degrees are always converted to the range
	 * between 0 and 360 degrees.
	 * 
	 * @param angle
	 *            The degrees for the direction.
	 */
	public void setDirection(double angle) {
	    if (direction == null) {
	        direction = new Angle(angle, false);
	    } else {
	        direction.setDegrees(angle);
	    }
	}

	/**
	 * @return Returns the inbox.
	 */
	public Inbox getInbox() {
		return inbox;
	}
	
	/**
	 * @return Returns the networkID.
	 */
	public NetworkID getAgvID(){
		return getNetworkID();
	}
	
	/**
	 * @return Returns the networkID.
	 */
	public NetworkID getNetworkID() {
	    return networkID;
	}
	/**
	 * @return Returns the currentOrder.
	 */
	public Order getCurrentOrder() {
		return currentOrder;
	}
	
	/**
	 * @return Returns the currentOrder.
	 */
	public OrderInfo getCurrentOrderInfo() {
		return new OrderInfo(getCurrentOrder());
	}
	
	public boolean hasCurrentOrder(){
		return (getCurrentOrder()!= null);
	}
	/**
	 * @param currentOrder The currentOrder to set.
	 */
	public void setCurrentOrder(Order currentOrder) {
		this.currentOrder = currentOrder;
	}
	
	/**
	 * Removes the current order from this AgvEnvironmentState
	 */
	public void removeCurrentOrder() {
		setCurrentOrder(null);
	}
	
	/**
	 * @param direction The direction to set.
	 */
	public void setDirection(Angle direction) {
		this.direction = direction;
	}
	
	/**
	 * @return Returns true if this AGV resides on a segment, false otherwise
	 */
	public Boolean isOnSegment(){
		return new Boolean(getCurrentMapComponent().isSegment());
	}
	
	/**
	 * @return True if this AGV resides on a station, false otherwise 
	 */
	public Boolean isOnStation(){
		return new Boolean(getCurrentMapComponent().isStation());
	}
	
	/**
	 * @return The station where this AGV is going to
	 * @throws IllegalStateException The current map component could not give this information
	 */
	public String getGoalStationId() throws IllegalStateException {
		try {
			return (String) getCurrentMapComponent().getGoalStationID();
		} catch (Throwable e) {
			throw new IllegalStateException("The current map component could not give this information");
		}
	}
	
	/**
	 * @return The ids of neighbouring stations
	 * @throws IllegalStateException The current map component could not give this information
	 */
	public List getNeighbouringStationIds() throws IllegalStateException{
		try {
			return getCurrentMapComponent().getNeighbouringStationIds();
		} catch (Throwable e) {
			throw new IllegalStateException("The current map component could not give this information");
		}
	}

    /**
     * @return a Message instance that was removed from the inbox of the agv
     */
    public Message retrieveMessage() {
        return getInbox().remove();
    }
	
	
}
