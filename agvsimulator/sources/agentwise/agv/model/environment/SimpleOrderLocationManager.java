/*
 * Created on Oct 25, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import agentwise.agv.controller.OrderManager;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Station;


/**
 * A simple OrderLocationManager that randomly puts orders on locations
 * and randomly takes away orders on locations
 * 
 * @author nelis
 */
public class SimpleOrderLocationManager extends OrderLocationManager {

	/**
	 * Constructor
	 */
	public SimpleOrderLocationManager() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see agentwise.agv.controller.ActiveEntity#step()
	 */
	public void step() {
		takeAwayOrders();
		generateOrders();	
	}

    /**
	 * Take (with a random change) the orders away on station where
	 * orders can be dropped.
	 *
	 */
	protected void takeAwayOrders(){
		double percentage = 0.01;
		Iterator iter = getDropOrderLocations().iterator();
		while(iter.hasNext()){
			OrderLocation ol = (OrderLocation)iter.next();
			// TODO [somebody] Do something usefull with the order
			double randomnr = Math.random();
			if(randomnr < percentage && ol.hasOrder()){
				Order o = ol.removeOrder();
				OrderManager.getInstance().removeOrder(o);
			}
		}	
	}

	/**
	 * Put (with a random change) an order on a station where the
	 * AGV can pick it.
	 *
	 */
	protected void generateOrders(){
		double percentage = 0.01;
		Iterator iter = getPickOrderLocations().iterator();
		while(iter.hasNext()){
			OrderLocation ol = (OrderLocation)iter.next();
			
			double randomnr = Math.random();
			if(randomnr < percentage && !ol.hasOrder()){
			    Order o = new Order(ol.getID(), getRandomDropOff());
				ol.addOrder(o);
				OrderManager.getInstance().addOrder(o);
			}
		}	
	}

    /**
     * Returns a random station id that is selected from
     * the collection of all order locations where an
     * order can be dropped/delivered.
     * 
     * @return a String representing the id of that order location
     */
    private String getRandomDropOff() {
        List l = new ArrayList(this.getDropOrderLocations());
        int index = (int)Math.floor(Math.random()*(l.size()));
        return ((Station)l.get(index)).getID();
    }

}
