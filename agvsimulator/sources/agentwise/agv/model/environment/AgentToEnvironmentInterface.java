/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.model.environment;

import timeManagementFramework.Identifiable;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;


/**
 * This interface incorporates the methods
 * that are needed by an agent to interact
 * with the environment (communication, perception and actions)
 * 
 * @author tomdw,nelis
 *
 */
public interface AgentToEnvironmentInterface extends Identifiable {
   
    /**
     * Senses for a specific given piece of perception.
     * This information is returned. When sensing
     * the information failed, null is returned.
     * 
     * @param perceptionName 
     * 		<p>This is the name for the wanted perception. 
     *  	Possible values are:</p>
     * 		<ul>
     * 			<li>coordinate</li>
	 * 			<li>direction</li>
	 * 			<li>networkid</li>
	 * 			<li>isonsegment</li>
	 * 			<li>isonstation</li>
	 * 			<li>goalstationid</li>
	 * 			<li>neighbouringstationids</li>
	 * 			<li>currentmapcomponentid</li>
	 * 			<li>orderholding</li>
	 * 			<li>hasorder</li>
	 * 			<li>canpickorder</li>
	 * 			<li>candroporder</li>
	 * 			<li>issuspended</li>
	 * 		</ul>
     */
    public Object sense(String perceptionName);

       
    /**
     * Influence the environment with the given AgvInfluence
     * An influence is an attempt to bring a certain
     * effect in the environment.
     * 
     * @param influence The influence of the AGV
     */
    public void influence(AgvInfluence influence);
    

    
    
    /**
     * Sends a message to a certain communication enabled node, given by the specified NetworkID.
     * @param destination The id on the network of the receiver
     * @param message The message to send
     */
    public void unicast(NetworkID destination, Message message);
    
    /**
     * Broadcasts a message to all AGVs withing a certain physical range from the sending
     * AGV, where the range is given by 
     * @param message the message to broadcast
     */
    public void broadcast(Message message);
    
    /**
     * @return message the first message in this AGV's inbox. Null if the inbox is empty.
     */
    public Message receive();
    
}