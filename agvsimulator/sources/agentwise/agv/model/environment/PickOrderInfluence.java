/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;

import agentwise.agv.model.environment.cremodel.PickOrderAgvEffect;

/**
 * An influence to pick an order on an OrderLocation that 
 * supports picking
 *  
 * @author nelis
 */
public class PickOrderInfluence extends AgvInfluence {

	/**
	 * Constructor
	 */
	public PickOrderInfluence() {
		super();
	}

	/**
	 * Calculate the effects of dropping an order
	 * The collection can either be empty or contain an
	 * PickOrderAgvEffect
	 * 
	 */	
	public Collection calculateEffects(EnvironmentState envState) {		
		if (!getAgvenvstate().isSuspended()) {
	    	if(getAgvenvstate().hasCurrentOrder()|| !getAgvenvstate().getCurrentMapComponent().canPickOrder()) return null;
			else{ 
				Collection temp = new ArrayList();
				temp.add(new PickOrderAgvEffect(getAgvenvstate()));
				return temp;
			}
		}
		return null;
	}

}
