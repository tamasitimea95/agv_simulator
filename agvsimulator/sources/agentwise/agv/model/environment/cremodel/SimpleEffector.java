/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.EnvironmentState;

/**
 * A simple implementation of the effector
 * 
 * @author nelis
 */
public class SimpleEffector extends Effector {

	/**
	 * Constructor
	 */
	public SimpleEffector() {
		super();		
	}

	/**
	 * Performs the effect in the environment
	 *  
	 * @see agentwise.agv.model.environment.cremodel.Effector#doEffect(agentwise.agv.model.environment.cremodel.Effect, agentwise.agv.model.environment.EnvironmentState)
	 */
	protected void doEffect(Effect effect, EnvironmentState envState) {
		effect.doEffect(envState);
	}

}
