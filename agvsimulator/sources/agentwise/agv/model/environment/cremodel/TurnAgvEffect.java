/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.util.Angle;
import agentwise.agv.util.Rectangle;

/**
 * When executing a TurnAgvEffect the Agv is turned
 * towards the direction with which this effect was
 * configured.
 * 
 * @author tomdw
 *
 */
public class TurnAgvEffect extends AgvNewSituationEffect {

	private Angle newDirection;
	
	/**
	 * Constructs an instance of TurnAgvEffect for
	 * the agv of which the state is given and turning
	 * it in the given direction.
	 * 
	 * @param agvEnvState the state of the agv to turn
	 * @param dir the given direction
	 */
	public TurnAgvEffect(AgvEnvironmentState agvEnvState, Angle dir) {
		super(agvEnvState);
		setNewDirection(dir);
	}

	/**
	 * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
	public void doEffect(EnvironmentState envState) {
		getAgvenvstate().setDirection(getNewDirection());
	}

	/**
	 * @return the direction in which to turn the agv after
	 * executing this effect
	 */
	protected Angle getNewDirection() {
		return newDirection;
	}
	/**
	 * Sets the direction in which to turn the agv after
	 * executing this effect
	 * 
	 * @param dir The Angle instance representing the new direction
	 */
	protected void setNewDirection(Angle dir) {
		this.newDirection = dir;
	}

    /**
     * @see agentwise.agv.model.environment.cremodel.AgvNewSituationEffect#simulateEffect(agentwise.agv.model.environment.EnvironmentState)
     */
    protected Rectangle simulateEffect(EnvironmentState envState) {
        Rectangle r = getAgvenvstate().getBox();
        Rectangle newR = r.turn(getNewDirection());
        return newR;
    }
}
