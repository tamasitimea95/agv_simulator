/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.EnvironmentState;

/**
 * 
 * @author nelis
 */
public abstract class Effect {

	/**
	 * 
	 */
	public Effect() {
		super();		
	}
	
	public abstract void doEffect(EnvironmentState envState);

}
