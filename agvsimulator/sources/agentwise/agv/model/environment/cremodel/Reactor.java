/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import java.util.Collection;

import agentwise.agv.model.environment.EnvironmentState;

/**
 * A class responsible for reacting on influences by calculating the effect of
 * an influences according to a certain law
 * 
 * @author nelis
 */
public abstract class Reactor {

	/**
	 * Constructor
	 */
	public Reactor() {
		super();
	}
	
	/**
	 * React on influences by calculating an collection of effects of this influences 
	 * 
	 * @param influences The collection of influences to calculate the effects from
	 * @param environment The environment state
	 * @return A collection of effects
	 */
	public abstract Collection doReact(Collection influences, EnvironmentState environment);
}
