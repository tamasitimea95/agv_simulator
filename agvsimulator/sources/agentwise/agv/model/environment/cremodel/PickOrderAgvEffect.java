/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.model.environment.Order;

/**
 * This class represents the effect of an AGV that picks up an order
 * 
 * @author nelis
 */
public class PickOrderAgvEffect extends AgvEffect {

	/**
	 * Constructor
	 * 
	 * @param agvEnvState The environmental state of the AGV
	 */
	public PickOrderAgvEffect(AgvEnvironmentState agvEnvState) {
		super(agvEnvState);
	}
	/**
	 * Picks the order from the current OrderLocation
	 * and sets this order as current order for the AGV
	 *    
	 * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
	public void doEffect(EnvironmentState envState) {
		try {
			Order o = getAgvenvstate().getCurrentMapComponent().pickOrder();
			getAgvenvstate().setCurrentOrder(o);
		} catch (ActionFailedException e) {
			e.printStackTrace();
		}
	}

}
