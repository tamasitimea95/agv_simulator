/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.environment.EnvironmentState;

/**
 * A class responsible for collecting influences entering the environment
 * 
 * @author nelis
 */
public abstract class Effector {

	/**
	 * 
	 */
	public Effector() {
		super();
	}
	
	public void doEffects(Collection effects, EnvironmentState envState){
		Iterator iter = effects.iterator();
		while(iter.hasNext()){
			doEffect((Effect)iter.next(), envState);
		}
	}
	
	protected abstract void doEffect(Effect effect, EnvironmentState envState);


}
