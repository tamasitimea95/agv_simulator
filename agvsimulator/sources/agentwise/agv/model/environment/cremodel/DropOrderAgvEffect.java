/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;

/**
 * This class represents the effect of dropping down an order 
 * on a OrderLocation
 * 
 * @author nelis
 *
 */
public class DropOrderAgvEffect extends AgvEffect {

	/**
	 * Constructor
	 * @param agvEnvState The environmental state of the AGV
	 */
	public DropOrderAgvEffect(AgvEnvironmentState agvEnvState) {
		super(agvEnvState);
	}

	/**
	 * Drops down the order on the location
	 * and removes the current order of the AGV
	 *  
	 * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
	public void doEffect(EnvironmentState envState) {
		try {
			getAgvenvstate().getCurrentMapComponent().dropOrder(getAgvenvstate().getCurrentOrder());
			getAgvenvstate().removeCurrentOrder();
		} catch (ActionFailedException e) {
			e.printStackTrace();
		}

	}

}
