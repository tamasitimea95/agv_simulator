/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.AgvEnvironmentState;

/**
 * This class represent an effect on an AGV in the environment that
 * is about to be considered
 * 
 * @author nelis
 */
public abstract class AgvEffect extends Effect {

	/**
	 * Constructor
	 */
	public AgvEffect(AgvEnvironmentState agvEnvState) {
		super();
		setAgvenvstate(agvEnvState);
	}

	/**
	 * The state of the Agv in the environment
	 */
	private AgvEnvironmentState agvenvstate;

	/**
	 * @return Returns the agvenvstate.
	 */
	protected AgvEnvironmentState getAgvenvstate() {
		return agvenvstate;
	}

	/**
	 * @param agvenvstate The agvenvstate to set.
	 */
	protected void setAgvenvstate(AgvEnvironmentState agvenvstate) {
		this.agvenvstate = agvenvstate;
	}

}
