/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A class responsible for collecting influences entering the environment 
 * 
 * @author nelis
 *
 */
public class Collector {

    /**
     * The collected influences
     */
	protected Collection influences = new ArrayList();
	
	/**
	 * Constructor
	 */
	public Collector() {
		super();
	}
	
	/**
	 * Put the given collection of influences in the collector
	 * 
	 * @param influences A collection of influences
	 */
	public synchronized void putInfluences(Collection influences){
		this.influences.addAll(influences);		
	}

	/**
	 * Return all collected influences and empty the collector\
	 * 
	 * @return All collected influences
	 */
	public synchronized Collection collectAlleCurrentInfluences(){
		Collection temp = getAlleInfluences();
		resetQueue();
		return temp;
	}

	/**
	 * @return All collected influences
	 */
	protected Collection getAlleInfluences(){
		return influences;
	}
	
	/**
	 * Empty the collector
	 */
	protected void resetQueue(){
		influences = new ArrayList();
	}

}
