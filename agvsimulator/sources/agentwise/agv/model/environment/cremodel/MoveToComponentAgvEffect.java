/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.model.environment.map.MapComponent;

/**
 * This class reprents the effect on an AGV for moving on a certain 
 * mapcomponent
 * 
 * @author nelis
 */
public class MoveToComponentAgvEffect extends AgvEffect {

    /**
     * The mapcomponent to move to
     */
	private MapComponent mapComponent;
	
	/**
	 * @return Returns the mapComponent.
	 */
	protected MapComponent getMapComponent() {
		return mapComponent;
	}
	/**
	 * @param mapComponent The mapComponent to set.
	 */
	protected void setMapComponent(MapComponent mapComponent) {
		this.mapComponent = mapComponent;
	}
	/**
	 * Constructor
	 * 
	 * @param agvEnvState The environmental state of the AGV
	 * @param mapComponent The mapcomponent to move to
	 */
	public MoveToComponentAgvEffect(AgvEnvironmentState agvEnvState, MapComponent mapComponent) {
		super(agvEnvState);
		setMapComponent(mapComponent);
	}

	/** 
	 * Set the current mapcomonent of the AGV to the new Mapcomponent
	 * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
	public void doEffect(EnvironmentState envState) {
		getAgvenvstate().setCurrentMapComponent(getMapComponent());
	}

}
