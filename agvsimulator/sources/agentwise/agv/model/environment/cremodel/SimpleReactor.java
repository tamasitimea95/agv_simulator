/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.model.environment.Influence;

/**
 * 
 * This class represents a simple reactor. It does not use the concept of an explicit 
 * law, instead the laws are hard coded in an influence. 
 * 
 * @author nelis
 *
 * TODO [somebody] explicit laws in the environment ??
 */
public class SimpleReactor extends Reactor {

	/**
	 * Constructor
	 */
	public SimpleReactor() {
		super();
	}

	/**
	 * Calculates a collection of effects on basis of the collection of influences
	 * and the environment state.  
	 *  
	 * @see agentwise.agv.model.environment.cremodel.Reactor#doReact(java.util.Collection, agentwise.agv.model.environment.EnvironmentState)
	 */
	public Collection doReact(Collection influences, EnvironmentState environment) {
		Collection effects = new ArrayList();
		Iterator iter = influences.iterator();
		
		while(iter.hasNext()){
			Influence i = (Influence)iter.next();
			Collection effects1 = i.calculateEffects(environment);
			if(effects1!=null){
				effects.addAll(effects1);
			}
		}
		return effects;
	}
	
	

}
