/*
 * Created on Oct 21, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;


/**
 * This effect suspends an Agv which means that
 * certain actions will fail as long as the agv is suspended.
 * 
 * @author tomdw
 *
 */
public class SuspendAgvEffect extends AgvEffect {
    
    /**
     * 
     * Constructs an instance of SuspendAgvEffect
     * for an agv with a given state
     *
     * @param state The AgvEnvironmentState to affect
     */
    public SuspendAgvEffect(AgvEnvironmentState state) {
        super(state);
    }

    /**
     * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
     */
    public void doEffect(EnvironmentState envState) {
        getAgvenvstate().suspend();
    }

}
