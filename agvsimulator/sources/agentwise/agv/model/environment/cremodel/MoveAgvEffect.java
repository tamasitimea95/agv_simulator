/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.util.Coordinate;
import agentwise.agv.util.Rectangle;

/**
 * This class represent the effect of moving towards a certain 
 * coordinate
 * 
 * @author nelis
 */
public class MoveAgvEffect extends AgvNewSituationEffect {

    /**
     * The coordinate to move to
     */
	private Coordinate coordinate;
	
	/**
	 * Constructor
	 * @param agvEnvState The state of the AGV that has to be moved
	 * @param c The coordinate to move to
	 */
	public MoveAgvEffect(AgvEnvironmentState agvEnvState, Coordinate c) {
		super(agvEnvState);
		setCoordinate(c);
	}

	/**
	 * Change the coordinate of the AGV to the new coordinate
	 *  
	 * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
	public void doEffect(EnvironmentState envState) {
		getAgvenvstate().setCoordinate(getCoordinate());
	}

	/**
	 * @return Returns the coordinate.
	 */
	protected Coordinate getCoordinate() {
		return coordinate;
	}
	/**
	 * @param coordinate The coordinate to set.
	 */
	protected void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

   	/**
     * @return a Rectangle instance representing the new situation of the Agv after the effect has been executed
     *
	 * @see agentwise.agv.model.environment.cremodel.AgvNewSituationEffect#simulateEffect(agentwise.agv.model.environment.EnvironmentState)
	 */
    protected Rectangle simulateEffect(EnvironmentState envState) {
        Rectangle r = getAgvenvstate().getBox();
        Rectangle newR = r.move(getCoordinate());
        return newR;
    }
}
