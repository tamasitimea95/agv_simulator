/*
 * Created on Oct 21, 2004
 *
 */
package agentwise.agv.model.environment.cremodel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.EnvironmentState;
import agentwise.agv.util.Rectangle;

/**
 * This abstract AgvEffect is a specific effect on an
 * Agv where the position (more specifically the surrounding
 * box) of the agv changes as a consequence.
 * 
 * This means that we need support for collision detection and
 * part of that support is handled by this intermediate class. 
 * Each agv effect that changes the position will subclass this
 * AgvNewSituationEffect.
 * 
 * @author tomdw
 *
 */
public abstract class AgvNewSituationEffect extends AgvEffect {

    /**
     * Constructs an instance of AgvNewSituationEffect
     * where the effect is to be done on the given state
     * of an agv
     *
     * @param agvEnvState The given AgvEnvironmentState
     */
    public AgvNewSituationEffect(AgvEnvironmentState agvEnvState) {
        super(agvEnvState);
    }

    /**
     * The effect changes the surrounding box of the agv
     * and possibly other things as well.
     * 
     * @see agentwise.agv.model.environment.cremodel.Effect#doEffect(agentwise.agv.model.environment.EnvironmentState)
     */
    public abstract void doEffect(EnvironmentState envState);
 
    
    /**
     * Checks if there is a collision with other agvs
     * by first simulating the effect and then searching
     * the agvs that collide. Those colliding agvs are returned
     * by this method (excluding the agv on which this effect is done).
     * 
     * @return a Collection of AgvEnvironmentStates
     */
    public Collection getOtherAgvsInCollision(EnvironmentState envState) {
        Collection result = new ArrayList();
        Rectangle newR = simulateEffect(envState);
        Collection agvstates = envState.getAgvStates();
        Iterator iter = agvstates.iterator();
        while (iter.hasNext()) {
            AgvEnvironmentState s = (AgvEnvironmentState)iter.next();
            if (s.getAgvID().getID()!=getAgvenvstate().getAgvID().getID()) {
	            Rectangle other = s.getBox();
	            if (other.intersectsWith(newR)) result.add(s);
            }
        }
        return result;
    }

    /**
     * Allows to simulate the new situation effect by
     * calculating the new surrounding box of the agv
     * after the effect would been executed.
     * 
     * @param envState The global state of the environment
     * @return a Rectangle instance that represents the new surrounding box
     */
    protected abstract Rectangle simulateEffect(EnvironmentState envState);


}
