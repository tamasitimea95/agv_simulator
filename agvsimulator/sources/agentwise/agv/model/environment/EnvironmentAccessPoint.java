/*
 * Created on Oct 5, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;

import agentwise.agv.model.environment.communication.CommunicationFailedException;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;

/**
 * This class represents an Access Point for an Agv to the environment. Each
 * Agv has one corresponding access point. All agent to environment 
 * methods are handled in here.
 * 
 * @author nelis,tomdw
 *
 */
public class EnvironmentAccessPoint implements AgentToEnvironmentInterface {
	
    /**
     * The state of the Agv that is part of the environment
     */
	protected AgvEnvironmentState agvEnvState;
	
	/**
	 * The central part of the environment
	 * that is responsible for the handling
	 * of influences and provides the
	 * communication infrastructure
	 */
	protected Environment environment;

	/**
	 * 
	 * Constructs an instance of EnvironmentAccessPoint where
	 * a given Environment will be used to handle influences and
	 * provide a communication infrastructure. A given AgvEnvironmentState
	 * is registered as the state of the Agv for which this EnvironmentAccessPoint is 
	 * an access point to the environment.
	 *
	 * @param env The environment
	 * @param aes The given state of the agv
	 * @throws IllegalArgumentException
	 */
    public EnvironmentAccessPoint(Environment env, AgvEnvironmentState aes) throws IllegalArgumentException {
        setEnvironment(env);
        setAgvEnvState(aes);
        getEnvironment().getCommunicationInfrastructure().registerReceiver(getAgvEnvState(), getAgvEnvState().getAgvID());
    } 
    
    
    /**
     * @see agentwise.agv.model.environment.AgentToEnvironmentInterface#sense(java.lang.String)
     */
    public Object sense(String perceptionName) {
    	/**
    	 * coordinate
    	 * direction
    	 * networkid
    	 * isonsegment
    	 * isonstation
    	 * goalstationid
    	 * neighbouringstationids
    	 * currentmapcomponentid
    	 * orderholding
    	 * hasorder
    	 * canpickorder
    	 * candroporder
    	 * issuspended
    	 */
    	// TODO [all] herzien sense in de richting van perceptionobjects
        if(perceptionName.equals("coordinate")){
    		return getAgvEnvState().getCoordinate();
    	}else if(perceptionName.equals("direction")){ 
    		return getAgvEnvState().getDirection();
    	}else if(perceptionName.equals("networkid")){ 
    		return getAgvEnvState().getAgvID();
    	}else if(perceptionName.equals("isonsegment")){
    		return getAgvEnvState().isOnSegment();
    	}else if(perceptionName.equals("isonstation")){
    		return getAgvEnvState().isOnStation();
    	}else if(perceptionName.equals("goalstationid")){
    		return getAgvEnvState().getGoalStationId();
    	}else if(perceptionName.equals("neighbouringstationids")){
    		return getAgvEnvState().getNeighbouringStationIds();
    	}else if(perceptionName.equals("currentmapcomponentid")){
    		return getAgvEnvState().getCurrentMapComponent().getID();
    	}else if(perceptionName.equals("orderholding")){
    		return getAgvEnvState().getCurrentOrderInfo();
    	}else if(perceptionName.equals("hasorder")){
    		return new Boolean(getAgvEnvState().hasCurrentOrder());
    	}else if(perceptionName.equals("candroporder")){
    		return new Boolean(getAgvEnvState().getCurrentMapComponent().canDropOrder());
    	}else if(perceptionName.equals("canpickorder")){
    		return new Boolean(getAgvEnvState().getCurrentMapComponent().canPickOrder());
    	}else if(perceptionName.equals("issuspended")) {
    	    return new Boolean(getAgvEnvState().isSuspended());
    	}
		return null;   
    }


    /**
     * Put a whole collection of influences to be processed
     * by the environment
     * @param influences The collection of influences
     */
    public void influence(Collection influences) {
    	getEnvironment().getCollector().putInfluences(influences);
    	// TODO [all] Aanpassen bij update tijdsmodel
    	// Link naar de environment mag dan weg!
    	environment.step();
    }
    
    /**
     * 
     * @see agentwise.agv.model.environment.AgentToEnvironmentInterface#influence(agentwise.agv.model.environment.AgvInfluence)
     */
    public void influence(AgvInfluence influence) {
    	ArrayList temp = new ArrayList();
    	influence.setAgvenvstate(getAgvEnvState());
    	temp.add(influence);
    	influence(temp);
    }
    
   

    /**
     * @return message the first message in this AGV's inbox. Null if the inbox is empty.
     * @see agentwise.agv.model.environment.AgentToEnvironmentInterface#receive()
     */   
    public Message receive(){
    	return getAgvEnvState().retrieveMessage();
    }
    
    
    /**
     * Sends a message to a certain AGV, given by the specified NetworkID.
     * @param destination The given networkid of the receiver
     * @param message the message to send
     *
     * @see agentwise.agv.model.environment.AgentToEnvironmentInterface#unicast(agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message)
     */
    public void unicast(NetworkID destination, Message message) {
        try {
            getEnvironment().getCommunicationInfrastructure().unicast(getAgvEnvState(),destination, message);
        } catch (CommunicationFailedException e) {
            // failure of delivery of message, do not feedback to agv
        }
    }

    /**
     * Broadcasts a message to all AGVs within reach of the communication infrastructure

     * @param message the message to broadcast
     * @see agentwise.agv.model.environment.AgentToEnvironmentInterface#broadcast(agentwise.agv.model.environment.communication.Message)
     */
    public void broadcast(Message message) {
    	getEnvironment().getCommunicationInfrastructure().broadcast(getAgvEnvState(),message);
        
    }
 
    /**
     * Sets the Environment that is the central part of the
     * environment where the agvs are situated. It is 
     * responsible for handling
     * influences and providing the communication
     * infrastructure.
     * 
     * @param env The given Environment
     * @throws IllegalArgumentException when env==null
     */
    protected void setEnvironment(Environment env) throws IllegalArgumentException {
        if (env==null) throw new IllegalArgumentException("The given environment cannot be null");
        environment = env;
    }
    
    /**
     * Returns the Environment that is responsible
     * for handling influences and providing the
     * communication infrastructure
     * 
     * @return the Environment instance used by this Access Point
     */
    protected Environment getEnvironment() {
        return environment;
    }

	/**
	 * @return Returns the agvEnvState of the agv to which this EnvironmentAccessPoint belongs
	 */
	protected AgvEnvironmentState getAgvEnvState() {
		return agvEnvState;
	}
	/**
	 * Sets the AgvEnvironmentState of the agv to which this EnvironmentAccessPoint belongs
	 * @param agvEnvState The agvEnvState to set.
	 * @throws IllegalArgumentException when the given agvenvState is null
	 */
	protected void setAgvEnvState(AgvEnvironmentState agvEnvState) throws IllegalArgumentException {
	    if (agvEnvState==null) throw new IllegalArgumentException("An EnvironmentAccessPoint cannot be constructed with a AgvEnvironmentState that is null");
		this.agvEnvState = agvEnvState;
	}

    /**
     * @see timeManagementFramework.Identifiable#getName()
     */
    public String getName() {
        return ""+getAgvEnvState().getAgvID().getID()+"";
    }
	
	
}
