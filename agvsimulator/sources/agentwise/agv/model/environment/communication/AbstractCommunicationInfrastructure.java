/*
 * Created on Oct 18, 2004
*/
package agentwise.agv.model.environment.communication;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * An abstract class that supports specific implementation of Infrastucture by maintaining
 * a map with NetworkIDs to CommunicationNodes (called receivers).
 * 
 * @author Kurt Schelfthout
 */
public abstract class AbstractCommunicationInfrastructure implements
        CommunicationInfrastructure {

    /**
     * Maps ids to communicationnodes 
     */
    private Map idToReceiver = new HashMap();

    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.CommunicationInfrastructure#registerReceiver(agentwise.agv.model.environment.communication.CommunicationNode, agentwise.agv.model.environment.communication.NetworkID)
     */
    public void registerReceiver(CommunicationNode rec, NetworkID id) {
        idToReceiver.put(id, rec);
    }

    /**
     * Returns all receivers known to this AbstractCommunicationInfrastructure.
     */
    protected Collection getReceivers() {
        return idToReceiver.values();
    }

    /**
     * @param destination the NetworkID of a CommunicationNode.
     * @return the CommunicationNode with the given destination as NetworkID.
     */
    protected CommunicationNode getReceiver(NetworkID destination) {
        return (CommunicationNode) idToReceiver.get(destination);
    }

}
