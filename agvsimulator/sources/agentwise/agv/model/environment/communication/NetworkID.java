/*
 * Created on Oct 7, 2004
*/
package agentwise.agv.model.environment.communication;

/**
 * The ID of something in the network. Each NetworkID should be unique for the simulation.
 * 
 * @author Kurt Schelfthout
 */
public interface NetworkID {

    /**
     * @return a long representation of this NetworkID.
     */
    public long getID();
    
}
