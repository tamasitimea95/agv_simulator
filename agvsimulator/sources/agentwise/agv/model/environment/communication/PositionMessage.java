/*
 * Created on Feb 4, 2005
*/
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.agv.Knowledge;
import agentwise.agv.util.Coordinate;

/**
 * @author Kurt Schelfthout
 */
public class PositionMessage extends KnowledgeMessage {

    public final static String COORDINATE = "coordinate";
    
    private Coordinate coordinate;
    
    /**
     * @param knowledge
     */
    public PositionMessage(Knowledge knowledge) {
        super(knowledge);
        //this.coordinate = (Coordinate) knowledge.get(COORDINATE);
    }

    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.KnowledgeMessage#update()
     */
    public void update() {
        this.coordinate = (Coordinate) getKnowledge().get(COORDINATE);
        //System.out.println("updating "+this);
    }

    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.Message#copy()
     */
    public Message copy() {
        PositionMessage result = new PositionMessage(null);
        result.coordinate = new Coordinate(this.coordinate);
        result.setSender(getSender());
        return result;
    }
    
    public String toString() {
        return "PositionMsg: "+coordinate.toString();
    }

}
