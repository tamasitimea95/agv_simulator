/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;


/**
 * @author Kurt Schelfthout
 * TODO [kurts] Documenting  and Implementing AccessPointInfrastructure
 */
public class AccessPointInfrastructure extends AbstractCommunicationInfrastructure {

    /**
     * 
     */
    public AccessPointInfrastructure() {
        super();
    }

    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.CommunicationInfrastructure#broadcast(agentwise.agv.model.environment.AgentToEnvironmentInterface, agentwise.agv.model.environment.communication.Message)
     */
    public void broadcast(CommunicationNode sender, Message message) {
        
    }




    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.CommunicationInfrastructure#unicast(agentwise.agv.model.environment.AgentToEnvironmentInterface, agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message)
     */
    public void unicast(CommunicationNode sender, NetworkID destintation, Message message) {
        
    }

}
