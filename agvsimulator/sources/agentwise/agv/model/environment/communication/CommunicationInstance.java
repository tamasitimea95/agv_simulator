/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * A Communication instance encapsulates how a certain message should be sent.
 * This can be broadcast, unicast, or other primitive ways of sending a message.
 * But, one can also use such a primitive to periodically do that communication
 * through a PeriodicCommunication.
 * 
 * @author tomdw
 *
 */
public interface CommunicationInstance {
    /**
     * Executes the communication which is represented by this communication instance
     * by sending the right instructions to the given AgentToEnvironmentInterface
     * 
     * @param environment the given environment interface through which all communication is sent
     * @throws Exception when the communication failed and was not executed
     */
    public void doCommunication(AgentToEnvironmentInterface environment)
            throws Exception;
}