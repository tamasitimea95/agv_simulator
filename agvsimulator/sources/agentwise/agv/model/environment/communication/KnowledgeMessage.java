/*
 * Created on Feb 4, 2005
*/
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.agv.Knowledge;

/**
 * A message that encapulates some knowledge. Be sure not to copy the complete knowledge
 * in subclasses. In update(), you can use the Knowledge to update the actual contents of the message.
 * 
 * @author Kurt Schelfthout
 */
public abstract class KnowledgeMessage extends DefaultMessage {

    
    protected Knowledge knowledge;
    
    /**
     * Constructs a message that uses the given knowledge to update itself.
     */
    public KnowledgeMessage(Knowledge knowledge) {
        super();
        this.knowledge = knowledge;
    }
    
    /**
     * Is called before the message is sent; in this method,
     * you can write code to update the state of the message to 
     * reflect the current Knowledge.
     *
     */
    public abstract void update();
    
    public Knowledge getKnowledge() {
        return knowledge;
    }

}
