/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * This PrimitiveCommunicationInstance encapuslates other CommunicationInstances
 * that need to be sent periodically according to a given time step (a
 * number of time steps to skip before sending again).
 * 
 * @author tomdw
 *
 */
public class PeriodicCommunication implements CommunicationInstance {

    /**
     * Constructs an instance of PeriodicCommunication that needs
     * to do a given PrimitiveCommunicationInstance every
     * given number of timesteps
     *
     * @param com The given PrimitiveCommunicationInstance
     * @param timesteps The given number of time steps to skip before sending again
     * @throws NullPointerException when the given PrimitiveCommunicationInstance is null
     * @throws IllegalArgumentException when the given timesteps is smaller than 0
     */
    public PeriodicCommunication(PrimitiveCommunicationInstance com, int timesteps) throws NullPointerException, IllegalArgumentException {
        setTimesteps(timesteps); // throws IllegalArgumentException
        setCommunication(com); // throws NullPointerException
        giveUniqueID();
    }
    
    /**
     * Assigns a unique ID to this PeriodicCommunication so
     * that each PeriodicCommunication instance can be
     * identified
     */
    private void giveUniqueID() {
        ID = nextFreeID;
        nextFreeID++;
    }

    private static long nextFreeID = 0;

    /**
     * 
     * @uml.property name="ID" 
     */
    private long ID;

    /**
     * Returns the unique ID that identifies
     * the PeriodicCommunication instances
     * 
     * @return a long representing the ID
     * 
     * @uml.property name="ID"
     */
    public long getID() {
        return ID;
    }

    /**
     * 
     * @uml.property name="timesteps" 
     */
    private int timesteps;
    
    private int timestepsleft = 0;
    
    /**
     * Resets the time steps left to
     * the period needed between two
     * communications
     *
     */
    private void resetTimeStepsLeft() {
        timestepsleft = timesteps;
    }
    
    /**
     * Decreases the number of time steps
     * left before a next communication is
     * issued
     *
     */
    private void decreaseTimeStepsLeft() {
        timestepsleft--;
    }
    
    /**
     * Returns the time left before
     * a next communication needs to be
     * issued
     * 
     * @return the number of time steps left
     */
    private int getTimeStepsLeft() {
        return timestepsleft;
    }
    
    /**
     * Checks if the time is up to issue the
     * next communication
     * 
     * @return true if time is up, false otherwise
     */
    private boolean timeIsUp() {
        return getTimeStepsLeft() == 0;
    }

    /**
     * 
     * @uml.property name="communication" 
     */
    private PrimitiveCommunicationInstance communication;



    /**
     * @see agentwise.agv.model.environment.communication.CommunicationInstance#doCommunication(agentwise.agv.model.environment.AgentToEnvironmentInterface)
     */
    public void doCommunication(AgentToEnvironmentInterface environment)
            throws Exception {
        if (timeIsUp()) {
            communication.doCommunication(environment);
            resetTimeStepsLeft();
        } else {
            decreaseTimeStepsLeft();
        }
    }

    /**
     * Sets the timesteps to the given value
     * 
     * @param timesteps The timesteps to set.
     * @throws IllegalArgumentException when the given number of timesteps is negative
     * 
     * @uml.property name="timesteps"
     */
    private void setTimesteps(int timesteps) throws IllegalArgumentException {
        if (timesteps < 0)
            throw new IllegalArgumentException(
                "the given time steps to skip before sending a message again should be positive");
        this.timesteps = timesteps;
    }

    /**
     * Returns the timesteps of this PeriodicCommunication
     * @return Returns the timesteps.
     * 
     * @uml.property name="timesteps"
     */
    public int getTimesteps() {
        return timesteps;
    }

    /**
     * Sets the communication to the given value
     * 
     * @param communication The communication to set.
     * @throws NullPointerException when the given PrimitiveCommunicationInstance is null
     * 
     * @uml.property name="communication"
     */
    private void setCommunication(PrimitiveCommunicationInstance communication) {
        if (communication==null) throw new NullPointerException("The given primitive communication instance to periodically send out cannot be null");
        this.communication = communication;
    }

    /**
     * Returns the communication of this PeriodicCommunication
     * @return Returns the communication.
     * 
     * @uml.property name="communication"
     */
    public PrimitiveCommunicationInstance getCommunication() {
        return communication;
    }

}
