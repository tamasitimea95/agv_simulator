/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;

/**
 * Convenience class for implementing a NetworkID. Provides a long as 
 * identifier. Should be enough for most applications.
 * @author Kurt Schelfthout
 */
public class DefaultNetworkID implements NetworkID {

    private long id;
    
    /**
     * Constructs a new DefaultNetworkID with the given id.
     */
    public DefaultNetworkID(long id) {
        this.id = id;
    }

    /**
     * @see agentwise.agv.model.environment.communication.NetworkID#getID()
     */
    public long getID() {
        return id;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "ID."+getID();
    }

    /**
     * Factory method for providing unique NetworkIDs. Obviously the method knows only
     * about previous NetworkIDs instantiated using this method, and so uniqueness only 
     * takes those into account.
     * @return a new unique NetworkID instance.
     */
    public static NetworkID nextUniqueID() {
        NetworkID result = new DefaultNetworkID(uniqueID);
        uniqueID++;
        return result;
    }

    private static long uniqueID=0;
}
