/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * This communication instance defines that the encapsulated 
 * message should be sent through a broadcast.
 * 
 * @author tomdw
 *
 */
public class Broadcast extends PrimitiveCommunicationInstance {

    /**
     * Constructs an instance of Broadcast for a
     * given Message
     *
     * @param msg The message to broadcast
     * @throws NullPointerException when the given message is null
     */
    public Broadcast(Message msg) throws NullPointerException {
        super(msg);
    }

    /**
     * Broadcasts the encapsulated message
     * 
     * @see agentwise.agv.model.environment.communication.CommunicationInstance#doCommunication(agentwise.agv.model.environment.AgentToEnvironmentInterface)
     */
    public void doCommunication(AgentToEnvironmentInterface environment) throws Exception {
        if (environment==null) throw new NullPointerException("The given environment interface cannot be null");
        if (getMessage() instanceof KnowledgeMessage) ((KnowledgeMessage) getMessage()).update();
        //System.out.println("broadc "+getMessage());
        environment.broadcast(this.getMessage().copy());
    }

}
