/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;

/**
 * CommunicationInfrastructure manages the communication between AGVs. An implemented
 * class specifies what the parameters of the communcation are (e.g. wireless ad hoc, wireless 
 * access point, wired,...) and how the communication is simulated.
 * @author Kurt Schelfthout
 * 
 * 
 */
public interface CommunicationInfrastructure {

    /**
     * Broadcasts a message.
     * @param sender the sender of the message.
     * @param message the message.
     */
    void broadcast(CommunicationNode sender, Message message);
    
    /**
     * Sends a message to one particular receiver.
     * @param sender sender of the message
     * @param destintation NetworkID of the intended receiver.
     * @param message the message.
     */
    void unicast(CommunicationNode sender, NetworkID destintation, Message message) throws CommunicationFailedException;
    
    /**
     * Registers the CommunicationNode s a receiver with the infrastructure. This must be
     * called first in order to allow a CommunicatioNode to receive messages.
     * @param receiver The receiver to register
     * @param id The networkId of the receiver
     */
    void registerReceiver(CommunicationNode receiver, NetworkID id);

}
