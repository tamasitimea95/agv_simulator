/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * This communication instance defines that the encapsulated 
 * message should be sent through a unicast to a specified 
 * destination (identified by a NetworkID).
 * 
 * @author tomdw
 *
 */
public class Unicast extends PrimitiveCommunicationInstance {

    /**
     * Constructs an instance of Unicast for a 
     * given message and given NetworkID
     *
     * @param msg The Message instance to unicast
     * @param netid The networkID of the destination of the unicast
     * @throws NullPointerException when msg==null or netid==null
     */
    public Unicast(Message msg, NetworkID netid) throws NullPointerException {
        super(msg);
        setDestinationid(netid); // throws NullPointerException
    }

    /**
     * 
     * @uml.property name="destinationid" 
     */
    private NetworkID destinationid;


    /**
     * Unicasts the encapsulated message to the encapsulated network id of the destination
     * 
     * @see agentwise.agv.model.environment.communication.CommunicationInstance#doCommunication(agentwise.agv.model.environment.AgentToEnvironmentInterface)
     */
    public void doCommunication(AgentToEnvironmentInterface environment)
            throws Exception {
        if (environment==null) throw new NullPointerException("the given environment interface cannot be null");
        if (getMessage() instanceof KnowledgeMessage) ((KnowledgeMessage) getMessage()).update();
        environment.unicast(getDestinationid(),this.getMessage().copy());

    }

    /**
     * Sets the destinationid to the given value
     * 
     * @param destinationid The destinationid to set.
     * @throws NullPointerException when the given NetworkID is null
     * 
     * @uml.property name="destinationid"
     */
    private void setDestinationid(NetworkID destinationid) throws NullPointerException {
        if (destinationid==null) throw new NullPointerException("The given network id for a unicast cannot be null");
        this.destinationid = destinationid;
    }

    /**
     * Returns the destinationid of this Unicast
     * @return Returns the destinationid.
     * 
     * @uml.property name="destinationid"
     */
    public NetworkID getDestinationid() {
        return destinationid;
    }

}
