/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.environment.communication;

import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * A PrimitiveCommunicationInstance encapsulates a Message
 * that needs to be sent through a specific type of
 * communication (broadcast, unicast, ...)
 * 
 * @author tomdw
 *
 */
public abstract class PrimitiveCommunicationInstance implements CommunicationInstance {

    /**
     * Constructs an instance of PrimitiveCommunicationInstance
     * that encapsulates a given Message
     * 
     * @param msg The given Message instance
     * @throws NullPointerException when the given message is null
     */
    public PrimitiveCommunicationInstance(Message msg) throws NullPointerException {
        super();
        if (msg==null) throw new NullPointerException("the given Message instance cannot be null");
        message = msg;
    }

    /**
     * 
     * @uml.property name="message" 
     */
    private Message message;

    /**
     * Returns the message encapsulated in this communication instance
     * 
     * @return the Message instance
     * 
     * @uml.property name="message"
     */
    public Message getMessage() {
        return message;
    }
    
    /**
     * Executes the communication which is represented by this communication instance
     * by sending the right instructions to the given AgentToEnvironmentInterface
     * 
     * @param environment the given environment interface through which all communication is sent
     * @throws Exception when the communication failed and was not executed
     */
    public abstract void doCommunication(AgentToEnvironmentInterface environment) throws Exception;

}
