/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.model.environment.communication;

/**
 * This exception is thrown when a communication
 *  in the Environment fails.
 * 
 * @author tomdw
 *
 */
public class CommunicationFailedException extends Exception {

    /**
     * Constructs an instance of CommunicationFailedException
     *
     * @param message A message that explains the failure
     */
    public CommunicationFailedException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of CommunicationFailedException
     *
     * @param message A message explaining the failure
     * @param cause The exception that caused the failure
     */
    public CommunicationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

}
