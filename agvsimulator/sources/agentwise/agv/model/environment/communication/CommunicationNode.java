/*
 * Created on Oct 11, 2004
*/
package agentwise.agv.model.environment.communication;

import agentwise.agv.util.Coordinate;

/**
 * @author Kurt Schelfthout
 * 
 * A CommunicationNode is an entity in the system that can send messages and receive messages using an
 * Inbox.
 */
public interface CommunicationNode {
    
    /**
     * @return this CommunicationNode's position
     */
    Coordinate getCoordinate();
    
    /**
     * @return this CommunicationNode's NetworkID
     */
    NetworkID getNetworkID();
    
    /**
     * @return this CommunicationNode's Inbox.
     */
    Inbox getInbox();
}
