/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;

import java.util.Iterator;

/**
 * AdHocInfrastructure is CommunicationInfrastructure that works without
 * wireless access points; communication range is thus limited to a certain range. The model
 * for communication used is very simple:
 * CommunicationNodes can reach each other if they are withing the given range. Messages are discarded
 * otherwise.
 * @author Kurt Schelfthout
 */
public class AdHocInfrastructure extends AbstractCommunicationInfrastructure{

    private double range;
    
    /**
     * New AdhocInfrastructure with as communication range the given range in meters.
     * @param range the communication range the nodes using this infrastuture have.
     */
    public AdHocInfrastructure(double range) {
        super();
        this.range = range;
    }

    /**
     * Boradcasts the message to all CommunicationNodes within range.
     * @see agentwise.agv.model.environment.communication.CommunicationInfrastructure#broadcast(agentwise.agv.model.environment.communication.CommunicationNode, agentwise.agv.model.environment.communication.Message)
     */
    public void broadcast(CommunicationNode sender, Message message) {
        //System.out.println("broadcasting "+ message);
        message = message.copy();
        message.setSender(sender.getNetworkID());
        for (Iterator iter = getReceivers().iterator(); iter.hasNext();) {
			CommunicationNode receiver = (CommunicationNode) iter.next();
			if (areWithinRange(sender, receiver) && (! receiver.equals(sender)))
				receiver.getInbox().add(message.copy());
		}      
    }

    /**
     * @param sender
     * @param receiver
     * @return
     */
    private boolean areWithinRange(CommunicationNode sender, CommunicationNode receiver) {
        //return sender.getCoordinate().distanceTo(receiver.getCoordinate()) <= range;
    	return true;
    }

    /**
     * Sends a message to one given destination, if that destination is within 
     * range (no multi-hop routing is done!)
     * 
     * @see agentwise.agv.model.environment.communication.CommunicationInfrastructure#unicast(CommunicationNode, agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message)
     */
    public void unicast(CommunicationNode sender, NetworkID destination, Message message) throws CommunicationFailedException{
       message = message.copy();
       message.setSender(sender.getNetworkID());
       CommunicationNode receiver = getReceiver(destination);
        if (areWithinRange(sender, receiver)) {
            receiver.getInbox().add(message);
        } else {
            throw new CommunicationFailedException("The range of the ad-hoc communication infrastructure was to small to reach to receiver");
        }
    }
    
    /**
     * Returns the range of communication. How
     * far does the communication infrastructure
     * reach.
     * 
     * @return a double representing the communication range (in coordinate distance)
     */
    public double getCommunicationRange() {
        return range;
    }
    
    
}
