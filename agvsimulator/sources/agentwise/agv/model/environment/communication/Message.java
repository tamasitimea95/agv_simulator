/*
 * Created on Oct 7, 2004
*/
package agentwise.agv.model.environment.communication;

/**
* a message that can be sent and received by the CommunicationInfrastrucuture.
 * @author Kurt Schelfthout
 */
public interface Message {
    
    /**
     * @return the NetworkID of the sender of the Message.
     */
    NetworkID getSender();

    /**
     * sets the sender of the message.
     * @param networkID
     */
    void setSender(NetworkID networkID);

    /**
     * @return a copy (deep clone) of this message.
     */
    Message copy();
    
    

}
