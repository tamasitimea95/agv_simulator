/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;

/**
 * Convenience class for implementing a Message. Provides the ability to set a 
 * NetworkID as sender.
 * @author Kurt Schelfthout
 */
public abstract class DefaultMessage implements Message {

    private NetworkID sender;
    
    /**
     * New DefaultMessage with the given NetworkID as sender.
     */
    protected DefaultMessage(NetworkID sender) {
        this.sender = sender;
    }
    
    /**
     * Constructs a new DefaultMessage. Sender is set by infrastructure if the message is sent.
     */
    public DefaultMessage() {
    }
    
    /**
     * @see agentwise.agv.model.environment.communication.Message#getSender()
     */
    public NetworkID getSender() {
        return sender;
    }

    /* (non-Javadoc)
     * @see agentwise.agv.model.environment.communication.Message#setSender(agentwise.agv.model.environment.communication.NetworkID)
     */
    public void setSender(NetworkID networkID) {
        this.sender = networkID;
    }

}
