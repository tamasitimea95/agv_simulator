/*
 * Created on Oct 11, 2004
*/
package agentwise.agv.model.environment.communication;

/**
 * This message allows to send a string message.
 * 
 * @author Kurt Schelfthout
 * 
 * 
 */
public class StringMessage extends DefaultMessage {

    private String message;

    /**
     * Constructs a StringMessage for a given sender
     * and message
     * 
     * @param sender the id of the sender
     * @param message The String message
     */
    protected StringMessage(NetworkID sender, String message) {
        super(sender);
        this.message = message;
    }
    
    /**
     * Constructs a StringMessage without a sender. The sender is set
     * by the CommunicationInfrastructure automatically when the message is sent.
     * 
     * @param message The String message.
     */
    public StringMessage(String message) {
        this.message = message;
    }
    
    /**
     * Returns the string message 
     * 
     * @return a String that represents the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "Msg from: "+getSender()+" msg="+getMessage();
    }
    
    public Message copy() {
        return new StringMessage(getSender(), getMessage());
    }
           
}
