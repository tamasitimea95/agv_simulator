/*
 * Created on Oct 8, 2004
*/
package agentwise.agv.model.environment.communication;

import java.util.ArrayList;
import java.util.List;

/**
 * A class responsible for receiving and buffering incoming messages.
 * 
 * @author Kurt Schelfthout
 */
public class Inbox {

    private List inbox = new ArrayList();
    
    /**
     * Adds a message to the inbox.
     * @param m
     */
    void add(Message m) {
        inbox.add(m);
    }
    
    /**
     * Returns and removes the oldest (first added) message in the Inbox.
     * @return the oldest message in the Inbox. Returns null if the Inbox does not currently
     * contain any message.
     */
    public Message remove() {
        
        return (inbox.size() == 0) ? null : (Message) inbox.remove(0);
    }
    
    /**
     * Returns and does not remove the oldest (first added message
     * in the inbox. Returns null if there are no messages in the Inbox.
     * @return the oldest message in the Inbox
     */
    public Message look() {
        return (inbox.size() == 0) ? null : (Message) inbox.get(0);
    }

}
