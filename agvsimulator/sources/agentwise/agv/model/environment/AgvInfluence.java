/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment;


/**
 * Represents an influence of an AGV on the environment
 * 
 * @author nelis
 */
public abstract class AgvInfluence extends Influence {

    /**
     * The state environment state of the influencing AGV
     */
	private AgvEnvironmentState agvenvstate;
	
	/**
	 * constructor
	 */
	public AgvInfluence() {
		super();		
	}

	
	/**
	 * @return Returns the agvenvstate.
	 */
	public AgvEnvironmentState getAgvenvstate() {
		return agvenvstate;
	}
	/**
	 * @param agvenvstate The agvenvstate to set.
	 */
	void setAgvenvstate(AgvEnvironmentState agvenvstate) {
		this.agvenvstate = agvenvstate;
	}
}
