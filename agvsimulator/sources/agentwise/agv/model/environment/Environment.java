/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.Collection;
import java.util.HashSet;

import agentwise.agv.model.environment.communication.CommunicationInfrastructure;
import agentwise.agv.model.environment.cremodel.Collector;
import agentwise.agv.model.environment.cremodel.Effector;
import agentwise.agv.model.environment.cremodel.Reactor;

/**
 * The Environment of the Automated Guided Vehicle simulation
 * is the world where the AGVs and other components of the environment
 * are situated in. The environment is a 2-dimensional plane
 * with an euclidean coordinate system on it. It has a height
 * and width.
 * 
 * The environment is also responsible to manage the
 * collector-reactor-effector mechanisms to handle
 * influences and execute their effects. Therefore
 * the states of the Agv and the state of the environment
 * itself are needed and available through the EnvironmentState
 * interface that this class implements.
 * 
 * The environment also supplies the CommunicationInfrastructure
 * to use when communicating
 * 
 * @author nelis,tomdw
 *
 */
public class Environment implements EnvironmentState{
    
    /* The width of the environment */
    private double width = 0;
    /* The height of the environment */
    private double height = 0;
    
    /**
     * The collector that collects the influences
     */
    private Collector collector;
    
    /**
     * The reactor that calculates the effects of the influences
     */
    private Reactor reactor;
    
    /**
     * The effector that executes the effects given to it
     */
    private Effector effector;
     
    /**
     * 
     * Constructs an instance of Environment with a given
     * width and height. The given collector, reactor
     * and effector are used to handle influences and
     * the given communicationinfrastructure is to be
     * used when communicating
     * 
     * @param width The width of the environment
     * @param height The height of the environment
     * @param collector The Collector for the environment
     * @param reactor The Reactor for the environment
     * @param effector The Effector for the environment
     * @param infrastructure The CommunicationInfrastructure for the environment
     * @throws IllegalArgumentException when the collector, reactor, effector or infrastructure are null
     * @throws IllegalArgumentException when the width or height are negative
     */
    public Environment(double width, double height
            , Collector collector, Reactor reactor
            , Effector effector
            , CommunicationInfrastructure infrastructure) 
            throws IllegalArgumentException {
        setWidth(width); //throws
        setHeight(height); //throws
        setCollector(collector); // throws
        setReactor(reactor); // throws
        setEffector(effector); // throws
        setCommunicationInfrastructure(infrastructure); // throws
    }

    /**
     * Sets the height of this environment to the given value
     * 
     * @param height The given value for the height
     * @throws IllegalArgumentException when the given height is negative or zero
     */
    protected void setHeight(double height) throws IllegalArgumentException {
        if (height <= 0) throw new IllegalArgumentException("The given height must be larger than 0");
        this.height = height;
    }

    /**
     * Sets the width of this environment to the given value
     * 
     * @param width The given value for the width
     * @throws IllegalArgumentException when the given width is negative or zero
     */
    protected void setWidth(double width) {
        if (width <= 0) throw new IllegalArgumentException("The given width must be larger than 0");
        this.width = width;
    }
    
    /**
     * Returns the width of this environment
     * 
     * @return an integer represting the number of coordinates in the width of this environment
     */
    public double getWidth() {
        return width;
    }
    
    /**
     * Returns the height of this environment
     * 
     * @return an integer represting the number of coordinates in the height of this environment
     */
    public double getHeight() {
        return height;
    }
    
    
       
    /**
     * On cycle step in the environment
     * Collector -> Reactor -> Effector
     * 
     * For more information, see on the webpage of the inventor
     *  
     * http://www.cs.kuleuven.ac.be/~danny/publications.html
     * 
     */
    public void step(){
    	Collection influences = getCollector().collectAlleCurrentInfluences();
    	if(influences == null) return;
    	Collection effects = getReactor().doReact(influences, this);
    	if(effects == null) return;
    	getEffector().doEffects(effects,this);
    }
    
        
    /**
	 * @return Returns the collector.
	 */
	public Collector getCollector() {
		return collector;
	}
	/**
	 * @param collector The collector to set.
	 * @throws IllegalArgumentException when collector==null
	 */
	private void setCollector(Collector collector) throws IllegalArgumentException {
	    if (collector==null) throw new IllegalArgumentException("The given collector cannot be null");
		this.collector = collector;
	}
    
    
	/**
	 * @return Returns the effector.
	 */
	public Effector getEffector() {
		return effector;
	}
	/**
	 * @param effector The effector to set.
	 * @throws IllegalArgumentException when effector==null
	 */
	private void setEffector(Effector effector) throws IllegalArgumentException {
		if (effector==null) throw new IllegalArgumentException("The given effector cannot be null");
	    this.effector = effector;
	}
	/**
	 * @return Returns the reactor.
	 */
	public Reactor getReactor() {
		return reactor;
	}
	/**
	 * @param reactor The reactor to set.
	 * @throws IllegalArgumentException when the given reactor==null
	 */
	private void setReactor(Reactor reactor) throws IllegalArgumentException {
	    if (reactor==null) throw new IllegalArgumentException("the given reactor cannot be null");
		this.reactor = reactor;
	}
	/**
	 * The collection of Agv instances of this Environment
	 */
	private Collection agvstates = new HashSet();




	/**
	 * Returns the collection of Agvs in this
	 * Environment. This collection is the 
	 * original collection present in the environment.
	 * Changes to it will affect the environment.
	 * 
	 * @return a Collection containing the Agv instances
	 */
	public Collection getAgvStates() {
	    return agvstates;
	}
	
	/**
	 * Adds the given state of an Agv to the Environment. This
	 * way it is situated in the environment of this 
	 * simulation. 
	 * 
	 * @param agvState The state of the Agv to add
	 * @throws IllegalArgumentException when the given agv is null
	 */
	public void registerAgv(AgvEnvironmentState agvState) throws IllegalArgumentException {
	    if (agvState==null) throw new IllegalArgumentException("The given Agv cannot be null when added to the environment");
	    agvstates.add(agvState);
	}

    /**
     * Sets the CommunicationInfrastructure to use
     * 
     * @param infrastructure The infrastructure
     */
    private void setCommunicationInfrastructure(CommunicationInfrastructure infrastructure) throws IllegalArgumentException {
        if (infrastructure==null) throw new IllegalArgumentException("The given communicationinfrastructure for an environment cannot be null");
        communicationInfrastructure = infrastructure;
    }
    
    /**
     * Returns the communicationinfrastructure that is to be used
     * 
     * @return a CommunicationInfrastructure instance
     */
    public CommunicationInfrastructure getCommunicationInfrastructure() {
        return communicationInfrastructure;
    }
    
    /**
     * The CommunicationInfrastructure to use
     */
    private CommunicationInfrastructure communicationInfrastructure;
}
