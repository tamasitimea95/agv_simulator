/*
 * Created on Oct 13, 2004
 *
 */
package agentwise.agv.model.environment.map;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.Order;
import agentwise.agv.util.Coordinate;

/**
 * A location that can contain an order
 * 
 * @author nelis
 */
public class OrderLocation extends Location{ //implements Observable{

	/**
	 * The order on this location
	 */
	private Order order = null;
	/**
	 * Does this OrderLocation support picking up an order?
	 */
	private boolean supportsPick=true;
	/**
	 * Does this OrderLocation support dropping an order?
	 */
	private boolean supportsDrop=true;
	
	/**
	 * Constructor
	 * @param ID The unique identifier of a location
	 * @param x The x-component of the coordinate of this location
	 * @param y The y-component of the coordinate of this location
	 */
	public OrderLocation(String ID, double x, double y) {
		super(ID, x, y);
	}

	/**
	 * Constructor
	 * @param ID The unique identifier of a location
	 * @param c The coordinate of this location
	 */
	public OrderLocation(String ID, Coordinate c) {
		super(ID, c);		
	}
	
	/**
	 * Set the properties
	 * @param supportsPick Does this OrderLocation supports picking up an order
	 * @param supportsDrop Does this OrderLocation supports dropping down an order
	 */
	public void setInOutProperties(boolean supportsPick, boolean supportsDrop){
		this.supportsPick = supportsPick;
		this.supportsDrop = supportsDrop;
	
	}
	
	/**
	 * Does this OrderLocation supports picking up an order
	 * @return true if so, false otherwise
	 */
	public boolean supportsPickOrder(){
		return supportsPick;
	} 
	/**
	 * Does this OrderLocation supports dropping down an order
	 * @return true if so, false otherwise
	 */
	public boolean supportsDropOrder(){
		return supportsDrop;
	}
	
	/**
	 * @return True if the OrderLocation has an Order, false otherwise
	 */
	public boolean hasOrder(){
		return (getOrder()!=null);
	}
	
	/**
	 * Can an order be picked up by an AGV on this moment
	 * @return true If this location supports picking up an order
	 * 				and the location contain an order
	 * 		   false otherwise
	 */
	public boolean canPickOrder(){
		return supportsPickOrder() && (getOrder()!=null);
	}
	
	/**
	 * Can an order be droped down by an AGV on this moment
	 * @return true If this location supports dropping down an order
	 * 				and the location does not contain an order
	 * 		   false otherwise
	 */
	public boolean canDropOrder(){
		return supportsDropOrder() && (getOrder()==null);
	}
	
	/**
	 * Drop the order
	 */
	public void dropOrder(Order order) throws ActionFailedException{
    	if(!canDropOrder()) throw new ActionFailedException("Illegal action: dropOrder");
    	setOrder(order);
    }
	
    /**
     * Pick up the order
     */
    public Order pickOrder() throws ActionFailedException{
    	if(canPickOrder()){
    		Order temp = removeOrder();    	
    		return temp;
    	}
    	else{
    		return null;
    	}
    }
	
    /**
     * Get the order on this location
     * @return an Order instance
     */
	public Order getOrder(){
		return order;
	}
	
	/**
	 * Adds an order to this location
	 * @param o The Order to add
	 * @throws IllegalStateException when the OrderLocation already has an order
	 */
	public void addOrder(Order o){
		if(hasOrder()) throw new IllegalStateException("The orderlocation already has an order");
		setOrder(o);
	}
	
	/**
	 * Removes the order from this OrderLocation
	 * 
	 * @return  the Order that was removed
	 */
	public Order removeOrder(){
		if(!hasOrder()) throw new IllegalStateException("The orderlocation has no order");
		Order temp = getOrder();
		setOrder(null);
		return temp;
	}
	
	
	/**
	 * @param order The order to set.
	 */
	protected void setOrder(Order order) {
		this.order = order;
	}
}
