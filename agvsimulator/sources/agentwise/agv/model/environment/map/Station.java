/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.model.environment.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.Order;
import agentwise.agv.util.Coordinate;

/**
 * This class represents a station in the environment where
 * multiple segments can start and end. Therefore a station
 * has a number of incomming and outgoing segments associated
 * to it.
 * 
 * @author tomdw, nelis
 *
 */
public class Station extends MapComponent {

    /* the coordinate of the station */
    private Coordinate coordinate = null; 
    
    /* the outgoing segments for this station */
    private Collection outSegments = new ArrayList();
    
    /* the incomming segments for this station */
    private Collection inSegments = new ArrayList();
       
    /**
     * Constructs an instance of Station
     * with a given id and on a given x and y
     * coordinate
     *
     * @param id The ID identifying this station
     * @param x The x coordinate of the station
     * @param y the y coordinate of the station
     */
    public Station(String id,double x, double y) {
        setCoordinate(x,y);
        setID(id);
    }

    /**
     * 
     * Constructs an instance of Station
     * with a given id and on a given coordinate
     *
     * @param id The id identifying this station
     * @param c The coordinate for the station
     */
    public Station(String id, Coordinate c) {
        this(id,c.getX(),c.getY());
    }
    
    /**
     * Sets the current coordinate of the station
     * to a given x and y coordinate
     * 
     * @param x The given x coordinate
     * @param y The given y coordinate
     */
    public void setCoordinate(double x, double y) {
        coordinate = new Coordinate(x,y);
    }
    
    /**
     * Sets the current coordinate of the station
     * to a given Coordinate
     * 
     * @param c The given Coordinate
     * @throws IllegalArgumentException when c==null
     */
    public void setCoordinate(Coordinate c) throws IllegalArgumentException {
        if (c==null) throw new IllegalArgumentException("The given coordinate cannot be null");
        coordinate = new Coordinate(c);
    }

    /**
     * Returns the Coordinate of this station
     * 
     * @return a new Coordinate instance, independent of the coordinate contained in the station
     */
    public Coordinate getCoordinate() {
        return new Coordinate(coordinate);
    }

    /**
     * Adds a given segments as a segment that departs from this station.
     * 
     * @param s the given Segmenet
     * @throws IllegalArgumentException when the given segment is null
     * @throws IllegalArgumentException when the given segment does not start at this station
     */
    public void addOutSegment(Segment s) throws IllegalArgumentException {
        if (s==null) throw new IllegalArgumentException("The given segment cannot be null");
        if (!s.getStart().equals(this)) throw new IllegalArgumentException("An outgoing segment should have the station where you want to add it as an outgoing segment as a start station");
        outSegments.add(s);
    }
    
    /**
     * Returns the outgoing segments of this station.
     * 
     * @return a Collection containing the outgoing segments, changes to this collection will not affect the collection inside the station
     */
    public Collection getOutSegments() {
        return new ArrayList(outSegments);
    }

    /**
     * Adds a given segments as an incomming segment for this station
     * @param s The Segment to add to the incomming segments
     * @throws IllegalArgumentException when the given segment is null
     * @throws IllegalArgumentException when the given segment does not end in this station
     */
    public void addInSegment(Segment s) throws IllegalArgumentException {
        if (s==null) throw new IllegalArgumentException("The given segment cannot be null");
        if (!s.getEnd().equals(this)) throw new IllegalArgumentException("An incomming segment should have the station where you want to add it as an incomming segment as a end station");
        inSegments.add(s);
    }
    
    /**
     * Returns the incomming segments for this station
     * @return a Collection containing the incomming segments, changes to this collection will not affect the collection inside the station
     */
    public Collection getInSegments() {
        return new ArrayList(inSegments);
    }
    
    
    
    
    /**
     * A given Coordinate is in range if it is in range
     * of the coordinate of this station
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#hasInRange(agentwise.agv.util.Coordinate)
     */
    public boolean hasInRange(Coordinate c) {
        return c.isInRangeOf(getCoordinate());
    }

    
    /**
     * Dropping an order on a normal station will fail by default
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#dropOrder(agentwise.agv.model.environment.Order)
     */
    public void dropOrder(Order order) throws ActionFailedException{
    	throw new ActionFailedException("Drop Order is an illegal Action on Station");
    }
    
   
    /**
     * Picking up an order on a normal station will fail by default
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#pickOrder()
     */
    public Order pickOrder() throws ActionFailedException{
    	throw new ActionFailedException("Pick Order is an illegal Action Action on Station");
    }

    /**
     * @return false
     * @see agentwise.agv.model.environment.map.MapComponent#isSegment()
     */
    public boolean isSegment() {
        return false;
    }

    /**
     * @return true
     * @see agentwise.agv.model.environment.map.MapComponent#isStation()
     */
    public boolean isStation() {
        return true;
    }

    /**
     * There is no goal station for a station, throws an exception
     * @see agentwise.agv.model.environment.map.MapComponent#getGoalStationID()
     */
    public String getGoalStationID() throws IllegalStateException {
        throw new IllegalStateException("A station cannot give a goal station");
    }

    /**
     * Returns the station ids of all the station
     * that are directly reachable by following one outgoing segment
     * in this station
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#getNeighbouringStationIds()
     */
    public List getNeighbouringStationIds() throws IllegalStateException {
        Iterator iter = outSegments.iterator();
        List result = new ArrayList();
        while(iter.hasNext()) {
            Segment s = (Segment)iter.next();
            result.add(s.getEnd().getID());
        }
        return result;
    }

    /**
     * @see agentwise.agv.model.environment.map.MapComponent#getNeighbouringStations()
     */
    public List getNeighbouringStations() throws IllegalStateException {
        Iterator iter = outSegments.iterator();
        List result = new ArrayList();
        while(iter.hasNext()) {
            Segment s = (Segment)iter.next();
            result.add(s.getEnd());
        }
        return result;
    }

}
