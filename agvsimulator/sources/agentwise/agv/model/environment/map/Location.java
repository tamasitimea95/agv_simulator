/*
 * Created on Oct 6, 2004
 *
 */
package agentwise.agv.model.environment.map;

import agentwise.agv.util.Coordinate;

/**
 * 
 * A locations is a special type of station where 
 * an AGV can perform an other action than moving
 * 
 * @author nelis 
 * 
 * TODO [somebody] ParkLocation: Agv can park here
 * TODO [somebody] BatteryLoadLocation: Agv can load its battery here | Battery in Agv
 */
public abstract class Location extends Station {

	
	/**
	 * Constructor
	 * @param ID The unique identifier of this location
	 * @param x The x-component of the coordinate of this location
	 * @param y The y-component of the coordinate of this location
	 */
	public Location(String ID,double x, double y)
	{ 	super(ID,x,y);
	}
	
	/**
	 * Constructor
	 * @param ID The unique identifier of this location
	 * @param c The coordinate of this location
	 */
	public Location(String ID,Coordinate c)
	{ 	super(ID,c);
	}
}
