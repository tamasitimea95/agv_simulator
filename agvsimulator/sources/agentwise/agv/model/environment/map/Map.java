/*
 * Created on Oct 28, 2004
 *
 */
package agentwise.agv.model.environment.map;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A class representing the map on which the AGV resides
 * 
 * @author nelis
 *
 */
public class Map {

    /**
     * A collection of Segment instances that
     * represent the segments or layout of the simulation
     */
    private Collection segments = new ArrayList();

    /**
     * Constructor
     *
     */
    public Map() {
        super();

    }

    /**
     * Adds a collection of Segments to the Simulation
     * as the segments or layout on which the agvs can
     * move.
     * 
     * @param segments The Collection of Segment instances
     */
    public void addSegments(Collection segments) {
        this.segments = segments;
    }

    /**
     * Returns the segments or layout on which the agvs in this
     * Simulation can move as a Collection of Segments
     * 
     * @return a Collection of Segment instances
     */
    public Collection getSegments() {
        return segments;
    }

}
