/*
 * Created on Oct 5, 2004
 *
 */
package agentwise.agv.model.environment.map;

import java.util.List;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.Order;
import agentwise.agv.util.Coordinate;

/**
 * A MapComponent is one part of the Map or network of segments, locations, and stations
 * on which an Agv is situated.
 * 
 * @author tomdw, nelis
 *
 */
public abstract class MapComponent {

    /**
     * Checks if the given coordinate is close to
     * this component in order for an Agv to move in one
     * step on that component
     * 
     * @param coordinate The coordinate of the agv
     * @return true if the coordinate is close enough, false otherwise
     */
    public abstract boolean hasInRange(Coordinate coordinate);

    /**
     * Drops the given Order at the current coordinate of
     * the agv. If it is not possible to drop an Order there
     * or dropping that order failed then an exception is thrown.
     * 
     * @param order The Order to drop
     * @throws ActionFailedException when dropping the order failed
     */
    public abstract void dropOrder(Order order) throws ActionFailedException;
    
    /**
     * Picks an Order that is situated at the current coordinate
     * of the agv. If no order is present or picking it up failed
     * an exception is thrown. If it succeeds then the Order that
     * is returned can no longer be situated in the environment.
     * 
     * @return the Order that is picked up.
     * @throws ActionFailedException
     */
    public abstract Order pickOrder() throws ActionFailedException;

    /**
     * The ID identifying this MapComponent
     */
	protected String ID;

	/**
	 * @return Returns the ID of the MapComponent
	 */
	public String getID() {
		return ID;
	}

	/**
	 * Sets the ID that identifies this MapComponent
	 * 
	 * @param mapcompID The ID to set.
	 */
	public void setID(String mapcompID) {
		this.ID = mapcompID;
	}

    /**
     * Checks if this MapComponent is a segment or not
     * 
     * @return true if the MapComponent is a Segment
     */
    public abstract boolean isSegment();
    
    /**
     * Checks if this MapComponent is a station or not
     * 
     * @return true if the MapComponent is a Station
     */
    public abstract boolean isStation();

    /**
     * Returns the ID of the station that is the 
     * goal of this MapComponent.
     * 
     * @return an ID of the station
     * @throws IllegalStateException when this MapComponent cannot return a goal station
     */
    public abstract String getGoalStationID() throws IllegalStateException;

    /**
     * Returns a list of ids of the stations that
     * are directly connected to this mapcomponent
     * via a segment.
     * 
     * @return a List of Strings that represent the map component ids
     * @throws IllegalStateException when this map component is not connected to stations via segments
     */
    public abstract List getNeighbouringStationIds() throws IllegalStateException;

    /**
     * Returns a list of station instances that
     * are directly connected to this mapcomponent
     * via a segment
     * 
     * @return a List of Station instances 
     * @throws IllegalStateException when this map component is not connected to stations via segments
     */
    public abstract List getNeighbouringStations() throws IllegalStateException;
    
	/**
	 * Can an order be droped down by an AGV on this moment
	 * @return true If this location supports dropping down an order
	 * 				and the location does not contain an order
	 * 		   false otherwise
	 */
	public boolean canDropOrder(){
		return false;
	}

	/**
	 * Can an order be picked up by an AGV on this moment
	 * @return true If this location supports picking up an order
	 * 				and the location contain an order
	 * 		   false otherwise
	 */
	public boolean canPickOrder(){
		return false;
	}

}
