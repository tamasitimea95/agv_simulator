/*
 * Created on Oct 13, 2004
 *
 * TODO [nelis] Implement parklocation 
 */
package agentwise.agv.model.environment.map;

import agentwise.agv.util.Coordinate;

/**
 * Unimplemented, represents a Location where the AGV can park
 * 
 * @author nelis
 *
 */
public class ParkLocation extends Location {

	/**
	 * @param ID
	 * @param x
	 * @param y
	 */
	public ParkLocation(String ID, double x, double y) {
		super(ID, x, y);
		
	}

	/**
	 * @param ID
	 * @param c
	 */
	public ParkLocation(String ID, Coordinate c) {
		super(ID, c);
		
	}

}
