/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.model.environment.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import agentwise.agv.model.agv.behaviour.ActionFailedException;
import agentwise.agv.model.environment.Order;
import agentwise.agv.util.Coordinate;

/**
 * This class represents a segment in the environment that
 * connects two stations with a certain path.
 * 
 * @author tomdw, nelis
 *
 */
public class Segment extends MapComponent {
    
    
    /**
     * @directed true
     * @label startsAt
     */

    protected Station startStation;
    /**
     * @directed true
     * @label endsAt
     */

    protected Station endStation;
    
    /**
     * indicates if the endstation has already bean
     * set when constructing the segment. If not
     * the segment is not yet complete and valid
     */
    protected boolean endStationSet = false;
    
    /**
     * The points or coordinates defining the segment
     * 
     */
    protected List points = new ArrayList();
    
    /**
     * Constructs a segment starting a a given start station.
     * A segment is only complete and valid when the end station
     * is set after adding all the points individually
     * 
     */
    public Segment(Station start) throws IllegalArgumentException {
        setStart(start); // throws
    }
    
    /**
     * Sets the start station of the segment
     * 
     * @param start The given Station
     * @throws IllegalArgumentException when the given station is null
     */
    public void setStart(Station start) throws IllegalArgumentException {
        if (start==null) throw new IllegalArgumentException("The given start station cannot be null");
        startStation = start;
        points.add(startStation.getCoordinate());
    }
    
    /**
     * Returns the station where this segment starts
     * 
     * @return a Station instance
     */
    public Station getStart() {
        return startStation;
    }
    
    /**
     * Sets the end station of this segment. Setting this
     * station means completing the segment. No other
     * points can be added afterwards
     * 
     * @param end The end station
     * @throws IllegalArgumentException when the given end station is null
     */
    public void setEnd(Station end) throws IllegalArgumentException {
        if (end==null) throw new IllegalArgumentException("The given end station cannot be null");
        addNextPoint(end.getCoordinate()); 
        endStationSet = true;
        endStation = end;
    }
    
    /**
     * Returns the station in which this segments ends.
     * 
     * @return a Station instance representing the end station
     */
    public Station getEnd() {
        return endStation;
    }
    
    /**
     * Adds a given Coordinate as the next point in the segment.
     * This can only be done if the segment has not yet been closed
     * by setting the end station.
     * 
     * @param c The Coordinate to add
     * @throws IllegalStateException when the end station was already set
     * @throws IllegalArgumentException when  c==null
     */
    public void addNextPoint(Coordinate c) throws IllegalStateException, IllegalArgumentException {
        if (!endStationSet) {
            if (c==null) throw new IllegalArgumentException("The given coordinate to add as a next point for a segment cannot be null");
            points.add(c);
        } else {
            throw new IllegalStateException("The segment can no longer accept new points, the end station was already set");
        }
    }
    
    /**
     * Returns the next coordinate when iterating over the segment. The current
     * is the given Coordinate, the next is calculated based on this given coordinate.
     * 
     * @param currentC The current Coordinate
     * @return a Coordinate that follows the given currentC in the segment
     * @throws NullPointerException when the given coordinate is null
     * @throws IllegalArgumentException when the given coordinate is associated with the end station and no next coordinate exists
     */
    public Coordinate getNextStep(Coordinate currentC) throws IllegalArgumentException, NullPointerException{
    	if (currentC==null) throw new NullPointerException("The given coordinate cannot be null");
        List points = this.getPoints();
    	int nearestCoordinateIndex = getNearestCoordinateIndex(currentC);   	
    	
    	if(nearestCoordinateIndex >= points.size()-1){
    		throw new IllegalArgumentException("The AGV is currently at the end station");
    	}else{
    		return (Coordinate) points.get(nearestCoordinateIndex+1);
    	}        
    }
    
    /**
     * Returns the coordinate that is nearest to the given coordinate and
     * is part of this segment.
     * 
     * @param currentC The given coordinate
     * @return a Coordinate on the segment that is nearest to the given coordinate
     * @throws IllegalArgumentException when the segment has no points
     */
    public Coordinate getNearestCoordinate(Coordinate currentC) throws IllegalArgumentException{
    	return (Coordinate) this.getPoints().get(getNearestCoordinateIndex(currentC));    	
    }
    
    /**
     * Returns the index into the points array of the coordinate that is nearest ot the given coordinate and
     * is part of this segement.
     * 
     * @param currentC The given coordinate
     * @return a integer representing the index into the points array
     * @throws IllegalArgumentException when the segment has no points
     */
    protected int getNearestCoordinateIndex(Coordinate currentC) throws IllegalArgumentException{
    	Coordinate c = null;
    	int minCoordIndex=0;
    	double minDistance = Double.MAX_VALUE;
    	List points = getPoints();
    	
    	if(points.size() == 0) throw new IllegalArgumentException("A segment without points????");
    	
    	for(int tel=0; tel< points.size(); tel++){
    		c = (Coordinate) points.get(tel);
    		if(c.distanceTo(currentC) < minDistance){
    			minCoordIndex = tel;
    			minDistance = currentC.distanceTo(c);
    		}
    	}
    	return minCoordIndex;
	 }    
    
    /**
     * Returns a list with the coordinates that define this segment.
     * 
     * @return a List that is a copy of the list of points inside the segment, adjusting a coordinate instance will affect the segment however
     */
    public List getPoints() {
        return new ArrayList(points);
    }   
       
    /**
     * @see agentwise.agv.model.environment.map.MapComponent#hasInRange(agentwise.agv.util.Coordinate)
     */
    public boolean hasInRange(Coordinate coordinate) {
        boolean result = false;
        Iterator iter = points.iterator();
        while(iter.hasNext()) {
            Coordinate c = (Coordinate)iter.next();
            if (c.isInRangeOf(coordinate)) result = true;
        }
        return result;
    }   
   
    /**
     * You cannot drop an order on a segment
     * @see agentwise.agv.model.environment.map.MapComponent#dropOrder(agentwise.agv.model.environment.Order)
     */
    public void dropOrder(Order order) throws ActionFailedException{
    	throw new ActionFailedException("Illegal ActionModule");
    }
    
   
    /**
     * You cannot drop an order from a segment
     * @see agentwise.agv.model.environment.map.MapComponent#pickOrder()
     */
    public Order pickOrder() throws ActionFailedException{
    	throw new ActionFailedException("Illegal ActionModule");
    }

    /**
     * @return true
     * @see agentwise.agv.model.environment.map.MapComponent#isSegment()
     */
    public boolean isSegment() {
        return true;
    }

    /**
     * @return false
     * @see agentwise.agv.model.environment.map.MapComponent#isStation()
     */
    public boolean isStation() {
        return false;
    }

    /**
     * The Goal station of the segment is the end station, the id
     * of that end station is returned
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#getGoalStationID()
     */
    public String getGoalStationID() throws IllegalStateException {
        return getEnd().getID();
    }

    /**
     * A segment had no neighbouring stations that can be reached
     * by following a segment that starts from this segment
     * 
     * @see agentwise.agv.model.environment.map.MapComponent#getNeighbouringStationIds()
     */
    public List getNeighbouringStationIds() throws IllegalStateException {
        throw new IllegalStateException("A segment cannot give neighbouring station ids");
    }

    /**
     * @see agentwise.agv.model.environment.map.MapComponent#getNeighbouringStations()
     */
    public List getNeighbouringStations() throws IllegalStateException {
        throw new IllegalStateException("A segment cannot give neighbouring stations");
    }

 

}
