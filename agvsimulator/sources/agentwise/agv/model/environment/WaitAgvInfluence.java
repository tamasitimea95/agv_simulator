/*
 * Created on Feb 3, 2005
 * 
 */
package agentwise.agv.model.environment;

import java.util.Collection;

/**
 * @author nelis
 *
 * TODO [nelis] document WaitAgvInfluence
 */
public class WaitAgvInfluence extends AgvInfluence {

    /**
     * 
     */
    public WaitAgvInfluence() {
        super();
    }
    
    /**
   	 * Calculate the effects of doing nothing :-)
   	 */
   	public Collection calculateEffects(EnvironmentState envState) {   	    
   	    return null;
   	}


}
