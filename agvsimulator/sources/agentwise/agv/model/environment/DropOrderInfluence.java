/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;

import agentwise.agv.model.environment.cremodel.DropOrderAgvEffect;

/**
 * An influence to drop an order on an OrderLocation that 
 * supports dropping
 *
 *  @author nelis
 */
public class DropOrderInfluence extends AgvInfluence {

	/**
	 * Constructor
	 */
	public DropOrderInfluence() {
		super();
	}

	/**
	 * Calculate the effects of dropping an order
	 * The collection can either be empty or contain an
	 * DropOrderAgvEffect
	 */
	public Collection calculateEffects(EnvironmentState envState) {
	    if (!getAgvenvstate().isSuspended()) {
			if(!getAgvenvstate().hasCurrentOrder()|| !getAgvenvstate().getCurrentMapComponent().canDropOrder()) return null;
			else{ 
				Collection temp = new ArrayList();
				temp.add(new DropOrderAgvEffect(getAgvenvstate()));
				return temp;
			}
	    } 
	    return null;
	}

}
