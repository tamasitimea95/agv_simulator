/*
 * Created on Oct 18, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.Collection;

/**
 * The state of the environment
 * 
 * @author nelis
 *
 */
public interface EnvironmentState {
	/**
	 * Returns the collection of Agvs in this
	 * Environment. This collection is the 
	 * original collection present in the environment.
	 * Changes to it will affect the environment.
	 * 
	 * @return a Collection containing the Agv instances
	 */
	public abstract Collection getAgvStates();
	/**
	 * Register a new AGV in the environment
	 * @param agvState The environmental state of this AGV
	 * @throws IllegalArgumentException
	 */
	public abstract void registerAgv(AgvEnvironmentState agvState)
			throws IllegalArgumentException;
}