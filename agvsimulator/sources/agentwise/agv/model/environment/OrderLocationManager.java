/*
 * Created on Oct 13, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;

import timeManagementFramework.Identifiable;
import agentwise.agv.controller.ActiveEntity;
import agentwise.agv.model.environment.map.OrderLocation;

/**
 * This class is responisble for managing the orders on OrderLocations
 * 
 * @author nelis
 */
public abstract  class OrderLocationManager extends ActiveEntity implements Identifiable{

    /**
     * A collections of Orderlocations where an AGV can drop an order
     */
	private Collection dropOrderLocations = new ArrayList();
	/**
	 * A collections of Orderlocations where an AGV can pick an order
	 */
	private Collection pickOrderLocations = new ArrayList();
	
	/**
	 * A collection of OrderLocations where an Order can be temporary dropped
	 */
	private Collection bufferOrderLocations = new ArrayList();
	
	/**
	 * Constructor
	 */
	public OrderLocationManager() {
		super();
	}
	
	
  /**
   * @return A collections of Orderlocations an Order can be temporary dropped
   */
    public Collection getBufferOrderLocations() {
        return bufferOrderLocations;
    }

    /**
     * 
     * @return A collections of Orderlocations where an AGV can drop an order
     */
    public Collection getDropOrderLocations() {
        return dropOrderLocations;
    }

   /**
    * 
    * @return A collections of Orderlocations where an AGV can pick an order
    */
    public Collection getPickOrderLocations() {
        return pickOrderLocations;
    }

    /**
     * Add an OrderLocation to the collection of OrderLocations where
     * a package can be dropped
     *  
     * @param loc The OrderLocation to add
     */
	public void addDropOrderLocation(OrderLocation loc) {
		this.dropOrderLocations.add(loc);
	}
	
	/**
	 * Add an OrderLocation to the collection of OrderLocations where
     * a package can be picked
     *  
     * @param loc The OrderLocation to add
	 */
	public void addPickOrderLocation(OrderLocation loc) {
		this.pickOrderLocations.add(loc);
	}
	
	/**
	 * Add an OrderLocation to the collection of OrderLocations where
     * a package can temporary left
     *  
     * @param loc The OrderLocation to add
	 */
	public void addBufferOrderLocations(OrderLocation loc) {
		this.bufferOrderLocations.add(loc);
	}


    /**
     * @see timeManagementFramework.Identifiable#getName()
     */
    public String getName() {
        return "OrderLocationManager";
    }
}
