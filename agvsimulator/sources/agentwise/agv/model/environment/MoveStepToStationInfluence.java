/*
 * Created on Oct 19, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import agentwise.agv.model.environment.cremodel.AgvNewSituationEffect;
import agentwise.agv.model.environment.cremodel.MoveAgvEffect;
import agentwise.agv.model.environment.cremodel.MoveToComponentAgvEffect;
import agentwise.agv.model.environment.cremodel.SuspendAgvEffect;
import agentwise.agv.model.environment.cremodel.TurnAgvEffect;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.util.Angle;
import agentwise.agv.util.Coordinate;

/**
 * 
 * An influence that moves an AGV one step towards a given station
 * 
 * @author nelis
 * 
 */
public class MoveStepToStationInfluence extends AgvInfluence {
    
    /**
     * The size of a standard turn
     */
    private int turnSize = 45;

    /**
     * Sets the standard turn size, used when turning towards the goal station
     * @param size The size
     * @throws IllegalArgumentException if (size<1)
     */
    public void setTurnSize(int size) throws IllegalArgumentException {
        if (size < 1)
            throw new IllegalArgumentException(
                "The turn of an agv must be minimum 1 degree in size");
        turnSize = size;
    }

    /**
     * The id of the current goal station
     */
	private String goalStationID;

    /**
     * @return Returns the id of the current goal station.
     * 
     * @uml.property name="stationID"
     */
    public String getGoalStationID() {
        return goalStationID;
    }

    /**
     * Sets the id of the current goal station
     * @param goalStationID
     */
    protected void setGoalStationID(String goalStationID) {
        this.goalStationID = goalStationID;
    }

	/**
	 * Constructor
	 * @param goalStationID The id of the goal station
	 */
	public MoveStepToStationInfluence(String goalStationID) {
		super();
		setGoalStationID(goalStationID);
	}

	/**
	 * This operation calculates the effects of this influence
	 * Notice that this operation contains hard-coded laws 
	 *  @see agentwise.agv.model.environment.Influence#calculateEffects(agentwise.agv.model.environment.EnvironmentState)
	 */
	public Collection calculateEffects(EnvironmentState envState) {
		Collection temp = new ArrayList();
		Coordinate c = null;
		if (!isSuspended()) {
			if(isOnStation()){
				if(!isOnGoalStation()){
					
					// Neem segment met als eindstation het goal station
					Segment seg = getSegmentTowardsGoal();
					// Als er geen zo'n segment bestaat return null
					if(seg==null) return null;
					c = seg.getNearestCoordinate(getAgvenvstate().getCoordinate());
					turnAndMoveToCoordinate(temp, c, envState);
					temp.add(new MoveToComponentAgvEffect(getAgvenvstate(),seg));			
				}						
			}else{
				Segment seg = (Segment) getAgvenvstate().getCurrentMapComponent();
				c = seg.getNextStep(getAgvenvstate().getCoordinate());
				
				
				if(seg.getEnd().hasInRange(c)){
					boolean moved = turnAndMoveToCoordinate(temp, c, envState);
					if (moved) temp.add(new MoveToComponentAgvEffect(getAgvenvstate(),getEndStation()));
				}else{
					turnAndMoveToCoordinate(temp, c, envState);
				}
			}
		}
		return temp;
	}

	/**
     * @return
     */
    private boolean isSuspended() {
        return getAgvenvstate().isSuspended();
    }

	/**
	 * Turn and move towards a certain coordinate
     * @return true if a move was performed, false otherwise
     */
    protected boolean turnAndMoveToCoordinate(Collection influenceCollection, Coordinate goal, EnvironmentState envState) {
        if (!goal.isInRangeOf(getAgvenvstate().getCoordinate())) {
	        Angle angleGoal = getAgvenvstate().getCoordinate().calculateAngleTowards(goal);
	        /*if (!angleGoal.inRange(getAgvenvstate().getDirection())) {
	            Angle newDirection = new Angle(getAgvenvstate().getDirection());
	            if (angleGoal.difference(getAgvenvstate().getDirection())<=turnSize) newDirection = angleGoal;
	            else newDirection.turnAngleTowards(angleGoal, turnSize);
	            AgvNewSituationEffect effect = new TurnAgvEffect(getAgvenvstate(),newDirection);
	            detectAndReactOnCollision(influenceCollection, envState, effect);
	            return false;*/
	        double difference = angleGoal.difference(getAgvenvstate().getDirection());
	        if(difference>turnSize){
	            Angle newDirection;
	            newDirection = new Angle(getAgvenvstate().getDirection());
	            if((difference % turnSize)!=0){
	                newDirection.turnAngleTowards(angleGoal, turnSize + (difference % turnSize)); 
	            }else{
	                newDirection.turnAngleTowards(angleGoal, turnSize);
	            }
	            AgvNewSituationEffect effect = new TurnAgvEffect(getAgvenvstate(),newDirection);
	            detectAndReactOnCollision(influenceCollection, envState, effect);
	            return false;
	        } else {
	            AgvNewSituationEffect effect = new TurnAgvEffect(getAgvenvstate(),angleGoal);
	            boolean collision = detectAndReactOnCollision(influenceCollection, envState, effect);
	          	AgvNewSituationEffect effect2 = new MoveAgvEffect(getAgvenvstate(), goal);
	          	collision = collision & detectAndReactOnCollision(influenceCollection, envState, effect2);
	          	if (collision) {
	          	    return false;
	          	} else {
	          	    return true; // a move was done
	          	}
	        }
        } else {
            return true; // if within range then moved
        }
    }
    
    /**
     * @param influenceCollection
     * @param envState
     * @param effect
     * 
     * @return true if there was a collision
     */
    private boolean detectAndReactOnCollision(Collection influenceCollection, EnvironmentState envState, AgvNewSituationEffect effect) {
        boolean result = false;
        Collection otherCollAgvs = effect.getOtherAgvsInCollision(envState);
        if (otherCollAgvs.isEmpty()) influenceCollection.add(effect);
        else {
            influenceCollection.add(new SuspendAgvEffect(getAgvenvstate()));
            Iterator iter = otherCollAgvs.iterator();
            while(iter.hasNext()) {
                AgvEnvironmentState s =  (AgvEnvironmentState)iter.next();
                influenceCollection.add(new SuspendAgvEffect(s));
            }
            result = true;
        }
        return result;
    }
    protected Segment getSegmentTowardsGoal(){
		Station st = (Station) getAgvenvstate().getCurrentMapComponent();
		Segment seg = null;
		boolean hasGoalStation = false;
		
		Iterator iter = st.getOutSegments().iterator();
		while(iter.hasNext() && !hasGoalStation){
			seg = (Segment) iter.next();
			hasGoalStation = (seg.getEnd().getID().equals(getGoalStationID()));
		}
		
		if(!hasGoalStation){
			return null;
		}else{
			return seg;
		}
	}
	
	protected Station getEndStation(){
		Segment seg = (Segment) getAgvenvstate().getCurrentMapComponent();
		return seg.getEnd();
	}

	protected boolean isOnStation(){
		return getAgvenvstate().isOnStation().booleanValue();
	}
	
	protected boolean isOnGoalStation(){
		return getAgvenvstate().getCurrentMapComponent().getID().equals(this.getGoalStationID());
	}
}
