/*
 * Created on Oct 5, 2004
 *
 */
package agentwise.agv.model.environment;

import java.util.Date;

/**
 * This class is a transport order in the AGV-system
 * 
 * @author nelis
 */
public class Order {
	public enum Status {
		FREE,
		INPROGRESS,
		PICKEDUP,
		DONE
	}
    /**
     * This static variable helps in determining the
     * next unique if for an Order. By incrementing
     * this each time a new order is created, each
     * order is ensured to have a unique id within
     * this system execution.
     */
    private static long nextID = 0;

    /**
     * This is an id that uniquely identifies
     * this order in the system.
     * 
     * @uml.property name="id" 
     */
    private long id;


    /**
     * This is the id of the station where the order
     * should start. Where it is delivered by
     * and external entity.
     * 
     * @uml.property name="startStationId" 
     */
    private String startStationId;

    /**
     * This is the id of the station where the order should
     * go to. The destination where it should be delivered
     * by an agv from the system.
     * 
     * @uml.property name="destinationStationId" 
     */
    private String destinationStationId;

    /**
     * This is the time stamp in the format of
     * a date on which this Order instance was
     * created.
     */
	private Date creationTime = new Date();

    /**
     * This is the priority of this Order
     * within the system. This value
     * can range between 0 and 10. 0 means
     * a very low priority and 10 means a 
     * very high priority.
     * 
     * @uml.property name="priority" 
     */
    private int priority;
    
    private Status status;
	
	/**
	 * Constructs an instance of Order that should
	 * start at the station that has the given
	 * startStationId and should be delivered
	 * at the station with the given destinationStationId.
	 * The order is also given a priority.
	 * 
	 * @param startStationId The id of the station to start at
	 * @param destinationStationId The id of the station to deliver at
	 * @param priority The priority between 0 (low priority) and 10 (high priority) for this Order
	 * 
	 * @throws IllegalArgumentException when the given priority is not between 0 and 10
	 */
	public Order(String startStationId, 
	        String destinationStationId,
	        int priority) throws IllegalArgumentException {
		super();
		setId(Order.nextID);
		Order.nextID++;
		setStartStationId(startStationId);
		setDestinationStationId(destinationStationId);
		setPriority(priority); // throws
		setStatus(Status.FREE);
	}
	
	/**
	 * Constructs an instance of Order that should
	 * start at the station that has the given
	 * startStationId and should be delivered
	 * at the station with the given destinationStationId.
	 * The order gets the default priority of 5 (not high, not low)
	 * 
	 * @param startStationId The id of the station to start at
	 * @param destinationStationId The id of the station to deliver at
	 * 
	 */
	public Order(String startStationId, String destinationStationId) {
	    this(startStationId,destinationStationId,5);
	}

    /**
     * This is the id of the station where the order
     * should start. Where it is delivered by
     * and external entity.
     * 
     * @return Returns the startStationId.
     * 
     * @uml.property name="startStationId"
     */
    public String getStartStationId() {
        return startStationId;
    }

    /**
     * Set the startStationId. This is the id of the station where the order
     * should start. Where it is delivered by
     * and external entity.
     * 
     * @param from The startStationId to use.
     * 
     * @uml.property name="startStationId"
     */
    protected void setStartStationId(String from) {
        this.startStationId = from;
    }

    /**
     * This is the id of the station where the order should
     * go to. The destination where it should be delivered
     * by an agv from the system.
     * 
     * @return Returns the destinationStationId.
     * 
     * @uml.property name="destinationStationId"
     */
    public String getDestinationStationId() {
        return destinationStationId;
    }

    /**
     * Set the destinationStationId. This is the id of the station where the order should
     * go to. The destination where it should be delivered
     * by an agv from the system.
     * 
     * @param to The destinationStationId to use.
     * 
     * @uml.property name="destinationStationId"
     */
    protected void setDestinationStationId(String to) {
        this.destinationStationId = to;
    }

	/**
	 * @return Returns the time on which this order is created
	 */
	public Date getCreationTime() {
		return creationTime;
	}

    /**
     * Sets the priority to the given value.
     * This value ranges between 0 (low priority)
     * and 10 (high priority).
     * 
     * @param priority The priority to set.
     * @throws IllegalArgumentException when the given priority is not between 0 and 10
     * 
     * @uml.property name="priority"
     */
    protected void setPriority(int priority) throws IllegalArgumentException {
        if (priority<0 || priority > 10) throw new IllegalArgumentException("The priority of an Order must range between 0 and 10");
        this.priority = priority;
    }

    /**
     * Returns the priority of this Order.
     * This value ranges between 0 (low priority)
     * and 10 (high priority).
     * 
     * @return Returns the priority.
     * 
     * @uml.property name="priority"
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Sets the id to the given value
     * 
     * @param id The id to set.
     * 
     * @uml.property name="id"
     */
    
    public Status getStatus() {
    	return status;
    }
    
    public void setStatus(Status newStatus) {
    	status = newStatus;
    }
    
    // For compatibility
    public boolean getProgress() {
    	return status == Status.INPROGRESS;
    }
    
    public void setProgress(boolean value) {
    	status = value?Status.INPROGRESS:Status.FREE;
    }
    
    protected void setId(long id) {
        this.id = id;
    }
    
    public boolean isDone() {
    	return status == Status.DONE;
    }

    /**
     * Returns the id of this Order
     * @return Returns the id.
     * 
     * @uml.property name="id"
     */
    public long getId() {
        return id;
    }
    
    public String toString() {
        String result = "";
        result = "Order "+getId() + ": \n" 
        	+ "   Priority  = " + getPriority() + "\n"
        	+ "   From      = " + getStartStationId() + "\n"
        	+ "   To        = " + getDestinationStationId() + "\n"
        	+ "   CreatedOn = " + getCreationTime().toString();
        return result;
    }

    /**
     * Resets the next free id to 0, restart assigning unique identifiers
     */
    public static void reset() {
        nextID = 0;
    }
}
