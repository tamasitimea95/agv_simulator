/*
 * Created on Oct 5, 2004
 *
 */
package agentwise.agv.model.agv;

import agentwise.agv.model.environment.AgvInfluence;
import agentwise.agv.model.environment.DropOrderInfluence;
import agentwise.agv.model.environment.MoveStepToStationInfluence;
import agentwise.agv.model.environment.PickOrderInfluence;
import agentwise.agv.model.environment.WaitAgvInfluence;

/**
 * This module is responsible for the execution of the actions
 * it provides in its interface. An Agv uses this module to
 * execute actions from within its behavioural logic that is
 * contained in the BehaviourModule it uses.
 * 
 * The ActionExecutionModule interprets the action to execute
 * and sends the necessary commands to the environment in which
 * the agvs are situated through the AgentToEnvironmentInterface.
 * 
 * It is mandatory that all actions are executed through this module
 * in order to guarantee normal behaviour of the simulator. This
 * module can only work if the Agv for which the actions should
 * be executed is set in this module (setAgv())
 *  
 * @author tomdw
 *
 */
public class ActionExecutionModule {
    
	/**
     * The Agv for which to execute actions
     */
    private Agv agv;
	
    /**
     * Sets the Agv for which this ActionExecutionModule
     * must execute actions. If this Agv is not present
     * in this ActionExecutionModule, this module
     * will not work.
     * 
     * @param agv The Agv for which to execute actions
     * @throws IllegalArgumentException when the given Agv is null
     */
    public void setAgv(Agv agv) throws IllegalArgumentException {
        if (agv==null) throw new IllegalArgumentException("The given Agv cannot be null");
        this.agv = agv;
    }
    
    /**
     * Returns the Agv for which this ActionExecutionModule
     * must execute actions.
     * 
     * @return an Agv instance
     */
    private Agv getAgv() {
        return agv;
    }

    /**
     * Executes the action where the Agv moves one step towards a 
     * station with a given ID. This can only succeed if the Agv
     * is located on a station from which a segment departs that ends
     * in the goal station or when the Agv is located on such
     * a segment itself. Executing this action will then move
     * the Agv one step on the segment-network towards the station.
     * 
     * @param stationID The id of the station to move to
     * @throws IllegalStateException when there was no Agv set in this ActionExecutionModule
     */
    public void moveStepToStation(String stationID) throws IllegalStateException{
        if (getAgv()==null) throw new IllegalStateException("The Agv for which to execute the actions could not be determined in ActionExecutionModule");
        AgvInfluence i = new MoveStepToStationInfluence(stationID);
        getAgv().getEnvironment().influence(i);
    }
    
    /**
     * Executes the action where the Agv picks up an Order
     * from its current position. This can only succeed if the
     * Agv is located at a Station/Location where an Order
     * is present.
     * 
     * @throws IllegalStateException when there was no Agv set in this ActionExecutionModule
     */
    public void pickOrder() throws IllegalStateException {
        if (getAgv()==null) throw new IllegalStateException("The Agv for which to execute the actions could not be determined in ActionExecutionModule");
        AgvInfluence i = new PickOrderInfluence();
        agv.getEnvironment().influence(i);        
    }
    
    /**
     * Executes the action where the Agv drops the order
     * it is currently holding. This can only succeed if
     * the Agv is located at a position that can contain
     * or accept such an Order.
     * 
     * @throws IllegalStateException when there was no Agv set in this ActionExecutionModule
     */
    public void dropOrder() throws IllegalStateException {
        if (getAgv()==null) throw new IllegalStateException("The Agv for which to execute the actions could not be determined in ActionExecutionModule");
        AgvInfluence i = new DropOrderInfluence();
        getAgv().getEnvironment().influence(i);
    }
    
    /**
     * Just wait a second and do nothing
     *
     */
    public void waitSecond(){
        if (getAgv()==null) throw new IllegalStateException("The Agv for which to execute the actions could not be determined in ActionExecutionModule");
        AgvInfluence i = new WaitAgvInfluence();
        getAgv().getEnvironment().influence(i);
    }
}
