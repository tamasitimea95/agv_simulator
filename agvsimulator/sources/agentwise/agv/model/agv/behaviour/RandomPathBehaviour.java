/*
 * Created on Sep 27, 2004
 *
 */
package agentwise.agv.model.agv.behaviour;

import java.util.List;

import agentwise.agv.model.agv.Knowledge;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.PositionMessage;
import agentwise.agv.model.environment.communication.StringMessage;

/**
 * This BehaviourModule makes an Agv move randomly on
 * the segment-network, ie the environment of the agv while
 * broadcasting constantly to other agvs.
 * 
 * @author nelis,tomdw
 *
 */
public class RandomPathBehaviour extends BehaviourModule {

    /**
     * Constructs an instance of RandomPathBehaviour
     */
    public RandomPathBehaviour() {
        super();
    }
    
    
    private String previousStation = "";
    private Knowledge knowledge = new Knowledge();
    private long broadcastid = 0;   
    
    
    /**
     * @see agentwise.agv.model.agv.behaviour.BehaviourModule#initialize()
     */
    public void initialize() {
//      put the current coordinate of the AGV in the knowledge, with the PositionMessage.COORDINATE
    	//as key
    	knowledge.put(PositionMessage.COORDINATE, getPerceptionModule().senseCoordinate());

        //start broadcasting a PositionMessage. Notice that a Knowledge object is given as an argument
    	//in that object, the BehaviorModule can update the current position so the PositionMessage can 
    	//reflect it. 
    	//The broadcast will be repeated every 100 time-units. As a result, an id is returned that can be
    	//used later to deregister the periodicbroadcast at the CommunicationModule.
   	    broadcastid = getCommunicationModule().periodicbroadcast(new PositionMessage(knowledge),50);
    }
    
    /**
     * Implements the actual behaviour of the Agv, in this case a random
     * walk on the map, picking up and dropping down packages when possible,
     * broadcasting and answering to broadcasts when possible.
     * 
     * 
     * @see agentwise.agv.model.agv.behaviour.BehaviourModule#step()
     */
    public void step() {
        	//implement the Random walk behavior
	        String goalstation = null;
	        if(getPerceptionModule().senseIsOnStation()){
	        	List choices = getPerceptionModule().senseNeighbouringStationIds();
	        	if (!(getPerceptionModule().senseCanDropOrder() || getPerceptionModule().senseCanPickOrder())) {
	        	    choices.remove(previousStation);
	        	}
	        	
	        	int index = (int) Math.floor(1+Math.random()*choices.size())-1;
	        	if (choices.size()>0) goalstation = (String) choices.get(index);
	        	
	        	if(getPerceptionModule().senseHasOrder() && getPerceptionModule().senseCanDropOrder()){
	        		getActionExecutionModule().dropOrder();
	        	}else if(!getPerceptionModule().senseHasOrder() && getPerceptionModule().senseCanPickOrder()){
	        		getActionExecutionModule().pickOrder();
	        	}
	        	previousStation = getPerceptionModule().senseCurrentMapComponentId();
	        }else{
	        	goalstation = getPerceptionModule().senseEndStationId();	        	
	        }
	        
	        // receive all messages in this cycle and react on them if needed
	        Message receivedMsg = getCommunicationModule().receive();
	        while(receivedMsg!=null) {
	            //try the cast to a PositionMessage; if you have many different kinds of messages
	            //in your system you should solve this more elegantly.
		        try {
	                PositionMessage strm = (PositionMessage)receivedMsg;
	                //send an acknowledgment to the sender of the PositionMessage
	                getCommunicationModule().unicast(strm.getSender(), new StringMessage("ACK"));
	                //wait some time since we're in collision range
	                //for (int i=0; i<=100; i++) getActionExecutionModule().waitSecond();
	            } catch (ClassCastException e1) {
	                //got a reply message since the cast failed, must be a StringMessage.
	            }
	            //pick up the next message in the buffer
		        receivedMsg = getCommunicationModule().receive();
	        }
	        
	        //move a step
	        getActionExecutionModule().moveStepToStation(goalstation);
	        //update knowledge so PositionMessage is changed
	        knowledge.put(PositionMessage.COORDINATE, getPerceptionModule().senseCoordinate());
    }
    
    public void cleanUp() {
        //end broadcast communication
        getCommunicationModule().cancelperiodiccommunication(broadcastid);
    }

    /**
     * @see agentwise.agv.model.agv.behaviour.BehaviourModule#cloneBehaviour()
     */
    public BehaviourModule cloneBehaviour() {
        return new RandomPathBehaviour();
    }
}
