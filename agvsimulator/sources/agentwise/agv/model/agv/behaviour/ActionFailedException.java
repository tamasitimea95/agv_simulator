/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.model.agv.behaviour;

/**
 * This exception is thrown when an action
 * in the Environment fails.
 * 
 * @author tomdw
 *
 */
public class ActionFailedException extends Exception {

    /**
     * Constructs an instance of ActionFailedException
     *
     * @param message A message that explains the failure
     */
    public ActionFailedException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of ActionFailedException
     *
     * @param message A message explaining the failure
     * @param cause The exception that caused the failure
     */
    public ActionFailedException(String message, Throwable cause) {
        super(message, cause);
    }

}
