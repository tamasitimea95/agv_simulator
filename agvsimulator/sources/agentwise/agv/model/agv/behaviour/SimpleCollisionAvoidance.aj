/*
 * Created on 19-Nov-2004
 *
 */
package agentwise.agv.model.agv.behaviour;

import java.awt.Color;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.MoveStepToStationInfluence;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.view.objects.OrderLocationDrawing;
import agentwise.agv.view.objects.StationDrawing;


/**
 * <p>This aspect encapsulates a very simple collision
 * avoidance mechanism. It makes sure that an
 * AGV that is located on a station will lock
 * all incomming segments towards that station untill
 * the AGV has moved two station further in the lay-out.
 * That way no collision can happen.</p>
 * 
 * <p>The mechanism adds infrastructure to the
 * stations in order for this to be possible!</p>
 * 
 * @author tomdw
 *
 */
public aspect SimpleCollisionAvoidance {
    
    /**
     * Adds a isLocked instpector to Station
     * to check if that station is locked
     */
    public synchronized boolean Station.isLocked() { return this.isLocked; }
    
    /**
     * Adds a isLockedBy inspector to Station
     * to check if the station is locked by an AGV
     * with a given id.
     * @param agvid The given id of the agv to check
     * @return true if the AGV with given id was the locker
     */
    public synchronized boolean Station.isLockedBy(long agvid) { return (this.isLocked && agvid==this.locker); }
    
    /**
     * Adds a variable isLocked to Station to 
     * indicate if the station is locked or not
     */
    private boolean Station.isLocked = false;
    
    /**
     * A station keeps track of which AGV locked that station
     */
    private long Station.locker = 0;
    
    /**
     * Locks a station and the given id if the id
     * of the AGV performing this lock.
     * 
     * @param agvid The given id of the agv
     */
    private synchronized void Station.lock(long agvid) { this.isLocked = true; this.locker=agvid; }
    
    /**
     * UnLocks a station that was previously locked.
     * If that station was not locked, nothing happens
     *
     */
    private synchronized void Station.unlock() { this.isLocked = false; }
    
    /**
     * This pointcut identifies the point where an AGV
     * chooses a segment to follow.
     */
    //pointcut segmentChoosen(MoveStepToStationInfluence influence): call(Segment MoveStepToStationInfluence.getSegmentTowardsGoal(..)) && target(influence);

    /**
     * This advice executes around the point where an AGV chooses
     * a segment to follow. The effect will be that when the choosen
     * segment was locked by another agv the choosen segment will
     * no longer be returned. Otherwise it will just have the normal
     * behaviour and follow that segment. Thus, this advice
     * makes sure that an AGV is blocked before a locked segment
     * 
     */
    /*Segment around(MoveStepToStationInfluence influence): segmentChoosen(influence) {
        Segment s = proceed(influence);
        if (s!=null && s.getEnd().isLocked() && !s.getEnd().isLockedBy(influence.getAgvenvstate().getAgvID().getID())) return null;
        else return s;
    }
    
    /**
     * This pointcut identifies where an AGV effectively moves
     * on a Segement.
     * @param agvS The AgvEnvironmentState of the AGV moving
     * @param s The segment where the AGV moved on
     */
    //pointcut moveToSegmentEffect(AgvEnvironmentState agvS, Segment s): call( * AgvEnvironmentState.setCurrentMapComponent(..)) && args(s) && target(agvS);

    /**
     * This advice executes before a move to a segment is done.
     * The effect will be that the End-station of that segment will
     * be locked and registered as to be unlocked when moved to a
     * station twice. (i.e. only unlock when moved two stations far)
     */
    /*before(AgvEnvironmentState agvS,Segment s): moveToSegmentEffect(agvS,s) {
        s.getEnd().lock(agvS.getAgvID().getID());
        agvS.nextToUnlock = s.getEnd();
    }
    
    /**
     * Adds information to the AgvEnvironmentState of the 
     * AGV to know for each AGV which station it will
     * unlock the second time it moves to a station
     */
    public Station AgvEnvironmentState.nextToUnlock = null;
    
    /**
     * Adds information to the AgvEnvironmentState of the 
     * AGV to know for each AGV which station it will
     * unlock the first time it moves to a station
     */
    public Station AgvEnvironmentState.toUnlock = null;
    
    /**
     * Pointcut identifies when an AGV moves on a Station.
     * 
     * @param agvS The AgvEnvironmentState of the AGV moving
     * @param s The station on which the AGV moved
     */
    //pointcut moveToStationEffect(AgvEnvironmentState agvS,Station s): call( * AgvEnvironmentState.setCurrentMapComponent(..)) && args(s) && target(agvS);

    /**
     * This advice executes before an AGV moves on a station. 
     * The effect is that the station on which the agv was located
     * two segments ago will be unlocked and the station on which the
     * agv was located one segment ago is registered as the one
     * to unlock the next time the agv moves to a station
     *
     */
    /*before(AgvEnvironmentState agvS,Station s): moveToStationEffect(agvS,s) {
        if (!s.isLocked()) { // to init rightly in the beginning
            s.lock(agvS.getAgvID().getID()); 
            agvS.nextToUnlock = s;
        }
        if (agvS.toUnlock!=null && !agvS.toUnlock.getID().equals(s.getID()))  {
            agvS.toUnlock.unlock();
        }
        if (agvS.nextToUnlock!=null) {
            agvS.toUnlock = agvS.nextToUnlock;
            agvS.nextToUnlock = null;
        } 
    }
    
    /**
     * Pointcut identifies where the Gui of
     * a station is updated.
     * 
     * @param drawing The StationDrawing to update
     * @param s The station corresponding with that drawing
     */
    //pointcut updateStation(StationDrawing drawing, Station s): call (* StationDrawing.update(..)) && args(s) && target(drawing);
    
    /**
     * This advice executes when the gui representation of
     * a station is updated. It makes sure that locked
     * stations are colored in red.
     */
    /*before(StationDrawing drawing, Station s): updateStation(drawing,s) {
        if (s.isLocked()) drawing.setColor(Color.RED);
        else drawing.setColor(Color.BLUE);
    }
    
    /**
     * Pointcut identifies where a station has changed. 
     * In particular when the lock situation of that
     * station changes.
     */
    /*pointcut stationChange(Station s):(
            call (* Station.lock(..))
            || call (* Station.unlock(..))
            ) && target(s);
    */
    /**
     * Advise executes when a station change succeeded.
     * When a lock or unlock was done. Then a change
     * in the station is signalled so that an update
     * of for example the gui is triggered.
     */
	/*void around(Station s): stationChange(s){
	    proceed(s); // throws
	    ChangeManager.getInstance().signalStationChangedEvent(s);
	}
	
	/**
     * Pointcut identifies where the Gui of
     * a orderlocation is updated.
     * 
     * @param drawing The OrderLocationDrawing to update
     * @param s The orderlocation corresponding with that drawing
     */
	//pointcut updateOrderLocation(OrderLocationDrawing drawing, Station s): call (* OrderLocationDrawing.update(..)) && args(s) && target(drawing);
    
	/**
     * This advice executes when the gui representation of
     * a orderlocation is updated. It makes sure that locked
     * orderlocations are colored in red.
     */
    /*before(OrderLocationDrawing drawing, Station s): updateOrderLocation(drawing,s) {
        if (s.isLocked()) drawing.setColor(Color.RED);
        else drawing.setColor(Color.BLUE);
    }*/
	
	/**
	 * Pointcut identifies where a orderlocation gets a lock
	 * state change
	 */
	/*pointcut orderLocationChange(OrderLocation l): (
            call (* Station.lock(..))
            || call (* Station.unlock(..))
            ) && target(l);
	*/
	/**
     * Advise executes when a orderlocation change succeeded.
     * When a lock or unlock was done. Then a change
     * in the orderlocation is signalled so that an update
     * of for example the gui is triggered.
     */
	/*after(OrderLocation l) returning: orderLocationChange(l) {
	    ChangeManager.getInstance().signalOrderLocationChangedEvent(l);
	}*/

}
