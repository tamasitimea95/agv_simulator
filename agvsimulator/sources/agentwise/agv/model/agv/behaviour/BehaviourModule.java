/*
 * Created on Oct 7, 2004
 *
 */
package agentwise.agv.model.agv.behaviour;

import agentwise.agv.model.agv.ActionExecutionModule;
import agentwise.agv.model.agv.Agv;
import agentwise.agv.model.agv.CommunicationModule;
import agentwise.agv.model.agv.PerceptionModule;

/**
 * This module encapsulates the behavioural algorithm of the AGV. You can define
 * your own behaviour by extending this class and implementing the behavioural
 * algorithm into the step-method.
 * 
 * This module uses the a PerceptionModule to percieve the environment, it uses
 * a CommunicationModule to communicate with other communication enabled
 * entities in that environment and it uses an ActionExecutionModule to execute
 * choosen actions in that environment.
 * 
 * None of this functionality can be used if the Agv for which this behaviour
 * should be executed is not set in this module (setAgv()).
 * 
 * BehaviourModule is abstract so a subclass must be used as a behaviour module
 * for an Agv and this subclass must implement this class' step method
 * 
 * @author tomdw
 *  
 */
public abstract class BehaviourModule {
    
    /**
     * The Agv instance for which the behaviour of this module is to be executed
     */
    private Agv agv = null;
    
    /**
     * Constructs an instance of BehaviourModule
     */
    public BehaviourModule() {
    }

    /**
     * Sets the Agv to the given instance for which the behaviour is to be
     * executed. This module cannot be used without this instance
     * 
     * @param agv
     *            The Agv to execute behaviour for
     * @throws IllegalArgumentException
     *             when the given agv is null
     * @throws IllegalStateException
     *             when there is no perceptionmodule, communicationmodule or
     *             actionexecutionmodule instance set
     */
    public void setAgv(Agv agv) throws IllegalArgumentException,
            IllegalStateException {
        if (agv == null)
            throw new IllegalArgumentException(
                    "The BehaviourModule cannot be created for a null Agv");
        this.agv = agv;
    }

    /**
     * Returns the Agv for which the behaviour should be executed
     * 
     * @return a Agv instance or null when no agv was set for this module
     */
    protected Agv getAgv() {
        return agv;
    }

    /**
     * Executes the behaviour of the Agv for one time step. In this method the
     * Agv can use the PerceptionModule for perception, the CommunicationModule
     * for communication, and the ActionExecutionModule for executing actions.
     *  
     */
    public abstract void step();

	/**
	 * Returns the CommunicationModule to be used to communicate with other
	 * communication enables entities in the environment
	 * 
	 * @return a CommunicationModule instance
	 */
	protected CommunicationModule getCommunicationModule() {
	    return getAgv().getCommunicationModule();
	}

	/**
	 * Returns the PerceptionModule that can be used by this BehaviourModule to
	 * percieve the environment
	 * 
	 * @return a PerceptionModule instance
	 */
	protected PerceptionModule getPerceptionModule() {
	    return getAgv().getPerceptionModule();
	}

	/**
	 * Returns the ActionExecutionModule instance to use for executing actions
	 * on the environment
	 * 
	 * @return an ActionExecutionModule instance
	 */
	protected ActionExecutionModule getActionExecutionModule() {
	    return getAgv().getActionExecutionModule();
	}
	
	/**
	 * Returns a deep copy of this BehaviourModule.
	 * 
	 * @return a new instance of BehaviourModule that is an exact copy of this 
	 */
	public abstract BehaviourModule cloneBehaviour();

    /**
     * Executes once, at the start-up of the AGV
     */
    public abstract void initialize();

    /**
     * Executes once, at the termination of the AGV
     */
    public abstract void cleanUp();

}