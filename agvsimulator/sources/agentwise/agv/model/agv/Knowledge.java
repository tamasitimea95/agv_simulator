/*
 * Created on Feb 4, 2005
*/
package agentwise.agv.model.agv;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Knowledge is essentially a strongly typed Map; the keys in the map must be
 * strings. If you attempt to use anything other than a string, you'll get a IllegalArgumentException.
 * Knowledge is also thread safe.
 * @author Kurt Schelfthout
 */
public class Knowledge implements Map {

    private Map map = Collections.synchronizedMap(new HashMap());
    
    /**
     * 
     */
    public Knowledge() {
        super();
    }
    
    public void clear() {
        map.clear();
    }
    
    public boolean containsKey(Object arg0) {
        if (! (arg0 instanceof String)) {
            throw new IllegalArgumentException("Key must be a string");
        }
        return map.containsKey(arg0);
    }
    public boolean containsValue(Object arg0) {
        return map.containsValue(arg0);
    }
    public Set entrySet() {
        return map.entrySet();
    }
    public boolean equals(Object arg0) {
        return map.equals(arg0);
    }
    public Object get(Object arg0) {
        if (! (arg0 instanceof String)) {
            throw new IllegalArgumentException("Key must be a string");
        }
        return map.get(arg0);
    }
    
    public Object get(String arg0) {
        return map.get(arg0);
    }
    
    public int hashCode() {
        return map.hashCode();
    }
    public boolean isEmpty() {
        return map.isEmpty();
    }
    public Set keySet() {
        return map.keySet();
    }
    public Object put(Object arg0, Object arg1) {
        if (! (arg0 instanceof String)) {
            throw new IllegalArgumentException("Key must be a string");
        }
        return map.put(arg0, arg1);
    }
    public Object put(String arg0, Object arg1) {
        return map.put(arg0, arg1);
    }
    
    public void putAll(Map arg0) {
        for (Iterator iter = arg0.keySet().iterator(); iter.hasNext();) {
            Object element = (Object) iter.next();
            if (! (element instanceof String)) throw new IllegalArgumentException("Key must be a string"); 
        }
        map.putAll(arg0);
    }
    public Object remove(Object arg0) {
        return map.remove(arg0);
    }
    public int size() {
        return map.size();
    }
    public String toString() {
        return map.toString();
    }
    public Collection values() {
        return map.values();
    }
}
