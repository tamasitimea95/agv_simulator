/*
 * Created on Dec 3, 2004
 * 
 */
package agentwise.agv.model.agv.map;

import java.util.HashMap;

/**
 * @author nelis
 * Repesents a component containing certain information based on a key
 */
public class InformationContainer {

    protected HashMap information = new HashMap();
    
    /**
     * 
     */
    public InformationContainer() {
        super();
    }
    
    /**
     * Add information to the container
     * @param key The key for this information
     * @param object The information itself
     */
    public void addInformationObject(Object key,Object object){
        information.put(key,object);
    }
    
    /**
     * Get the information associated with this key
     * @param key The key for wich the associated information is asked
     */
    public void getInformation(Object key){
        information.get(key);
    }
    
    /**
     * 
     * @param key
     */
    public void containtsInformation(Object key){
        information.containsValue(key);
    }
    
    public Object removeInformation(Object key){
        return information.remove(key);
    }
}
