/*
 * Created on Dec 3, 2004
 *
 */
package agentwise.agv.model.agv.map;

import java.util.Collection;

/**
 * @author nelis
 * TODO [nelis] Documenting
 */

public class InformationMap {
    
    protected Collection nodes;
    protected Collection links;
	
	public InformationMap(Collection nodes, Collection links){
		this.nodes = nodes;
		this.links = links;
	}
	
	 /**
     * Adds a collection of links map.
     * 
     * @param links The Collection of Segment instances
     */
    protected void addLinks(Collection links) {
        this.links = links;
    }

    /**
     * Retursn the links of this map
     * 
     * @return a Collection of Segment instances
     */
    public Collection getLinks() {
        return links;
    }
	
	 /**
     * Adds a collection of nodes to the map
     * 
     * @param nodes The Collection of Node instances
     */
    protected void addNodes(Collection nodes) {
        this.nodes = nodes;
    }
	
    /**
     * Retursn the nodes of this map
     * 
     * @return a Collection of Node instances
     */
    public Collection getNodes() {
        return nodes;
    }
    
}
