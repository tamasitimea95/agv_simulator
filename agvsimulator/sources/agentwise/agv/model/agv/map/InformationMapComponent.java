/*
 * Created on Dec 3, 2004
 * 
 */
package agentwise.agv.model.agv.map;

/**
 * @author nelis
 *
 * TODO [nelis] Documenting
 */
public class InformationMapComponent extends InformationContainer{

    /**
     * The ID identifying this MapComponent
     */
    protected String ID;
    
    /**
     * 
     */
    public InformationMapComponent(String id) {
        super();
        setID(id);
        
    }  

    /**
     * @return Returns the iD.
     */
    public String getID() {
        return ID;
    }
    /**
     * @param id The iD to set.
     */
    protected void setID(String id) {
        ID = id;
    }
}
