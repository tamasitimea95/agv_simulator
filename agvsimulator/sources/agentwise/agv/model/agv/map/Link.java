/*
 * Created on Dec 3, 2004
 *
 */
package agentwise.agv.model.agv.map;

/**
 * @author nelis
 * TODO [nelis] Documenting
 */
public class Link extends InformationMapComponent{

    protected Node startNode;
    protected Node endNode;
    
    public Link(String id, Node startNode, Node endNode){
        super(id);
        setEndNode(endNode);
        setStartNode(startNode);
    }

    
/*    public Object clone(){
        return new Link(this);
    }*/
    
    /**
     * @return Returns the endNode.
     */
    public Node getEndNode() {
        return endNode;
    }
    /**
     * @param endNode The endNode to set.
     */
    protected void setEndNode(Node endNode) {
        this.endNode = endNode;
        endNode.addInLink(this);
    }
    /**
     * @return Returns the startNode.
     */
    public Node getStartNode() {
        return startNode;
    }
    /**
     * @param startNode The startNode to set.
     */
    protected void setStartNode(Node startNode) {
        this.startNode = startNode;
        startNode.addOutLink(this);
    }
}
