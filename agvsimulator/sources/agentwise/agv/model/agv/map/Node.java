/*
 * Created on Dec 3, 2004
 * 
 */
package agentwise.agv.model.agv.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import agentwise.agv.elteMod.WaitLink;

/**
 * @author nelis
 *
 * TODO [nelis] Documenting
 */
public class Node extends InformationMapComponent{

    public Collection inLinks;
    public Collection outLinks;
        
    /**
     * 
     */
    public Node(String id) {
        super(id);
        inLinks = new ArrayList();
        outLinks = new ArrayList();
    }

    /*public Node(Node n){
        this(n.ID);
        Iterator i = n.inLinks.iterator();
        while(i.hasNext()){
            this.addInLink((Link)i.next());
        }
        
        i = n.outLinks.iterator();
        while(i.hasNext()){
            this.addOutLink((Link)i.next());
        }
    }
    
    public Object clone(){
        return new Node(this);
    }*/
    
    /**
     * @return Returns the inNodes.
     */
    public Collection getInLinks() {
        return inLinks;
    }
    
    public void addInLink(Link l){
        inLinks.add(l);
    }
   
    public void addOutLink(Link l){
        outLinks.add(l);
    }
    
    /**
     * @return Returns the outNodes.
     */
    public Collection getOutLinks() {
        return outLinks;
    }
    
    public Collection getRealOutLinks()
    {
    	return (Collection) outLinks.stream()
				.filter(l -> !(l instanceof WaitLink))
				.collect(Collectors.toCollection(ArrayList::new));
    }
    
}
