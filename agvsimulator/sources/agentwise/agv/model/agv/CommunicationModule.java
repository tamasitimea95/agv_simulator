/*
 * Created on Oct 7, 2004
 *
 */
package agentwise.agv.model.agv;

import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;

/**
 * The CommunicationModule allows the Agv to communicate
 * with other communication enables entities in the
 * environment (Agvs, locations, ...). This module needs 
 * a reference to the Agv for which it must communicate (setAgv())
 * 
 * @author tomdw
 *
 */
public abstract class CommunicationModule {
    
    /**
     * Broadcasts a given Message to the Agvs that
     * can be reached with the used communication
     * infrastructure.
     * 
     * @param msg The message to send
     */
    public abstract void broadcast(Message msg); 
    
    /**
     * Broadcasts repeatedly a given Message to the Agvs that
     * can be reached with the used communication
     * infrastructure. The message will be send every given number
     * of time steps.
     * 
     * @param msg The message to send
     * @param timesteps The given number of time steps to skip, must be positive
     * @return the id that identifies the issued periodic communication
     */
    public abstract long periodicbroadcast(Message msg, int timesteps); 
    
    /**
     * Retrieves a message from the received messages
     * in the inbox of the Agv. If no
     * messages are in its inbox, null is returned
     * 
     * @return a Message instance or null
     */
    public abstract Message receive(); 
    
    /**
     * Sends a given message to a communication enabled entity in the
     * environment that is denoted with a given NetworkID. There is
     * no guarantee that this message will also be delivered. For example
     * in an Ad-Hoc network there is a limited sending range.
     * 
     * @param msg The message instance to send
     * @param receiver The NetworkID of the receiver
     */
    public abstract void unicast(NetworkID receiver, Message msg);
    
    /**
     * Sends repeatedly a given message to a communication enabled entity in the
     * environment that is denoted with a given NetworkID. The message
     * will be send every given number of time steps. There is
     * no guarantee that this message will also be delivered. For example
     * in an Ad-Hoc network there is a limited sending range.
     * 
     * @param msg The message instance to send
     * @param receiver The NetworkID of the receiver
     * @param timesteps The number of time steps to skip before sending again, must be positive
     * @return the id that identifies the issued periodic communication
     */
    public abstract long periodicunicast(NetworkID receiver, Message msg, int timesteps);
    
    /**
     * Cancels the periodic communication identified by the given ID
     * @param periodicID
     * @return true if cancelled, false otherwise
     */
    public abstract boolean cancelperiodiccommunication(long periodicID);

    /**
     * Sets the Agv for which this CommunicationModule should
     * communicate. The sender of all messages. The CommunicationModule
     * can only work properly if this Agv instance is present in 
     * this module.
     * 
     * @param agv
     * @throws IllegalArgumentException
     */
    public void setAgv(Agv agv) throws IllegalArgumentException {
        if (agv==null) throw new IllegalArgumentException("The given agv cannot be null");
        this.agv = agv;
    }
    
    /**
     * The Agv for which to communicate
     */
    private Agv agv;
    
    /**
     * Returns the Agv for which to communicate the messages
     * 
     * @return an Agv instance
     */
    protected Agv getAgv() {
        return agv;
    }
}
