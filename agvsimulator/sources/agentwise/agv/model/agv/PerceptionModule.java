/*
 * Created on Sep 23, 2004
 *
 */
package agentwise.agv.model.agv;

import java.util.List;

import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.util.Angle;
import agentwise.agv.util.Coordinate;

/**
 * This module is responsible for the perception functionality
 * that can be used by an Agv to sense the environment
 * in which it is situated. 
 * 
 * The method of active perception is used here. This means that
 * when calling the sense methods on this module immediately
 * the environment is asked to return the wanted information.
 * 
 * @author tomdw, nelis
 *
 */
public class PerceptionModule {
    
	  /**
     * The Agv for which to sense
     */
    private Agv agv;
    
    
    /**
     * 
     * Constructs an instance of PerceptionModule.
     * An instance of Agv must be registered afterwards.
     * 
     */
    public PerceptionModule() { }
    
    /**
     * Sets the Agv for which this PerceptionModule should sense
     * the environment. If this instance is not set in this
     * module then the PerceptionModule cannot perform its
     * behaviour
     * 
     * @param agv The Agv to set
     * @throws IllegalArgumentException when the given Agv is null
     */
    public void setAgv(Agv agv) throws IllegalArgumentException {
        if (agv==null) throw new IllegalArgumentException("The given agv cannot be null");
        this.agv = agv;
    }
    
    /**
     * Returns the Agv for which the perception must be performed
     * 
     * @return an Agv instance
     */
    private Agv getAgv() {
        return agv;
    }
    
    /**
     * Returns the OrderInfo instance that this agv is currently holding.
     * When the Agv does not hold an Order then null is returned.
     * 
     * @return an OrderInfo instance or null
     */
    public OrderInfo senseOrderHolding() {
        return (OrderInfo)getAgv().getEnvironment().sense("orderholding");
    }
       
    /**
     * Returns true if at the time of the last sensing operation
     * the percieving agv was situated on a segment between
     * stations
     * 
     * @return True if on a segment, false otherwise
     */
    public boolean senseIsOnSegment() {
        return ((Boolean)getAgv().getEnvironment().sense("isonsegment")).booleanValue();
    }
    
    /**
     * Returns the id of the end station of the segment on which the 
     * agv is located if the agv was located on a segment 
     * the last time it sensed. If in the mean time the agv 
     * moved then no guarantee is given that the station id
     * can be returned
     * 
     * @return a string identifying the station
     */
    public String senseEndStationId() {
        return (String)getAgv().getEnvironment().sense("goalstationid");
    }
      
    /**
     * Returns a collection containing the ids of
     * the stations that are directly connected
     * with the station on which the agv is located and
     * which are connected by a segment that the agv
     * can follow to reach that station.  If no segments
     * leave the current station then the collection is empty
     * 
     * @return a Collection of String ids of stations
     */
    public List senseNeighbouringStationIds() {
        return (List)getAgv().getEnvironment().sense("neighbouringstationids");
    }  
    
    /**
     * Returns the id of the map component on which
     * the agv is currently situated.
     * 
     * @return a string representing this id
     */
    public String senseCurrentMapComponentId() {
        return (String)getAgv().getEnvironment().sense("currentmapcomponentid");
    }
    
    
    /**
     * Returns true if the percieving agv is situated on a station
     * 
     * @return True if on a station, false otherwise
     */
    public boolean senseIsOnStation() {
        return ((Boolean)getAgv().getEnvironment().sense("isonstation")).booleanValue();
    }
    
    /**
     * Returns the current direction of this Agv by sensing it.
     * 
     * @return a Angle instance representing the direction
     */
    public Angle senseDirection() {
        return (Angle)getAgv().getEnvironment().sense("direction");
    }
    
    /**
     * Returns the current coordinate of this Agv by sensing it.
     * 
     * @return a Coordinate instance representing the coordinate
     */
    public Coordinate senseCoordinate() {
        return (Coordinate)getAgv().getEnvironment().sense("coordinate");
    }
    
    /**
     * Returns the NetworkID of this agvs inbox that is used for
     * communication with this Agv
     * 
     * @return a NetworkID instance
     */
    public NetworkID senseNetworkId() {
        return (NetworkID)getAgv().getEnvironment().sense("networkid");
    }
    
    /**
     * Returns the id of this Agv. This
     * is the same as the networkid used in communication
     * 
     * 
     * @return a NetworkID instance
     */
    public NetworkID senseAgvId() {
        return senseNetworkId();
    }
    
    /**
     * 
     * @return True if the Agv can drop an order, false otherwise
     */
    public boolean senseCanDropOrder() {
        return ((Boolean) getAgv().getEnvironment().sense("candroporder")).booleanValue();
    }
    
    /**
     *
     * @return True if the AGV can pick an order, false otherwise 
     */
    public boolean senseCanPickOrder() {
        return ((Boolean) getAgv().getEnvironment().sense("canpickorder")).booleanValue();
    }
    
    /**
     * 
     * @return True if the AGV currently has an order
     */
    public boolean senseHasOrder() {
        return ((Boolean) getAgv().getEnvironment().sense("hasorder")).booleanValue();
    }

    /**
     * Senses the state of the Agv to check
     * if it is suspended or not. This
     * information used when the agv fails due
     * to external influences. Ex. collisions
     * 
     * All actions that have to do with moving
     * the agv in some way (move or turn) will
     * fail after the agv was suspended. Communication
     * and perception are still allowed.
     * 
     * @return true if the agv has failed and is suspended, false otherwise
     */
    public boolean senseSuspended() {
        return ((Boolean)getAgv().getEnvironment().sense("issuspended")).booleanValue();
    }
    
}
