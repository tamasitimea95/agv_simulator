/*
 * Created on Oct 7, 2004
 *
 */
package agentwise.agv.model.agv;

import agentwise.agv.model.environment.communication.Broadcast;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.model.environment.communication.PeriodicCommunication;
import agentwise.agv.model.environment.communication.PrimitiveCommunicationInstance;
import agentwise.agv.model.environment.communication.Unicast;

/**
 * This is a default implementation of a CommunicationModule.
 * This implementation redirects all communication calls
 * to the environment through the AgentToEnvironmentInterface
 * 
 * @author tomdw
 *
 */
public class DefaultCommunicationModule extends CommunicationModule {
    
    public DefaultCommunicationModule() {
        communicationhandler = new CommunicationHandler();
    }
     
    private CommunicationHandler communicationhandler;

    /**
     * 
     * @see agentwise.agv.model.agv.CommunicationModule#broadcast(agentwise.agv.model.environment.communication.Message)
     */
    public void broadcast(Message msg) {
        Broadcast bc = createBroadcast(msg);
        communicationhandler.addCommunication(bc);
    }
    
    /**
     * 
     * @see agentwise.agv.model.agv.CommunicationModule#receive()
     */
    public Message receive() {
        return getAgv().getEnvironment().receive();
    }
    
    /**
     * 
     * @see agentwise.agv.model.agv.CommunicationModule#unicast(agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message)
     */
    public void unicast(NetworkID receiver, Message msg) {
        Unicast uc = createUnicast(msg, receiver);
        communicationhandler.addCommunication(uc);
    }
    
    private Broadcast createBroadcast(Message msg) {
        msg.setSender(getAgv().getPerceptionModule().senseAgvId());
        Broadcast bc = new Broadcast(msg);
        return bc;
    }
    
    private Unicast createUnicast(Message msg, NetworkID receiver) {
        msg.setSender(getAgv().getPerceptionModule().senseAgvId());
        Unicast uc = new Unicast(msg, receiver);
        return uc;
    }

    /**
     * @see agentwise.agv.model.agv.CommunicationModule#periodicbroadcast(agentwise.agv.model.environment.communication.Message, int)
     */
    public long periodicbroadcast(Message msg, int timesteps) {
        Broadcast bc = createBroadcast(msg);
        long result = createPeriodic(bc, timesteps);
        return result;
	}
    
    private long createPeriodic(PrimitiveCommunicationInstance c, int timesteps) {
        long result = -1;
        if (timesteps >= 0) {
	        PeriodicCommunication com = new PeriodicCommunication(c,timesteps);
	        communicationhandler.addPeriodic(com);
	        result = com.getID();
        }
        return result;
    }

    /**
     * @see agentwise.agv.model.agv.CommunicationModule#periodicunicast(agentwise.agv.model.environment.communication.NetworkID, agentwise.agv.model.environment.communication.Message, int)
     */
    public long periodicunicast(NetworkID receiver, Message msg, int timesteps) {
        Unicast uc = createUnicast(msg, receiver);
	    long result = createPeriodic(uc, timesteps);
        return result;
    }

    /**
     * 
     * @see agentwise.agv.model.agv.CommunicationModule#cancelperiodiccommunication(long)
     */
    public boolean cancelperiodiccommunication(long periodicID) {
        return communicationhandler.cancelPeriodic(periodicID);
    }
    
    public void setAgv(Agv agv) throws IllegalArgumentException {
        super.setAgv(agv);
        communicationhandler.setAgv(agv);
    }
}
