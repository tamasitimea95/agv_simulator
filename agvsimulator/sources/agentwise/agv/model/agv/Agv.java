/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.model.agv;

import agentwise.agv.controller.ActiveEntity;
import agentwise.agv.model.agv.behaviour.BehaviourModule;
import agentwise.agv.model.agv.map.InformationMap;
import agentwise.agv.model.environment.AgentToEnvironmentInterface;

/**
 * The Automated Guided Vehicle that is situated in an Environment. 
 * An AGV has a direction which it is facing and this
 * direction is represented as the number of degrees of the angle with the
 * horizontal east direction in the plane of the environment.
 * 
 * An AGV is an active entity and interacts with the environment
 * through the AgentToEnvironmentInterface. Its behaviour is specified
 * in the BehaviourModule which can use the PerceptionModule, 
 * the CommunicationModule and the ActionExecutionModule. The latter
 * will execute ActionModules that encapsulate behaviour logic
 * into a reusable module.
 * 
 * @author tomdw
 *  
 */
public class Agv extends ActiveEntity {

    /**
     * The BehaviourModule that defines the behaviour of the Agv
     */
    private BehaviourModule behaviourModule = null;
    
    /**
     *  The access point towards the environment 
     */
    private AgentToEnvironmentInterface environment = null;

    /**
     *  The internal map of an agent, containing
     * both static and dynamic information
     */
    protected InformationMap imap=null;
      
    /**
     * 
     * Constructs an instance of Agv in a given environment. 
     * The given environment determines the position and direction
     * of this Agv
     * 
     * Each Agv also gets a unique identification number determined by the environment.
     * 
     * @param environment
     *            This is the access point for the Agv towards its environment
     * 
     * @throws IllegalArgumentException
     *             when the given environment access point is null
     */
    public Agv(AgentToEnvironmentInterface environment)
            throws IllegalArgumentException {
        super();
        setEnvironment(environment); //throws
    }

    /**
     * Sets the access point for the agv towards the Environment in which
     * it is situated.
     * 
     * @param env an instance that implements the AgentToEnvironmentInterface and provides all the functionality in that interface
     * @throws IllegalArgumentException when the given instance is null
     */
    private void setEnvironment(AgentToEnvironmentInterface env)
            throws IllegalArgumentException {
        if (env == null)
            throw new IllegalArgumentException(
                    "The given environment access point for an agv can not be null");
        environment = env;
    }

    /**
     * Returns the access point to the environment in which the agv is situated
     * @return a instance that implements the AgentToEnvironmentInterface
     */
    public AgentToEnvironmentInterface getEnvironment() {
        return environment;
    }
    
    /**
     * This method is executes once, at the start-up of
     * the AGV
     * 
     * @see agentwise.agv.controller.ActiveEntity#initialize()
     */
    public void initialize() {
        behaviourModule.initialize();
    }
   
    /**
     * Each time it gets active the behaviour in the BehaviourModule
     * is executed
     * 
     * @see agentwise.agv.controller.ActiveEntity#step()
     */
    public void step() {
        behaviourModule.step();
    }
    
    /**
     * This method is executed once when the AGV is terminated
     * 
     * @see agentwise.agv.controller.ActiveEntity#cleanUp()
     */
    public void cleanUp() {
        behaviourModule.cleanUp();
    }

    /**
     * Sets the BehaviourModule to use for this Agv. This Agv
     * is registered with the given module as the agv for
     * which to execute the behaviour.
     * 
     * @param bModule The BehaviourModule that defines the behaviour of the agv
     * @throws IllegalArgumentException when the given bModule is null
     */
    public void setBehaviourModule(BehaviourModule bModule) throws IllegalArgumentException {
        if (bModule==null) throw new IllegalArgumentException("The given behaviourModule for the Agv cannot be null");
        behaviourModule = bModule;
        behaviourModule.setAgv(this);
    }
    
	/**
	 * The ActionExecutionModule instance to use
	 * @clientCardinality  1
	 * @directed  true
	 * @label  uses
	 * @supplierCardinality  1
	 */
	private ActionExecutionModule actionExecutionModule = null;

	/**
	 * The CommunicationModule instance to use
	 */
	private CommunicationModule communicationModule = null;

	/**
	 * The PerceptionModule instance to use
	 */
	private PerceptionModule perceptionModule = null;



	/**
	 * Returns the ActionExecutionModule instance to use for executing actions
	 * on the environment
	 * 
	 * @return an ActionExecutionModule instance
	 */
	public ActionExecutionModule getActionExecutionModule() {
	    return actionExecutionModule;
	}

	/**
	 * Returns the CommunicationModule to be used to communicate with other
	 * communication enables entities in the environment
	 * 
	 * @return a CommunicationModule instance
	 */
	public CommunicationModule getCommunicationModule() {
	    return communicationModule;
	}

	/**
	 * Returns the PerceptionModule that can be used by this BehaviourModule to
	 * percieve the environment
	 * 
	 * @return a PerceptionModule instance
	 */
	public PerceptionModule getPerceptionModule() {
	    return perceptionModule;
	}

	/**
	 * Sets the ActionExecutionModule to use by the BehaviourModule to execute
	 * actions. This agv is also registered with the given module as the
	 * agv for which to execute the actions.
	 * 
	 * @param aeModule
	 *            the ActionExecutionModule instance to use
	 * @throws IllegalArgumentException
	 *             when the given aeModule is null
	 */
	public void setActionExecutionModule(ActionExecutionModule aeModule)
	        throws IllegalArgumentException {
	    if (aeModule == null)
	        throw new IllegalArgumentException(
	                "The given action execution module cannot be null");
	    actionExecutionModule = aeModule;
	    actionExecutionModule.setAgv(this);
	}

	/**
	 * Sets the CommunicationModule to use by this BehaviourModule to
	 * communicate with other communication enables entities in the environment.
	 * This agv is also registered with the given module as the agv
	 * for which to send messages, as the sender of all messages.
	 * 
	 * @param cModule
	 *            The CommunicationModule instance to use
	 * @throws IllegalArgumentException
	 *             when the given cModule is null
	 */
	public void setCommunicationModule(CommunicationModule cModule)
	        throws IllegalArgumentException {
	    if (cModule == null)
	        throw new IllegalArgumentException(
	                "The given communication module cannot be null");
	    communicationModule = cModule;
	    communicationModule.setAgv(this);
	}

	/**
	 * Sets the PerceptionModule to use by this BehaviourModule to percieve the
	 * environment. This agv is registered with the given module as the
	 * agv for which to perceive the environment.
	 * 
	 * @param pModule
	 *            The PerceptionModule instance to use
	 * @throws IllegalArgumentException
	 *             when the given pModule is null
	 */
	public void setPerceptionModule(PerceptionModule pModule)
	        throws IllegalArgumentException {
	    if (pModule == null)
	        throw new IllegalArgumentException(
	                "The given perception module cannot be null");
	    perceptionModule = pModule;
	    perceptionModule.setAgv(this);
	}

    /**
     * @return Returns the map on with the AGV resides,
     * containing both static and dynamic information
     */
    public InformationMap getInformationMap() {
        return imap;
    }
    
    /**
     * @param imap The InformationMap to set.
     */
    public void setInformationMap(InformationMap imap) {
        this.imap = imap;
    }
}