/*
 * Created on Oct 25, 2004
 *
 */
package agentwise.agv.model.agv;

import agentwise.agv.model.environment.Order;

/**
 * A class representing the information about an order that an 
 * AGV can percept
 * 
 * @author nelis
 *
 */
public class OrderInfo {

    public OrderInfo() {
        super();

    }

    private String from;
    private String to;

    /**
     * 
     * @uml.property name="priority" 
     */
    private int priority;

    /**
     * 
     * @uml.property name="id" 
     */
    private long id;



    /**
     * Constructor
     * @param order The Order instance to extract the info from
     */
    public OrderInfo(Order order) {
    	super();
    	setFrom(order.getStartStationId());
    	setTo(order.getDestinationStationId());
    	setPriority(order.getPriority());
    	setId(order.getId());
    }

    /**
     * @return Returns the from.
     * 
     * @uml.property name="from"
     */
    public String getFrom() {
        return from;
    }

    /**
     * @return Returns the to.
     * 
     * @uml.property name="to"
     */
    public String getTo() {
        return to;
    }
    
    /**
     * Returns the priority of this OrderInfo
     * @return Returns the priority.
     * 
     * @uml.property name="priority"
     */
    public int getPriority() {
        return priority;
    }
    
    /**
     * Returns the id of this OrderInfo
     * @return Returns the id.
     * 
     * @uml.property name="id"
     */
    public long getId() {
        return id;
    }

    /**
     * Set the from
     * @param from The from to set.
     * 
     * @uml.property name="from"
     */
    protected void setFrom(String from) {
        this.from = from;
    }

    /**
     * Set the to
     * @param to The to to set.
     * 
     * @uml.property name="to"
     */
    protected void setTo(String to) {
        this.to = to;
    }

    /**
     * Sets the priority to the given value
     * 
     * @param priority The priority to set.
     * 
     * @uml.property name="priority"
     */
    protected void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Sets the id to the given value
     * 
     * @param id The id to set.
     * 
     * @uml.property name="id"
     */
    protected void setId(long id) {
        this.id = id;
    }



}
