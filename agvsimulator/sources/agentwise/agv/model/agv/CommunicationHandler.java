/*
 * Created on 03-Feb-2005
 *
 */
package agentwise.agv.model.agv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import agentwise.agv.controller.ActiveEntity;
import agentwise.agv.model.environment.AgentToEnvironmentInterface;
import agentwise.agv.model.environment.communication.CommunicationInstance;
import agentwise.agv.model.environment.communication.PeriodicCommunication;
import agentwise.agv.model.environment.communication.PrimitiveCommunicationInstance;

/**
 * This active entity handles are communication instances. It
 * allows to issue periodic communications which are repeated
 * every defined number of time steps and also normal
 * communication instances are autonomically handled by this entity.
 * This way the communication can be done in parrallel to the normal
 * behavioural activity of the Agvs
 * 
 * @author tomdw
 *
 */
public class CommunicationHandler extends ActiveEntity {

    /**
     * Constructs an instance of CommunicationHandler that
     * does not contain any messages are periodic communications.
     * This handler will send communication instructions to the
     * AgentToEnvironmentInterface instance of the Agv that needs
     * to be set for this handler (setAgv). This
     * environment should handle all given messages.
     */
    public CommunicationHandler() {
        super();
        periodiccommunications = Collections.synchronizedMap(new HashMap());
        communications = Collections.synchronizedList(new ArrayList());
    }

    /**
     * Iterates all present periodic communications and sends them
     * if necessary. Also all issued messages are sent independently 
     * of the control flow of the AGV itself
     * 
     * @see agentwise.agv.controller.ActiveEntity#step()
     */
    public void step() {
        synchronized(communications) {
	        iterateAndDoCommunications(communications);
	        communications.clear();
        }
        synchronized(periodiccommunications) {
            iterateAndDoCommunications(periodiccommunications.values());
        }
        waitOneTimeStep();
    }
    
    private void waitOneTimeStep() {
        // waiting
    }
    
    /**
     * Iterates a given collection of CommunicationInstances and
     * executes the doCommunication method on each of them.
     * 
     */
    private void iterateAndDoCommunications(Collection communicationsl){
        synchronized(communicationsl) {
            Iterator comiter = communicationsl.iterator();
            while (comiter.hasNext()) {
                CommunicationInstance com = (CommunicationInstance)comiter.next();
                try {
                    com.doCommunication(getEnvironment());
                } catch (Exception e) {
                    //is only thrown when the given env is null
                    e.printStackTrace();
                }
            }
        }
    }

    private Map periodiccommunications;
    private List communications;

    /**
     * 
     * @uml.property name="agv" 
     */
    private Agv agv;

    
    public AgentToEnvironmentInterface getEnvironment() {
        return agv.getEnvironment();
    }
    
    /**
     * Adds a given periodic communication to this communication
     * handler in order for it to be sent every specified time
     * steps untill it is cancelled explicitely.
     * 
     * @param p The periodic communication 
     * @throws NullPointerException when p==null
     */
    public void addPeriodic(PeriodicCommunication p) throws NullPointerException {
        if (p==null) throw new NullPointerException("The given PeriodicCommunication cannot be null");
        periodiccommunications.put(""+p.getID()+"",p);  
    }
    
    /**
     * Makes sure that a periodic communication with an ID equal
     * to the given ID is cancelled and will never be sent out again.
     * The method returns true if the specified periodic communication
     * was present and cancellation succeeded, false will be returned
     * when the periodic was not present or cancellation failed.
     * 
     * @param periodicID The ID of the periodic communication to cancel
     * @return true if succes, false otherwise
     */
    public boolean cancelPeriodic(long periodicID) {
        boolean result = false;
        synchronized(periodiccommunications) {
	        if (periodiccommunications.containsKey("" + periodicID +"")) {
	            periodiccommunications.remove("" + periodicID + "");
	            result = true;
	        }
        }
        return result;
    }
    
    /**
     * Adds a given message to be sent the next time step this active
     * entity becomes active. Once added, it can no longer be removed
     * and will be sent out. The message is encapsulated in a CommunicationInstance
     * that represents the type of communication to do.
     * 
     * @param com the CommunicationInstance that encapsulates the message to send
     * @throws NullPointerException when com==null
     */
    public void addCommunication(PrimitiveCommunicationInstance com) throws NullPointerException {
        if (com==null) throw new NullPointerException("The given PrimitiveCommunicationInstance cannot be null");
        communications.add(com);
    }

    /**
     * Sets the Agv to the given instance for which this handler
     * handles communication. This module cannot be used without this instance
     * 
     * @param agv
     *            The Agv to communicate for
     * @throws IllegalArgumentException
     *             when the given agv is null
     * 
     * @uml.property name="agv"
     */
    public void setAgv(Agv agv)
        throws IllegalArgumentException,
        IllegalStateException {
        if (agv == null)
            throw new IllegalArgumentException(
                "The BehaviourModule cannot be created for a null Agv");
        this.agv = agv;
    }

    /**
     * Returns the Agv for which the communicationhandler handles communication
     * 
     * @return a Agv instance or null when no agv was set for this module
     * 
     * @uml.property name="agv"
     */
    protected Agv getAgv() {
        return agv;
    }

}
