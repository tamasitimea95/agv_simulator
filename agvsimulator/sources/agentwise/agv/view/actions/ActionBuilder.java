/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import javax.swing.JTextField;

/**
 * Builds the network of Actions to be used in the user interface.
 * Each action sends user actions to the agentwise.agv.controller.Controller
 * class of the system which will execute that action. Each
 * Action has references to other actions it needs to enable/disable
 * when it has succeeded its execution or not. This allows
 * to enforce a certain use policy of the simulator. For example,
 * you cannot construct a new simulation when the current simulation
 * is still running.
 * 
 * @author tomdw
 *
 */
public class ActionBuilder {
    
    /**
     * The only instance of ActionBuilder
     */
    protected static ActionBuilder instance = null;
    
    /*
     * The Different instances of actions that are created
     */
    protected StartSimulationAction startAction;
    protected StopSimulationAction stopAction;
    protected BrowseFileAction browseXmlFileAction;
    protected ConstructSimulationAction constructAction;
    protected PauseSimulationAction pauseAction;
    protected ResumeSimulationAction resumeAction;
    protected ZoomAction zoomInAction;
    protected ZoomAction zoomOutAction;
    protected ZoomAction zoomResetAction;
    
    /* The TextField that is used by the BrowseFileAction to set the file path in */
    protected JTextField txt;

            
    /**
     * ActionBuilder implements the Singleton pattern and
     * thus only allows to construct one instance of the
     * builder. This method returns that instance and
     * constructs it when it does not exist yet.
     * 
     * @return the only ActionBuilder instance
     */
    public static ActionBuilder getInstance() {
        if (instance==null) instance = new ActionBuilder();
        return instance;
    }
    
    /**
     * 
     * Constructs an instance of ActionBuilder.
     * This constructor cannot be publically used
     * to enforce the singleton pattern.
     * 
     */
    protected ActionBuilder() {
        build();
    }
    
    /**
     * Builds the network of action instances by
     * connecting them and initialising them.
     *
     */
    protected void build() {
        resumeAction = new ResumeSimulationAction();
        pauseAction = new PauseSimulationAction(resumeAction);
        resumeAction.setPauseSimulationAction(pauseAction);
        browseXmlFileAction = new BrowseFileAction("xml","XML file");
        constructAction = new ConstructSimulationAction();
        stopAction = new StopSimulationAction(constructAction, pauseAction, resumeAction);
        startAction = new StartSimulationAction(pauseAction, stopAction, constructAction);
        constructAction.setStartSimulationAction(startAction);
        zoomInAction = new ZoomAction(ZoomAction.ZOOM_IN);
        zoomOutAction = new ZoomAction(ZoomAction.ZOOM_OUT);
        zoomResetAction = new ZoomAction(ZoomAction.ZOOM_RESET);
    }
    
    /**
     * Sets the TextField that must contain the choosen 
     * file path after executing the BrowseFileAction
     * 
     * @param tf The TextField instance
     */
    public void setTxt(JTextField tf){
    	browseXmlFileAction.setTxt(tf);
    	constructAction.setTxt(tf);
    }
    
    /**
     * Returns the StartSimulationAction that
     * was built by this ActionBuilder.
     * 
     * @return a StartSimulationAction instance
     */
    public StartSimulationAction getStartAction() {
        return startAction;
    }
    
    /**
     * Returns the BrowseFileAction that
     * was built by this ActionBuilder.
     * 
     * @return a BrowseFileAction instance
     */
    public BrowseFileAction getBrowseXmlFileAction() {    	
        return browseXmlFileAction;
    }
    
    /**
     * Returns the StopSimulationAction that
     * was built by this ActionBuilder.
     * 
     * @return a StopSimulationAction instance
     */
    public StopSimulationAction getStopAction() {
        return stopAction;
    }
    
    /**
     * Returns the PauseSimulationAction that
     * was built by this ActionBuilder.
     * 
     * @return a PauseSimulationAction instance
     */
    public PauseSimulationAction getPauseAction() {
        return pauseAction;
    }
    
    /**
     * Returns the ResumeSimulationAction that
     * was built by this ActionBuilder.
     * 
     * @return a ResumeSimulationAction instance
     */
    public ResumeSimulationAction getResumeAction() {
        return resumeAction;
    }
    
    /**
     * Returns the ConstructSimulationAction that
     * was built by this ActionBuilder.
     * 
     * @return a ConstructSimulationAction instance
     */
    public ConstructSimulationAction getConstructAction() {
        return constructAction;
    }
    
    /**
     * Returns the ZoomAction to zoom in that
     * was built by this ActionBuilder.
     * 
     * @return a ZoomAction instance
     */
    public ZoomAction getZoomInAction() {
        return zoomInAction;
    }
    
    /**
     * Returns the ZoomAction to zoom out that
     * was built by this ActionBuilder.
     * 
     * @return a ZoomAction instance
     */
    public ZoomAction getZoomOutAction() {
        return zoomOutAction;
    }
    
    /**
     * Returns the ZoomAction to zoom to
     * the 100% scale that
     * was built by this ActionBuilder.
     * 
     * @return a ZoomAction instance
     */
    public ZoomAction getZoomResetAction() {
        return zoomResetAction;
    }
    

}
