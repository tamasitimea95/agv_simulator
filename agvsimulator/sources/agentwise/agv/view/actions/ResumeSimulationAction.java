/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import agentwise.agv.controller.Controller;
import agentwise.agv.view.SimulationCanvas;

/**
 * This action resumes the current simulation.
 * It can only be done when the simulation is paused.
 * 
 * @author tomdw
 *
 */
public class ResumeSimulationAction extends AbstractAction {
    
    /**
     * Constructs an instance of ResumeSimulationAction.
     * This action starts disabled. It depends on the pause
     * simulation action. They cannot be enabled together
     * at the same time.
     *
     */
    public ResumeSimulationAction() {
        super("Resume");
        putValue(SHORT_DESCRIPTION,"Resume the current simulation");
        setEnabled(false);
        
    }
    
    /**
     * Sets the PauseSimulationAction on which this action has
     * a enable/disable dependency.
     * 
     * @param pauseAction The pause action to set.
     */
    public void setPauseSimulationAction(PauseSimulationAction pauseAction) {
        this.pauseAction = pauseAction;
    }
    
    // the action on which this action has a disable/enable dependency
    private PauseSimulationAction pauseAction = null;

    /**
     * Performs this action on the Controller and
     * enables the pause action and disables this action
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().resumeSimulation();
        SimulationCanvas.setPaused(false);
        pauseAction.setEnabled(true);
        setEnabled(false);
    }

}
