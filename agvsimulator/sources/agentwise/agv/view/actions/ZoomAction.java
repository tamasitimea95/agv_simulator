/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import agentwise.agv.view.MainWindow;

/**
 * This action lets the gui zoom into or out of the simulation.
 * The type of zoom is a configuration when creating this
 * action.
 * 
 * @author tomdw
 */
public class ZoomAction extends AbstractAction {

    /**
     * A constant identifying a configuration of the zoom action that
     * zooms in on the simulation view.
     */
    public static final int ZOOM_IN = 0;
    
    /**
     * A constant identifying a configuration of the zoom action that
     * zooms out on the simulation view.
     */
    public static final int ZOOM_OUT = 1;
    
    /**
     * A constant identifying a configuration of the zoom action that
     * zooms in/out on the simulation view so that it is reset
     * to the 100% view.
     */
    public static final int ZOOM_RESET = 2;
    
    /**
     * 
     * Constructs an instance of ZoomAction based
     * on a given configuration type.
     *
     * @param type the configuration type (ZOOM_IN, ZOOM_OUT, ZOOM_RESET)
     */
    public ZoomAction(int type) {
        super();
        this.type = type;
        if (type==ZOOM_IN) {
            putValue(NAME, "Zoom In");
            putValue(SHORT_DESCRIPTION, "Zoom in on the simulation");
        } else if (type==ZOOM_OUT) {
            putValue(NAME, "Zoom Out");
            putValue(SHORT_DESCRIPTION, "Zoom out on the simulation");
        } else {
            putValue(NAME,"Reset Zoom");
            putValue(SHORT_DESCRIPTION, "Reset the zoom to the original size");
        }
    }

    /**
     * The current configuration type of the zoom action
     */
    private int type = ZOOM_IN;
    
    /**
     * This is called when the action has to be performed. It sends
     * the necessary commands to the gui.
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        if (type==ZOOM_IN) {
            MainWindow.getInstance().zoomIn();
        } else if (type==ZOOM_OUT){
            MainWindow.getInstance().zoomOut();
        } else {
            MainWindow.getInstance().resetZoom();
        }
    }

}
