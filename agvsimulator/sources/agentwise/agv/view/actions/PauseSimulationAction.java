/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import agentwise.agv.controller.Controller;
import agentwise.agv.view.SimulationCanvas;

/**
 * This action pauses the current simulation.
 * 
 * @author tomdw
 *
 */
public class PauseSimulationAction extends AbstractAction {
    
    /**
     * 
     * Constructs an instance of PauseSimulationAction and
     * links it to the given other actions so that
     * enabling and disabling dependencies can be enforced.
     * This action starts disabled.
     * 
     * @param resumeAction The ResumeSimulationAction that can only be enabled when the simulation is paused
     */
    public PauseSimulationAction(ResumeSimulationAction resumeAction) {
        super("Pause");
        putValue(SHORT_DESCRIPTION, "Pauses the running simulation");
        this.resumeAction = resumeAction;
        setEnabled(false);
    }

    // the disable/enable dependent actions
    private ResumeSimulationAction resumeAction = null;
    
    /**
     * Performs the action on the Controller and enables/disables
     * the dependent actions. It enables the resume action and
     * disables this action
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().pauseSimulation();
        SimulationCanvas.setPaused(true);
        setEnabled(false);
        resumeAction.setEnabled(true);
    }

}
