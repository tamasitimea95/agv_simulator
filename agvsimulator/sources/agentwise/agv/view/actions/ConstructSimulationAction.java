/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.JTextField;

import agentwise.agv.controller.Controller;
import agentwise.agv.controller.builder.ParseException;
import agentwise.agv.view.MainWindow;

/**
 * When this action is executed the Controller is asked
 * to construct a new simulation. 
 * 
 * @author tomdw, nelis
 * 
 */
public class ConstructSimulationAction extends AbstractAction {
    
    // the disable/enable dependent actions
	protected StartSimulationAction startAction = null;
	// the textfield containing the xml file path
	protected JTextField txt;
	
	/**
	 * 
	 * Constructs an instance of ConstructSimulationAction.
	 * This action has enable/disable dependencies
	 * with the StartSimulationAction and a JTextField
	 * that contains the xml file that contains the
	 * simulation description that needs to be constructed
	 * 
	 *
	 */
    public ConstructSimulationAction() {
        super("Construct");
        putValue(SHORT_DESCRIPTION, "Construct a new simulation");
    }
    
    

    /**
     * Performs this action on the Controller and
     * reflects the enable/disable dependencies
     * according to this execution. The start action
     * is enabled
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
    		Controller.getInstance().constructSimulation(txt.getText());
    		startAction.setEnabled(true);     
    }

    /**
     * 
     * Sets the TextField from which the file name of the xml file
     * containing the simulation description to construct from.
     * 
     * @param tf The JTextField instance.
     */
    public void setTxt(JTextField tf){
		txt = tf;		
	}
    
    /**
     * Sets the StartSimulationAction that must be enabled when
     * the simulation is constructed.
     * 
     * @param startAction
     */
    public void setStartSimulationAction(StartSimulationAction startAction) {
        this.startAction = startAction;
    }

    
}
