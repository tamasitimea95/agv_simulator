/*
 * Created on Oct 6, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JTextField;

import agentwise.agv.view.MainWindow;

/**
 * When this action is executed the MainWindow is asked to browse
 * for a XML-map for this simulation and put the path in a specified
 * JTextField somewhere in the user interface.
 * 
 * @author nelis
 * 
 */
public class BrowseFileAction extends AbstractAction implements ActionListener {

	protected JTextField txt; // The textfield containing the current file
	protected String filetype;
	protected String filedescription;
	/**
	 * 
	 * @param filetype The type (extension) used for filtering files during
	 * 					browsing
	 * @param filedescription The description belonging to this type
	 */
	public BrowseFileAction(String filetype, String filedescription) {
		super("Browse...");
        putValue(SHORT_DESCRIPTION, "Browse for a map");
        this.filetype = filetype;
        this.filedescription = filedescription;
	}
	/**
	 *  Set the textfield
	 * @param tf The textfield containing the current file
	 */	
	public void setTxt(JTextField tf){
		txt = tf;		
	}
	
	/**
	 * If an action is perfomed, the GUI is asked to provide a browse window
	 * and if the text is selected, the value of the textbox is set.
	 */
	public void actionPerformed(ActionEvent e){
		String file = MainWindow.getInstance().browse(filetype,filedescription,txt.getText());
		if(file != null){
			txt.setText(file);
		}
	}
}
