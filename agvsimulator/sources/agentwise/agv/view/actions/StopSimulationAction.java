/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import agentwise.agv.controller.Controller;
import agentwise.agv.view.SimulationCanvas;

/**
 * This action stops the current simulation.
 * 
 * @author tomdw, nelis
 * 
 */
public class StopSimulationAction extends AbstractAction {
    
    /**
     * 
     * Constructs an instance of StopSimulationAction and
     * links it to the given other actions so that
     * enabling and disabling dependencies can be enforced.
     * This action starts disabled.
     *
     * @param constructAction The ConstructSimulationAction that can only be done when the simulation is stopped
     * @param pauseAction The PauseSimulationAction that can only be done when the simulation is running
     * @param resumeAction The ResumeSimulationAction that can only be done when the simulation is paused
     */
    public StopSimulationAction(ConstructSimulationAction constructAction, PauseSimulationAction pauseAction, ResumeSimulationAction resumeAction) {
        super("Stop");
        putValue(SHORT_DESCRIPTION, "Stop the current simulation");
        setEnabled(false);
        this.constructAction = constructAction;
        this.pauseAction = pauseAction;
        this.resumeAction = resumeAction;
    }
    
    // the other actions that have enable/disable dependencies with this action
    private ConstructSimulationAction constructAction = null;
    private PauseSimulationAction pauseAction = null;
    private ResumeSimulationAction resumeAction = null;
    
    /**
     * Performs this action on the controller and
     * reflects the enable/disable state of all depending
     * actions to this exection. 
     * 
     * The construct action is enabled and the pause, resume and this
     * action are disabled.
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().stopSimulation();
        constructAction.setEnabled(true);
        SimulationCanvas.setPaused(true);
        pauseAction.setEnabled(false);
        resumeAction.setEnabled(false);
        setEnabled(false);
    }

}
