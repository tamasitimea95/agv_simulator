/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import agentwise.agv.controller.Controller;
import agentwise.agv.view.SimulationCanvas;

/**
 * This action starts the current simulation.
 * 
 * @author tomdw, nelis
 *
 */
public class StartSimulationAction extends AbstractAction {
    
    /**
     * 
     * Constructs an instance of StartSimulationAction and
     * links it to the given other actions so that
     * enabling and disabling dependencies can be enforced.
     * This action starts disabled.
     *
     * @param pauseAction The PauseSimulationAction that can only be done when the simulation is running
     * @param stopAction The StopSimulationAction that can only be done when the simulation is paused
     * @param constructAction The ConstructSimulationAction that can only be done when the simulation is stopped
     */
    public StartSimulationAction(PauseSimulationAction pauseAction, StopSimulationAction stopAction,ConstructSimulationAction constructAction) {
        super("Start");
        putValue(SHORT_DESCRIPTION, "Start the simulation");
        setEnabled(false);
        this.pauseAction = pauseAction;
        this.stopAction = stopAction;
        this.constructAction = constructAction;
    }
    
    // the other actions that have enable/disable dependencies with this action
    private PauseSimulationAction pauseAction = null;
    private StopSimulationAction stopAction = null;
    private ConstructSimulationAction constructAction = null;
    
    /**
     * Performs this action on the controller and
     * reflects the enable/disable state of all depending
     * actions to this exection.
     * 
     * The pause, and stop action are enabled, the construct
     * action and this action are disabled.
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().startSimulation();
        SimulationCanvas.setPaused(false);
        pauseAction.setEnabled(true);
        stopAction.setEnabled(true);
        setEnabled(false);
        constructAction.setEnabled(false);
    }

}
