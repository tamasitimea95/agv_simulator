/*
 * Created on Oct 22, 2004
 *
 */
package agentwise.agv.view;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;

/**
 * A GuiConfigTab allows the user
 * to config the available visualisation features. Should
 * the broadcasts for ad-hoc networks be visualised for example, etc
 * 
 * @author tomdw
 *
 */
public class GuiConfigTab extends Tab implements ItemListener  {

    /**
     * Constructs an instance of GuiConfigTab
     * and gives it the title "View"
     */
    public GuiConfigTab() {
        super("View");
    }

    /**
     * @see agentwise.agv.view.Tab#construct()
     */
    public void construct() {
        this.setLayout(new GridLayout(0,1));
        
        drawAgvSurroundingBox = new JCheckBox("Show Agv Surrounding Box");
        drawAgvSurroundingBox.setSelected(GuiConfig.drawAgvSurroundingBox);
        this.add(drawAgvSurroundingBox);
        
        drawAgvAdHocBroadCasts = new JCheckBox("Show Ad-Hoc Broadcast-communication");
        drawAgvAdHocBroadCasts.setSelected(GuiConfig.drawAgvAdHocBroadCasts);
        this.add(drawAgvAdHocBroadCasts);
        
        drawAgvUniCasts = new JCheckBox("Show Unicast-communication");
        drawAgvUniCasts.setSelected(GuiConfig.drawAgvUniCasts);
        this.add(drawAgvUniCasts);
        
        drawAgvReceive = new JCheckBox("Show Agv Receive Message");
        drawAgvReceive.setSelected(GuiConfig.drawAgvReceive);
        this.add(drawAgvReceive);
        
        drawAgvID = new JCheckBox("Show Agv ID");
        drawAgvID.setSelected(GuiConfig.drawAgvID);
        this.add(drawAgvID);
        
        drawStationID = new JCheckBox("Show Station ID");
        drawStationID.setSelected(GuiConfig.drawStationID);
        this.add(drawStationID);
        
//      Register a listener for the check boxes.
        drawAgvSurroundingBox.addItemListener(this);
        drawAgvAdHocBroadCasts.addItemListener(this);
        drawAgvUniCasts.addItemListener(this);
        drawAgvReceive.addItemListener(this);
        drawAgvID.addItemListener(this);
        drawStationID.addItemListener(this);

        setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
    }
    
    private JCheckBox drawAgvSurroundingBox;
    private JCheckBox drawAgvAdHocBroadCasts;
    private JCheckBox drawAgvUniCasts;
    private JCheckBox drawAgvReceive;
    private JCheckBox drawAgvID;
    private JCheckBox drawStationID;
    
    /**
     * Is called when a checkbox is (un)checked 
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    public void itemStateChanged(ItemEvent e) {
        //find out
        //whether it was selected or deselected.
        boolean selected = true;
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            selected = false;
        }
        
        Object source = e.getSource();
        
        if (source == drawAgvSurroundingBox) {
            if (selected) GuiConfig.drawAgvSurroundingBox = true;
            else GuiConfig.drawAgvSurroundingBox = false;
        } else if (source == drawAgvAdHocBroadCasts) {
            if (selected) GuiConfig.drawAgvAdHocBroadCasts = true;
            else GuiConfig.drawAgvAdHocBroadCasts = false;
        } else if (source == drawAgvUniCasts) {
            if (selected) GuiConfig.drawAgvUniCasts = true;
            else GuiConfig.drawAgvUniCasts = false;
        } else if (source == drawAgvReceive) {
            if (selected) GuiConfig.drawAgvReceive = true;
            else GuiConfig.drawAgvReceive = false;
        } else if (source == drawAgvID) {
            if (selected) GuiConfig.drawAgvID = true;
            else GuiConfig.drawAgvID = false;
        } else if (source == drawStationID) {
            if (selected) GuiConfig.drawStationID = true;
            else GuiConfig.drawStationID = false;
        }
        
        SimulationCanvas.getInstance().repaint();
    }

    

}
