/*
 * Created on Oct 22, 2004
 *
 */
package agentwise.agv.view;

import javax.swing.JPanel;

/**
 * A Tab is a JPanel that is added
 * to the InformationPanel of the gui and
 * is set in a JTabbedPane as a tab of it.
 * It has a title and subclasses construct their
 * components in construct().
 * 
 * @author tomdw
 *
 */
public abstract class Tab extends JPanel {

    /**
     * Constructs an instance of Tab
     * and gives it a given title.
     *
     * @param t the given title
     */
    public Tab(String t) {
        super();
        setTitle(t);
        construct();
    }
    
    /**
     * Returns the title for the tab
     * that must be shown on on the
     * selector in the JTabbedPane
     * 
     * @return a string representing the title
     */
    public String getTitle() {
        return title;
    }
    
    /**
     * The title for the tab
     */
    private String title = "Tab";
    

    /**
     * Sets the title for this tab to the given value
     * 
     * @param t The new title for the tab
     * @throws NullPointerException when t == null
     */
    public void setTitle(String t) throws NullPointerException {
        if (t==null) throw new NullPointerException("the given title cannot be null");
        title = t;
    }
    
    /**
     * Constructs the gui components that
     * must be shown inside this tab
     * 
     *
     */
    public abstract void construct();

}
