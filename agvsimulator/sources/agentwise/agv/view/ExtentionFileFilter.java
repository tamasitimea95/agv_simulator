/*
 * Created on Oct 6, 2004
 *
 */
package agentwise.agv.view;

import java.io.File;
import java.util.Vector;

import javax.swing.filechooser.FileFilter;

import agentwise.agv.util.Utils;

/**
 * Filters files and provides a description of the filtering for a 
 * Swing filechooser. Filtering happens by using a string extension 
 * 
 * @author nelis
 */
public class ExtentionFileFilter extends FileFilter {

	/**
	 * The description of this filter
	 */	
	protected String description;
	/**
	 * The extensions belonging to this filter
	 */
	protected Vector extensions;
	
	/**
	 * Constructor
	 */
	public ExtentionFileFilter() {
		super();
		extensions = new Vector();
	}
	
	/**
	 * Add an extension to this filter
	 * @param s A string containing an extension, e.g. "xml"
	 */
	public void addExtension(String s){
		extensions.add(s);
	}
	
	/**
	 * Set the description of this filter
	 * @param description The description of this filter
	 */	
	public void setDescription(String description){
		this.description = description;
	}

	/**
	 * The filtering method, accepts a file if
	 * - It is a directory
	 * - It has an extension contained in this filter
	 */
	public boolean accept(File f) {
		
		if (f.isDirectory()) {
            return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null) {
            return extensions.contains(extension);
        }

        return false;
    }

    /**
     * Returns the description of this filter
     */
    public String getDescription() {
        return description;
    }


}
