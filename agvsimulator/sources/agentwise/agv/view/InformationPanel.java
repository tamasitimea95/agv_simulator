/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.view;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * This panel shows the information that is available 
 * of a selected item in the simulator and other options,
 * information that is necessary.
 * 
 * The panel consists out of a tabbed pane where
 * a Tab instance can be added for each feature
 * to show in the informationpanel.
 * 
 * construct a subclass of Tab to add your own
 * custom information and configuration options
 * to this informationpanel.
 * 
 * @author tomdw
 *
 */
public class InformationPanel extends JPanel {

    /**
     * Constructs an instance of InformationPanel
     *
     * 
     */
    public InformationPanel() {
        super();
        setLayout(new GridLayout(1,1));
        tabbed = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.WRAP_TAB_LAYOUT);
        add(tabbed);
    }
    
    /**
     * The tabbed pane that contains all Tabs
     */
    private JTabbedPane tabbed = null;
   
    /**
     * The Tabs that are visualised on this panel
     */
    private Collection tabs = new ArrayList();
    
    /**
     * Adds a new Tab to this panel. The tab
     * is put at a given index (from 0 to ...)
     * 
     * @param tab the Tab instance to add
     * @param index the index to put the tab at
     */
    public void addTab(Tab tab, int index) {
        if (tab!=null && !tabs.contains(tab)){
            tabbed.insertTab(tab.getTitle(),null,tab,null, index);
            tabs.add(tab);
        }
    }
    
    /**
     * Adds a new Tab to this panel. The tab
     * is put at the front of the row of tabs
     * 
     * @param tab the Tab instance to add
     */
    public void addTab(Tab tab) {
        if (tab!=null && !tabs.contains(tab)){
            tabbed.insertTab(tab.getTitle(),null,tab,null, 0);
            tabs.add(tab);
        }
    }

    /**
     * Clears this panel from all its tabs
     */
    public void reset() {
        tabbed.removeAll();
        tabs.clear();
    }

}
