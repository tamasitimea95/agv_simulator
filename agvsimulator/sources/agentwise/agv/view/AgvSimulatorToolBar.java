/*
 * Created on Sep 9, 2004
 *
 */
package agentwise.agv.view;

import java.io.File;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import agentwise.agv.view.actions.ActionBuilder;

/**
 * This Toolbar contains the necessary control buttons
 * to control the simulation system.
 * 
 * @author tomdw, nelis
 *
 */
public class AgvSimulatorToolBar extends JToolBar {
    
    /**
     * 
     * Constructs an instance of AgvSimulatorToolBar
     * and adds all the necessary buttons to the bar
     *
     */
    public AgvSimulatorToolBar() {
        super("Main");        
        addButtons();
    }
    
    /**
     * Adds the buttons to the toolbar
     *
     */
    private void addButtons() {
        JButton button;
        createButton(ActionBuilder.getInstance().getStartAction(),"Start","Start the simulation");
        createButton(ActionBuilder.getInstance().getStopAction(),"Stop","Stop the simulation");
        createButton(ActionBuilder.getInstance().getPauseAction(),"Pause","Pause the simulation");
        createButton(ActionBuilder.getInstance().getResumeAction(),"Resume","Resume the simulation");
        createButton(ActionBuilder.getInstance().getZoomInAction(),"Zoom In","Zoom into the simulation");
        createButton(ActionBuilder.getInstance().getZoomOutAction(),"Zoom Out","Zoom out of the simulation");
        createButton(ActionBuilder.getInstance().getZoomResetAction(),"Zoom 100%","Zoom to original size");
        
        // A textfield containing the standard testing file
        String defaultFile = System.getProperty("user.dir") + File.separator +"XML" + File.separator + "blokk_33_2c.xml"; 
        JTextField txt = new JTextField(defaultFile);
        txt.setToolTipText("The map for this simulation");
        this.add(txt);
        ActionBuilder.getInstance().setTxt(txt);
        
        createButton(ActionBuilder.getInstance().getBrowseXmlFileAction(),"Browse...","Browse for a map");
        createButton(ActionBuilder.getInstance().getConstructAction(),"Construct","Construct the simulation");
        
    }

    /**
     * Creates a new JButton with a given action as to be executed when clicked, a
     * given text as caption, and a given tooltip.
     */ 
    private void createButton(Action action,String text, String tooltip) {
        JButton button = null;

        button = new JButton();
        button.setAction(action);
        button.setText(text);
        button.setToolTipText(tooltip);
        this.add(button);
    }

}
