/*
 * Created on 15-Nov-2004
 *
 */
package agentwise.agv.view;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.controller.Controller;
import agentwise.agv.controller.Simulation;
import agentwise.agv.model.environment.OrderLocationManager;
import agentwise.agv.model.environment.map.Location;

/**
 * This tab can be used to manage the orders for
 * the system. For the moment it allows to
 * add Order instances at certain OrderLocations to the
 * system.
 * 
 * @author tomdw
 */
public class OrderManagerTab extends Tab implements ActionListener {

    /**
     * Constructs an instance of OrderManagerTab
     * and attaches the observer behaviour
     * 
     */
    public OrderManagerTab() {
        super("Orders");
        initObservers();
    }

    /**
     * Registers this tab as interested
     * for the event of setting a new simulation
     * so that the available pick and drop orderlocations
     * can be updated.
     */
    private void initObservers() {
        ChangeManager.getInstance().registerForNewSimulationSetEvent(new ChangeObserverAdapter(){
            public void updateNewSimulationSet(Simulation sim) {
                OrderLocationManager om = sim.getOrderLocationManager();
                Collection pick = om.getPickOrderLocations();
                Collection drop = om.getDropOrderLocations();
                updateFromList(pick);
                updateToList(drop);
                updatePriorityList();
                if (pick.size()>0 && drop.size()>0) addOrder.setEnabled(true);
            }
        });
    }

    /**
     * Updates the list of OrderLocations that
     * can serve as destinations to correspond
     * with the given collection of Location instances.
     * The ID of the location is used to show in the list
     * 
     * @param drop the collection of Location instances
     */
    protected void updateToList(Collection drop) {
        String[] data = new String[drop.size()];
        Iterator iter = drop.iterator();
        int i = 0;
        while(iter.hasNext()) {
            Location loc = (Location)iter.next();
            data[i] = new String(loc.getID());
            i++;
        }
        tolist.setModel(new DefaultComboBoxModel(data));
    }

    /**
     * Updates the list of OrderLocations that
     * can serve as sources to correspond
     * with the given collection of Location instances.
     * The ID of the location is used to show in the list
     * 
     * @param pick the collection of Location instances
     */
    protected void updateFromList(Collection pick) {
        String[] data = new String[pick.size()];
        Iterator iter = pick.iterator();
        int i = 0;
        while(iter.hasNext()) {
            Location loc = (Location)iter.next();
            data[i] = new String(loc.getID());
            i++;
        }
        fromlist.setModel(new DefaultComboBoxModel(data));
        
    }
    
    /**
     * Updates/initialized the list
     * with priorities from 0 to 10
     *
     */
    protected void updatePriorityList() {
        Integer[] data = new Integer[11];
        for (int i=0;i<=10;i++) {
            data[i] = new Integer(i);
        }
        prioritylist.setModel(new DefaultComboBoxModel(data));
        prioritylist.setSelectedIndex(5);
    }

    /**
     * Constructs the Form to add orders
     * in this Tab
     * 
     * @see agentwise.agv.view.Tab#construct()
     */
    public void construct() {
        setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0,2,10,5));
        fromlist = new JComboBox();
        tolist = new JComboBox();
        prioritylist = new JComboBox();
        fromlist.setEditable(false);
        tolist.setEditable(false);
        prioritylist.setEditable(false);
        
        JLabel lFrom = new JLabel("From");
        lFrom.setLabelFor(fromlist);
        JLabel lTo = new JLabel("To");
        lTo.setLabelFor(tolist);
        JLabel lpriority = new JLabel("Priority");
        lpriority.setLabelFor(prioritylist);
        
        panel.add(lFrom);
        panel.add(fromlist);
        panel.add(lTo);
        panel.add(tolist);
        panel.add(lpriority);
        panel.add(prioritylist);
        
        addOrder = new JButton("Add Order");
        addOrder.setEnabled(false);
        addOrder.addActionListener(this);
        panel.add(new JLabel(""));
        panel.add(addOrder);
        add(panel);
        
    }
    
    /**
     * The list with sources
     */
    private JComboBox fromlist;
    
    /**
     * The list with destinations
     */
    private JComboBox tolist;
    
    /**
     * The list with priorities
     */
    private JComboBox prioritylist;
    
    /**
     * The button that is pressed when the order
     * configured in the tab must be added
     */
    private JButton addOrder;

    /**
     * When the addOrder button is pressed
     * a new Order instance is added to the simulation
     * configured with the information currently set
     * in this Tab
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        Controller.getInstance().addOrder((String)fromlist.getSelectedItem(), (String)tolist.getSelectedItem(), prioritylist.getSelectedIndex());
    }

}
