/*
 * Created on Oct 21, 2004
 *
 */
package agentwise.agv.view;

/**
 * GuiConfig contains the configuration information
 * for the user interface. More specifically for what
 * to visualise and what not.
 * 
 * @author tomdw
 * 
 */
public class GuiConfig {
    
    /**
     * Indicates if the user interface should visualise
     * the surrounding box of an Agv
     */
    public static boolean drawAgvSurroundingBox = false;
    
    /**
     * Indicates if the user interface should visualise
     * the broadcasts done in an ad hoc network
     */
    public static boolean drawAgvAdHocBroadCasts = true;
    
    /**
     * Indicates if the user interface should visualise
     * unicasts between agvs
     */
    public static boolean drawAgvUniCasts = true;
    
    /**
     * Indicates if the user interface should visualise
     * messages that an agv receives
     */
    public static boolean drawAgvReceive = true;
    
    /**
     * Indicates if the user interface should visualise
     * the ID of an Agv
     */
    public static boolean drawAgvID = true;
    
    /**
     * Indicates if the user interface should visualise
     * the ID of a station and location
     */
    public static boolean drawStationID = true;

}
