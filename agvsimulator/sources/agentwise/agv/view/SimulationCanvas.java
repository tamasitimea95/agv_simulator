/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.view.objects.AgvDrawing;
import agentwise.agv.view.objects.DrawObject;
import agentwise.agv.view.objects.OrderLocationDrawing;
import agentwise.agv.view.objects.SegmentDrawing;
import agentwise.agv.view.objects.StationDrawing;

/**
 * This panel provides the view on the environment of the simulation by drawing
 * it and the objects in it (agvs, stations, ...). 
 * The canvas also allows to zoom on the picture and
 * to pause the drawing of temporary objects which will
 * disappear after their time to show has passed. When paused
 * this time will not diminish but the objects will still
 * be drawn.
 * 
 * @author tomdw
 *  
 */
public class SimulationCanvas extends JPanel {
    
    /**
     * Used to know when the simulation is paused.
     * This is needed in order for the canvas
     * not to remove temporary objects to soon.
     * 
     * When this is true their time left to show
     * should not be diminished.
     */
    private static boolean isPaused = true;
    
    /**
     * Informs the gui if the simulation should be
     * paused or not. Needed in order for the canvas
     * not to remove temporary objects to soon.
     * 
     * When this is set to true the time left to
     * show of temporary drawobjects is not
     * diminished.
     * 
     * @param b The new value of isPaused
     */
    public static void setPaused(boolean b) {
        isPaused = b;
    }
    
    /**
     * Returns if the simulation should be shown
     * paused or not. Needed in order for the canvas
     * not to remove temporary objects to soon.
     * 
     * When this returns true the time left to show
     * of temporary drawobjects should not be 
     * diminished.
     * 
     * @return a boolean indicating if the simulation is paused or not
     */
    public static boolean isPaused() {
        return isPaused;
    }

    /**
     * Constructs an instance of SimulationCanvas 
     * with nothing on it. This Canvas is also 
     * registered as interested in certain events
     * from the system (ie agv added, start construction
     * simulation, map added).
     * 
     * The instance last constructed through this
     * constructor is available through the
     * static getInstance() method on this class.
     *  
     */
    public SimulationCanvas() {
        super();
        instance = this;
        clear();
        initialiseObservers();
    }
    
    /**
     * Initialised this canvas as interested for certain
     * events from the system by registering observers
     * with the ChangeManager. The canvas is registered 
     * for the following events: agv added, start of construction
     * simulation, map added.
     *
     */
    private void initialiseObservers() {
        final SimulationCanvas thisObject = this;
        ChangeManager.getInstance().registerForMultipleEvents(new ChangeObserverAdapter() {
           
            /**
             * Adding an Agv implies the construction of a 
             * new AgvDrawing.
             * 
             * @see agentwise.agv.controller.ChangeObserver#updateAgvAdded(agentwise.agv.model.environment.AgvEnvironmentState)
             */
           public void updateAgvAdded(AgvEnvironmentState agvstate) {
               AgvDrawing drawing = new AgvDrawing(agvstate, thisObject);
               addDrawObject(drawing, 4);
           }
           
           /**
            * When a new simulation is started to be constructed
            * then the canvas should be cleared.
            * 
            * @see agentwise.agv.controller.ChangeObserver#updateStartConstructionNewSimulation()
            */
           public void updateStartConstructionNewSimulation() {
               clear();
           }
           
           /**
            * When a new map is added to a simulation
            * all the components of that map are reflected
            * in this canvas with the respective draw objects
            * (segments, stations, locations, ...)
            * 
            * @see agentwise.agv.controller.ChangeObserver#updateMapAdded(java.util.Collection)
            */
           public void updateMapAdded(Collection segments) {
               Iterator iter = segments.iterator();
               while (iter.hasNext()) {
                   Segment s = (Segment)iter.next();
                   SegmentDrawing drawing = new SegmentDrawing(s, thisObject);
                   addDrawObject(drawing, 1);
                   if (s.getStart() instanceof OrderLocation) {
                       OrderLocationDrawing l1dr = new OrderLocationDrawing((OrderLocation)s.getStart(), thisObject);
                       addDrawObject(l1dr, 2);
                   } else {
                       StationDrawing s1dr = new StationDrawing(s.getStart(), thisObject);
                       addDrawObject(s1dr, 2);
                   }
                   if (s.getEnd() instanceof OrderLocation) {
                       OrderLocationDrawing l2dr = new OrderLocationDrawing((OrderLocation)s.getEnd(), thisObject);
                       addDrawObject(l2dr, 2);
                   } else {
                       StationDrawing s2dr = new StationDrawing(s.getEnd(), thisObject);
                       addDrawObject(s2dr, 2);
                   }
                   
               }
               
           }
        });
    }

    /**
     * Returns the zooming factor for the simulation. It returns a number that
     * represents the percent in which the panel is shown. 100% means no
     * zooming, smaller than 100% means zooming out and larger than 100% means
     * zooming in.
     * 
     * @return a double that represents the zoom percentage
     */
    public double getZoomFactor() {
        return getScalingFactor()*2 * 100.0;
    }

    /**
     * Sets the zooming factor for the simulation. It sets the percentage that
     * indicates how much the panel should zoom in or out. 100% means no
     * zooming, smaller than 100% means zooming out, and larger than 100% means
     * zooming in.
     * 
     * @param factor
     *            The new zooming factor
     * @throws IllegalArgumentException
     *             when the given factor is smaller or equal to 0 %
     */
    private void setZoomFactor(double factor) throws IllegalArgumentException {
        setScalingFactor(factor/2/100.0);
    }

    /**
     * Adds a object that must be drawn on the panel. The paintComponent method
     * of DrawObject will be called when painting the panel. A higher layer means
     * being draw more to the foreground of the canvas.
     * 
     * The given layer can be any value but when larger than 5 it
     * is converted to 5 and when smaller than 0 it is converted to 0.
     * So 6 layers are supported (0,1,2,3,4,5).
     * 
     * @param obj
     *            the instance of the DrawObject to add
     * @param layer The layer on which to draw it.
     */
    public void addDrawObject(DrawObject obj, int layer) {
        final DrawObject toAdd = obj;
        if (layer > 5) layer = 5;
        if (layer < 0) layer = 0;
        final int layerToAddTo = layer;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                Collection layerObjects = (Collection)permanentDrawObjects.get(new Integer(layerToAddTo));
                layerObjects.add(toAdd);
            }
        });

    }
       
    /**
     * The DrawObject instances in a hasmap with
     * as key the layer on which they must
     * be shown and as value a collection of
     * DrawObjects on that layer.
     * 
     */
    HashMap permanentDrawObjects = new HashMap();
    
    /**
     * Initialises the permanentDrawObjects HashMap
     * by making a collection for each layer and
     * using the layer as a key.
     *
     */
    private void initDrawObjects() {
        permanentDrawObjects.clear();
        for (int i = 0; i <= 5; i++) {
            permanentDrawObjects.put(new Integer(i), new HashSet());
        }
    }
    
    /**
     * Clears this canvas of all simulation
     * information (environment, drawobjects, ...)
     *
     */
    private void clear() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // remove the environment
                setEnvironmentHeight(0);
                setEnvironmentWidth(0);
                // remove the Drawobjects
                initDrawObjects();
            }
        });
    }

    /**
     * Draws the environment and the DrawObjects on it. The method also scales
     * the drawing according to the zooming factor of this panel.
     * 
     * Depending on the configuration in GuiConfig some
     * features are drawn and others are not.
     * 
     */
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D) graphics;
        // zoom
        g.scale(getScalingFactor(), getScalingFactor());
        
        // anti-aliasing
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        // draw (background of) the environment
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, (new Double(envWidth)).intValue(), (new Double(envHeight)).intValue());
        
        // draw each permanent DrawObject in this panel
        for(int i = 0; i <= 5; i++) {
            drawLayer(g, (Collection)permanentDrawObjects.get(new Integer(i)));
        }
        
        // undo the zooming
        g.scale(1 / getScalingFactor(), 1 / getScalingFactor());
    }
    
    
    /**
     * Draws a given collection of DrawObjects.
     * To draw different layers on to of each other this
     * method should be called for each layer in ascending
     * order so that a lower layer is behind a higher layer.
     * 
     * @param g the canvas to draw on
     * @param drawobjects The collection of drawobjects
     */
    private void drawLayer(Graphics2D g, Collection drawobjects) {
        Iterator iter = drawobjects.iterator();
        while (iter.hasNext()) {
            DrawObject obj = (DrawObject) iter.next();
            obj.paintComponent(g);
        }
    }

    /**
     * Refreshes this canvas by setting its
     * (preferred)size to reflect the dimensions
     * of the (zoomed) environment and repaints/revalidates
     * the panel.
     * 
     * Should be called whenever the scaling factor or
     * the environment dimensions change.
     *
     */
    private void refresh() {
        setPreferredSize(new Dimension((int) Math.round(envWidth
                * getScalingFactor()), (int) Math.round(envHeight
                * getScalingFactor())));
        setSize(new Dimension((int) Math.round(envWidth
                * getScalingFactor()), (int) Math.round(envHeight
                * getScalingFactor())));
        repaint();
        revalidate();    
    }
    
    /**
     * Sets the environment that this panel should represent 
     * by extracting the needed information from the given
     * environment.
     * 
     * @param envir
     *            The environment to represent
     */
    void setEnvironment(Environment envir) {
        final Environment env = envir;
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                setEnvironmentWidth(env.getWidth());
                setEnvironmentHeight(env.getHeight());
                refresh();
            }
        });
    }


    /**
     * Sets the height of the environment as information to reflect in the view
     * 
     * @param height
     *            The height of the environment
     */
    private void setEnvironmentHeight(double height) {
        envHeight = height;
    }

    /**
     * Sets the width of the environment as information to reflect in the view
     * 
     * @param width
     *            The width of the environment
     */
    private void setEnvironmentWidth(double width) {
        envWidth = width;
    }

    /**
     * Returns the factor with which each distance on this panel should be
     * multiplied before it is drawn on the panel. It influences the zooming
     * factor of the panel.
     * 
     * @return
     */
    private double getScalingFactor() {
        return scalingfactor;
    }

    /**
     * Sets the scaling factor for this panel. It sets the factor with which
     * each distance on this panel must be multiplied before it is drawn.
     * 
     * @param factor
     *            The new scaling factor
     * @throws IllegalArgumentException
     *             when the given factor is smaller or equal to 0.0
     */
    private void setScalingFactor(double factor)
            throws IllegalArgumentException {
        if (factor <= 0.0)
            throw new IllegalArgumentException(
                    "The given scaling factor must be larger than 0.0");
        scalingfactor = factor;
        refresh();
    }

    /**
     * determines the zoom factor for the panel. if it is 0.5 then no zooming
     * occurs, smaller than 0.5 means zooming out, larger than 0.5 means zooming in
     * with factor*2*100 %.
     */
    private double scalingfactor = 0.5;

    /**
     * The width of the environment that this panel should represent
     */
    private double envWidth;

    /**
     * The height of the environment that this panel should represent
     */
    private double envHeight;

    /**
     * Makes the canvas zoom into the drawn picture
     * by adjusting the zooming factor by 5 % upwards.
     */
    void zoomIn() {
        double currentFactor = getZoomFactor();
        setZoomFactor(currentFactor+5);
    }
    
    /**
     * Makes the canvas zoom out from the drawn picture
     * by adjusting the zooming factor by 5 % downwards
     *
     */
    void zoomOut() {
        double currentFactor = getZoomFactor();
        if (currentFactor-5 <= 5) setZoomFactor(5);
        else setZoomFactor(currentFactor-5);
    }
    
    /**
     * Makes the canvas zoom to the original size of
     * the drawn picture by adjusting the zooming factor
     * back to 100 %
     *
     */
    void resetZoom() {
        setZoomFactor(100);
    }

    /**
     * Returns the current instance of the SimulationCanvas.
     * This is not a singleton pattern but just a reference
     * to the latest constructed SimulationCanvas.
     * 
     * @return a SimulationCanvas instance
     */
    public static SimulationCanvas getInstance() {
        return instance;
    }
    
    /**
     * The latest constructed SimulationCanvas instance
     */
    private static SimulationCanvas instance;

}