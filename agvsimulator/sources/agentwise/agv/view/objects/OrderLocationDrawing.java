/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Polygon;

import javax.swing.SwingUtilities;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.model.environment.map.OrderLocation;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.view.GuiConfig;
import agentwise.agv.view.SimulationCanvas;

/**
 * An OrderLocationDrawing draws an OrderLocation on screen by
 * drawing a visualisation of it on the coordinate, drawing an
 * order when there is one available and draws arrows to indicate
 * if it is an in/out or buffer order location
 * 
 * @author tomdw
 *
 */
public class OrderLocationDrawing extends DrawObject {
    
    /**
     * Constructs an instance of OrderLocationDrawing
     * to reflect the given OrderLocation and
     * to be drawn on the given SimulationCanvas.
     * 
     * This drawing is also registered for order location
     * changed events.
     *
     * @param l The given OrderLocation
     * @param canvas The SimulationCanvas to draw on
     */
    public OrderLocationDrawing(OrderLocation l, SimulationCanvas canvas) {
        locationID = l.getID();
        this.canvas = canvas;
        initialiseObservers();
        update(l);
    }
    
    /**
     * The canvas to draw on
     */
    private SimulationCanvas canvas;
    
    /**
     * The id of this location, a station id
     */
    private String locationID;
    
    /**
     * Registers this drawing for an order location changed
     * event so that this drawing is updated when the order location
     * that is represented by this locationdrawing has changed.
     *
     */
    private void initialiseObservers() {
        ChangeManager.getInstance().registerForOrderLocationChangedEvent(new ChangeObserverAdapter() {
            public void updateOrderLocationChanged(OrderLocation s) {
                if (s.getID()==locationID) {
                    final OrderLocation toUse = s;
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            update(toUse);
                        }
                    }); 
                }
            }
        });
        
        ChangeManager.getInstance().registerForStationChangedEvent(new ChangeObserverAdapter() {
            public void updateStationChanged(Station s) {
                if (s.getID()==locationID) {
                    final OrderLocation toUse = (OrderLocation)s;
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            update(toUse);
                        }
                    }); 
                }
            }
        });
    }
    
    /**
     * Is true when the OrderLocation support
     * dropping an order.
     */
    private boolean supportDropOrder = false;
    
    /**
     * Is true when the OrderLocation supports
     * picking up an order.
     */
    private boolean supportPickOrder = false;
    
    /**
     * Is true when there is an order available
     * in the OrderLocation
     */
    private boolean hasOrder = false;

    /**
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        g.translate(xcoord, ycoord);
        g.setColor(Color.WHITE);
        g.setStroke(new BasicStroke(2.0f));
        g.fillOval(-100,-100,200,200);
        g.setColor(c);
        g.fillRoundRect(-20,-20,40,40,10,10);
        g.setColor(c);
        g.drawOval(-100,-100,200,200);
        if (hasOrder) {
            int ordersize = 80;
            int xorderstart = -40;
            int yorderstart = -40;
	        g.setColor(new Color(Color.GRAY.getRed(), Color.GRAY.getGreen(), Color.GRAY.getBlue(), 150));
	        g.setStroke(new BasicStroke(4.0f));
	        g.fillRoundRect(xorderstart, yorderstart,ordersize,ordersize, 40,40);
	        g.setColor(Color.BLACK);
	        g.drawRoundRect(xorderstart, yorderstart,ordersize,ordersize, 40,40);
        
        }
        
        // in and out arrows
        Polygon arrow = new Polygon();
        int h = 20; // height of arrow line
        int w = 80; // width from arrow point untill end of arrow line
        int h2 = 30; // height of the arrow triangle
        int w2 = 20; // width from the arrow point untill end of arrow triangle
        arrow.addPoint(0,0);
        arrow.addPoint(-w2,-h2/2);
        arrow.addPoint(-w2,-h/2);
        arrow.addPoint(-w,-h/2);
        arrow.addPoint(-w,h/2);
        arrow.addPoint(-w2,h/2);
        arrow.addPoint(-w2,h2/2);
        
        // only drop order, then an out location
        if (supportDropOrder && !supportPickOrder) {
            g.setColor(c);
            g.translate(-100-w/2,0);
            g.rotate(Math.PI);
            g.fill(arrow);
            g.rotate(-Math.PI);
            g.translate(100+w/2,0);
        }
        
        // only pick order, then an in location
        if (supportPickOrder && !supportDropOrder) {
            g.setColor(c);
            g.translate(-100+w/2,0);
            g.fill(arrow);
            g.translate(100-w/2,0);
        }
        // drop and pick then a buffer location
        
        if (GuiConfig.drawStationID) drawID(g);
        
        g.translate(-xcoord, -ycoord);

    }

    
    /**
     * Draws the id of the location
     * 
     * @param g The Graphics2D to draw on
     */
    private void drawID(Graphics2D g) {
        g.setFont(new Font(null, Font.BOLD, 24));
        String id = "" + locationID + "";
        FontMetrics metrics = g.getFontMetrics();
        int width = metrics.stringWidth( id );
        int height = metrics.getHeight();
        g.drawString(locationID,-width/2,-25-height);
    }
    
    private Color c = Color.BLUE;
    
    public void setColor(Color color) {
        if (color!=null) c = color;
    }

    /**
     * The x coordinate for the location
     */
    private int xcoord = 0;
    
    /**
     * The y coordinate for the location
     */
    private int ycoord = 0;
    
    /**
     * Updates this drawing to reflect the given OrderLocation 
     * and calls repaint on the canvas.
     * 
     * @param l
     */
    private void update(OrderLocation l) {
        xcoord = (int)Math.round(l.getCoordinate().getX());
        ycoord = (int)Math.round(l.getCoordinate().getY());
        hasOrder = l.hasOrder();
        supportDropOrder = l.supportsDropOrder();
        supportPickOrder = l.supportsPickOrder();
        canvas.repaint();
    }

}
