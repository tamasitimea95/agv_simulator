/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.SwingUtilities;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.model.environment.AgvEnvironmentState;
import agentwise.agv.model.environment.communication.Message;
import agentwise.agv.model.environment.communication.NetworkID;
import agentwise.agv.util.Coordinate;
import agentwise.agv.util.Line;
import agentwise.agv.util.Rectangle;
import agentwise.agv.view.GuiConfig;
import agentwise.agv.view.SimulationCanvas;

/**
 * This class draws an AGV on the parent panel. Drawing
 * an Agv involves information like the coordinate, 
 * the direction, the id of the agv, etc. 
 * 
 * Also the visualisation of communication 
 * that is send or received by this agv is
 * drawn from within this class.
 * 
 * @author tomdw
 *
 */
public class AgvDrawing extends DrawObject {
    
    // the id identifying the agv
    private long agvid;
    
    // the panel on which to draw
    private SimulationCanvas canvas;

    /**
     * 
     * Constructs an instance of AgvDrawing based
     * on the needed information:
     *
     * @param x The x coordinate
     * @param y The y coordinate
     * @param dir The direction in degrees
     * @param id The id identifying the Agv
     * @param canvas the canvas to draw on
     */
    public AgvDrawing(int x, int y, double dir, long id, SimulationCanvas canvas) {
        setX(x);
        setY(y);
        setDirection(dir);
        setAgvID(id);
        this.canvas = canvas;
        
        initialiseObservers();
    }
    
    /**
     * Initialises the observers by registering
     * this class as an observer for certain
     * events with the ChangeManager.
     * 
     * The events handled by this class are the 
     * changes of an agv and its communication.
     * 
     */
    private void initialiseObservers() {
        final AgvDrawing thisObject = this;
        ChangeManager.getInstance().registerForAgvChangedEvent(new ChangeObserverAdapter() {
           
            public void updateAgvChanged(AgvEnvironmentState agvstate) {
                if (agvstate.getAgvID().getID()==thisObject.getAgvID()) {
                    update(agvstate);
                }
            }
        });
        ChangeManager.getInstance().registerForBroadCastEvent(new ChangeObserverAdapter() {
            public void updateBroadCast(Coordinate c, Message msg,  double range) {
                if (msg.getSender().getID() == getAgvID()) {
                    final Coordinate coord = c;
                    final int r = (int)Math.round(range);
                    SwingUtilities.invokeLater(new Runnable() {
                       public void run() {
                           adhocBroadCast = new AdHocBroadCastDrawing(coord,r);
                           canvas.repaint();
                       }
                    });
                    
                    
                }
            }
        });
        ChangeManager.getInstance().registerForUniCastEvent(new ChangeObserverAdapter() {
            public void updateUniCast(Coordinate sc, Message msg, NetworkID r) {
                if (r.getID()==getAgvID()) {
                    final Coordinate sendc = sc;
                    final Coordinate recc = new Coordinate(getX(),getY());
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            UniCastDrawing unicast = new UniCastDrawing(sendc,recc);
                            unicasts.add(unicast);
                            canvas.repaint();
                        }
                    });
                }
            }
        });
        ChangeManager.getInstance().registerForReceiveEvent(new ChangeObserverAdapter() {
            public void updateReceive(NetworkID receiver,Message msg) {
                if (receiver.getID()==getAgvID()) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            received = true;
                            canvas.repaint();
                        }
                    });
                }
            }
        });
        
   
    }
    
    /**
     * The unicast drawings that were last sent and
     * must still be drawn (ie its timelefttoshow is
     * not yet 0)
     */
    private Collection unicasts = new ArrayList();
    
    /**
     * The broadcast that was last sent and
     * must still be drawn (ie its timeleftotshow is not
     * yet 0)
     */
    private DrawObject adhocBroadCast = null;
    
    /**
     * Is true when the last time there was a message received
     */
    private boolean received = false;
 

    /**
     * Sets the ID identifying the Agv that
     * is drawn by this drawobject
     * 
     * @param id The ID of the Agv
     */
    private void setAgvID(long id) {
        this.agvid = id;
    }
    
    /**
     * Returns the ID of the Agv that
     * is drawn by this drawobject.
     * This is also the networkId.
     * 
     * @return a long representing the id
     */
    private long getAgvID() {
        return agvid;
    }

    /**
     * Constructs an instance of AgvDrawing based
     * on a given Agv and a given simulationcanvas on
     * which the agv should be drawn
     *
     * @param agvstate The state of the Agv to draw in this drawing
     * @param canvas The SimulationCanvas to draw on
     */
    public AgvDrawing(AgvEnvironmentState agvstate, SimulationCanvas canvas) {
        this((int)Math.round(agvstate.getCoordinate().getX()), (int)Math.round(agvstate.getCoordinate().getY()), agvstate.getDirection().getDegrees(), agvstate.getAgvID().getID(), canvas);
        update(agvstate);
    }

    /**
     * Sets the x coordinate on which the agv must be drawn
     * 
     * @param x The new x coordinate
     */
    private void setX(int x) {
        xcoord = x;
    }
    
    /**
     * Sets the y coordinate on which the agv must be drawn
     * @param y The new y coordinate
     */
    private void setY(int y) {
        ycoord = y;
    }
    
    /**
     * Sets the driection in degrees on in which
     * the agv is directed
     * 
     * @param dir The direction in degrees
     */
    private void setDirection(double dir) {
        direction = Math.PI*dir/180.0;
    }
    
    /**
     * The x coordinate
     */
    private int xcoord = 0;
    
    /**
     * The y coordinate
     */
    private int ycoord = 0;
    
    /**
     * The direction in radians
     */
    private double direction = 0.0;
    
    /**
     * Has an order or not
     * 
     */
    private boolean hasOrder = false;
    
    /**
     * Sets if the agv drawn by this drawing holds
     * an Order or not
     * 
     * @param b The value for the hasOrder info
     */
    private void setHasOrder(boolean b){
        hasOrder = b;
    }
    
    /**
     * Is suspended or not
     */
    private boolean isSuspended = false;
    
    /**
     * Sets if the agv drawn by this drawing is
     * suspended or not.
     * 
     * @param b The value for the isSuspended info
     */
    private void setSuspended(boolean b) {
        isSuspended =b;
    }
    
    /**
     * This is the surrounding box for this agv.
     * It contains the whole Agv.
     * 
     */
    private Rectangle box = null;
    
    
    /**
     * Paints this agv on the destination panel
     * 
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        // configuration variables to configure the drawing
        int boxWidth = (int)Math.round(box.getWidth()*0.385);
        int boxHeight = (int)Math.round(box.getHeight());
        int forkWidth = (int)Math.round(box.getWidth()*0.615+10);
        int middleX = (int)Math.round(box.getWidth()/2-(box.getMiddle().distanceTo(new Coordinate(getX(),getY()))));
        int middleY = boxHeight/2;
        
        
        
        // rotate to reflect the current direction and position
        g.rotate(-(direction+Math.PI),getX(),getY());
        //draw the forks
        g.setColor(Color.RED);
        int forkHeight = boxHeight/5;
        int firstStart = (int)Math.round(forkHeight*1.5);
        int secondStart = (int)Math.round(forkHeight*0.5);
        g.fillRoundRect(getX()-middleX+boxWidth-10,getY()-firstStart,forkWidth,forkHeight, 20, 20);
        g.fillRoundRect(getX()-middleX+boxWidth-10,getY()+secondStart,forkWidth,forkHeight, 20, 20);
        
        // draw the box 
        g.setColor(Color.ORANGE);
        Stroke current = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        g.fillRoundRect(getX()-middleX, getY()-middleY, boxWidth, boxHeight,20,20);
        // draw the border
        g.setColor(Color.RED);
        g.drawRoundRect(getX()-middleX,getY()-middleY,boxWidth, boxHeight, 20, 20);
        // draw the middle point
        g.fillRect(getX()-2,getY()-2,8,8);
        g.setStroke(current);
        
        if (isSuspended) drawSuspended(g, middleX, middleY);
        if (GuiConfig.drawAgvReceive && received) drawReceive(g, middleX, middleY);
        if (hasOrder) drawOrder(g, boxWidth, forkWidth, middleX, forkHeight, firstStart, secondStart);
                
        // restore the rotation and translation
        g.rotate(direction+Math.PI,getX(),getY());
        
        if (GuiConfig.drawAgvAdHocBroadCasts) drawAdHocBroadCast(g);
        if (GuiConfig.drawAgvUniCasts) drawUnicasts(g);
        if (GuiConfig.drawAgvID) drawAgvID(g, boxWidth);
        if (GuiConfig.drawAgvSurroundingBox) drawSurroundingBox(g);
    }

    /**
     * Draws the suspended sign on the agv.
     * 
     * @param g the Graphics2D object to draw on
     * @param middleX the distance between the upper left corner of the agv and the x coordinate of the agv
     * @param middleY the distance between the upper left corner of the agv and the y coordinate of the agv
     */
    private void drawSuspended(Graphics2D g, int middleX, int middleY) {
        int r = (int)Math.round(box.getHeight()*0.4);
        g.setColor(Color.WHITE);
        g.fillOval(getX()-middleX-r/2,getY()-middleY-r/2,r,r);
        g.setColor(Color.RED);
        Stroke sc = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        g.drawOval(getX()-middleX-r/2,getY()-middleY-r/2,r,r);
        g.setStroke(new BasicStroke(8.0f));
        int lline = (int)Math.round((r*0.2));
        g.drawLine(getX()-middleX-lline,getY()-middleY -lline,getX()-middleX + lline,getY()-middleY+lline);
        g.drawLine(getX()-middleX-lline,getY()-middleY +lline,getX()-middleX + lline,getY()-middleY-lline);
        g.setStroke(sc);
        /*unicasts.clear();
        adhocBroadCast = null;
        received = false;*/
    }

    /**
     * Draws an envelope to indicate that this
     * agv has received a message.
     * 
     * @param g the Graphics2D object to draw on
     * @param middleX the distance between the upper left corner of the agv and the x coordinate of the agv
     * @param middleY the distance between the upper left corner of the agv and the y coordinate of the agv
     */
    private void drawReceive(Graphics2D g, int middleX, int middleY) {
        int w = (int)Math.round(box.getHeight()*0.4);
        int h = (int)Math.round(box.getHeight()*0.3);
        Stroke sc = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        g.setColor(Color.WHITE);
        g.fillRect(getX()-middleX-h/2,getY()+middleY-w/2,h,w);
        g.setColor(Color.BLACK);
        g.drawRect(getX()-middleX-h/2,getY()+middleY-w/2,h,w);
        g.drawLine(getX()-middleX-h/2,getY()+middleY-w/2,getX()-middleX,getY()+middleY);
        g.drawLine(getX()-middleX-h/2,getY()+middleY+w/2,getX()-middleX,getY()+middleY);
        g.setStroke(sc);
        if (!SimulationCanvas.isPaused()) received = false;
    }

    /**
     * Draws the surroundin box of the agv
     * 
     * @param g the Graphics2D object to draw on
     */
    private void drawSurroundingBox(Graphics2D g) {
        //      draw box
        g.setColor(Color.BLACK);
        Stroke current = g.getStroke();
        g.setStroke(new BasicStroke(2.0f));
        drawLine(g, box.getLine12());
        drawLine(g, box.getLine23());
        drawLine(g, box.getLine34());
        drawLine(g, box.getLine41());
        g.setStroke(current);
    }

    /**
     * Draws the order on the agv to indicate
     * that it holds an order.
     * 
     * @param g the Graphics2D object to draw on
     * @param boxWidth
     * @param forkWidth
     * @param middleX the distance between the upper left corner of the agv and the x coordinate of the agv
     * @param forkHeight
     * @param firstStart
     * @param secondStart
     */
    private void drawOrder(Graphics2D g, int boxWidth, int forkWidth, int middleX, int forkHeight, int firstStart, int secondStart) {
        // draw order
        Stroke cur = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        int ordersize = Math.abs((getY()-firstStart)-(getY()+secondStart+forkHeight))- forkHeight;
        int xorderstart = getX()-middleX+boxWidth + (forkWidth-10-ordersize)/2;
        int yorderstart = getY()-firstStart+forkHeight/2;
        g.setColor(new Color(Color.GRAY.getRed(),Color.GRAY.getGreen(),Color.GRAY.getBlue(), 150));
        g.fillRoundRect(xorderstart, yorderstart,ordersize,ordersize, 10,10);
        g.setColor(Color.BLACK);
        g.drawRoundRect(xorderstart, yorderstart,ordersize,ordersize, 10,10);
        g.setStroke(cur);
    }

    /**
     * Draws an ad hoc broadcast, diminishes
     * the time left ot show of the broadcast if
     * the simulation is not paused and removes
     * the broadcast drawing when this time left
     * to show reaches 0.
     * 
     * @param g the Graphics2D object to draw on
     */
    private void drawAdHocBroadCast(Graphics2D g) {
        // draw broadcast if needed
        if (adhocBroadCast!=null && adhocBroadCast.getTimeLeftToShow() > 0) {
            adhocBroadCast.paintComponent(g);
            if (!SimulationCanvas.isPaused()) adhocBroadCast.diminshTimeLeftToShow();
        }
    }

    /**
     * Draws the unicasts of this agv, diminshes
     * their time left to show if the simulation
     * is not paused and removes
     * them when their time left to show has reached
     * 0.
     * 
     * @param g the Graphics2D object to draw on
     */
    private void drawUnicasts(Graphics2D g) {
        // draw unicasts if needed
        Iterator iter = unicasts.iterator();
        Collection toRemove = new ArrayList();
        while(iter.hasNext()) {
            DrawObject o = (DrawObject)iter.next();
            if (o.getTimeLeftToShow() > 0) {
                o.paintComponent(g);
                if (!SimulationCanvas.isPaused()) o.diminshTimeLeftToShow();
                if (o.getTimeLeftToShow() <= 0) {
                    toRemove.add(o);
                }
            } 
        }
        Iterator iterR = toRemove.iterator();
        while(iterR.hasNext()) {
            DrawObject o = (DrawObject)iterR.next();
            unicasts.remove(o);
        }
    }

    /**
     * Draws the ID of the Agv on the Agv
     * 
     * @param g the Graphics2D object to draw on
     * @param boxWidth
     */
    private void drawAgvID(Graphics2D g, int boxWidth) {
        // draw the ID of the AGV
        g.rotate(-(direction+3*Math.PI/2),getX(),getY());
        g.setColor(Color.BLACK);
        g.setFont(new Font(null, Font.BOLD, 3*boxWidth/4));
        String id = "" + getAgvID() + "";
        FontMetrics metrics = g.getFontMetrics();
        int width = metrics.stringWidth( id );
        g.drawString(id,getX()-width/2,getY());
        g.rotate((direction+3*Math.PI/2),getX(),getY());
    }

    /**
     * Draws a given line on the given graphics object
     * 
     * @param g the Graphics2D object to draw on
     * @param l The Line to draw
     */
    private void drawLine(Graphics2D g, Line l) {
        int x1 = (int)Math.round(l.getX1());
        int y1 = (int)Math.round(l.getY1());
        int x2 = (int)Math.round(l.getX2());
        int y2 = (int)Math.round(l.getY2());
        g.drawLine(x1,y1,x2,y2);
    }

    /**
     * Returns the y coordinate of the AGV
     * on the main panel.
     * 
     * @return an integer representing the y coordinate
     */
    private int getY() {
        return ycoord;
    }

    /**
     * Returns the x coordinate of the AGV
     * on the main panel.
     * 
     * @return an integer representing the x coordinate
     */
    private int getX() {
        return xcoord;
    }

    /**
     * Updates the information of this drawing
     * to reflect the given AgvEnvironmentState
     * 
     * @param a The environment state of the Agv
     */
    private void update(AgvEnvironmentState a) {
        final AgvEnvironmentState agvstate =(AgvEnvironmentState)a;
        
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                setX((int)Math.round(agvstate.getCoordinate().getX()));
                setY((int)Math.round(agvstate.getCoordinate().getY()));
                setDirection(agvstate.getDirection().getDegrees());
                setHasOrder(agvstate.hasCurrentOrder());
                box = agvstate.getBox();
                setSuspended(agvstate.isSuspended());
                canvas.repaint();
            }
            
        });
    }

}
