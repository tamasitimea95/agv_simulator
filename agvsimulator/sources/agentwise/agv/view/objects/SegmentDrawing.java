/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Stroke;
import java.util.Iterator;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.model.environment.map.Segment;
import agentwise.agv.util.Angle;
import agentwise.agv.util.Coordinate;
import agentwise.agv.view.SimulationCanvas;

/**
 * Draws a segment on the canvas. This is 
 * repainted each time the segment changes.
 * 
 * A drawing of a segment includes the path
 * it follows, arrows indicating the allowed
 * travel direction and other needed
 * visualisation of segment related information.
 * 
 * @author tomdw
 *
 */
public class SegmentDrawing extends DrawObject {
    
    /**
     * 
     * Constructs an instance of SegmentDrawing
     * that has to reflect the given Segment and
     * is to be drawn on the given SimulationCanvas.
     *
     * @param seg The segment to reflect
     * @param canvas the canvas to draw on
     * @throws NullPointerException when seg or canvas are null
     */
    public SegmentDrawing(Segment seg, SimulationCanvas canvas) throws NullPointerException {
        if (seg==null || canvas==null) throw new NullPointerException("The given segment or canvas cannot be null for a segmentdrawing");
        this.canvas = canvas;
        initialiseObservers();
        update(seg);
    }
    
    /**
     * The canvas to drawn on, to repaint
     */
    private SimulationCanvas canvas = null;
    
    /**
     * Initialises this drawing as interested
     * in the changes of a segment.
     * 
     */
    private void initialiseObservers() {
        final SegmentDrawing thisObject = this;
        ChangeManager.getInstance().registerForSegmentChangedEvent(new ChangeObserverAdapter() {
           public void updateSegmentChanged(Segment s) {
               if (s.getID()==thisObject.getID()) {
                   update(s);
               }
           }
        });
    }
    
    /**
     * Returns the ID of the segment for which
     * this drawing is a representation.
     * 
     * @return a String representing the ID
     */
    private String getID() {
        return id;
    }
    
    /**
     * The ID of the segment to represent
     */
    private String id;

    /**
     * Indicates if the forward arrow to 
     * denote the direction of allowed
     * travel over the segment has to
     * be drawn.
     * 
     */
    private boolean forward = false;
    
    /**
     * Indicates if the backward arrow to 
     * denote the direction of allowed
     * travel over the segment has to 
     * be drawn
     */
    private boolean backward = false;
    
    /**
     * This polygon has as its points the points of the segment
     * and will be drawn as a polyline connecting all those points
     * 
     */
    private Polygon points;
    
    /**
     * Draws the segment's line, the allowed travel direction
     * and other visualisations of a segment.
     * 
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        drawSegmentLine(g);
        drawAllowedTravelDirection(g);
    }

    /**
     * Draws the segment line on the given Graphics2D instance
     * 
     * @param g The Graphics2D instance to draw on
     */
    private void drawSegmentLine(Graphics2D g) {
        g.setColor(Color.BLUE);
        Stroke current = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        g.drawPolyline(points.xpoints, points.ypoints, points.npoints);
        /*for (int i =0; i<points.npoints;i++){
            g.fillRect(points.xpoints[i]-2,points.ypoints[i]-2,4,4);
        }*/
        g.setStroke(current);
    }

    /**
     * Draws the allowed travel direction arrows on the given Graphics2D instance
     * 
     * @param g The Graphics2D instance to draw on
     */
    private void drawAllowedTravelDirection(Graphics2D g) {
        // direction visualisation of segment
        g.setColor(Color.BLUE);
        Polygon triangle = new Polygon();
        Coordinate c1 = new Coordinate(points.xpoints[(int)points.npoints/2],points.ypoints[(int)points.npoints/2]);
        triangle.addPoint((int)Math.round(c1.getX()),(int)Math.round(c1.getY())-20);
        triangle.addPoint((int)Math.round(c1.getX()),(int)Math.round(c1.getY())+20);
        triangle.addPoint((int)Math.round(c1.getX())+20,(int)Math.round(c1.getY()));
        
        if (forward) { // from start to end
	        Coordinate c2 = new Coordinate(points.xpoints[3*(int)points.npoints/4],points.ypoints[3*(int)points.npoints/4]);
	        Angle a =  c1.calculateAngleTowards(c2);
	        g.rotate(-a.getRadians(), c1.getX(),c1.getY());
	        g.fillPolygon(triangle);
	        g.rotate(a.getRadians(), c1.getX(), c1.getY());
        }
        if (backward) { // from end to start
	        Coordinate c2 = new Coordinate(points.xpoints[(int)points.npoints/4],points.ypoints[(int)points.npoints/4]);
	        Angle a =  c1.calculateAngleTowards(c2);
	        g.rotate(-a.getRadians(), c1.getX(),c1.getY());
	        g.fillPolygon(triangle);
	        g.rotate(a.getRadians(), c1.getX(), c1.getY());
        }
    }

    /**
     * Updates this drawing to reflect the given segment
     * 
     * @param seg
     */
    private void update(Segment seg) {
        id = seg.getID();
        Polygon p = new Polygon();
        Iterator cIter = seg.getPoints().iterator();
        while (cIter.hasNext()) {
            Coordinate c = (Coordinate)cIter.next();
            p.addPoint((int)Math.round(c.getX()),(int)Math.round(c.getY()));
        }
        points = p;
        // direction
        if(seg.getEnd().getInSegments().contains(seg)) forward = true;
        else forward = false;
        if(seg.getEnd().getOutSegments().contains(seg)) backward = true;
        else backward = false;
        // pheromones
        /*GreenPheromone gp = ((GreenPheromone)seg.getPheromoneInfrastructureModule().getPheromone(new GreenPheromone()));
        if (gp!=null) greenPheromoneStrength = gp.getStrength();*/
        canvas.repaint();
    }

}
