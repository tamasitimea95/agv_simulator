/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.Graphics2D;

/**
 * This interface must be implemented by every entity that should
 * be drawn on the SimulationCanvas, on the environment.
 * 
 * @author tomdw
 *
 */
public abstract class DrawObject {
    
    /**
     * This method is called from the simulation panel where
     * the environment is drawn giving the local Graphics2D instance.
     * 
     * The method should draw the object as if it is located into
     * the environment coordinate system
     * 
     * @param g the graphics instance of the simulation panel/ environment
     */
    public abstract void paintComponent(Graphics2D g);

    /**
     * Returns the time steps this draw object must be shown.
     * If 0 is returned, then it is a permanent object. Override
     * this method if there should be a limited time.
     * 
     * @return an integer larger than 0
     */
    public int getTimeToShow() {
        return 0;
    }
    
    private int timeleft = getTimeToShow();
    
    /**
     * Returns the time that is left for this drawobject
     * to show.
     *  
     * @return an integer representing the time steps
     */
    public int getTimeLeftToShow() {
        return timeleft;
    }
    
    /**
     * Diminish the time left for this drawobject
     * to show. This is to be called each
     * time the object was drawn
     *
     */
    public void diminshTimeLeftToShow() {
        timeleft--;
    }
    
    /**
     * Resets the time left to show
     * to the time to show value.
     *
     */
    public void resetTimeLeftToShow() {
        timeleft = getTimeToShow();
    }
    
}
