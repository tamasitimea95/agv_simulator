/*
 * Created on Oct 13, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;

import agentwise.agv.util.Coordinate;

/**
 * An AdHocBroadCastDrawing visualises a broadcast
 * made by an ad hoc network infrastructure by
 * drawing a circle picture around a given
 * sender coordinate with a given range, ie the
 * broadcast range.
 * 
 * A broadcast is typically drawn temporarily, the
 * number of draws is given by the timetoshow method.
 * 
 * @author tomdw
 */
public class AdHocBroadCastDrawing extends DrawObject {
    
    /**
     * 
     * Constructs an instance of AdHocBroadCastDrawing
     *
     * @param senderC The coordinate to draw around
     * @param range The range to draw the broadcast
     */
    public AdHocBroadCastDrawing(Coordinate senderC, int range) {
        xcoord = (int)Math.round(senderC.getX());
        ycoord = (int)Math.round(senderC.getY());
        this.range = range;
        
    }
    
    /**
     * The x coord of the sender
     */
    private int xcoord;
    
    /**
     * The y coord of the sender
     */
    private int ycoord;
    
    /**
     * The range of the broadcast
     */
    private int range;

    /**
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        g.translate(xcoord, ycoord);
        Color c = new Color(255,0,0,50);
        Color c2 = new Color(255, 0,0, 15);
        g.setColor(c);
        Stroke current = g.getStroke();
        g.setStroke(new BasicStroke(4.0f));
        
        int range0 = range/8;
        g.drawOval(-range0,-range0,range0*2, range0*2);
        int range1 = range/4;
        g.drawOval(-range1,-range1,range1*2, range1*2);
        int range2 = range/2;
        g.drawOval(-range2,-range2,range2*2, range2*2);
        int range3 = range;
        g.drawOval(-range3,-range3,range3*2, range3*2);
        g.setColor(c2);
        g.fillOval(-range3,-range3,range3*2, range3*2);
        
        g.setStroke(current);
        
        g.translate(-xcoord, -ycoord);
    }

    /**
     * A AdHocBroadCastDrawing should be shown temporarily.
     * 
     * @see agentwise.agv.view.objects.DrawObject#getTimeToShow()
     */
    public int getTimeToShow() {
        return 5;
    }

}
