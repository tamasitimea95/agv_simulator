/*
 * Created on Sep 13, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.model.environment.map.Station;
import agentwise.agv.view.GuiConfig;
import agentwise.agv.view.SimulationCanvas;

/**
 * A StationDrawing draws a station
 * as a small rectangle once a station
 * is added to the simulation.
 * 
 * @author tomdw
 *
 */
public class StationDrawing extends DrawObject {
    
    /**
     * 
     * Constructs an instance of StationDrawing
     * that will draw the state of the given
     * station on the canvas.
     * 
     * The construction will register this drawing
     * as interested for a stationChanged event.
     * 
     * @param s The station to represent
     * @param canvas The SimulationCanvas to draw on
     * @throws NullPointerException when the station or canvas is null
     */
    public StationDrawing(Station s, SimulationCanvas canvas) throws NullPointerException {
        if (s==null || canvas==null) throw new NullPointerException("the given station or canvas cannot be null for a stationdrawing");
        stationID = s.getID();
        this.canvas = canvas;
        initialiseObservers();
        update(s);
    }
    
    /* The simulation canvas to draw on */
    private SimulationCanvas canvas;
    
    /* the id of the station to represent */
    private String stationID = null;
    
    /**
     * Initialises for this drawing some observers
     * to be interested in events from
     * the simulation.
     *
     */
    private void initialiseObservers() {
        ChangeManager.getInstance().registerForStationChangedEvent(new ChangeObserverAdapter() {
            public void updateStationChanged(Station s) {
                if (s.getID()==stationID) {
                    final Station toUse = s;
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            update(toUse);
                        }
                    }); 
                }
            }
        });
    }

    /**
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        g.translate(xcoord, ycoord);
               
        g.setColor(c);
        g.fillRoundRect(-20,-20,40,40,10,10);        
        if (GuiConfig.drawStationID) drawID(g);

        g.translate(-xcoord, -ycoord);

    }

    
    /**
     * Draws the ID of this station in a box
     * on top of the station drawing
     * 
     * @param g The canvas to draw on
     */
    private void drawID(Graphics2D g) {
        g.setFont(new Font(null, Font.BOLD, 24));
        String id = "" + stationID + "";
        FontMetrics metrics = g.getFontMetrics();
        int width = metrics.stringWidth( id );
        int height = metrics.getHeight();
        g.setColor(Color.WHITE);
        g.fillRoundRect(-width/2-8,-24-height-8,width+16, height+12,10,10);
        g.setColor(c);
        g.setStroke(new BasicStroke(2.0f));
        g.drawRoundRect(-width/2-8,-24-height-8,width+16, height+12,10,10);
        g.drawString(stationID,-width/2,-20-height/2);
    }
    
    private Color c = Color.BLUE;
    
    public void setColor(Color color) {
        if (color!=null) c = color;
    }
    
    /* the x coordinate of the station */
    private int xcoord = 0;
    /* the y coordinate of the station */
    private int ycoord = 0;
    
    /**
     * Updates the state of this drawing
     * to reflect the state of the given station s
     * 
     * @param s The Station to reflect
     */
    private void update(Station s) {
        xcoord = (int)Math.round(s.getCoordinate().getX());
        ycoord = (int)Math.round(s.getCoordinate().getY());
        canvas.repaint();
    }
}
