/*
 * Created on Oct 13, 2004
 *
 */
package agentwise.agv.view.objects;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import agentwise.agv.util.Coordinate;

/**
 * A UniCastDrawing allows to draw a unicast
 * event between two coordinates. The coordinate
 * of the sender and that of the receiver of
 * the message.
 * 
 * @author tomdw
 */
public class UniCastDrawing extends DrawObject {
    
    /**
     * 
     * Constructs an instance of UniCastDrawing
     * so that when this DrawObject is drawn a
     * line is drawn from the senderC coordinate to
     * the receiverC coordinate
     *
     * @param senderC The coordinate of the sender of the unicast
     * @param receiverC The coordinate of the receiver of the unicast
     */
    public UniCastDrawing(Coordinate senderC, Coordinate receiverC) {
        xscoord = (int)Math.round(senderC.getX());
        yscoord = (int)Math.round(senderC.getY());
        xrcoord = (int)Math.round(receiverC.getX());
        yrcoord = (int)Math.round(receiverC.getY());
        
    }
    
    /* the x coordinate of the sender*/
    private int xscoord;
    /* the y coordinate of the sender*/
    private int yscoord;
    /* the x coordinate of the receiver */
    private int xrcoord;
    /* the y coordinate of the receiver */
    private int yrcoord;

    /**
     * @see agentwise.agv.view.objects.DrawObject#paintComponent(java.awt.Graphics2D)
     */
    public void paintComponent(Graphics2D g) {
        Color c = new Color(Color.GRAY.getRed(),Color.GRAY.getGreen(),Color.GRAY.getBlue(),150);
        g.setColor(c);
        g.setStroke(new BasicStroke(4.0f));
        
        g.drawLine(xscoord+4,yscoord+4,xrcoord+4,yrcoord+4);
    }

    /**
     * A unicast is only visible for one time step
     * 
     * @see agentwise.agv.view.objects.DrawObject#getTimeToShow()
     */
    public int getTimeToShow() {
        return 1;
    }

}
