/*
 * Created on Aug 31, 2004
 *
 */
package agentwise.agv.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.controller.Simulation;
import agentwise.agv.model.environment.Environment;
import agentwise.agv.model.environment.InputOrderLocationManager;

/**
 * Represents the main window of the 
 * Automated Guided Vehicle simulator.
 * 
 * The window contains a toolbar that allows
 * to initiate all the necessary commands to the
 * system. Also, a simulationcanvas is shown in
 * this window where the simulation is visualised.
 * And finally, there is an informationpanel at
 * the right hand side where extra information and
 * configuration options can be shown.
 * 
 * @author tomdw, nelis
 *
 */
public class MainWindow extends JFrame {
    
    /**
     * The only instance of this class
     */
    private static MainWindow instance = null;
        
    /**
     * MainWindow is a Singleton and thus there
     * is only one MainWindow instance in the 
     * system run.
     * 
     * @return The only instance of this class
     */
    public static MainWindow getInstance() {
        if (instance==null) instance = new MainWindow();
        return instance;
    }
    
    /**
     * 
     * Constructs an instance of MainWindow
     *
     */
    private MainWindow() {
        super("Automated Guided Vehicle Simuator, (c) AgentWise, DistriNet, KULeuven, Belgium");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainWindow.setDefaultLookAndFeelDecorated(true);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screendimension = toolkit.getScreenSize();
        this.setSize((int)(screendimension.getWidth()*0.8),(int)(screendimension.getHeight()*0.8));
        this.setLocationRelativeTo(null); // center on screen (after resize);	
        constructGuiComponents();
        setVisible(true);
        initialiseObservers();
    }
    
    /**
     * Registers this class as interested in the event where
     * a new simulation is set as the current simulation so
     * that the environment can be set and the window can be adjusted
     * according to the size of that environment.
     * 
     *
     */
    private void initialiseObservers() {
        ChangeManager.getInstance().registerForNewSimulationSetEvent(new ChangeObserverAdapter() {          
           public void updateNewSimulationSet(Simulation sim) {
               setEnvironment(sim.getEnvironment());
               initialise();
           }
        });
        ChangeManager.getInstance().registerForStartUseOfInputOrderLocationManagerEvent(new ChangeObserverAdapter() {
            public void updateStartUsingInputOrderLocationManager(InputOrderLocationManager om) {
                orderManager = new OrderManagerTab();
            }
        });
    }
    
    /**
     * Initialises the main window and its components as if
     * the program was first started.
     * 
     */
    protected void initialise() {
        infopanelbottom.reset();
        infopanelright.reset();
        infopanelbottom.addTab(new OrderListTab());
        infopanelright.addTab(new GuiConfigTab());
        if (orderManager!=null) {
            infopanelright.addTab(orderManager,0);
        }
    }
    protected OrderManagerTab orderManager = null;

    /**
     * Constructs and lays out the gui components in the
     * main window.
     *
     */
    private void constructGuiComponents() {
        getContentPane().setLayout(new BorderLayout());
        AgvSimulatorToolBar toolbar = new AgvSimulatorToolBar();
        getContentPane().add(toolbar, BorderLayout.PAGE_START);
        
        infopanelbottom = new InformationPanel();
        JPanel toppanel = new JPanel();
        JPanel bottompanel = infopanelbottom;
        leftsplitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,toppanel, bottompanel);
        leftsplitpane.setOneTouchExpandable(true);
        leftsplitpane.setResizeWeight(1.0);
        
        infopanelright = new InformationPanel();
        simulationcanvas = new SimulationCanvas();
        JSplitPane leftpanel = leftsplitpane;
        JPanel rightpanel = infopanelright;
        
        mainsplitpane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,leftpanel,rightpanel);
        mainsplitpane.setOneTouchExpandable(true);
        mainsplitpane.setResizeWeight(1.0);
        
        getContentPane().add(mainsplitpane,BorderLayout.CENTER);
        initialise();
        
    }
    
    // the information panels
    InformationPanel infopanelright = null;
    
    InformationPanel infopanelbottom = null;
    
    // the split pane on the main window
    JSplitPane mainsplitpane = null;
    
    JSplitPane leftsplitpane = null;
    
    // the canvas that visualises the simulation
    SimulationCanvas simulationcanvas = null;
    
    /**
     * Makes the gui zoom into the visualisation
     *
     */
    public void zoomIn() {
        if (simulationcanvas!=null) simulationcanvas.zoomIn();
    }
    
    /**
     * Makes the gui zoom out from the visualisation
     *
     */
    public void zoomOut() {
        if (simulationcanvas!=null) simulationcanvas.zoomOut();
    }
    
    /**
     * Makes the gui to zoom the visualisation
     * to the original size
     *
     */
    public void resetZoom() {
        if (simulationcanvas!=null) simulationcanvas.resetZoom();
    }
    
    /**
     * To Browse for a map file
     * 
     * @param type The type of file to browse for
     * @param description The description for this type
     * @param file The previous file where has been browsed for
     * @return A string representing the path of the file
     */
    public String browse(String type, String description, String file){
    	JFileChooser chooser = new JFileChooser();
        // Note: source for ExampleFileFilter can be found in FileChooserDemo,
        // under the demo/jfc directory in the Java 2 SDK, Standard Edition.
        ExtentionFileFilter filter = new ExtentionFileFilter();
        filter.addExtension(type);        
        filter.setDescription("XML file");
        chooser.setFileFilter(filter);
        
        File f = new File(file);
        if(!f.isFile()){
        	 String curDir = System.getProperty("user.dir");
        	 f = new File(curDir);
        }
        chooser.setSelectedFile(f);
        
        int returnVal = chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           return chooser.getSelectedFile().getPath();
        }else{
        	return null;	
        }    	
    }
   
    /**
     * Shows a given error message for
     * this gui
     * 
     * @param s The error message to show
     */
    public void showErrorMessage(String s){
    	JOptionPane.showMessageDialog(this, s);
    }
    
    /**
     * Sets a given environment as the current
     * simulation environment and constructs
     * the scroll panel and the canvas inside
     * it where the environment will be drawn
     * 
     * @param env The environment to draw
     */
    private void setEnvironment(Environment env) {
        simulationcanvas.setEnvironment(env);
        SwingUtilities.invokeLater(new Runnable(){

            public void run() {
                JPanel jPanel1 = new JPanel();
                jPanel1.setLayout(new FlowLayout());
                jPanel1.setBackground(new Color(179, 174, 166));
                jPanel1.add(simulationcanvas);

                JScrollPane scrollpane = new JScrollPane();
                scrollpane.setViewportView(jPanel1);
                leftsplitpane.setLeftComponent(scrollpane);
                scrollpane.repaint();
            }
        });
    }
    
   
}
