/*
 * Created on 15-Nov-2004
 *
 */
package agentwise.agv.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import agentwise.agv.controller.ChangeManager;
import agentwise.agv.controller.ChangeObserverAdapter;
import agentwise.agv.controller.OrderManager;
import agentwise.agv.model.environment.Order;

/**
 * This Tab shows a table with a list
 * of all the Orders present in the system.
 * It therefore uses the information
 * in the OrderManager instance of the system.
 * 
 * @author tomdw
 *
 */
public class OrderListTab extends Tab {

    /**
     * Constructs an instance of OrderListTab
     */
    public OrderListTab() {
        super("Orders On Floor");
    }

    /**
     * Initializes the view and
     * attaches the observer behaviour
     * to this view.
     * 
     * @see agentwise.agv.view.Tab#construct()
     */
    public void construct() {
        initialize();
	    initObservers();
    }
    
    /**
     * Registers this view as being interested in
     * the event where a new order is added to the
     * system and to the event where an existing order
     * is removed from the system so that it can
     * update its view.
     *
     */
    private void initObservers() {
        ChangeManager.getInstance().registerForNewOrderEvent(new ChangeObserverAdapter() {
            
            public void updateNewOrder(OrderManager om, Order o) {
                update(om);
            }
        });
        
        ChangeManager.getInstance().registerForRemoveOrderEvent(new ChangeObserverAdapter() {
            public void updateRemoveOrder(OrderManager om, Order o) {
                update(om);
            }    
        });
    }
    
    /**
     * Updates the view based on the information
     * contained in the given OrderManager.
     * 
     * @param om The given OrderManager instance
     */
    private void update(OrderManager om) {
        
        String[] columnNames = {"ID",
                "Priority",
                "From",
                "To",
                "CreationTime"};
        
        Object [][] data = new Object[om.getCurrentOrders().size()][5];
		Iterator iter = om.getCurrentOrders().iterator();
		int i = 0;
		while(iter.hasNext()) {
		    Order o = (Order)iter.next();
		    data[i][0] = " Order " + o.getId() + " ";
		    data[i][1] = " " + o.getPriority() + " ";
		    data[i][2] = " " + o.getStartStationId() + " ";
		    data[i][3] = " " + o.getDestinationStationId() + " ";
		    data[i][4] = " " + o.getCreationTime().toString() + " ";
		    i++;
		}
        
        TableModel tm = new DefaultTableModel(data,columnNames);
        ordertable.setModel(tm);
        this.adjustTableColumnWidths();
        
    }
    
    /**
     * Adjust the size of this tab so that
     * the minimum size is 400 px at 100 px
     *
     */
    private void adjustSize() {
        setMinimumSize(new Dimension(400,100));
        this.invalidate();
    }

    /**
     * Initialized this tab by
     * creating the lay-out, the 
     * table, etc
     *
     */
    private void initialize() {
        setLayout(new BorderLayout());
        String[] columnNames = {"ID",
                "Priority",
                "From",
                "To",
                "CreationTime"};

		TableModel tm = new DefaultTableModel(columnNames,4);
		ordertable = new JTable(tm);
		
		this.adjustTableColumnWidths();
        
		JScrollPane scrollPane = new JScrollPane(ordertable);
		        
        add(scrollPane,BorderLayout.CENTER);
        setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        adjustSize();
    }
    
    /**
     * Adjusts the column widths according
     * to the preferences for the date contained
     * in each column
     *
     */
    private void adjustTableColumnWidths() {
        TableColumn column = null;
		for (int i = 0; i < 5; i++) {
		    column = ordertable.getColumnModel().getColumn(i);
		    if (i <= 1) {
		        column.setPreferredWidth(70);
		        column.setMaxWidth(80);
		        column.setMinWidth(70);
		    } 
		    if (i == 2 || i == 3) {
		        column.setPreferredWidth(75);
		        column.setMinWidth(75);
		    }
		    if (i == 4) {
		        column.setPreferredWidth(150);
		        column.setMinWidth(150);
		    }
		}
    }
    
    /**
     * The table that contains the data
     */
    private JTable ordertable;

}
