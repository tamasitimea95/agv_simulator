import random

def orders_generate(in_locations, out_locations, step, mode):
	f = open("orders.txt","w+")
	for i in range(1, 100, step):
		if mode == 0:
			line = str(i) + " " + str(random.choice(in_locations)) + " " + str(random.choice(out_locations)) + "\n"
			f.write(line)
		if mode == 1:
			line = str(i) + " "
			for j in range(3):
				line += str(random.choice(in_locations)) + " " + str(random.choice(out_locations)) + " "
			line += "\n"
			f.write(line)
	f.close() 
		
orders_generate([11, 15, 19, 24, 28, 33, 37, 42, 46, 410, 51, 55, 59, 64, 68, 73, 77, 82, 86, 810, 91, 95, 99, 104, 108],
                [13, 17, 22, 26, 210, 31, 35, 39, 44, 48, 53, 57, 62, 66, 610, 71, 75, 79, 84, 88, 93, 97, 102, 106, 1010], 3, 0)
