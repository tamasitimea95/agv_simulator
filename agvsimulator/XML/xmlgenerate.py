import xml.etree.cElementTree as ET

def xml_generate(n, m, start_x, start_y):

    root = ET.Element("sim:AgvMap", name = "blokk_" + str(n) + str(m))

    env = ET.SubElement(root, "Environment", width="2500", height="2500")

    stat = ET.SubElement(env, "Stations")
    for i in range(n):
        for j in range(m):
            sid = "station_"+str(i+1)+str(j+1)
            sx = str(start_x + i*400) 
            sy = str(start_y + j*400) 
            
            station = ET.SubElement(stat, "Station", id=sid)
            ET.SubElement(station, "Coordinate", x=sx, y=sy)

    segm = ET.SubElement(env, "Segments")
    for i in range(n-1):
        for j in range(m):
            sid = "segment_"+str(i+2)+str(j+1)+"_"+str(i+1)+str(j+1)
            sf = "station_"+str(i+1)+str(j+1)
            st = "station_"+str(i+2)+str(j+1)
            ET.SubElement(segm, "Segment", attrib = {"id" : sid, "from" : sf, "to" : st})
            ET.SubElement(segm, "Segment", attrib = {"id" : sid, "from" : st, "to" : sf})

    for i in range(n):
        for j in range(m-1):
            sid = "segment_"+str(i+1)+str(j+2)+"_"+str(i+1)+str(j+1)
            sf = "station_"+str(i+1)+str(j+1)
            st = "station_"+str(i+1)+str(j+2)
            ET.SubElement(segm, "Segment", attrib = {"id" : sid, "from" : sf, "to" : st})
            ET.SubElement(segm, "Segment", attrib = {"id" : sid, "from" : st, "to" : sf})

    tree = ET.ElementTree(root)
    filename = "blokk_" + str(n) + str(m) + ".xml"   
    tree.write(filename)

xml_generate(4,3,500,500)
